#!/bin/bash

##Clear ~/.vimrc
echo "" > ~/.vimrc

cat <<EOF >> ~/.vimrc
"####For Python####
set textwidth=79    "lines longer than 79 columns will be broken
set shiftwidth=4    "operation >> indents 4 columns; << unindents 4 columns
set tabstop=4       "an hard TAB displays as 4 columns
set expandtab       "insert spaces when hitting TABs
set softtabstop=4   "insert/delete 4 spaces when hitting a TAB/BACKSPACE
set shiftround      "round indent to multiple of ‘shiftwidth’
set showmatch
set incsearch   " search as characters are entered
set hlsearch    " highlight matches
set undolevels=1500     "how many times the user can undo
set wildmenu    "Better command-line completion
"Set Color of ColorColumn after 'textwidth'
set colorcolumn=+1                                                              
hi ColorColumn ctermbg=0 guibg=0 

EOF

####Optional####
cat <<EOF >> ~/.vimrc                                                           
"####Optional#### 
"Set Color of ColorColumn after 'textwidth'                                     
set colorcolumn=+1                                                
hi ColorColumn ctermbg=0 guibg=0
"Set cursorcolumn (vertical cursor line)
set cursorcolumn                                                                
highlight CursorColumn ctermbg=8 guibg=0 

EOF

####Install pathogen#### 
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim
##Configure
cat <<EOF >> ~/.vimrc
"####For pathogen plugin####
execute pathogen#infect()

EOF

####Install indentLine####
if [ ! -d ~/.vim/bundle/indentLine ];
then
    #IF directory doesn't exist.
    git clone -n https://github.com/Yggdroot/indentLine.git ~/.vim/bundle/indentLine
fi
#Configure 
cat <<EOF >> ~/.vimrc
"####For indentLine plugin####
let g:indentLine_char = '.'

EOF


