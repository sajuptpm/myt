import re

###Find VM subscribe request of compute nodes
#MESSAGE_FILTERS = ["IFMapXmppVmSubUnsub"]
###Find all messages send by control "vpc-ctrl1" to vrouters
#MESSAGE_FILTERS = ["IFMapClientSendInfo"]
###Find all messages send by control "vpc-ctrl1" to vrouter "10.140.210.33"
#MESSAGE_FILTERS = ["IFMapClientSendInfo", "vpc-ctrl1:10.140.210.33"]
MESSAGE_FILTERS = None

def get_message(line):
    m = re.search("\w* \[SYS.*$", line)
    if m:
        return m.group()

def get_date(line):
    m = re.search("\d{4}-\d{2}-\d{2} .* UTC", line)
    if m:
        return m.group()

def get_thread(line):
    m = re.search("Thread \d*", line)
    if m:
        return m.group()    

def get_pid(line):
    m = re.search("Pid \d*", line)
    if m:
        return m.group()

def get_module_and_action(line):
    m = re.search("(\w*) \[SYS_DEBUG\]:( \w*: ).*$", line)
    if m:
        module = m.group(1)
        action = m.group(2)
        return (module, action)

fp = open("file.txt")
for line in fp.readlines():
    _date = get_date(line)
    _message = get_message(line)
    _thread = get_thread(line)
    _pid = get_pid(line)
    _module_action = get_module_and_action(line)
    if MESSAGE_FILTERS:
        for FILTER in MESSAGE_FILTERS:
            if FILTER not in _message:
                break
            else:
                print "\n", _date, _message
    else:
        #print "\n", _date, _message
        print _module_action





