import os
import boto
import boto.ec2
region = boto.ec2.regioninfo.RegionInfo(name="nova",endpoint="vpc.ind-west-1.staging.jiocloudservices.com")
#print region

#Location of certificate file
boto.connection.DEFAULT_CA_CERTS_FILE = os.path.join(os.path.dirname(os.path.abspath(__file__)), "mycertificate.crt")
#print boto.connection.DEFAULT_CA_CERTS_FILE

AWS_ACCESS_KEY="xxxxx"
AWS_SECRET_ACCESS_KEY="xxxxx"

conn = boto.connect_vpc(debug=2, aws_access_key_id=AWS_ACCESS_KEY,
                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                        is_secure=True,
                        region=region,
                        port=443,
                        path="/services/Cloud")

print conn
print conn.get_all_vpcs()
