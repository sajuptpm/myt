#!/bin/bash

IAM_ENDPOINT="iam.ind-west-1.staging.jiocloudservices.com:5000"
VPC_ENDPOINT="vpc.ind-west-1.staging.jiocloudservices.com:443"
COMPUTE_ENDPOINT="compute.ind-west-1.staging.jiocloudservices.com:443"
STORAGE_ENDPOINT=""

openssl s_client -showcerts -connect $IAM_ENDPOINT | openssl x509 > mycertificate.crt
openssl s_client -showcerts -connect $VPC_ENDPOINT | openssl x509 >> mycertificate.crt
openssl s_client -showcerts -connect $COMPUTE_ENDPOINT | openssl x509 >> mycertificate.crt


####How to specify cacert with curl command###
#$curl -i --cacert mycertificate.crt . -H "Content-Type: application/json" -H "X-Auth-Token: cda797262052429eb6a729009787f182"  https://iam.ind-west-1.staging.jiocloudservices.com:35357/v3/?Action=CreateCredential
#

