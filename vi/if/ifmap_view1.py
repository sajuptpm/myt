from vnc_cfg_api_server.gen.vnc_ifmap_client_gen import NewSessionRequest, newSessionResult, Identity, SearchRequest, client

ifmap_ip = '127.0.0.1'
ifmap_port = 8443
ifmap_user = 'api-server'
ifmap_passwd = 'api-server'

class IfmapClient():

    def __init__(self, ifmap_srv_ip, ifmap_srv_port, uname, passwd):
        """
        """
        self._CONTRAIL_XSD = "http://www.contrailsystems.com/vnc_cfg.xsd"

        self._NAMESPACES = {
            'a': 'http://www.w3.org/2003/05/soap-envelope',
            'b': 'http://www.trustedcomputinggroup.org/2010/IFMAP/2',
            'c': self._CONTRAIL_XSD
        }

        namespaces = {
            'env':   "http://www.w3.org/2003/05/soap-envelope",
            'ifmap':   "http://www.trustedcomputinggroup.org/2010/IFMAP/2",
            'meta':   "http://www.trustedcomputinggroup.org/"
                      "2010/IFMAP-METADATA/2",
            'contrail':   self._CONTRAIL_XSD
        }

        mapclient = client(("%s" % (ifmap_srv_ip), "%s" % (ifmap_srv_port)),
                           uname, passwd, namespaces)

        result = mapclient.call('newSession', NewSessionRequest())
        mapclient.set_session_id(newSessionResult(result).get_session_id())
        mapclient.set_publisher_id(newSessionResult(result).get_publisher_id())

        self._mapclient = mapclient


    def search(self, start_identity_name=None, meta_filter=None, result_filter=None, max_depth=10):
        """
         meta_filter Examples:
         --------------------
         meta_filter = 'contrail:config-root-domain' (retunn only domains)
         meta_filter = ' or '.join(['contrail:config-root-domain',
                     'contrail:config-root-virtual-router']) (domain or virtual-router)
         meta_filter = 'contrail:config-root-domain or
                      contrail:config-root-virtual-router' (same as above)
         meta_filter = 'contrail:domain-project' (search all projects)
         meta_filter = None (search everything)        
        """
        ###session###
        session_id = self._mapclient.get_session_id()

        ###start_identity###
        if not start_identity_name:
            start_identity_name = 'contrail:config-root:root'
        start_identity = str(Identity(name=start_identity_name, type='other', other_type='extended'))

        ###search_params###
        search_params = {}
        search_params['max-depth'] = str(max_depth)
        search_params['max-size'] = '50000000'        

        if meta_filter:
            search_params['match-links'] = meta_filter
        if result_filter:
            search_params['result-filter'] = result_filter

        search_req = SearchRequest(session_id, start_identity,
                                 search_parameters=search_params)

        result = self._mapclient.call('search', search_req)
        return result

    def poll(self):
        pass


ifmap_client_obj = IfmapClient(ifmap_ip, ifmap_port, ifmap_user, ifmap_passwd)
res = ifmap_client_obj.search()
print res

