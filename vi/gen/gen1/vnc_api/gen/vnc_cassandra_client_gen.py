
# AUTO-GENERATED file from IFMapApiGenerator. Do Not Edit!

import re
import json
import pycassa
import datetime
from operator import itemgetter
import cfgm_common.exceptions
from cfgm_common import utils

class VncCassandraClientGen(object):
    def __init__(self):
        self._re_match_parent = re.compile('parent:')
        self._re_match_prop = re.compile('prop:')
        self._re_match_ref = re.compile('ref:')
        self._re_match_backref = re.compile('backref:')
        self._re_match_children = re.compile('children:')
    #end __init__

    def _cassandra_domain_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_domain_alloc

    def _cassandra_domain_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('domain')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'domain', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('domain_limits', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'domain_limits', field)

        field = obj_dict.get('api_access_list', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'api_access_list', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('domain', fq_name_cols)

        return (True, '')
    #end _cassandra_domain_create

    def _cassandra_domain_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'projects' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['projects'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['projects'] = sorted_children
                [child.pop('tstamp') for child in result['projects']]

            if 'namespaces' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['namespaces'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['namespaces'] = sorted_children
                [child.pop('tstamp') for child in result['namespaces']]

            if 'service_templates' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['service_templates'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['service_templates'] = sorted_children
                [child.pop('tstamp') for child in result['service_templates']]

            if 'virtual_DNSs' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['virtual_DNSs'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['virtual_DNSs'] = sorted_children
                [child.pop('tstamp') for child in result['virtual_DNSs']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_domain_read

    def _cassandra_domain_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'domain_limits' in new_obj_dict:
            new_props['domain_limits'] = new_obj_dict['domain_limits']
        if 'api_access_list' in new_obj_dict:
            new_props['api_access_list'] = new_obj_dict['api_access_list']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'domain', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'domain', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_domain_update

    def _cassandra_domain_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:domain:'
            col_fin = 'children:domain;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:domain:'
            col_fin = 'backref:domain;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('domain', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_domain_list

    def _cassandra_domain_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'domain', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'domain', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('domain', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_domain_delete

    def _cassandra_global_vrouter_config_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_global_vrouter_config_alloc

    def _cassandra_global_vrouter_config_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('global_vrouter_config')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'global_vrouter_config', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('linklocal_services', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'linklocal_services', field)

        field = obj_dict.get('encapsulation_priorities', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'encapsulation_priorities', field)

        field = obj_dict.get('vxlan_network_identifier_mode', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'vxlan_network_identifier_mode', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('global_vrouter_config', fq_name_cols)

        return (True, '')
    #end _cassandra_global_vrouter_config_create

    def _cassandra_global_vrouter_config_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_global_vrouter_config_read

    def _cassandra_global_vrouter_config_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'linklocal_services' in new_obj_dict:
            new_props['linklocal_services'] = new_obj_dict['linklocal_services']
        if 'encapsulation_priorities' in new_obj_dict:
            new_props['encapsulation_priorities'] = new_obj_dict['encapsulation_priorities']
        if 'vxlan_network_identifier_mode' in new_obj_dict:
            new_props['vxlan_network_identifier_mode'] = new_obj_dict['vxlan_network_identifier_mode']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'global_vrouter_config', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'global_vrouter_config', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_global_vrouter_config_update

    def _cassandra_global_vrouter_config_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:global_vrouter_config:'
            col_fin = 'children:global_vrouter_config;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:global_vrouter_config:'
            col_fin = 'backref:global_vrouter_config;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('global_vrouter_config', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_global_vrouter_config_list

    def _cassandra_global_vrouter_config_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'global_vrouter_config', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'global_vrouter_config', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('global_vrouter_config', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_global_vrouter_config_delete

    def _cassandra_instance_ip_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_instance_ip_alloc

    def _cassandra_instance_ip_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('instance_ip')

        # Properties
        field = obj_dict.get('instance_ip_address', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'instance_ip_address', field)

        field = obj_dict.get('instance_ip_family', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'instance_ip_family', field)

        field = obj_dict.get('instance_ip_mode', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'instance_ip_mode', field)

        field = obj_dict.get('subnet_uuid', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'subnet_uuid', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('virtual_network_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_network', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'instance_ip', obj_ids['uuid'], 'virtual_network', ref_uuid, ref_data)
        refs = obj_dict.get('virtual_machine_interface_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'instance_ip', obj_ids['uuid'], 'virtual_machine_interface', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('instance_ip', fq_name_cols)

        return (True, '')
    #end _cassandra_instance_ip_create

    def _cassandra_instance_ip_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_instance_ip_read

    def _cassandra_instance_ip_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'virtual_network_refs' in new_obj_dict:
            new_ref_infos['virtual_network'] = {}
            new_refs = new_obj_dict['virtual_network_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_network', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_network'][new_ref_uuid] = new_ref_data

        if 'virtual_machine_interface_refs' in new_obj_dict:
            new_ref_infos['virtual_machine_interface'] = {}
            new_refs = new_obj_dict['virtual_machine_interface_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_machine_interface'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'instance_ip_address' in new_obj_dict:
            new_props['instance_ip_address'] = new_obj_dict['instance_ip_address']
        if 'instance_ip_family' in new_obj_dict:
            new_props['instance_ip_family'] = new_obj_dict['instance_ip_family']
        if 'instance_ip_mode' in new_obj_dict:
            new_props['instance_ip_mode'] = new_obj_dict['instance_ip_mode']
        if 'subnet_uuid' in new_obj_dict:
            new_props['subnet_uuid'] = new_obj_dict['subnet_uuid']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'instance_ip', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'instance_ip', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_instance_ip_update

    def _cassandra_instance_ip_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:instance_ip:'
            col_fin = 'children:instance_ip;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:instance_ip:'
            col_fin = 'backref:instance_ip;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('instance_ip', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_instance_ip_list

    def _cassandra_instance_ip_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'instance_ip', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'instance_ip', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('instance_ip', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_instance_ip_delete

    def _cassandra_network_policy_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_network_policy_alloc

    def _cassandra_network_policy_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('network_policy')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'network_policy', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('network_policy_entries', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'network_policy_entries', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('network_policy', fq_name_cols)

        return (True, '')
    #end _cassandra_network_policy_create

    def _cassandra_network_policy_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_network_policy_read

    def _cassandra_network_policy_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'network_policy_entries' in new_obj_dict:
            new_props['network_policy_entries'] = new_obj_dict['network_policy_entries']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'network_policy', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'network_policy', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_network_policy_update

    def _cassandra_network_policy_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:network_policy:'
            col_fin = 'children:network_policy;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:network_policy:'
            col_fin = 'backref:network_policy;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('network_policy', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_network_policy_list

    def _cassandra_network_policy_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'network_policy', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'network_policy', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('network_policy', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_network_policy_delete

    def _cassandra_virtual_DNS_record_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_virtual_DNS_record_alloc

    def _cassandra_virtual_DNS_record_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('virtual_DNS_record')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'virtual_DNS_record', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('virtual_DNS_record_data', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_DNS_record_data', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('virtual_DNS_record', fq_name_cols)

        return (True, '')
    #end _cassandra_virtual_DNS_record_create

    def _cassandra_virtual_DNS_record_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_virtual_DNS_record_read

    def _cassandra_virtual_DNS_record_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'virtual_DNS_record_data' in new_obj_dict:
            new_props['virtual_DNS_record_data'] = new_obj_dict['virtual_DNS_record_data']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'virtual_DNS_record', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'virtual_DNS_record', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_virtual_DNS_record_update

    def _cassandra_virtual_DNS_record_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:virtual_DNS_record:'
            col_fin = 'children:virtual_DNS_record;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:virtual_DNS_record:'
            col_fin = 'backref:virtual_DNS_record;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('virtual_DNS_record', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_virtual_DNS_record_list

    def _cassandra_virtual_DNS_record_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'virtual_DNS_record', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'virtual_DNS_record', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('virtual_DNS_record', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_virtual_DNS_record_delete

    def _cassandra_route_target_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_route_target_alloc

    def _cassandra_route_target_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('route_target')

        # Properties
        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('route_target', fq_name_cols)

        return (True, '')
    #end _cassandra_route_target_create

    def _cassandra_route_target_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_route_target_read

    def _cassandra_route_target_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'route_target', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'route_target', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_route_target_update

    def _cassandra_route_target_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:route_target:'
            col_fin = 'children:route_target;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:route_target:'
            col_fin = 'backref:route_target;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('route_target', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_route_target_list

    def _cassandra_route_target_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'route_target', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'route_target', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('route_target', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_route_target_delete

    def _cassandra_floating_ip_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_floating_ip_alloc

    def _cassandra_floating_ip_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('floating_ip')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'floating_ip', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('floating_ip_address', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'floating_ip_address', field)

        field = obj_dict.get('floating_ip_is_virtual_ip', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'floating_ip_is_virtual_ip', field)

        field = obj_dict.get('floating_ip_fixed_ip_address', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'floating_ip_fixed_ip_address', field)

        field = obj_dict.get('floating_ip_address_family', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'floating_ip_address_family', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('project_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('project', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'floating_ip', obj_ids['uuid'], 'project', ref_uuid, ref_data)
        refs = obj_dict.get('virtual_machine_interface_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'floating_ip', obj_ids['uuid'], 'virtual_machine_interface', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('floating_ip', fq_name_cols)

        return (True, '')
    #end _cassandra_floating_ip_create

    def _cassandra_floating_ip_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_floating_ip_read

    def _cassandra_floating_ip_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'project_refs' in new_obj_dict:
            new_ref_infos['project'] = {}
            new_refs = new_obj_dict['project_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('project', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['project'][new_ref_uuid] = new_ref_data

        if 'virtual_machine_interface_refs' in new_obj_dict:
            new_ref_infos['virtual_machine_interface'] = {}
            new_refs = new_obj_dict['virtual_machine_interface_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_machine_interface'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'floating_ip_address' in new_obj_dict:
            new_props['floating_ip_address'] = new_obj_dict['floating_ip_address']
        if 'floating_ip_is_virtual_ip' in new_obj_dict:
            new_props['floating_ip_is_virtual_ip'] = new_obj_dict['floating_ip_is_virtual_ip']
        if 'floating_ip_fixed_ip_address' in new_obj_dict:
            new_props['floating_ip_fixed_ip_address'] = new_obj_dict['floating_ip_fixed_ip_address']
        if 'floating_ip_address_family' in new_obj_dict:
            new_props['floating_ip_address_family'] = new_obj_dict['floating_ip_address_family']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'floating_ip', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'floating_ip', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_floating_ip_update

    def _cassandra_floating_ip_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:floating_ip:'
            col_fin = 'children:floating_ip;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:floating_ip:'
            col_fin = 'backref:floating_ip;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('floating_ip', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_floating_ip_list

    def _cassandra_floating_ip_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'floating_ip', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'floating_ip', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('floating_ip', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_floating_ip_delete

    def _cassandra_floating_ip_pool_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_floating_ip_pool_alloc

    def _cassandra_floating_ip_pool_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('floating_ip_pool')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'floating_ip_pool', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('floating_ip_pool_prefixes', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'floating_ip_pool_prefixes', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('floating_ip_pool', fq_name_cols)

        return (True, '')
    #end _cassandra_floating_ip_pool_create

    def _cassandra_floating_ip_pool_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'floating_ips' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['floating_ips'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['floating_ips'] = sorted_children
                [child.pop('tstamp') for child in result['floating_ips']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_floating_ip_pool_read

    def _cassandra_floating_ip_pool_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'floating_ip_pool_prefixes' in new_obj_dict:
            new_props['floating_ip_pool_prefixes'] = new_obj_dict['floating_ip_pool_prefixes']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'floating_ip_pool', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'floating_ip_pool', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_floating_ip_pool_update

    def _cassandra_floating_ip_pool_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:floating_ip_pool:'
            col_fin = 'children:floating_ip_pool;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:floating_ip_pool:'
            col_fin = 'backref:floating_ip_pool;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('floating_ip_pool', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_floating_ip_pool_list

    def _cassandra_floating_ip_pool_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'floating_ip_pool', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'floating_ip_pool', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('floating_ip_pool', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_floating_ip_pool_delete

    def _cassandra_physical_router_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_physical_router_alloc

    def _cassandra_physical_router_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('physical_router')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'physical_router', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('physical_router_management_ip', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'physical_router_management_ip', field)

        field = obj_dict.get('physical_router_dataplane_ip', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'physical_router_dataplane_ip', field)

        field = obj_dict.get('physical_router_vendor_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'physical_router_vendor_name', field)

        field = obj_dict.get('physical_router_user_credentials', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'physical_router_user_credentials', field)

        field = obj_dict.get('physical_router_snmp_credentials', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'physical_router_snmp_credentials', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('virtual_router_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_router', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'physical_router', obj_ids['uuid'], 'virtual_router', ref_uuid, ref_data)
        refs = obj_dict.get('bgp_router_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('bgp_router', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'physical_router', obj_ids['uuid'], 'bgp_router', ref_uuid, ref_data)
        refs = obj_dict.get('virtual_network_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_network', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'physical_router', obj_ids['uuid'], 'virtual_network', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('physical_router', fq_name_cols)

        return (True, '')
    #end _cassandra_physical_router_create

    def _cassandra_physical_router_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'physical_interfaces' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['physical_interfaces'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['physical_interfaces'] = sorted_children
                [child.pop('tstamp') for child in result['physical_interfaces']]

            if 'logical_interfaces' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['logical_interfaces'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['logical_interfaces'] = sorted_children
                [child.pop('tstamp') for child in result['logical_interfaces']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_physical_router_read

    def _cassandra_physical_router_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'virtual_router_refs' in new_obj_dict:
            new_ref_infos['virtual_router'] = {}
            new_refs = new_obj_dict['virtual_router_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_router', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_router'][new_ref_uuid] = new_ref_data

        if 'bgp_router_refs' in new_obj_dict:
            new_ref_infos['bgp_router'] = {}
            new_refs = new_obj_dict['bgp_router_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('bgp_router', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['bgp_router'][new_ref_uuid] = new_ref_data

        if 'virtual_network_refs' in new_obj_dict:
            new_ref_infos['virtual_network'] = {}
            new_refs = new_obj_dict['virtual_network_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_network', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_network'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'physical_router_management_ip' in new_obj_dict:
            new_props['physical_router_management_ip'] = new_obj_dict['physical_router_management_ip']
        if 'physical_router_dataplane_ip' in new_obj_dict:
            new_props['physical_router_dataplane_ip'] = new_obj_dict['physical_router_dataplane_ip']
        if 'physical_router_vendor_name' in new_obj_dict:
            new_props['physical_router_vendor_name'] = new_obj_dict['physical_router_vendor_name']
        if 'physical_router_user_credentials' in new_obj_dict:
            new_props['physical_router_user_credentials'] = new_obj_dict['physical_router_user_credentials']
        if 'physical_router_snmp_credentials' in new_obj_dict:
            new_props['physical_router_snmp_credentials'] = new_obj_dict['physical_router_snmp_credentials']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'physical_router', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'physical_router', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_physical_router_update

    def _cassandra_physical_router_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:physical_router:'
            col_fin = 'children:physical_router;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:physical_router:'
            col_fin = 'backref:physical_router;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('physical_router', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_physical_router_list

    def _cassandra_physical_router_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'physical_router', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'physical_router', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('physical_router', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_physical_router_delete

    def _cassandra_bgp_router_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_bgp_router_alloc

    def _cassandra_bgp_router_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('bgp_router')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'bgp_router', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('bgp_router_parameters', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'bgp_router_parameters', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('bgp_router_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('bgp_router', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'bgp_router', obj_ids['uuid'], 'bgp_router', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('bgp_router', fq_name_cols)

        return (True, '')
    #end _cassandra_bgp_router_create

    def _cassandra_bgp_router_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_bgp_router_read

    def _cassandra_bgp_router_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'bgp_router_refs' in new_obj_dict:
            new_ref_infos['bgp_router'] = {}
            new_refs = new_obj_dict['bgp_router_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('bgp_router', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['bgp_router'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'bgp_router_parameters' in new_obj_dict:
            new_props['bgp_router_parameters'] = new_obj_dict['bgp_router_parameters']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'bgp_router', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'bgp_router', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_bgp_router_update

    def _cassandra_bgp_router_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:bgp_router:'
            col_fin = 'children:bgp_router;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:bgp_router:'
            col_fin = 'backref:bgp_router;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('bgp_router', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_bgp_router_list

    def _cassandra_bgp_router_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'bgp_router', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'bgp_router', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('bgp_router', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_bgp_router_delete

    def _cassandra_virtual_router_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_virtual_router_alloc

    def _cassandra_virtual_router_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('virtual_router')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'virtual_router', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('virtual_router_type', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_router_type', field)

        field = obj_dict.get('virtual_router_ip_address', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_router_ip_address', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('bgp_router_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('bgp_router', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_router', obj_ids['uuid'], 'bgp_router', ref_uuid, ref_data)
        refs = obj_dict.get('virtual_machine_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_machine', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_router', obj_ids['uuid'], 'virtual_machine', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('virtual_router', fq_name_cols)

        return (True, '')
    #end _cassandra_virtual_router_create

    def _cassandra_virtual_router_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_virtual_router_read

    def _cassandra_virtual_router_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'bgp_router_refs' in new_obj_dict:
            new_ref_infos['bgp_router'] = {}
            new_refs = new_obj_dict['bgp_router_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('bgp_router', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['bgp_router'][new_ref_uuid] = new_ref_data

        if 'virtual_machine_refs' in new_obj_dict:
            new_ref_infos['virtual_machine'] = {}
            new_refs = new_obj_dict['virtual_machine_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_machine', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_machine'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'virtual_router_type' in new_obj_dict:
            new_props['virtual_router_type'] = new_obj_dict['virtual_router_type']
        if 'virtual_router_ip_address' in new_obj_dict:
            new_props['virtual_router_ip_address'] = new_obj_dict['virtual_router_ip_address']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'virtual_router', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'virtual_router', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_virtual_router_update

    def _cassandra_virtual_router_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:virtual_router:'
            col_fin = 'children:virtual_router;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:virtual_router:'
            col_fin = 'backref:virtual_router;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('virtual_router', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_virtual_router_list

    def _cassandra_virtual_router_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'virtual_router', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'virtual_router', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('virtual_router', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_virtual_router_delete

    def _cassandra_config_root_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_config_root_alloc

    def _cassandra_config_root_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('config_root')

        # Properties
        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('config_root', fq_name_cols)

        return (True, '')
    #end _cassandra_config_root_create

    def _cassandra_config_root_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'global_system_configs' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['global_system_configs'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['global_system_configs'] = sorted_children
                [child.pop('tstamp') for child in result['global_system_configs']]

            if 'domains' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['domains'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['domains'] = sorted_children
                [child.pop('tstamp') for child in result['domains']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_config_root_read

    def _cassandra_config_root_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'config_root', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'config_root', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_config_root_update

    def _cassandra_config_root_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:config_root:'
            col_fin = 'children:config_root;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:config_root:'
            col_fin = 'backref:config_root;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('config_root', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_config_root_list

    def _cassandra_config_root_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'config_root', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'config_root', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('config_root', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_config_root_delete

    def _cassandra_subnet_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_subnet_alloc

    def _cassandra_subnet_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('subnet')

        # Properties
        field = obj_dict.get('subnet_ip_prefix', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'subnet_ip_prefix', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('virtual_machine_interface_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'subnet', obj_ids['uuid'], 'virtual_machine_interface', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('subnet', fq_name_cols)

        return (True, '')
    #end _cassandra_subnet_create

    def _cassandra_subnet_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_subnet_read

    def _cassandra_subnet_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'virtual_machine_interface_refs' in new_obj_dict:
            new_ref_infos['virtual_machine_interface'] = {}
            new_refs = new_obj_dict['virtual_machine_interface_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_machine_interface'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'subnet_ip_prefix' in new_obj_dict:
            new_props['subnet_ip_prefix'] = new_obj_dict['subnet_ip_prefix']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'subnet', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'subnet', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_subnet_update

    def _cassandra_subnet_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:subnet:'
            col_fin = 'children:subnet;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:subnet:'
            col_fin = 'backref:subnet;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('subnet', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_subnet_list

    def _cassandra_subnet_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'subnet', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'subnet', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('subnet', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_subnet_delete

    def _cassandra_global_system_config_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_global_system_config_alloc

    def _cassandra_global_system_config_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('global_system_config')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'global_system_config', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('autonomous_system', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'autonomous_system', field)

        field = obj_dict.get('config_version', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'config_version', field)

        field = obj_dict.get('plugin_tuning', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'plugin_tuning', field)

        field = obj_dict.get('ibgp_auto_mesh', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'ibgp_auto_mesh', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('bgp_router_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('bgp_router', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'global_system_config', obj_ids['uuid'], 'bgp_router', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('global_system_config', fq_name_cols)

        return (True, '')
    #end _cassandra_global_system_config_create

    def _cassandra_global_system_config_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'global_vrouter_configs' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['global_vrouter_configs'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['global_vrouter_configs'] = sorted_children
                [child.pop('tstamp') for child in result['global_vrouter_configs']]

            if 'physical_routers' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['physical_routers'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['physical_routers'] = sorted_children
                [child.pop('tstamp') for child in result['physical_routers']]

            if 'virtual_routers' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['virtual_routers'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['virtual_routers'] = sorted_children
                [child.pop('tstamp') for child in result['virtual_routers']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_global_system_config_read

    def _cassandra_global_system_config_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'bgp_router_refs' in new_obj_dict:
            new_ref_infos['bgp_router'] = {}
            new_refs = new_obj_dict['bgp_router_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('bgp_router', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['bgp_router'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'autonomous_system' in new_obj_dict:
            new_props['autonomous_system'] = new_obj_dict['autonomous_system']
        if 'config_version' in new_obj_dict:
            new_props['config_version'] = new_obj_dict['config_version']
        if 'plugin_tuning' in new_obj_dict:
            new_props['plugin_tuning'] = new_obj_dict['plugin_tuning']
        if 'ibgp_auto_mesh' in new_obj_dict:
            new_props['ibgp_auto_mesh'] = new_obj_dict['ibgp_auto_mesh']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'global_system_config', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'global_system_config', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_global_system_config_update

    def _cassandra_global_system_config_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:global_system_config:'
            col_fin = 'children:global_system_config;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:global_system_config:'
            col_fin = 'backref:global_system_config;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('global_system_config', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_global_system_config_list

    def _cassandra_global_system_config_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'global_system_config', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'global_system_config', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('global_system_config', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_global_system_config_delete

    def _cassandra_loadbalancer_member_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_loadbalancer_member_alloc

    def _cassandra_loadbalancer_member_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('loadbalancer_member')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'loadbalancer_member', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('loadbalancer_member_properties', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'loadbalancer_member_properties', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('loadbalancer_member', fq_name_cols)

        return (True, '')
    #end _cassandra_loadbalancer_member_create

    def _cassandra_loadbalancer_member_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_loadbalancer_member_read

    def _cassandra_loadbalancer_member_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'loadbalancer_member_properties' in new_obj_dict:
            new_props['loadbalancer_member_properties'] = new_obj_dict['loadbalancer_member_properties']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'loadbalancer_member', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'loadbalancer_member', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_loadbalancer_member_update

    def _cassandra_loadbalancer_member_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:loadbalancer_member:'
            col_fin = 'children:loadbalancer_member;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:loadbalancer_member:'
            col_fin = 'backref:loadbalancer_member;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('loadbalancer_member', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_loadbalancer_member_list

    def _cassandra_loadbalancer_member_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'loadbalancer_member', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'loadbalancer_member', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('loadbalancer_member', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_loadbalancer_member_delete

    def _cassandra_service_instance_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_service_instance_alloc

    def _cassandra_service_instance_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('service_instance')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'service_instance', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('service_instance_properties', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'service_instance_properties', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('service_template_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('service_template', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'service_instance', obj_ids['uuid'], 'service_template', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('service_instance', fq_name_cols)

        return (True, '')
    #end _cassandra_service_instance_create

    def _cassandra_service_instance_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_service_instance_read

    def _cassandra_service_instance_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'service_template_refs' in new_obj_dict:
            new_ref_infos['service_template'] = {}
            new_refs = new_obj_dict['service_template_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('service_template', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['service_template'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'service_instance_properties' in new_obj_dict:
            new_props['service_instance_properties'] = new_obj_dict['service_instance_properties']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'service_instance', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'service_instance', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_service_instance_update

    def _cassandra_service_instance_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:service_instance:'
            col_fin = 'children:service_instance;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:service_instance:'
            col_fin = 'backref:service_instance;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('service_instance', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_service_instance_list

    def _cassandra_service_instance_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'service_instance', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'service_instance', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('service_instance', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_service_instance_delete

    def _cassandra_namespace_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_namespace_alloc

    def _cassandra_namespace_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('namespace')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'namespace', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('namespace_cidr', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'namespace_cidr', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('namespace', fq_name_cols)

        return (True, '')
    #end _cassandra_namespace_create

    def _cassandra_namespace_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_namespace_read

    def _cassandra_namespace_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'namespace_cidr' in new_obj_dict:
            new_props['namespace_cidr'] = new_obj_dict['namespace_cidr']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'namespace', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'namespace', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_namespace_update

    def _cassandra_namespace_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:namespace:'
            col_fin = 'children:namespace;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:namespace:'
            col_fin = 'backref:namespace;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('namespace', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_namespace_list

    def _cassandra_namespace_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'namespace', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'namespace', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('namespace', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_namespace_delete

    def _cassandra_route_table_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_route_table_alloc

    def _cassandra_route_table_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('route_table')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'route_table', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('routes', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'routes', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('route_table', fq_name_cols)

        return (True, '')
    #end _cassandra_route_table_create

    def _cassandra_route_table_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_route_table_read

    def _cassandra_route_table_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'routes' in new_obj_dict:
            new_props['routes'] = new_obj_dict['routes']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'route_table', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'route_table', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_route_table_update

    def _cassandra_route_table_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:route_table:'
            col_fin = 'children:route_table;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:route_table:'
            col_fin = 'backref:route_table;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('route_table', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_route_table_list

    def _cassandra_route_table_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'route_table', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'route_table', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('route_table', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_route_table_delete

    def _cassandra_physical_interface_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_physical_interface_alloc

    def _cassandra_physical_interface_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('physical_interface')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'physical_interface', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('physical_interface', fq_name_cols)

        return (True, '')
    #end _cassandra_physical_interface_create

    def _cassandra_physical_interface_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'logical_interfaces' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['logical_interfaces'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['logical_interfaces'] = sorted_children
                [child.pop('tstamp') for child in result['logical_interfaces']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_physical_interface_read

    def _cassandra_physical_interface_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'physical_interface', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'physical_interface', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_physical_interface_update

    def _cassandra_physical_interface_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:physical_interface:'
            col_fin = 'children:physical_interface;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:physical_interface:'
            col_fin = 'backref:physical_interface;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('physical_interface', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_physical_interface_list

    def _cassandra_physical_interface_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'physical_interface', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'physical_interface', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('physical_interface', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_physical_interface_delete

    def _cassandra_access_control_list_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_access_control_list_alloc

    def _cassandra_access_control_list_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('access_control_list')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'access_control_list', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('access_control_list_entries', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'access_control_list_entries', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('access_control_list', fq_name_cols)

        return (True, '')
    #end _cassandra_access_control_list_create

    def _cassandra_access_control_list_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_access_control_list_read

    def _cassandra_access_control_list_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'access_control_list_entries' in new_obj_dict:
            new_props['access_control_list_entries'] = new_obj_dict['access_control_list_entries']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'access_control_list', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'access_control_list', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_access_control_list_update

    def _cassandra_access_control_list_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:access_control_list:'
            col_fin = 'children:access_control_list;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:access_control_list:'
            col_fin = 'backref:access_control_list;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('access_control_list', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_access_control_list_list

    def _cassandra_access_control_list_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'access_control_list', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'access_control_list', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('access_control_list', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_access_control_list_delete

    def _cassandra_virtual_DNS_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_virtual_DNS_alloc

    def _cassandra_virtual_DNS_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('virtual_DNS')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'virtual_DNS', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('virtual_DNS_data', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_DNS_data', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('virtual_DNS', fq_name_cols)

        return (True, '')
    #end _cassandra_virtual_DNS_create

    def _cassandra_virtual_DNS_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'virtual_DNS_records' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['virtual_DNS_records'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['virtual_DNS_records'] = sorted_children
                [child.pop('tstamp') for child in result['virtual_DNS_records']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_virtual_DNS_read

    def _cassandra_virtual_DNS_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'virtual_DNS_data' in new_obj_dict:
            new_props['virtual_DNS_data'] = new_obj_dict['virtual_DNS_data']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'virtual_DNS', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'virtual_DNS', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_virtual_DNS_update

    def _cassandra_virtual_DNS_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:virtual_DNS:'
            col_fin = 'children:virtual_DNS;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:virtual_DNS:'
            col_fin = 'backref:virtual_DNS;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('virtual_DNS', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_virtual_DNS_list

    def _cassandra_virtual_DNS_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'virtual_DNS', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'virtual_DNS', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('virtual_DNS', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_virtual_DNS_delete

    def _cassandra_customer_attachment_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_customer_attachment_alloc

    def _cassandra_customer_attachment_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('customer_attachment')

        # Properties
        field = obj_dict.get('attachment_address', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'attachment_address', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('virtual_machine_interface_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'customer_attachment', obj_ids['uuid'], 'virtual_machine_interface', ref_uuid, ref_data)
        refs = obj_dict.get('floating_ip_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('floating_ip', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'customer_attachment', obj_ids['uuid'], 'floating_ip', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('customer_attachment', fq_name_cols)

        return (True, '')
    #end _cassandra_customer_attachment_create

    def _cassandra_customer_attachment_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_customer_attachment_read

    def _cassandra_customer_attachment_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'virtual_machine_interface_refs' in new_obj_dict:
            new_ref_infos['virtual_machine_interface'] = {}
            new_refs = new_obj_dict['virtual_machine_interface_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_machine_interface'][new_ref_uuid] = new_ref_data

        if 'floating_ip_refs' in new_obj_dict:
            new_ref_infos['floating_ip'] = {}
            new_refs = new_obj_dict['floating_ip_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('floating_ip', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['floating_ip'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'attachment_address' in new_obj_dict:
            new_props['attachment_address'] = new_obj_dict['attachment_address']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'customer_attachment', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'customer_attachment', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_customer_attachment_update

    def _cassandra_customer_attachment_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:customer_attachment:'
            col_fin = 'children:customer_attachment;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:customer_attachment:'
            col_fin = 'backref:customer_attachment;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('customer_attachment', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_customer_attachment_list

    def _cassandra_customer_attachment_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'customer_attachment', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'customer_attachment', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('customer_attachment', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_customer_attachment_delete

    def _cassandra_loadbalancer_pool_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_loadbalancer_pool_alloc

    def _cassandra_loadbalancer_pool_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('loadbalancer_pool')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'loadbalancer_pool', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('loadbalancer_pool_properties', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'loadbalancer_pool_properties', field)

        field = obj_dict.get('loadbalancer_pool_provider', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'loadbalancer_pool_provider', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('service_instance_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('service_instance', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'loadbalancer_pool', obj_ids['uuid'], 'service_instance', ref_uuid, ref_data)
        refs = obj_dict.get('virtual_machine_interface_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'loadbalancer_pool', obj_ids['uuid'], 'virtual_machine_interface', ref_uuid, ref_data)
        refs = obj_dict.get('loadbalancer_healthmonitor_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('loadbalancer_healthmonitor', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'loadbalancer_pool', obj_ids['uuid'], 'loadbalancer_healthmonitor', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('loadbalancer_pool', fq_name_cols)

        return (True, '')
    #end _cassandra_loadbalancer_pool_create

    def _cassandra_loadbalancer_pool_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'loadbalancer_members' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['loadbalancer_members'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['loadbalancer_members'] = sorted_children
                [child.pop('tstamp') for child in result['loadbalancer_members']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_loadbalancer_pool_read

    def _cassandra_loadbalancer_pool_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'service_instance_refs' in new_obj_dict:
            new_ref_infos['service_instance'] = {}
            new_refs = new_obj_dict['service_instance_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('service_instance', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['service_instance'][new_ref_uuid] = new_ref_data

        if 'virtual_machine_interface_refs' in new_obj_dict:
            new_ref_infos['virtual_machine_interface'] = {}
            new_refs = new_obj_dict['virtual_machine_interface_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_machine_interface'][new_ref_uuid] = new_ref_data

        if 'loadbalancer_healthmonitor_refs' in new_obj_dict:
            new_ref_infos['loadbalancer_healthmonitor'] = {}
            new_refs = new_obj_dict['loadbalancer_healthmonitor_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('loadbalancer_healthmonitor', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['loadbalancer_healthmonitor'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'loadbalancer_pool_properties' in new_obj_dict:
            new_props['loadbalancer_pool_properties'] = new_obj_dict['loadbalancer_pool_properties']
        if 'loadbalancer_pool_provider' in new_obj_dict:
            new_props['loadbalancer_pool_provider'] = new_obj_dict['loadbalancer_pool_provider']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'loadbalancer_pool', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'loadbalancer_pool', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_loadbalancer_pool_update

    def _cassandra_loadbalancer_pool_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:loadbalancer_pool:'
            col_fin = 'children:loadbalancer_pool;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:loadbalancer_pool:'
            col_fin = 'backref:loadbalancer_pool;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('loadbalancer_pool', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_loadbalancer_pool_list

    def _cassandra_loadbalancer_pool_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'loadbalancer_pool', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'loadbalancer_pool', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('loadbalancer_pool', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_loadbalancer_pool_delete

    def _cassandra_virtual_machine_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_virtual_machine_alloc

    def _cassandra_virtual_machine_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('virtual_machine')

        # Properties
        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('service_instance_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('service_instance', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': True}
            self._create_ref(bch, 'virtual_machine', obj_ids['uuid'], 'service_instance', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('virtual_machine', fq_name_cols)

        return (True, '')
    #end _cassandra_virtual_machine_create

    def _cassandra_virtual_machine_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'virtual_machine_interfaces' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['virtual_machine_interfaces'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['virtual_machine_interfaces'] = sorted_children
                [child.pop('tstamp') for child in result['virtual_machine_interfaces']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_virtual_machine_read

    def _cassandra_virtual_machine_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'service_instance_refs' in new_obj_dict:
            new_ref_infos['service_instance'] = {}
            new_refs = new_obj_dict['service_instance_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('service_instance', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': True}
                    new_ref_infos['service_instance'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'virtual_machine', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'virtual_machine', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_virtual_machine_update

    def _cassandra_virtual_machine_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:virtual_machine:'
            col_fin = 'children:virtual_machine;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:virtual_machine:'
            col_fin = 'backref:virtual_machine;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('virtual_machine', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_virtual_machine_list

    def _cassandra_virtual_machine_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'virtual_machine', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'virtual_machine', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('virtual_machine', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_virtual_machine_delete

    def _cassandra_interface_route_table_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_interface_route_table_alloc

    def _cassandra_interface_route_table_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('interface_route_table')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'interface_route_table', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('interface_route_table_routes', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'interface_route_table_routes', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('interface_route_table', fq_name_cols)

        return (True, '')
    #end _cassandra_interface_route_table_create

    def _cassandra_interface_route_table_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_interface_route_table_read

    def _cassandra_interface_route_table_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'interface_route_table_routes' in new_obj_dict:
            new_props['interface_route_table_routes'] = new_obj_dict['interface_route_table_routes']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'interface_route_table', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'interface_route_table', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_interface_route_table_update

    def _cassandra_interface_route_table_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:interface_route_table:'
            col_fin = 'children:interface_route_table;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:interface_route_table:'
            col_fin = 'backref:interface_route_table;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('interface_route_table', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_interface_route_table_list

    def _cassandra_interface_route_table_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'interface_route_table', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'interface_route_table', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('interface_route_table', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_interface_route_table_delete

    def _cassandra_service_template_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_service_template_alloc

    def _cassandra_service_template_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('service_template')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'service_template', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('service_template_properties', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'service_template_properties', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('service_template', fq_name_cols)

        return (True, '')
    #end _cassandra_service_template_create

    def _cassandra_service_template_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_service_template_read

    def _cassandra_service_template_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'service_template_properties' in new_obj_dict:
            new_props['service_template_properties'] = new_obj_dict['service_template_properties']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'service_template', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'service_template', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_service_template_update

    def _cassandra_service_template_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:service_template:'
            col_fin = 'children:service_template;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:service_template:'
            col_fin = 'backref:service_template;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('service_template', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_service_template_list

    def _cassandra_service_template_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'service_template', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'service_template', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('service_template', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_service_template_delete

    def _cassandra_virtual_ip_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_virtual_ip_alloc

    def _cassandra_virtual_ip_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('virtual_ip')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'virtual_ip', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('virtual_ip_properties', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_ip_properties', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('loadbalancer_pool_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('loadbalancer_pool', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_ip', obj_ids['uuid'], 'loadbalancer_pool', ref_uuid, ref_data)
        refs = obj_dict.get('virtual_machine_interface_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_ip', obj_ids['uuid'], 'virtual_machine_interface', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('virtual_ip', fq_name_cols)

        return (True, '')
    #end _cassandra_virtual_ip_create

    def _cassandra_virtual_ip_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_virtual_ip_read

    def _cassandra_virtual_ip_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'loadbalancer_pool_refs' in new_obj_dict:
            new_ref_infos['loadbalancer_pool'] = {}
            new_refs = new_obj_dict['loadbalancer_pool_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('loadbalancer_pool', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['loadbalancer_pool'][new_ref_uuid] = new_ref_data

        if 'virtual_machine_interface_refs' in new_obj_dict:
            new_ref_infos['virtual_machine_interface'] = {}
            new_refs = new_obj_dict['virtual_machine_interface_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_machine_interface'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'virtual_ip_properties' in new_obj_dict:
            new_props['virtual_ip_properties'] = new_obj_dict['virtual_ip_properties']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'virtual_ip', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'virtual_ip', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_virtual_ip_update

    def _cassandra_virtual_ip_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:virtual_ip:'
            col_fin = 'children:virtual_ip;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:virtual_ip:'
            col_fin = 'backref:virtual_ip;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('virtual_ip', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_virtual_ip_list

    def _cassandra_virtual_ip_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'virtual_ip', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'virtual_ip', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('virtual_ip', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_virtual_ip_delete

    def _cassandra_security_group_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_security_group_alloc

    def _cassandra_security_group_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('security_group')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'security_group', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('security_group_id', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'security_group_id', field)

        field = obj_dict.get('security_group_entries', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'security_group_entries', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('security_group', fq_name_cols)

        return (True, '')
    #end _cassandra_security_group_create

    def _cassandra_security_group_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'access_control_lists' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['access_control_lists'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['access_control_lists'] = sorted_children
                [child.pop('tstamp') for child in result['access_control_lists']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_security_group_read

    def _cassandra_security_group_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'security_group_id' in new_obj_dict:
            new_props['security_group_id'] = new_obj_dict['security_group_id']
        if 'security_group_entries' in new_obj_dict:
            new_props['security_group_entries'] = new_obj_dict['security_group_entries']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'security_group', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'security_group', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_security_group_update

    def _cassandra_security_group_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:security_group:'
            col_fin = 'children:security_group;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:security_group:'
            col_fin = 'backref:security_group;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('security_group', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_security_group_list

    def _cassandra_security_group_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'security_group', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'security_group', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('security_group', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_security_group_delete

    def _cassandra_provider_attachment_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_provider_attachment_alloc

    def _cassandra_provider_attachment_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('provider_attachment')

        # Properties
        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('virtual_router_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_router', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'provider_attachment', obj_ids['uuid'], 'virtual_router', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('provider_attachment', fq_name_cols)

        return (True, '')
    #end _cassandra_provider_attachment_create

    def _cassandra_provider_attachment_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_provider_attachment_read

    def _cassandra_provider_attachment_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'virtual_router_refs' in new_obj_dict:
            new_ref_infos['virtual_router'] = {}
            new_refs = new_obj_dict['virtual_router_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_router', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_router'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'provider_attachment', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'provider_attachment', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_provider_attachment_update

    def _cassandra_provider_attachment_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:provider_attachment:'
            col_fin = 'children:provider_attachment;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:provider_attachment:'
            col_fin = 'backref:provider_attachment;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('provider_attachment', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_provider_attachment_list

    def _cassandra_provider_attachment_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'provider_attachment', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'provider_attachment', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('provider_attachment', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_provider_attachment_delete

    def _cassandra_network_ipam_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_network_ipam_alloc

    def _cassandra_network_ipam_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('network_ipam')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'network_ipam', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('network_ipam_mgmt', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'network_ipam_mgmt', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('virtual_DNS_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_DNS', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'network_ipam', obj_ids['uuid'], 'virtual_DNS', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('network_ipam', fq_name_cols)

        return (True, '')
    #end _cassandra_network_ipam_create

    def _cassandra_network_ipam_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_network_ipam_read

    def _cassandra_network_ipam_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'virtual_DNS_refs' in new_obj_dict:
            new_ref_infos['virtual_DNS'] = {}
            new_refs = new_obj_dict['virtual_DNS_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_DNS', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_DNS'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'network_ipam_mgmt' in new_obj_dict:
            new_props['network_ipam_mgmt'] = new_obj_dict['network_ipam_mgmt']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'network_ipam', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'network_ipam', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_network_ipam_update

    def _cassandra_network_ipam_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:network_ipam:'
            col_fin = 'children:network_ipam;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:network_ipam:'
            col_fin = 'backref:network_ipam;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('network_ipam', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_network_ipam_list

    def _cassandra_network_ipam_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'network_ipam', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'network_ipam', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('network_ipam', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_network_ipam_delete

    def _cassandra_loadbalancer_healthmonitor_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_loadbalancer_healthmonitor_alloc

    def _cassandra_loadbalancer_healthmonitor_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('loadbalancer_healthmonitor')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'loadbalancer_healthmonitor', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('loadbalancer_healthmonitor_properties', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'loadbalancer_healthmonitor_properties', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('loadbalancer_healthmonitor', fq_name_cols)

        return (True, '')
    #end _cassandra_loadbalancer_healthmonitor_create

    def _cassandra_loadbalancer_healthmonitor_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_loadbalancer_healthmonitor_read

    def _cassandra_loadbalancer_healthmonitor_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        new_props = {}
        if 'loadbalancer_healthmonitor_properties' in new_obj_dict:
            new_props['loadbalancer_healthmonitor_properties'] = new_obj_dict['loadbalancer_healthmonitor_properties']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'loadbalancer_healthmonitor', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'loadbalancer_healthmonitor', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_loadbalancer_healthmonitor_update

    def _cassandra_loadbalancer_healthmonitor_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:loadbalancer_healthmonitor:'
            col_fin = 'children:loadbalancer_healthmonitor;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:loadbalancer_healthmonitor:'
            col_fin = 'backref:loadbalancer_healthmonitor;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('loadbalancer_healthmonitor', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_loadbalancer_healthmonitor_list

    def _cassandra_loadbalancer_healthmonitor_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'loadbalancer_healthmonitor', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'loadbalancer_healthmonitor', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('loadbalancer_healthmonitor', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_loadbalancer_healthmonitor_delete

    def _cassandra_virtual_network_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_virtual_network_alloc

    def _cassandra_virtual_network_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('virtual_network')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'virtual_network', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('virtual_network_properties', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_network_properties', field)

        field = obj_dict.get('route_target_list', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'route_target_list', field)

        field = obj_dict.get('router_external', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'router_external', field)

        field = obj_dict.get('is_shared', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'is_shared', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('network_ipam_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('network_ipam', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_network', obj_ids['uuid'], 'network_ipam', ref_uuid, ref_data)
        refs = obj_dict.get('network_policy_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('network_policy', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_network', obj_ids['uuid'], 'network_policy', ref_uuid, ref_data)
        refs = obj_dict.get('route_table_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('route_table', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_network', obj_ids['uuid'], 'route_table', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('virtual_network', fq_name_cols)

        return (True, '')
    #end _cassandra_virtual_network_create

    def _cassandra_virtual_network_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'access_control_lists' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['access_control_lists'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['access_control_lists'] = sorted_children
                [child.pop('tstamp') for child in result['access_control_lists']]

            if 'floating_ip_pools' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['floating_ip_pools'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['floating_ip_pools'] = sorted_children
                [child.pop('tstamp') for child in result['floating_ip_pools']]

            if 'routing_instances' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['routing_instances'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['routing_instances'] = sorted_children
                [child.pop('tstamp') for child in result['routing_instances']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_virtual_network_read

    def _cassandra_virtual_network_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'network_ipam_refs' in new_obj_dict:
            new_ref_infos['network_ipam'] = {}
            new_refs = new_obj_dict['network_ipam_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('network_ipam', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['network_ipam'][new_ref_uuid] = new_ref_data

        if 'network_policy_refs' in new_obj_dict:
            new_ref_infos['network_policy'] = {}
            new_refs = new_obj_dict['network_policy_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('network_policy', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['network_policy'][new_ref_uuid] = new_ref_data

        if 'route_table_refs' in new_obj_dict:
            new_ref_infos['route_table'] = {}
            new_refs = new_obj_dict['route_table_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('route_table', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['route_table'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'virtual_network_properties' in new_obj_dict:
            new_props['virtual_network_properties'] = new_obj_dict['virtual_network_properties']
        if 'route_target_list' in new_obj_dict:
            new_props['route_target_list'] = new_obj_dict['route_target_list']
        if 'router_external' in new_obj_dict:
            new_props['router_external'] = new_obj_dict['router_external']
        if 'is_shared' in new_obj_dict:
            new_props['is_shared'] = new_obj_dict['is_shared']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'virtual_network', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'virtual_network', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_virtual_network_update

    def _cassandra_virtual_network_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:virtual_network:'
            col_fin = 'children:virtual_network;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:virtual_network:'
            col_fin = 'backref:virtual_network;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('virtual_network', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_virtual_network_list

    def _cassandra_virtual_network_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'virtual_network', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'virtual_network', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('virtual_network', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_virtual_network_delete

    def _cassandra_project_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_project_alloc

    def _cassandra_project_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('project')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'project', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('quota', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'quota', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('namespace_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('namespace', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'project', obj_ids['uuid'], 'namespace', ref_uuid, ref_data)
        refs = obj_dict.get('floating_ip_pool_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('floating_ip_pool', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'project', obj_ids['uuid'], 'floating_ip_pool', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('project', fq_name_cols)

        return (True, '')
    #end _cassandra_project_create

    def _cassandra_project_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'security_groups' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['security_groups'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['security_groups'] = sorted_children
                [child.pop('tstamp') for child in result['security_groups']]

            if 'virtual_networks' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['virtual_networks'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['virtual_networks'] = sorted_children
                [child.pop('tstamp') for child in result['virtual_networks']]

            if 'network_ipams' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['network_ipams'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['network_ipams'] = sorted_children
                [child.pop('tstamp') for child in result['network_ipams']]

            if 'network_policys' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['network_policys'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['network_policys'] = sorted_children
                [child.pop('tstamp') for child in result['network_policys']]

            if 'virtual_machine_interfaces' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['virtual_machine_interfaces'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['virtual_machine_interfaces'] = sorted_children
                [child.pop('tstamp') for child in result['virtual_machine_interfaces']]

            if 'service_instances' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['service_instances'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['service_instances'] = sorted_children
                [child.pop('tstamp') for child in result['service_instances']]

            if 'route_tables' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['route_tables'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['route_tables'] = sorted_children
                [child.pop('tstamp') for child in result['route_tables']]

            if 'interface_route_tables' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['interface_route_tables'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['interface_route_tables'] = sorted_children
                [child.pop('tstamp') for child in result['interface_route_tables']]

            if 'logical_routers' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['logical_routers'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['logical_routers'] = sorted_children
                [child.pop('tstamp') for child in result['logical_routers']]

            if 'loadbalancer_pools' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['loadbalancer_pools'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['loadbalancer_pools'] = sorted_children
                [child.pop('tstamp') for child in result['loadbalancer_pools']]

            if 'loadbalancer_healthmonitors' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['loadbalancer_healthmonitors'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['loadbalancer_healthmonitors'] = sorted_children
                [child.pop('tstamp') for child in result['loadbalancer_healthmonitors']]

            if 'virtual_ips' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['virtual_ips'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['virtual_ips'] = sorted_children
                [child.pop('tstamp') for child in result['virtual_ips']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_project_read

    def _cassandra_project_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'namespace_refs' in new_obj_dict:
            new_ref_infos['namespace'] = {}
            new_refs = new_obj_dict['namespace_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('namespace', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['namespace'][new_ref_uuid] = new_ref_data

        if 'floating_ip_pool_refs' in new_obj_dict:
            new_ref_infos['floating_ip_pool'] = {}
            new_refs = new_obj_dict['floating_ip_pool_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('floating_ip_pool', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['floating_ip_pool'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'quota' in new_obj_dict:
            new_props['quota'] = new_obj_dict['quota']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'project', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'project', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_project_update

    def _cassandra_project_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:project:'
            col_fin = 'children:project;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:project:'
            col_fin = 'backref:project;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('project', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_project_list

    def _cassandra_project_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'project', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'project', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('project', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_project_delete

    def _cassandra_logical_interface_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_logical_interface_alloc

    def _cassandra_logical_interface_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('logical_interface')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'logical_interface', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('logical_interface_vlan_tag', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'logical_interface_vlan_tag', field)

        field = obj_dict.get('logical_interface_type', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'logical_interface_type', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('virtual_machine_interface_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'logical_interface', obj_ids['uuid'], 'virtual_machine_interface', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('logical_interface', fq_name_cols)

        return (True, '')
    #end _cassandra_logical_interface_create

    def _cassandra_logical_interface_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_logical_interface_read

    def _cassandra_logical_interface_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'virtual_machine_interface_refs' in new_obj_dict:
            new_ref_infos['virtual_machine_interface'] = {}
            new_refs = new_obj_dict['virtual_machine_interface_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_machine_interface'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'logical_interface_vlan_tag' in new_obj_dict:
            new_props['logical_interface_vlan_tag'] = new_obj_dict['logical_interface_vlan_tag']
        if 'logical_interface_type' in new_obj_dict:
            new_props['logical_interface_type'] = new_obj_dict['logical_interface_type']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'logical_interface', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'logical_interface', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_logical_interface_update

    def _cassandra_logical_interface_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:logical_interface:'
            col_fin = 'children:logical_interface;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:logical_interface:'
            col_fin = 'backref:logical_interface;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('logical_interface', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_logical_interface_list

    def _cassandra_logical_interface_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'logical_interface', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'logical_interface', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('logical_interface', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_logical_interface_delete

    def _cassandra_routing_instance_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_routing_instance_alloc

    def _cassandra_routing_instance_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('routing_instance')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'routing_instance', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('service_chain_information', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'service_chain_information', field)

        field = obj_dict.get('routing_instance_is_default', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'routing_instance_is_default', field)

        field = obj_dict.get('static_route_entries', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'static_route_entries', field)

        field = obj_dict.get('default_ce_protocol', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'default_ce_protocol', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('routing_instance_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('routing_instance', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'routing_instance', obj_ids['uuid'], 'routing_instance', ref_uuid, ref_data)
        refs = obj_dict.get('route_target_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('route_target', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'routing_instance', obj_ids['uuid'], 'route_target', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('routing_instance', fq_name_cols)

        return (True, '')
    #end _cassandra_routing_instance_create

    def _cassandra_routing_instance_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names

            if 'bgp_routers' in result:
                # sort children; TODO do this based on schema
                sorted_children = sorted(result['bgp_routers'], key = itemgetter('tstamp'))
                # re-write result's children without timestamp
                result['bgp_routers'] = sorted_children
                [child.pop('tstamp') for child in result['bgp_routers']]

            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_routing_instance_read

    def _cassandra_routing_instance_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'routing_instance_refs' in new_obj_dict:
            new_ref_infos['routing_instance'] = {}
            new_refs = new_obj_dict['routing_instance_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('routing_instance', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['routing_instance'][new_ref_uuid] = new_ref_data

        if 'route_target_refs' in new_obj_dict:
            new_ref_infos['route_target'] = {}
            new_refs = new_obj_dict['route_target_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('route_target', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['route_target'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'service_chain_information' in new_obj_dict:
            new_props['service_chain_information'] = new_obj_dict['service_chain_information']
        if 'routing_instance_is_default' in new_obj_dict:
            new_props['routing_instance_is_default'] = new_obj_dict['routing_instance_is_default']
        if 'static_route_entries' in new_obj_dict:
            new_props['static_route_entries'] = new_obj_dict['static_route_entries']
        if 'default_ce_protocol' in new_obj_dict:
            new_props['default_ce_protocol'] = new_obj_dict['default_ce_protocol']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'routing_instance', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'routing_instance', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_routing_instance_update

    def _cassandra_routing_instance_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:routing_instance:'
            col_fin = 'children:routing_instance;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:routing_instance:'
            col_fin = 'backref:routing_instance;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('routing_instance', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_routing_instance_list

    def _cassandra_routing_instance_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'routing_instance', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'routing_instance', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('routing_instance', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_routing_instance_delete

    def _cassandra_virtual_machine_interface_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_virtual_machine_interface_alloc

    def _cassandra_virtual_machine_interface_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('virtual_machine_interface')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'virtual_machine_interface', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('virtual_machine_interface_mac_addresses', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_machine_interface_mac_addresses', field)

        field = obj_dict.get('virtual_machine_interface_dhcp_option_list', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_machine_interface_dhcp_option_list', field)

        field = obj_dict.get('virtual_machine_interface_host_routes', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_machine_interface_host_routes', field)

        field = obj_dict.get('virtual_machine_interface_allowed_address_pairs', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_machine_interface_allowed_address_pairs', field)

        field = obj_dict.get('vrf_assign_table', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'vrf_assign_table', field)

        field = obj_dict.get('virtual_machine_interface_device_owner', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_machine_interface_device_owner', field)

        field = obj_dict.get('virtual_machine_interface_properties', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'virtual_machine_interface_properties', field)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('security_group_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('security_group', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_machine_interface', obj_ids['uuid'], 'security_group', ref_uuid, ref_data)
        refs = obj_dict.get('virtual_machine_interface_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_machine_interface', obj_ids['uuid'], 'virtual_machine_interface', ref_uuid, ref_data)
        refs = obj_dict.get('virtual_machine_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_machine', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_machine_interface', obj_ids['uuid'], 'virtual_machine', ref_uuid, ref_data)
        refs = obj_dict.get('virtual_network_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_network', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_machine_interface', obj_ids['uuid'], 'virtual_network', ref_uuid, ref_data)
        refs = obj_dict.get('routing_instance_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('routing_instance', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_machine_interface', obj_ids['uuid'], 'routing_instance', ref_uuid, ref_data)
        refs = obj_dict.get('interface_route_table_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('interface_route_table', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'virtual_machine_interface', obj_ids['uuid'], 'interface_route_table', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('virtual_machine_interface', fq_name_cols)

        return (True, '')
    #end _cassandra_virtual_machine_interface_create

    def _cassandra_virtual_machine_interface_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_virtual_machine_interface_read

    def _cassandra_virtual_machine_interface_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'security_group_refs' in new_obj_dict:
            new_ref_infos['security_group'] = {}
            new_refs = new_obj_dict['security_group_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('security_group', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['security_group'][new_ref_uuid] = new_ref_data

        if 'virtual_machine_interface_refs' in new_obj_dict:
            new_ref_infos['virtual_machine_interface'] = {}
            new_refs = new_obj_dict['virtual_machine_interface_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_machine_interface'][new_ref_uuid] = new_ref_data

        if 'virtual_machine_refs' in new_obj_dict:
            new_ref_infos['virtual_machine'] = {}
            new_refs = new_obj_dict['virtual_machine_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_machine', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_machine'][new_ref_uuid] = new_ref_data

        if 'virtual_network_refs' in new_obj_dict:
            new_ref_infos['virtual_network'] = {}
            new_refs = new_obj_dict['virtual_network_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_network', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_network'][new_ref_uuid] = new_ref_data

        if 'routing_instance_refs' in new_obj_dict:
            new_ref_infos['routing_instance'] = {}
            new_refs = new_obj_dict['routing_instance_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('routing_instance', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['routing_instance'][new_ref_uuid] = new_ref_data

        if 'interface_route_table_refs' in new_obj_dict:
            new_ref_infos['interface_route_table'] = {}
            new_refs = new_obj_dict['interface_route_table_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('interface_route_table', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['interface_route_table'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'virtual_machine_interface_mac_addresses' in new_obj_dict:
            new_props['virtual_machine_interface_mac_addresses'] = new_obj_dict['virtual_machine_interface_mac_addresses']
        if 'virtual_machine_interface_dhcp_option_list' in new_obj_dict:
            new_props['virtual_machine_interface_dhcp_option_list'] = new_obj_dict['virtual_machine_interface_dhcp_option_list']
        if 'virtual_machine_interface_host_routes' in new_obj_dict:
            new_props['virtual_machine_interface_host_routes'] = new_obj_dict['virtual_machine_interface_host_routes']
        if 'virtual_machine_interface_allowed_address_pairs' in new_obj_dict:
            new_props['virtual_machine_interface_allowed_address_pairs'] = new_obj_dict['virtual_machine_interface_allowed_address_pairs']
        if 'vrf_assign_table' in new_obj_dict:
            new_props['vrf_assign_table'] = new_obj_dict['vrf_assign_table']
        if 'virtual_machine_interface_device_owner' in new_obj_dict:
            new_props['virtual_machine_interface_device_owner'] = new_obj_dict['virtual_machine_interface_device_owner']
        if 'virtual_machine_interface_properties' in new_obj_dict:
            new_props['virtual_machine_interface_properties'] = new_obj_dict['virtual_machine_interface_properties']
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'virtual_machine_interface', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'virtual_machine_interface', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_virtual_machine_interface_update

    def _cassandra_virtual_machine_interface_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:virtual_machine_interface:'
            col_fin = 'children:virtual_machine_interface;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:virtual_machine_interface:'
            col_fin = 'backref:virtual_machine_interface;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('virtual_machine_interface', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_virtual_machine_interface_list

    def _cassandra_virtual_machine_interface_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'virtual_machine_interface', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'virtual_machine_interface', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('virtual_machine_interface', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_virtual_machine_interface_delete

    def _cassandra_logical_router_alloc(self, fq_name):
        return (True, '')
    #end _cassandra_logical_router_alloc

    def _cassandra_logical_router_create(self, obj_ids, obj_dict):
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        bch = self._obj_uuid_cf.batch()

        obj_cols = {}
        obj_cols['fq_name'] = json.dumps(obj_dict['fq_name'])
        obj_cols['type'] = json.dumps('logical_router')
        if 'parent_type' in obj_dict:
            # non config-root child
            parent_type = obj_dict['parent_type']
            parent_method_type = parent_type.replace('-', '_')
            parent_fq_name = obj_dict['fq_name'][:-1]
            obj_cols['parent_type'] = json.dumps(parent_type)
            parent_uuid = self.fq_name_to_uuid(parent_method_type, parent_fq_name)
            self._create_child(bch, parent_method_type, parent_uuid, 'logical_router', obj_ids['uuid'])

        # Properties
        field = obj_dict.get('id_perms', None)
        if field is not None:
            field['created'] = datetime.datetime.utcnow().isoformat()
            field['last_modified'] = field['created']
            self._create_prop(bch, obj_ids['uuid'], 'id_perms', field)

        field = obj_dict.get('display_name', None)
        if field is not None:
            self._create_prop(bch, obj_ids['uuid'], 'display_name', field)


        # References
        refs = obj_dict.get('virtual_machine_interface_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'logical_router', obj_ids['uuid'], 'virtual_machine_interface', ref_uuid, ref_data)
        refs = obj_dict.get('route_target_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('route_target', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'logical_router', obj_ids['uuid'], 'route_target', ref_uuid, ref_data)
        refs = obj_dict.get('virtual_network_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('virtual_network', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'logical_router', obj_ids['uuid'], 'virtual_network', ref_uuid, ref_data)
        refs = obj_dict.get('service_instance_refs', [])
        for ref in refs:
            ref_uuid = self.fq_name_to_uuid('service_instance', ref['to'])
            ref_attr = ref.get('attr', None)
            ref_data = {'attr': ref_attr, 'is_weakref': False}
            self._create_ref(bch, 'logical_router', obj_ids['uuid'], 'service_instance', ref_uuid, ref_data)

        bch.insert(obj_ids['uuid'], obj_cols)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(obj_dict['fq_name'])
        fq_name_cols = {utils.encode_string(fq_name_str) + ':' + obj_ids['uuid']: json.dumps(None)}
        self._obj_fq_name_cf.insert('logical_router', fq_name_cols)

        return (True, '')
    #end _cassandra_logical_router_create

    def _cassandra_logical_router_read(self, obj_uuids, field_names = None):
        # if field_names = None, all fields will be read/returned

        obj_uuid_cf = self._obj_uuid_cf
        obj_rows = obj_uuid_cf.multiget(obj_uuids,
                                   column_count = 10000000,
                                   include_timestamp = True)

        if (len(obj_uuids) == 1) and not obj_rows:
            raise cfgm_common.exceptions.NoIdError(obj_uuids[0])

        results = []
        for row_key in obj_rows:
            obj_uuid = row_key
            obj_cols = obj_rows[obj_uuid]
            result = {}
            result['uuid'] = obj_uuid
            result['fq_name'] = json.loads(obj_cols['fq_name'][0])
            for col_name in obj_cols.keys():
                if self._re_match_parent.match(col_name):
                    # non config-root child
                    (_, _, parent_uuid) = col_name.split(':')
                    parent_type = json.loads(obj_cols['parent_type'][0])
                    result['parent_type'] = parent_type
                    try:
                        result['parent_uuid'] = parent_uuid
                        result['parent_href'] = self._generate_url(parent_type, parent_uuid)
                    except cfgm_common.exceptions.NoIdError:
                        err_msg = 'Unknown uuid for parent ' + result['fq_name'][-2]
                        return (False, err_msg)

                # TODO use compiled RE
                if self._re_match_prop.match(col_name):
                    (_, prop_name) = col_name.split(':')
                    result[prop_name] = json.loads(obj_cols[col_name][0])

                # TODO use compiled RE
                if self._re_match_children.match(col_name):
                    (_, child_type, child_uuid) = col_name.split(':')
                    if field_names and '%ss' %(child_type) not in field_names:
                        continue

                    child_tstamp = obj_cols[col_name][1]
                    try:
                        self._read_child(result, obj_uuid, child_type, child_uuid, child_tstamp)
                    except cfgm_common.exceptions.NoIdError:
                        continue

                # TODO use compiled RE
                if self._re_match_ref.match(col_name):
                    (_, ref_type, ref_uuid) = col_name.split(':')
                    self._read_ref(result, obj_uuid, ref_type, ref_uuid, obj_cols[col_name][0])

                if self._re_match_backref.match(col_name):
                    (_, back_ref_type, back_ref_uuid) = col_name.split(':')
                    if field_names and '%s_back_refs' %(back_ref_type) not in field_names:
                        continue

                    try:
                        self._read_back_ref(result, obj_uuid, back_ref_type, back_ref_uuid,
                                            obj_cols[col_name][0])
                    except cfgm_common.exceptions.NoIdError:
                        continue

            # for all column names


            results.append(result)
        # end for all rows

        return (True, results)
    #end _cassandra_logical_router_read

    def _cassandra_logical_router_update(self, obj_uuid, new_obj_dict):
        # Grab ref-uuids and properties in new version
        new_ref_infos = {}

        if 'virtual_machine_interface_refs' in new_obj_dict:
            new_ref_infos['virtual_machine_interface'] = {}
            new_refs = new_obj_dict['virtual_machine_interface_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_machine_interface', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_machine_interface'][new_ref_uuid] = new_ref_data

        if 'route_target_refs' in new_obj_dict:
            new_ref_infos['route_target'] = {}
            new_refs = new_obj_dict['route_target_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('route_target', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['route_target'][new_ref_uuid] = new_ref_data

        if 'virtual_network_refs' in new_obj_dict:
            new_ref_infos['virtual_network'] = {}
            new_refs = new_obj_dict['virtual_network_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('virtual_network', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['virtual_network'][new_ref_uuid] = new_ref_data

        if 'service_instance_refs' in new_obj_dict:
            new_ref_infos['service_instance'] = {}
            new_refs = new_obj_dict['service_instance_refs']
            if new_refs:
                for new_ref in new_refs:
                    new_ref_uuid = self.fq_name_to_uuid('service_instance', new_ref['to'])
                    new_ref_attr = new_ref.get('attr', None)
                    new_ref_data = {'attr': new_ref_attr, 'is_weakref': False}
                    new_ref_infos['service_instance'][new_ref_uuid] = new_ref_data

        new_props = {}
        if 'id_perms' in new_obj_dict:
            new_props['id_perms'] = new_obj_dict['id_perms']
        if 'display_name' in new_obj_dict:
            new_props['display_name'] = new_obj_dict['display_name']
        # Gather column values for obj and updates to backrefs
        # in a batch and write it at the end
        obj_uuid_cf = self._obj_uuid_cf
        obj_cols_iter = obj_uuid_cf.xget(obj_uuid)
        # TODO optimize this (converts tuple to dict)
        obj_cols = {}
        for col_info in obj_cols_iter:
            obj_cols[col_info[0]] = col_info[1]

        bch = obj_uuid_cf.batch()
        for col_name in obj_cols.keys():
            # TODO use compiled RE
            if re.match('prop:', col_name):
                (_, prop_name) = col_name.split(':')
                if prop_name == 'id_perms':
                    # id-perms always has to be updated for last-mod timestamp
                    # get it from request dict(or from db if not in request dict)
                    new_id_perms = new_obj_dict.get(prop_name, json.loads(obj_cols[col_name]))
                    self.update_last_modified(bch, obj_uuid, new_id_perms)
                elif prop_name in new_obj_dict:
                    self._update_prop(bch, obj_uuid, prop_name, new_props)

            # TODO use compiled RE
            if re.match('ref:', col_name):
                (_, ref_type, ref_uuid) = col_name.split(':')
                self._update_ref(bch, 'logical_router', obj_uuid, ref_type, ref_uuid, new_ref_infos)
        # for all column names

        # create new refs
        for ref_type in new_ref_infos.keys():
            for ref_uuid in new_ref_infos[ref_type].keys():
                ref_data = new_ref_infos[ref_type][ref_uuid]
                self._create_ref(bch, 'logical_router', obj_uuid, ref_type, ref_uuid, ref_data)

        # create new props
        for prop_name in new_props.keys():
            self._create_prop(bch, obj_uuid, prop_name, new_props[prop_name])

        bch.send()

        return (True, '')
    #end _cassandra_logical_router_update

    def _cassandra_logical_router_list(self, parent_uuids=None, back_ref_uuids=None,
                           obj_uuids=None, count=False):
        children_fq_names_uuids = []
        if parent_uuids:
            # go from parent to child
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'children:logical_router:'
            col_fin = 'children:logical_router;'
            try:
                obj_rows = obj_uuid_cf.multiget(parent_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            unsorted_fq_names_uuids = []
            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val_ts in cols.items():
                    child_uuid = col_name.split(':')[2]
                    if obj_uuids and child_uuid not in obj_uuids:
                        continue
                    child_fq_name = self.uuid_to_fq_name(child_uuid)
                    tstamp = col_val_ts[1]
                    unsorted_fq_names_uuids.append({'fq_name': child_fq_name, 'uuid': child_uuid,
                                                    'tstamp': tstamp})

            # sort children; TODO do this based on schema
            sorted_children = sorted(unsorted_fq_names_uuids, key = itemgetter('tstamp'))
            # re-write result's children without timestamp
            children_fq_names_uuids = [(child['fq_name'], child['uuid']) for child in sorted_children]
        if back_ref_uuids:
            # go from anchor to backrefs
            obj_uuid_cf = self._obj_uuid_cf
            col_start = 'backref:logical_router:'
            col_fin = 'backref:logical_router;'
            try:
                obj_rows = obj_uuid_cf.multiget(back_ref_uuids,
                                       column_start = col_start,
                                       column_finish = col_fin,
                                       column_count = 10000000,
                                       include_timestamp = True)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                child_count = sum([len(obj_rows[row]) for row in obj_rows])
                return (True, child_count)

            for row in obj_rows:
                cols = obj_rows[row]
                for col_name, col_val in cols.items():
                    col_name_arr = col_name.split(':')
                    fq_name = col_name_arr[:-1]
                    obj_uuid = col_name_arr[-1]
                    if obj_uuids and obj_uuid not in obj_uuids:
                        continue
                    children_fq_names_uuids.append((fq_name, obj_uuid))
        if not parent_uuids and not back_ref_uuids:
            # grab all resources of this type
            obj_fq_name_cf = self._obj_fq_name_cf
            try:
                cols = obj_fq_name_cf.get('logical_router', column_count = 10000000)
            except pycassa.NotFoundException:
                if count:
                    return (True, 0)
                else:
                    return (True, children_fq_names_uuids)

            if count:
                return (True, len(cols.items()))

            for col_name, col_val in cols.items():
                col_name_arr = utils.decode_string(col_name).split(':')
                fq_name = col_name_arr[:-1]
                obj_uuid = col_name_arr[-1]
                if obj_uuids and obj_uuid not in obj_uuids:
                    continue
                children_fq_names_uuids.append((fq_name, obj_uuid))

        return (True, children_fq_names_uuids)
    #end _cassandra_logical_router_list

    def _cassandra_logical_router_delete(self, obj_uuid):
        obj_uuid_cf = self._obj_uuid_cf
        fq_name = json.loads(obj_uuid_cf.get(obj_uuid, columns = ['fq_name'])['fq_name'])
        bch = obj_uuid_cf.batch()

        # unlink from parent
        col_start = 'parent:'
        col_fin = 'parent;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, parent_type, parent_uuid) = col_name.split(':')
            self._delete_child(bch, parent_type, parent_uuid, 'logical_router', obj_uuid)

        # remove refs
        col_start = 'ref:'
        col_fin = 'ref;'
        col_name_iter = obj_uuid_cf.xget(obj_uuid, column_start = col_start, column_finish = col_fin)
        for (col_name, col_val) in col_name_iter:
            (_, ref_type, ref_uuid) = col_name.split(':')
            self._delete_ref(bch, 'logical_router', obj_uuid, ref_type, ref_uuid)

        bch.remove(obj_uuid)
        bch.send()

        # Update fqname table
        fq_name_str = ':'.join(fq_name)
        fq_name_col = utils.encode_string(fq_name_str) + ':' + obj_uuid
        self._obj_fq_name_cf.remove('logical_router', columns = [fq_name_col])


        return (True, '')
    #end _cassandra_logical_router_delete

