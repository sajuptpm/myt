
# AUTO-GENERATED file from IFMapApiGenerator. Do Not Edit!

import re
import json
import cStringIO
from lxml import etree

from xml.sax.saxutils import escape
from cfgm_common.ifmap.client import client, namespaces
from cfgm_common.ifmap.request import NewSessionRequest, RenewSessionRequest
from cfgm_common.ifmap.request import EndSessionRequest, PublishRequest
from cfgm_common.ifmap.request import SearchRequest, SubscribeRequest, PurgeRequest, PollRequest
from cfgm_common.ifmap.id import IPAddress, MACAddress, Device, AccessRequest, Identity, CustomIdentity
from cfgm_common.ifmap.operations import PublishUpdateOperation, PublishNotifyOperation
from cfgm_common.ifmap.operations import PublishDeleteOperation, SubscribeUpdateOperation, SubscribeDeleteOperation
from cfgm_common.ifmap.util import attr, link_ids
from cfgm_common.ifmap.response import Response, newSessionResult
from cfgm_common.ifmap.metadata import Metadata

import cfgm_common.imid
import cfgm_common.exceptions
from resource_xsd import *

class VncIfmapClientGen(object):
    def __init__(self):
        self._parent_metas = {}
        self._parent_metas['domain'] = {}
        self._parent_metas['domain']['project'] = 'domain-project'
        self._parent_metas['domain']['namespace'] = 'domain-namespace'
        self._parent_metas['domain']['service-template'] = 'domain-service-template'
        self._parent_metas['domain']['virtual-DNS'] = 'domain-virtual-DNS'
        self._parent_metas['global-vrouter-config'] = {}
        self._parent_metas['instance-ip'] = {}
        self._parent_metas['network-policy'] = {}
        self._parent_metas['virtual-DNS-record'] = {}
        self._parent_metas['route-target'] = {}
        self._parent_metas['floating-ip'] = {}
        self._parent_metas['floating-ip-pool'] = {}
        self._parent_metas['floating-ip-pool']['floating-ip'] = 'floating-ip-pool-floating-ip'
        self._parent_metas['physical-router'] = {}
        self._parent_metas['physical-router']['physical-interface'] = 'physical-router-physical-interface'
        self._parent_metas['physical-router']['logical-interface'] = 'physical-router-logical-interface'
        self._parent_metas['bgp-router'] = {}
        self._parent_metas['virtual-router'] = {}
        self._parent_metas['config-root'] = {}
        self._parent_metas['config-root']['global-system-config'] = 'config-root-global-system-config'
        self._parent_metas['config-root']['domain'] = 'config-root-domain'
        self._parent_metas['subnet'] = {}
        self._parent_metas['global-system-config'] = {}
        self._parent_metas['global-system-config']['global-vrouter-config'] = 'global-system-config-global-vrouter-config'
        self._parent_metas['global-system-config']['physical-router'] = 'global-system-config-physical-router'
        self._parent_metas['global-system-config']['virtual-router'] = 'global-system-config-virtual-router'
        self._parent_metas['loadbalancer-member'] = {}
        self._parent_metas['service-instance'] = {}
        self._parent_metas['namespace'] = {}
        self._parent_metas['route-table'] = {}
        self._parent_metas['physical-interface'] = {}
        self._parent_metas['physical-interface']['logical-interface'] = 'physical-interface-logical-interface'
        self._parent_metas['access-control-list'] = {}
        self._parent_metas['virtual-DNS'] = {}
        self._parent_metas['virtual-DNS']['virtual-DNS-record'] = 'virtual-DNS-virtual-DNS-record'
        self._parent_metas['customer-attachment'] = {}
        self._parent_metas['loadbalancer-pool'] = {}
        self._parent_metas['loadbalancer-pool']['loadbalancer-member'] = 'loadbalancer-pool-loadbalancer-member'
        self._parent_metas['virtual-machine'] = {}
        self._parent_metas['virtual-machine']['virtual-machine-interface'] = 'virtual-machine-virtual-machine-interface'
        self._parent_metas['interface-route-table'] = {}
        self._parent_metas['service-template'] = {}
        self._parent_metas['virtual-ip'] = {}
        self._parent_metas['security-group'] = {}
        self._parent_metas['security-group']['access-control-list'] = 'security-group-access-control-list'
        self._parent_metas['provider-attachment'] = {}
        self._parent_metas['network-ipam'] = {}
        self._parent_metas['loadbalancer-healthmonitor'] = {}
        self._parent_metas['virtual-network'] = {}
        self._parent_metas['virtual-network']['access-control-list'] = 'virtual-network-access-control-list'
        self._parent_metas['virtual-network']['floating-ip-pool'] = 'virtual-network-floating-ip-pool'
        self._parent_metas['virtual-network']['routing-instance'] = 'virtual-network-routing-instance'
        self._parent_metas['project'] = {}
        self._parent_metas['project']['security-group'] = 'project-security-group'
        self._parent_metas['project']['virtual-network'] = 'project-virtual-network'
        self._parent_metas['project']['network-ipam'] = 'project-network-ipam'
        self._parent_metas['project']['network-policy'] = 'project-network-policy'
        self._parent_metas['project']['virtual-machine-interface'] = 'project-virtual-machine-interface'
        self._parent_metas['project']['service-instance'] = 'project-service-instance'
        self._parent_metas['project']['route-table'] = 'project-route-table'
        self._parent_metas['project']['interface-route-table'] = 'project-interface-route-table'
        self._parent_metas['project']['logical-router'] = 'project-logical-router'
        self._parent_metas['project']['loadbalancer-pool'] = 'project-loadbalancer-pool'
        self._parent_metas['project']['loadbalancer-healthmonitor'] = 'project-loadbalancer-healthmonitor'
        self._parent_metas['project']['virtual-ip'] = 'project-virtual-ip'
        self._parent_metas['logical-interface'] = {}
        self._parent_metas['routing-instance'] = {}
        self._parent_metas['routing-instance']['bgp-router'] = 'instance-bgp-router'
        self._parent_metas['virtual-machine-interface'] = {}
        self._parent_metas['logical-router'] = {}
    #end __init__

    def _ifmap_domain_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.domain_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_domain_alloc

    def _ifmap_domain_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('domain_limits', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['domain_limits']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                DomainLimitsType(**field).exportChildren(buf, level = 1, name_ = 'domain-limits', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'domain-limits', pretty_print = False)
            domain_limits_xml = buf.getvalue()
            buf.close()
            meta = Metadata('domain-limits' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = domain_limits_xml)

            if (existing_metas and 'domain-limits' in existing_metas and
                str(existing_metas['domain-limits'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('api_access_list', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['api_access_list']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                ApiAccessListType(**field).exportChildren(buf, level = 1, name_ = 'api-access-list', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'api-access-list', pretty_print = False)
            api_access_list_xml = buf.getvalue()
            buf.close()
            meta = Metadata('api-access-list' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = api_access_list_xml)

            if (existing_metas and 'api-access-list' in existing_metas and
                str(existing_metas['api-access-list'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('project_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'project'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                domain_project_xml = ''
                meta = Metadata('domain-project' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = domain_project_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('namespace_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'namespace'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                domain_namespace_xml = ''
                meta = Metadata('domain-namespace' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = domain_namespace_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('service_template_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'service-template'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                domain_service_template_xml = ''
                meta = Metadata('domain-service-template' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = domain_service_template_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_DNS_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-DNS'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                domain_virtual_DNS_xml = ''
                meta = Metadata('domain-virtual-DNS' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = domain_virtual_DNS_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_domain_set

    def _ifmap_domain_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['domain']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_domain_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_domain_create


    def _ifmap_domain_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_domain_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'domain'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:domain-limits'):
            result_elems = metas['contrail:domain-limits']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = DomainLimitsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['domain_limits'] = obj_val

        if metas.has_key('contrail:api-access-list'):
            result_elems = metas['contrail:api-access-list']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = ApiAccessListType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['api_access_list'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:domain-project'):
            result['projects'] = []
            result_elems = metas['contrail:domain-project']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('project', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['projects'].append(has_info)


        if metas.has_key('contrail:domain-namespace'):
            result['namespaces'] = []
            result_elems = metas['contrail:domain-namespace']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('namespace', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['namespaces'].append(has_info)


        if metas.has_key('contrail:domain-service-template'):
            result['service_templates'] = []
            result_elems = metas['contrail:domain-service-template']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('service_template', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['service_templates'].append(has_info)


        if metas.has_key('contrail:domain-virtual-DNS'):
            result['virtual_DNSs'] = []
            result_elems = metas['contrail:domain-virtual-DNS']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('virtual_DNS', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['virtual_DNSs'].append(has_info)


        return (True, result)
    #end _ifmap_domain_read

    def _ifmap_domain_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'domain-limits', u'api-access-list', u'id-perms', u'display-name', u'domain-project', u'domain-namespace', u'domain-service-template', u'domain-virtual-DNS']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('domain_limits'):
                req_metas.append('domain-limits')
            if field_names.has_key('api_access_list'):
                req_metas.append('api-access-list')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('domain_project'):
                req_metas.append('domain-project')
            if field_names.has_key('domain_namespace'):
                req_metas.append('domain-namespace')
            if field_names.has_key('domain_service_template'):
                req_metas.append('domain-service-template')
            if field_names.has_key('domain_virtual_DNS'):
                req_metas.append('domain-virtual-DNS')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_domain_read_to_meta_index

    def _ifmap_domain_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_domain_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('domain-limits' in existing_metas) and ('domain_limits' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:domain-limits')

        if ('api-access-list' in existing_metas) and ('api_access_list' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:api-access-list')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        (ok, result) = self._ifmap_domain_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_domain_update

    def _ifmap_domain_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['domain']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_domain_list

    def _ifmap_domain_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_domain_delete

    def _ifmap_global_vrouter_config_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.global_vrouter_config_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_global_vrouter_config_alloc

    def _ifmap_global_vrouter_config_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('linklocal_services', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['linklocal_services']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                LinklocalServicesTypes(**field).exportChildren(buf, level = 1, name_ = 'linklocal-services', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'linklocal-services', pretty_print = False)
            linklocal_services_xml = buf.getvalue()
            buf.close()
            meta = Metadata('linklocal-services' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = linklocal_services_xml)

            if (existing_metas and 'linklocal-services' in existing_metas and
                str(existing_metas['linklocal-services'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('encapsulation_priorities', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['encapsulation_priorities']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                EncapsulationPrioritiesType(**field).exportChildren(buf, level = 1, name_ = 'encapsulation-priorities', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'encapsulation-priorities', pretty_print = False)
            encapsulation_priorities_xml = buf.getvalue()
            buf.close()
            meta = Metadata('encapsulation-priorities' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = encapsulation_priorities_xml)

            if (existing_metas and 'encapsulation-priorities' in existing_metas and
                str(existing_metas['encapsulation-priorities'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('vxlan_network_identifier_mode', None)
        if field is not None:
            norm_str = escape(str(obj_dict['vxlan_network_identifier_mode']))
            meta = Metadata('vxlan-network-identifier-mode', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'vxlan-network-identifier-mode' in existing_metas and
                str(existing_metas['vxlan-network-identifier-mode'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_global_vrouter_config_set

    def _ifmap_global_vrouter_config_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['global-vrouter-config']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_global_vrouter_config_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_global_vrouter_config_create


    def _ifmap_global_vrouter_config_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_global_vrouter_config_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'global-vrouter-config'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:linklocal-services'):
            result_elems = metas['contrail:linklocal-services']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = LinklocalServicesTypes()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['linklocal_services'] = obj_val

        if metas.has_key('contrail:encapsulation-priorities'):
            result_elems = metas['contrail:encapsulation-priorities']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = EncapsulationPrioritiesType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['encapsulation_priorities'] = obj_val

        if metas.has_key('contrail:vxlan-network-identifier-mode'):
            result_elems = metas['contrail:vxlan-network-identifier-mode']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['vxlan_network_identifier_mode'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_global_vrouter_config_read

    def _ifmap_global_vrouter_config_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'linklocal-services', u'encapsulation-priorities', u'vxlan-network-identifier-mode', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('linklocal_services'):
                req_metas.append('linklocal-services')
            if field_names.has_key('encapsulation_priorities'):
                req_metas.append('encapsulation-priorities')
            if field_names.has_key('vxlan_network_identifier_mode'):
                req_metas.append('vxlan-network-identifier-mode')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_global-vrouter-config_read_to_meta_index

    def _ifmap_global_vrouter_config_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_global_vrouter_config_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('linklocal-services' in existing_metas) and ('linklocal_services' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:linklocal-services')

        if ('encapsulation-priorities' in existing_metas) and ('encapsulation_priorities' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:encapsulation-priorities')

        if ('vxlan-network-identifier-mode' in existing_metas) and ('vxlan_network_identifier_mode' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:vxlan-network-identifier-mode')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        (ok, result) = self._ifmap_global_vrouter_config_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_global_vrouter_config_update

    def _ifmap_global_vrouter_config_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['global-vrouter-config']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_global_vrouter_config_list

    def _ifmap_global_vrouter_config_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_global_vrouter_config_delete

    def _ifmap_instance_ip_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.instance_ip_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_instance_ip_alloc

    def _ifmap_instance_ip_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('instance_ip_address', None)
        if field is not None:
            norm_str = escape(str(obj_dict['instance_ip_address']))
            meta = Metadata('instance-ip-address', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'instance-ip-address' in existing_metas and
                str(existing_metas['instance-ip-address'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('instance_ip_family', None)
        if field is not None:
            norm_str = escape(str(obj_dict['instance_ip_family']))
            meta = Metadata('instance-ip-family', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'instance-ip-family' in existing_metas and
                str(existing_metas['instance-ip-family'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('instance_ip_mode', None)
        if field is not None:
            norm_str = escape(str(obj_dict['instance_ip_mode']))
            meta = Metadata('instance-ip-mode', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'instance-ip-mode' in existing_metas and
                str(existing_metas['instance-ip-mode'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('subnet_uuid', None)
        if field is not None:
            norm_str = escape(str(obj_dict['subnet_uuid']))
            meta = Metadata('subnet-uuid', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'subnet-uuid' in existing_metas and
                str(existing_metas['subnet-uuid'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_network_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-network'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                instance_ip_virtual_network_xml = ''
                meta = Metadata('instance-ip-virtual-network' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = instance_ip_virtual_network_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                instance_ip_virtual_machine_interface_xml = ''
                meta = Metadata('instance-ip-virtual-machine-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = instance_ip_virtual_machine_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_instance_ip_set

    def _ifmap_instance_ip_create(self, obj_ids, obj_dict):
        (ok, result) = self._ifmap_instance_ip_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_instance_ip_create


    def _ifmap_instance_ip_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_instance_ip_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'instance-ip'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:instance-ip-virtual-network'):
            result['virtual_network_refs'] = []
            result_elems = metas['contrail:instance-ip-virtual-network']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_network', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_network_refs'].append(ref_info)


        if metas.has_key('contrail:instance-ip-virtual-machine-interface'):
            result['virtual_machine_interface_refs'] = []
            result_elems = metas['contrail:instance-ip-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine_interface', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_interface_refs'].append(ref_info)


        if metas.has_key('contrail:instance-ip-address'):
            result_elems = metas['contrail:instance-ip-address']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['instance_ip_address'] = obj_val

        if metas.has_key('contrail:instance-ip-family'):
            result_elems = metas['contrail:instance-ip-family']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['instance_ip_family'] = obj_val

        if metas.has_key('contrail:instance-ip-mode'):
            result_elems = metas['contrail:instance-ip-mode']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['instance_ip_mode'] = obj_val

        if metas.has_key('contrail:subnet-uuid'):
            result_elems = metas['contrail:subnet-uuid']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['subnet_uuid'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_instance_ip_read

    def _ifmap_instance_ip_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'instance-ip-virtual-network', u'instance-ip-virtual-machine-interface', u'instance-ip-address', u'instance-ip-family', u'instance-ip-mode', u'subnet-uuid', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_network_refs'):
                req_metas.append('instance-ip-virtual-network')
            if field_names.has_key('virtual_machine_interface_refs'):
                req_metas.append('instance-ip-virtual-machine-interface')
            if field_names.has_key('instance_ip_address'):
                req_metas.append('instance-ip-address')
            if field_names.has_key('instance_ip_family'):
                req_metas.append('instance-ip-family')
            if field_names.has_key('instance_ip_mode'):
                req_metas.append('instance-ip-mode')
            if field_names.has_key('subnet_uuid'):
                req_metas.append('subnet-uuid')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_instance-ip_read_to_meta_index

    def _ifmap_instance_ip_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_instance_ip_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('instance-ip-address' in existing_metas) and ('instance_ip_address' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:instance-ip-address')

        if ('instance-ip-family' in existing_metas) and ('instance_ip_family' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:instance-ip-family')

        if ('instance-ip-mode' in existing_metas) and ('instance_ip_mode' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:instance-ip-mode')

        if ('subnet-uuid' in existing_metas) and ('subnet_uuid' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:subnet-uuid')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('instance-ip-virtual-network' in existing_metas):
             for meta_info in existing_metas['instance-ip-virtual-network']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_network_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_network_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-network', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:instance-ip-virtual-network')

        old_refs = []
        if ('instance-ip-virtual-machine-interface' in existing_metas):
             for meta_info in existing_metas['instance-ip-virtual-machine-interface']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_machine_interface_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_machine_interface_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-machine-interface', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:instance-ip-virtual-machine-interface')

        (ok, result) = self._ifmap_instance_ip_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_instance_ip_update

    def _ifmap_instance_ip_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        return (True, children_fq_names)
    #end _ifmap_instance_ip_list

    def _ifmap_instance_ip_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:instance-ip-virtual-network', u'contrail:instance-ip-virtual-machine-interface'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_instance_ip_delete

    def _ifmap_network_policy_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.network_policy_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_network_policy_alloc

    def _ifmap_network_policy_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('network_policy_entries', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['network_policy_entries']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                PolicyEntriesType(**field).exportChildren(buf, level = 1, name_ = 'network-policy-entries', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'network-policy-entries', pretty_print = False)
            network_policy_entries_xml = buf.getvalue()
            buf.close()
            meta = Metadata('network-policy-entries' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = network_policy_entries_xml)

            if (existing_metas and 'network-policy-entries' in existing_metas and
                str(existing_metas['network-policy-entries'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_network_policy_set

    def _ifmap_network_policy_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['network-policy']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_network_policy_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_network_policy_create


    def _ifmap_network_policy_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_network_policy_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'network-policy'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:virtual-network-network-policy'):
            result['virtual_network_back_refs'] = []
            result_elems = metas['contrail:virtual-network-network-policy']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = VirtualNetworkPolicyType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_network', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_network_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:network-policy-entries'):
            result_elems = metas['contrail:network-policy-entries']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = PolicyEntriesType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['network_policy_entries'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_network_policy_read

    def _ifmap_network_policy_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'virtual-network-network-policy', u'network-policy-entries', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_network_back_refs'):
                req_metas.append('virtual-network-network-policy')
            if field_names.has_key('network_policy_entries'):
                req_metas.append('network-policy-entries')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_network-policy_read_to_meta_index

    def _ifmap_network_policy_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_network_policy_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('network-policy-entries' in existing_metas) and ('network_policy_entries' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:network-policy-entries')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        (ok, result) = self._ifmap_network_policy_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_network_policy_update

    def _ifmap_network_policy_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['network-policy']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_network_policy_list

    def _ifmap_network_policy_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_network_policy_delete

    def _ifmap_virtual_DNS_record_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.virtual_DNS_record_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_virtual_DNS_record_alloc

    def _ifmap_virtual_DNS_record_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('virtual_DNS_record_data', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['virtual_DNS_record_data']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                VirtualDnsRecordType(**field).exportChildren(buf, level = 1, name_ = 'virtual-DNS-record-data', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'virtual-DNS-record-data', pretty_print = False)
            virtual_DNS_record_data_xml = buf.getvalue()
            buf.close()
            meta = Metadata('virtual-DNS-record-data' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = virtual_DNS_record_data_xml)

            if (existing_metas and 'virtual-DNS-record-data' in existing_metas and
                str(existing_metas['virtual-DNS-record-data'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_virtual_DNS_record_set

    def _ifmap_virtual_DNS_record_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['virtual-DNS-record']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_virtual_DNS_record_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_virtual_DNS_record_create


    def _ifmap_virtual_DNS_record_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_virtual_DNS_record_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'virtual-DNS-record'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:virtual-DNS-record-data'):
            result_elems = metas['contrail:virtual-DNS-record-data']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = VirtualDnsRecordType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['virtual_DNS_record_data'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_virtual_DNS_record_read

    def _ifmap_virtual_DNS_record_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'virtual-DNS-record-data', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_DNS_record_data'):
                req_metas.append('virtual-DNS-record-data')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_virtual-DNS-record_read_to_meta_index

    def _ifmap_virtual_DNS_record_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_virtual_DNS_record_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('virtual-DNS-record-data' in existing_metas) and ('virtual_DNS_record_data' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-DNS-record-data')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        (ok, result) = self._ifmap_virtual_DNS_record_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_virtual_DNS_record_update

    def _ifmap_virtual_DNS_record_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['virtual-DNS-record']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_virtual_DNS_record_list

    def _ifmap_virtual_DNS_record_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_virtual_DNS_record_delete

    def _ifmap_route_target_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.route_target_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_route_target_alloc

    def _ifmap_route_target_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_route_target_set

    def _ifmap_route_target_create(self, obj_ids, obj_dict):
        (ok, result) = self._ifmap_route_target_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_route_target_create


    def _ifmap_route_target_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_route_target_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'route-target'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:logical-router-target'):
            result['logical_router_back_refs'] = []
            result_elems = metas['contrail:logical-router-target']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('logical_router', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['logical_router_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:instance-target'):
            result['routing_instance_back_refs'] = []
            result_elems = metas['contrail:instance-target']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = InstanceTargetType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('routing_instance', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['routing_instance_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_route_target_read

    def _ifmap_route_target_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'logical-router-target', u'instance-target', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('logical_router_back_refs'):
                req_metas.append('logical-router-target')
            if field_names.has_key('routing_instance_back_refs'):
                req_metas.append('instance-target')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_route-target_read_to_meta_index

    def _ifmap_route_target_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_route_target_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        (ok, result) = self._ifmap_route_target_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_route_target_update

    def _ifmap_route_target_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        return (True, children_fq_names)
    #end _ifmap_route_target_list

    def _ifmap_route_target_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_route_target_delete

    def _ifmap_floating_ip_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.floating_ip_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_floating_ip_alloc

    def _ifmap_floating_ip_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('floating_ip_address', None)
        if field is not None:
            norm_str = escape(str(obj_dict['floating_ip_address']))
            meta = Metadata('floating-ip-address', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'floating-ip-address' in existing_metas and
                str(existing_metas['floating-ip-address'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('floating_ip_is_virtual_ip', None)
        if field is not None:
            norm_str = escape(str(obj_dict['floating_ip_is_virtual_ip']))
            meta = Metadata('floating-ip-is-virtual-ip', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'floating-ip-is-virtual-ip' in existing_metas and
                str(existing_metas['floating-ip-is-virtual-ip'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('floating_ip_fixed_ip_address', None)
        if field is not None:
            norm_str = escape(str(obj_dict['floating_ip_fixed_ip_address']))
            meta = Metadata('floating-ip-fixed-ip-address', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'floating-ip-fixed-ip-address' in existing_metas and
                str(existing_metas['floating-ip-fixed-ip-address'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('floating_ip_address_family', None)
        if field is not None:
            norm_str = escape(str(obj_dict['floating_ip_address_family']))
            meta = Metadata('floating-ip-address-family', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'floating-ip-address-family' in existing_metas and
                str(existing_metas['floating-ip-address-family'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('project_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'project'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                floating_ip_project_xml = ''
                meta = Metadata('floating-ip-project' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = floating_ip_project_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                floating_ip_virtual_machine_interface_xml = ''
                meta = Metadata('floating-ip-virtual-machine-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = floating_ip_virtual_machine_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_floating_ip_set

    def _ifmap_floating_ip_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['floating-ip']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_floating_ip_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_floating_ip_create


    def _ifmap_floating_ip_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_floating_ip_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'floating-ip'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:floating-ip-project'):
            result['project_refs'] = []
            result_elems = metas['contrail:floating-ip-project']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('project', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['project_refs'].append(ref_info)


        if metas.has_key('contrail:floating-ip-virtual-machine-interface'):
            result['virtual_machine_interface_refs'] = []
            result_elems = metas['contrail:floating-ip-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine_interface', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_interface_refs'].append(ref_info)


        if metas.has_key('contrail:customer-attachment-floating-ip'):
            result['customer_attachment_back_refs'] = []
            result_elems = metas['contrail:customer-attachment-floating-ip']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('customer_attachment', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['customer_attachment_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:floating-ip-address'):
            result_elems = metas['contrail:floating-ip-address']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['floating_ip_address'] = obj_val

        if metas.has_key('contrail:floating-ip-is-virtual-ip'):
            result_elems = metas['contrail:floating-ip-is-virtual-ip']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['floating_ip_is_virtual_ip'] = obj_val

        if metas.has_key('contrail:floating-ip-fixed-ip-address'):
            result_elems = metas['contrail:floating-ip-fixed-ip-address']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['floating_ip_fixed_ip_address'] = obj_val

        if metas.has_key('contrail:floating-ip-address-family'):
            result_elems = metas['contrail:floating-ip-address-family']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['floating_ip_address_family'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_floating_ip_read

    def _ifmap_floating_ip_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'floating-ip-project', u'floating-ip-virtual-machine-interface', u'customer-attachment-floating-ip', u'floating-ip-address', u'floating-ip-is-virtual-ip', u'floating-ip-fixed-ip-address', u'floating-ip-address-family', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('project_refs'):
                req_metas.append('floating-ip-project')
            if field_names.has_key('virtual_machine_interface_refs'):
                req_metas.append('floating-ip-virtual-machine-interface')
            if field_names.has_key('customer_attachment_back_refs'):
                req_metas.append('customer-attachment-floating-ip')
            if field_names.has_key('floating_ip_address'):
                req_metas.append('floating-ip-address')
            if field_names.has_key('floating_ip_is_virtual_ip'):
                req_metas.append('floating-ip-is-virtual-ip')
            if field_names.has_key('floating_ip_fixed_ip_address'):
                req_metas.append('floating-ip-fixed-ip-address')
            if field_names.has_key('floating_ip_address_family'):
                req_metas.append('floating-ip-address-family')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_floating-ip_read_to_meta_index

    def _ifmap_floating_ip_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_floating_ip_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('floating-ip-address' in existing_metas) and ('floating_ip_address' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:floating-ip-address')

        if ('floating-ip-is-virtual-ip' in existing_metas) and ('floating_ip_is_virtual_ip' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:floating-ip-is-virtual-ip')

        if ('floating-ip-fixed-ip-address' in existing_metas) and ('floating_ip_fixed_ip_address' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:floating-ip-fixed-ip-address')

        if ('floating-ip-address-family' in existing_metas) and ('floating_ip_address_family' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:floating-ip-address-family')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('floating-ip-project' in existing_metas):
             for meta_info in existing_metas['floating-ip-project']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('project_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['project_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('project', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:floating-ip-project')

        old_refs = []
        if ('floating-ip-virtual-machine-interface' in existing_metas):
             for meta_info in existing_metas['floating-ip-virtual-machine-interface']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_machine_interface_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_machine_interface_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-machine-interface', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:floating-ip-virtual-machine-interface')

        (ok, result) = self._ifmap_floating_ip_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_floating_ip_update

    def _ifmap_floating_ip_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['floating-ip']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_floating_ip_list

    def _ifmap_floating_ip_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:floating-ip-project', u'contrail:floating-ip-virtual-machine-interface'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_floating_ip_delete

    def _ifmap_floating_ip_pool_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.floating_ip_pool_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_floating_ip_pool_alloc

    def _ifmap_floating_ip_pool_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('floating_ip_pool_prefixes', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['floating_ip_pool_prefixes']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                FloatingIpPoolType(**field).exportChildren(buf, level = 1, name_ = 'floating-ip-pool-prefixes', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'floating-ip-pool-prefixes', pretty_print = False)
            floating_ip_pool_prefixes_xml = buf.getvalue()
            buf.close()
            meta = Metadata('floating-ip-pool-prefixes' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = floating_ip_pool_prefixes_xml)

            if (existing_metas and 'floating-ip-pool-prefixes' in existing_metas and
                str(existing_metas['floating-ip-pool-prefixes'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('floating_ip_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'floating-ip'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                floating_ip_pool_floating_ip_xml = ''
                meta = Metadata('floating-ip-pool-floating-ip' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = floating_ip_pool_floating_ip_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_floating_ip_pool_set

    def _ifmap_floating_ip_pool_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['floating-ip-pool']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_floating_ip_pool_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_floating_ip_pool_create


    def _ifmap_floating_ip_pool_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_floating_ip_pool_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'floating-ip-pool'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:project-floating-ip-pool'):
            result['project_back_refs'] = []
            result_elems = metas['contrail:project-floating-ip-pool']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('project', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['project_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:floating-ip-pool-prefixes'):
            result_elems = metas['contrail:floating-ip-pool-prefixes']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = FloatingIpPoolType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['floating_ip_pool_prefixes'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:floating-ip-pool-floating-ip'):
            result['floating_ips'] = []
            result_elems = metas['contrail:floating-ip-pool-floating-ip']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('floating_ip', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['floating_ips'].append(has_info)


        return (True, result)
    #end _ifmap_floating_ip_pool_read

    def _ifmap_floating_ip_pool_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'project-floating-ip-pool', u'floating-ip-pool-prefixes', u'id-perms', u'display-name', u'floating-ip-pool-floating-ip']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('project_back_refs'):
                req_metas.append('project-floating-ip-pool')
            if field_names.has_key('floating_ip_pool_prefixes'):
                req_metas.append('floating-ip-pool-prefixes')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('floating_ip_pool_floating_ip'):
                req_metas.append('floating-ip-pool-floating-ip')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_floating-ip-pool_read_to_meta_index

    def _ifmap_floating_ip_pool_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_floating_ip_pool_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('floating-ip-pool-prefixes' in existing_metas) and ('floating_ip_pool_prefixes' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:floating-ip-pool-prefixes')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        (ok, result) = self._ifmap_floating_ip_pool_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_floating_ip_pool_update

    def _ifmap_floating_ip_pool_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['floating-ip-pool']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_floating_ip_pool_list

    def _ifmap_floating_ip_pool_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_floating_ip_pool_delete

    def _ifmap_physical_router_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.physical_router_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_physical_router_alloc

    def _ifmap_physical_router_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('physical_router_management_ip', None)
        if field is not None:
            norm_str = escape(str(obj_dict['physical_router_management_ip']))
            meta = Metadata('physical-router-management-ip', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'physical-router-management-ip' in existing_metas and
                str(existing_metas['physical-router-management-ip'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('physical_router_dataplane_ip', None)
        if field is not None:
            norm_str = escape(str(obj_dict['physical_router_dataplane_ip']))
            meta = Metadata('physical-router-dataplane-ip', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'physical-router-dataplane-ip' in existing_metas and
                str(existing_metas['physical-router-dataplane-ip'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('physical_router_vendor_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['physical_router_vendor_name']))
            meta = Metadata('physical-router-vendor-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'physical-router-vendor-name' in existing_metas and
                str(existing_metas['physical-router-vendor-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('physical_router_user_credentials', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['physical_router_user_credentials']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                UserCredentials(**field).exportChildren(buf, level = 1, name_ = 'physical-router-user-credentials', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'physical-router-user-credentials', pretty_print = False)
            physical_router_user_credentials_xml = buf.getvalue()
            buf.close()
            meta = Metadata('physical-router-user-credentials' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = physical_router_user_credentials_xml)

            if (existing_metas and 'physical-router-user-credentials' in existing_metas and
                str(existing_metas['physical-router-user-credentials'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('physical_router_snmp_credentials', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['physical_router_snmp_credentials']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                SNMPCredentials(**field).exportChildren(buf, level = 1, name_ = 'physical-router-snmp-credentials', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'physical-router-snmp-credentials', pretty_print = False)
            physical_router_snmp_credentials_xml = buf.getvalue()
            buf.close()
            meta = Metadata('physical-router-snmp-credentials' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = physical_router_snmp_credentials_xml)

            if (existing_metas and 'physical-router-snmp-credentials' in existing_metas and
                str(existing_metas['physical-router-snmp-credentials'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_router_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-router'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                physical_router_virtual_router_xml = ''
                meta = Metadata('physical-router-virtual-router' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = physical_router_virtual_router_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('bgp_router_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'bgp-router'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                physical_router_bgp_router_xml = ''
                meta = Metadata('physical-router-bgp-router' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = physical_router_bgp_router_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_network_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-network'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                physical_router_virtual_network_xml = ''
                meta = Metadata('physical-router-virtual-network' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = physical_router_virtual_network_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('physical_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'physical-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                physical_router_physical_interface_xml = ''
                meta = Metadata('physical-router-physical-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = physical_router_physical_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('logical_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'logical-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                physical_router_logical_interface_xml = ''
                meta = Metadata('physical-router-logical-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = physical_router_logical_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_physical_router_set

    def _ifmap_physical_router_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['physical-router']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_physical_router_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_physical_router_create


    def _ifmap_physical_router_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_physical_router_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'physical-router'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:physical-router-virtual-router'):
            result['virtual_router_refs'] = []
            result_elems = metas['contrail:physical-router-virtual-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_router', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_router_refs'].append(ref_info)


        if metas.has_key('contrail:physical-router-bgp-router'):
            result['bgp_router_refs'] = []
            result_elems = metas['contrail:physical-router-bgp-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('bgp_router', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['bgp_router_refs'].append(ref_info)


        if metas.has_key('contrail:physical-router-virtual-network'):
            result['virtual_network_refs'] = []
            result_elems = metas['contrail:physical-router-virtual-network']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_network', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_network_refs'].append(ref_info)


        if metas.has_key('contrail:physical-router-management-ip'):
            result_elems = metas['contrail:physical-router-management-ip']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['physical_router_management_ip'] = obj_val

        if metas.has_key('contrail:physical-router-dataplane-ip'):
            result_elems = metas['contrail:physical-router-dataplane-ip']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['physical_router_dataplane_ip'] = obj_val

        if metas.has_key('contrail:physical-router-vendor-name'):
            result_elems = metas['contrail:physical-router-vendor-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['physical_router_vendor_name'] = obj_val

        if metas.has_key('contrail:physical-router-user-credentials'):
            result_elems = metas['contrail:physical-router-user-credentials']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = UserCredentials()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['physical_router_user_credentials'] = obj_val

        if metas.has_key('contrail:physical-router-snmp-credentials'):
            result_elems = metas['contrail:physical-router-snmp-credentials']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = SNMPCredentials()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['physical_router_snmp_credentials'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:physical-router-physical-interface'):
            result['physical_interfaces'] = []
            result_elems = metas['contrail:physical-router-physical-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('physical_interface', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['physical_interfaces'].append(has_info)


        if metas.has_key('contrail:physical-router-logical-interface'):
            result['logical_interfaces'] = []
            result_elems = metas['contrail:physical-router-logical-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('logical_interface', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['logical_interfaces'].append(has_info)


        return (True, result)
    #end _ifmap_physical_router_read

    def _ifmap_physical_router_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'physical-router-virtual-router', u'physical-router-bgp-router', u'physical-router-virtual-network', u'physical-router-management-ip', u'physical-router-dataplane-ip', u'physical-router-vendor-name', u'physical-router-user-credentials', u'physical-router-snmp-credentials', u'id-perms', u'display-name', u'physical-router-physical-interface', u'physical-router-logical-interface']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_router_refs'):
                req_metas.append('physical-router-virtual-router')
            if field_names.has_key('bgp_router_refs'):
                req_metas.append('physical-router-bgp-router')
            if field_names.has_key('virtual_network_refs'):
                req_metas.append('physical-router-virtual-network')
            if field_names.has_key('physical_router_management_ip'):
                req_metas.append('physical-router-management-ip')
            if field_names.has_key('physical_router_dataplane_ip'):
                req_metas.append('physical-router-dataplane-ip')
            if field_names.has_key('physical_router_vendor_name'):
                req_metas.append('physical-router-vendor-name')
            if field_names.has_key('physical_router_user_credentials'):
                req_metas.append('physical-router-user-credentials')
            if field_names.has_key('physical_router_snmp_credentials'):
                req_metas.append('physical-router-snmp-credentials')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('physical_router_physical_interface'):
                req_metas.append('physical-router-physical-interface')
            if field_names.has_key('physical_router_logical_interface'):
                req_metas.append('physical-router-logical-interface')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_physical-router_read_to_meta_index

    def _ifmap_physical_router_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_physical_router_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('physical-router-management-ip' in existing_metas) and ('physical_router_management_ip' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:physical-router-management-ip')

        if ('physical-router-dataplane-ip' in existing_metas) and ('physical_router_dataplane_ip' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:physical-router-dataplane-ip')

        if ('physical-router-vendor-name' in existing_metas) and ('physical_router_vendor_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:physical-router-vendor-name')

        if ('physical-router-user-credentials' in existing_metas) and ('physical_router_user_credentials' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:physical-router-user-credentials')

        if ('physical-router-snmp-credentials' in existing_metas) and ('physical_router_snmp_credentials' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:physical-router-snmp-credentials')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('physical-router-virtual-router' in existing_metas):
             for meta_info in existing_metas['physical-router-virtual-router']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_router_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_router_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-router', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:physical-router-virtual-router')

        old_refs = []
        if ('physical-router-bgp-router' in existing_metas):
             for meta_info in existing_metas['physical-router-bgp-router']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('bgp_router_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['bgp_router_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('bgp-router', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:physical-router-bgp-router')

        old_refs = []
        if ('physical-router-virtual-network' in existing_metas):
             for meta_info in existing_metas['physical-router-virtual-network']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_network_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_network_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-network', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:physical-router-virtual-network')

        (ok, result) = self._ifmap_physical_router_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_physical_router_update

    def _ifmap_physical_router_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['physical-router']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_physical_router_list

    def _ifmap_physical_router_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:physical-router-virtual-router', u'contrail:physical-router-bgp-router', u'contrail:physical-router-virtual-network'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_physical_router_delete

    def _ifmap_bgp_router_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.bgp_router_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_bgp_router_alloc

    def _ifmap_bgp_router_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('bgp_router_parameters', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['bgp_router_parameters']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                BgpRouterParams(**field).exportChildren(buf, level = 1, name_ = 'bgp-router-parameters', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'bgp-router-parameters', pretty_print = False)
            bgp_router_parameters_xml = buf.getvalue()
            buf.close()
            meta = Metadata('bgp-router-parameters' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = bgp_router_parameters_xml)

            if (existing_metas and 'bgp-router-parameters' in existing_metas and
                str(existing_metas['bgp-router-parameters'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('bgp_router_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'bgp-router'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                bgp_peering_xml = ''
                ref_data = ref['attr']
                if ref_data:
                    buf = cStringIO.StringIO()
                    BgpPeeringAttributes(**ref_data).exportChildren(buf, level = 1, name_ = 'bgp-peering', pretty_print = False)
                    bgp_peering_xml = bgp_peering_xml + buf.getvalue()
                    buf.close()
                meta = Metadata('bgp-peering' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = bgp_peering_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_bgp_router_set

    def _ifmap_bgp_router_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['bgp-router']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_bgp_router_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_bgp_router_create


    def _ifmap_bgp_router_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_bgp_router_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'bgp-router'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:bgp-peering'):
            result['bgp_router_refs'] = []
            result_elems = metas['contrail:bgp-peering']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = BgpPeeringAttributes()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('bgp_router', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['bgp_router_refs'].append(ref_info)


        if metas.has_key('contrail:global-system-config-bgp-router'):
            result['global_system_config_back_refs'] = []
            result_elems = metas['contrail:global-system-config-bgp-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('global_system_config', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['global_system_config_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:physical-router-bgp-router'):
            result['physical_router_back_refs'] = []
            result_elems = metas['contrail:physical-router-bgp-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('physical_router', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['physical_router_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:virtual-router-bgp-router'):
            result['virtual_router_back_refs'] = []
            result_elems = metas['contrail:virtual-router-bgp-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_router', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_router_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:bgp-peering'):
            result['bgp_router_refs'] = []
            result_elems = metas['contrail:bgp-peering']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = BgpPeeringAttributes()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('bgp_router', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['bgp_router_refs'].append(ref_info)


        if metas.has_key('contrail:bgp-router-parameters'):
            result_elems = metas['contrail:bgp-router-parameters']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = BgpRouterParams()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['bgp_router_parameters'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_bgp_router_read

    def _ifmap_bgp_router_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'bgp-peering', u'global-system-config-bgp-router', u'physical-router-bgp-router', u'virtual-router-bgp-router', u'bgp-peering', u'bgp-router-parameters', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('bgp_router_refs'):
                req_metas.append('bgp-peering')
            if field_names.has_key('global_system_config_back_refs'):
                req_metas.append('global-system-config-bgp-router')
            if field_names.has_key('physical_router_back_refs'):
                req_metas.append('physical-router-bgp-router')
            if field_names.has_key('virtual_router_back_refs'):
                req_metas.append('virtual-router-bgp-router')
            if field_names.has_key('bgp_router_refs'):
                req_metas.append('bgp-peering')
            if field_names.has_key('bgp_router_parameters'):
                req_metas.append('bgp-router-parameters')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_bgp-router_read_to_meta_index

    def _ifmap_bgp_router_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_bgp_router_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('bgp-router-parameters' in existing_metas) and ('bgp_router_parameters' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:bgp-router-parameters')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('bgp-peering' in existing_metas):
             for meta_info in existing_metas['bgp-peering']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('bgp_router_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['bgp_router_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('bgp-router', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:bgp-peering')

        (ok, result) = self._ifmap_bgp_router_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_bgp_router_update

    def _ifmap_bgp_router_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['bgp-router']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_bgp_router_list

    def _ifmap_bgp_router_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:bgp-peering'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_bgp_router_delete

    def _ifmap_virtual_router_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.virtual_router_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_virtual_router_alloc

    def _ifmap_virtual_router_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('virtual_router_type', None)
        if field is not None:
            norm_str = escape(str(obj_dict['virtual_router_type']))
            meta = Metadata('virtual-router-type', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'virtual-router-type' in existing_metas and
                str(existing_metas['virtual-router-type'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('virtual_router_ip_address', None)
        if field is not None:
            norm_str = escape(str(obj_dict['virtual_router_ip_address']))
            meta = Metadata('virtual-router-ip-address', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'virtual-router-ip-address' in existing_metas and
                str(existing_metas['virtual-router-ip-address'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('bgp_router_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'bgp-router'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_router_bgp_router_xml = ''
                meta = Metadata('virtual-router-bgp-router' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_router_bgp_router_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_router_virtual_machine_xml = ''
                meta = Metadata('virtual-router-virtual-machine' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_router_virtual_machine_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_virtual_router_set

    def _ifmap_virtual_router_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['virtual-router']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_virtual_router_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_virtual_router_create


    def _ifmap_virtual_router_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_virtual_router_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'virtual-router'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:virtual-router-bgp-router'):
            result['bgp_router_refs'] = []
            result_elems = metas['contrail:virtual-router-bgp-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('bgp_router', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['bgp_router_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-router-virtual-machine'):
            result['virtual_machine_refs'] = []
            result_elems = metas['contrail:virtual-router-virtual-machine']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_refs'].append(ref_info)


        if metas.has_key('contrail:physical-router-virtual-router'):
            result['physical_router_back_refs'] = []
            result_elems = metas['contrail:physical-router-virtual-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('physical_router', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['physical_router_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:provider-attachment-virtual-router'):
            result['provider_attachment_back_refs'] = []
            result_elems = metas['contrail:provider-attachment-virtual-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('provider_attachment', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['provider_attachment_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:virtual-router-type'):
            result_elems = metas['contrail:virtual-router-type']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['virtual_router_type'] = obj_val

        if metas.has_key('contrail:virtual-router-ip-address'):
            result_elems = metas['contrail:virtual-router-ip-address']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['virtual_router_ip_address'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_virtual_router_read

    def _ifmap_virtual_router_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'virtual-router-bgp-router', u'virtual-router-virtual-machine', u'physical-router-virtual-router', u'provider-attachment-virtual-router', u'virtual-router-type', u'virtual-router-ip-address', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('bgp_router_refs'):
                req_metas.append('virtual-router-bgp-router')
            if field_names.has_key('virtual_machine_refs'):
                req_metas.append('virtual-router-virtual-machine')
            if field_names.has_key('physical_router_back_refs'):
                req_metas.append('physical-router-virtual-router')
            if field_names.has_key('provider_attachment_back_refs'):
                req_metas.append('provider-attachment-virtual-router')
            if field_names.has_key('virtual_router_type'):
                req_metas.append('virtual-router-type')
            if field_names.has_key('virtual_router_ip_address'):
                req_metas.append('virtual-router-ip-address')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_virtual-router_read_to_meta_index

    def _ifmap_virtual_router_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_virtual_router_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('virtual-router-type' in existing_metas) and ('virtual_router_type' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-router-type')

        if ('virtual-router-ip-address' in existing_metas) and ('virtual_router_ip_address' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-router-ip-address')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('virtual-router-bgp-router' in existing_metas):
             for meta_info in existing_metas['virtual-router-bgp-router']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('bgp_router_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['bgp_router_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('bgp-router', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-router-bgp-router')

        old_refs = []
        if ('virtual-router-virtual-machine' in existing_metas):
             for meta_info in existing_metas['virtual-router-virtual-machine']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_machine_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_machine_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-machine', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-router-virtual-machine')

        (ok, result) = self._ifmap_virtual_router_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_virtual_router_update

    def _ifmap_virtual_router_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['virtual-router']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_virtual_router_list

    def _ifmap_virtual_router_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:virtual-router-bgp-router', u'contrail:virtual-router-virtual-machine'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_virtual_router_delete

    def _ifmap_config_root_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.config_root_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_config_root_alloc

    def _ifmap_config_root_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('global_system_config_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'global-system-config'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                config_root_global_system_config_xml = ''
                meta = Metadata('config-root-global-system-config' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = config_root_global_system_config_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('domain_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'domain'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                config_root_domain_xml = ''
                meta = Metadata('config-root-domain' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = config_root_domain_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_config_root_set

    def _ifmap_config_root_create(self, obj_ids, obj_dict):
        (ok, result) = self._ifmap_config_root_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_config_root_create


    def _ifmap_config_root_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_config_root_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'config-root'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:config-root-global-system-config'):
            result['global_system_configs'] = []
            result_elems = metas['contrail:config-root-global-system-config']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('global_system_config', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['global_system_configs'].append(has_info)


        if metas.has_key('contrail:config-root-domain'):
            result['domains'] = []
            result_elems = metas['contrail:config-root-domain']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('domain', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['domains'].append(has_info)


        return (True, result)
    #end _ifmap_config_root_read

    def _ifmap_config_root_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'id-perms', u'display-name', u'config-root-global-system-config', u'config-root-domain']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('config_root_global_system_config'):
                req_metas.append('config-root-global-system-config')
            if field_names.has_key('config_root_domain'):
                req_metas.append('config-root-domain')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_config-root_read_to_meta_index

    def _ifmap_config_root_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_config_root_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        (ok, result) = self._ifmap_config_root_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_config_root_update

    def _ifmap_config_root_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        return (True, children_fq_names)
    #end _ifmap_config_root_list

    def _ifmap_config_root_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_config_root_delete

    def _ifmap_subnet_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.subnet_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_subnet_alloc

    def _ifmap_subnet_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('subnet_ip_prefix', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['subnet_ip_prefix']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                SubnetType(**field).exportChildren(buf, level = 1, name_ = 'subnet-ip-prefix', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'subnet-ip-prefix', pretty_print = False)
            subnet_ip_prefix_xml = buf.getvalue()
            buf.close()
            meta = Metadata('subnet-ip-prefix' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = subnet_ip_prefix_xml)

            if (existing_metas and 'subnet-ip-prefix' in existing_metas and
                str(existing_metas['subnet-ip-prefix'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                subnet_virtual_machine_interface_xml = ''
                meta = Metadata('subnet-virtual-machine-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = subnet_virtual_machine_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_subnet_set

    def _ifmap_subnet_create(self, obj_ids, obj_dict):
        (ok, result) = self._ifmap_subnet_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_subnet_create


    def _ifmap_subnet_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_subnet_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'subnet'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:subnet-virtual-machine-interface'):
            result['virtual_machine_interface_refs'] = []
            result_elems = metas['contrail:subnet-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine_interface', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_interface_refs'].append(ref_info)


        if metas.has_key('contrail:subnet-ip-prefix'):
            result_elems = metas['contrail:subnet-ip-prefix']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = SubnetType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['subnet_ip_prefix'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_subnet_read

    def _ifmap_subnet_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'subnet-virtual-machine-interface', u'subnet-ip-prefix', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_machine_interface_refs'):
                req_metas.append('subnet-virtual-machine-interface')
            if field_names.has_key('subnet_ip_prefix'):
                req_metas.append('subnet-ip-prefix')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_subnet_read_to_meta_index

    def _ifmap_subnet_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_subnet_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('subnet-ip-prefix' in existing_metas) and ('subnet_ip_prefix' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:subnet-ip-prefix')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('subnet-virtual-machine-interface' in existing_metas):
             for meta_info in existing_metas['subnet-virtual-machine-interface']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_machine_interface_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_machine_interface_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-machine-interface', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:subnet-virtual-machine-interface')

        (ok, result) = self._ifmap_subnet_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_subnet_update

    def _ifmap_subnet_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        return (True, children_fq_names)
    #end _ifmap_subnet_list

    def _ifmap_subnet_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:subnet-virtual-machine-interface'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_subnet_delete

    def _ifmap_global_system_config_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.global_system_config_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_global_system_config_alloc

    def _ifmap_global_system_config_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('autonomous_system', None)
        if field is not None:
            norm_str = escape(str(obj_dict['autonomous_system']))
            meta = Metadata('autonomous-system', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'autonomous-system' in existing_metas and
                str(existing_metas['autonomous-system'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('config_version', None)
        if field is not None:
            norm_str = escape(str(obj_dict['config_version']))
            meta = Metadata('config-version', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'config-version' in existing_metas and
                str(existing_metas['config-version'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('plugin_tuning', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['plugin_tuning']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                PluginProperties(**field).exportChildren(buf, level = 1, name_ = 'plugin-tuning', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'plugin-tuning', pretty_print = False)
            plugin_tuning_xml = buf.getvalue()
            buf.close()
            meta = Metadata('plugin-tuning' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = plugin_tuning_xml)

            if (existing_metas and 'plugin-tuning' in existing_metas and
                str(existing_metas['plugin-tuning'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('ibgp_auto_mesh', None)
        if field is not None:
            norm_str = escape(str(obj_dict['ibgp_auto_mesh']))
            meta = Metadata('ibgp-auto-mesh', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'ibgp-auto-mesh' in existing_metas and
                str(existing_metas['ibgp-auto-mesh'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('bgp_router_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'bgp-router'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                global_system_config_bgp_router_xml = ''
                meta = Metadata('global-system-config-bgp-router' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = global_system_config_bgp_router_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('global_vrouter_config_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'global-vrouter-config'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                global_system_config_global_vrouter_config_xml = ''
                meta = Metadata('global-system-config-global-vrouter-config' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = global_system_config_global_vrouter_config_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('physical_router_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'physical-router'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                global_system_config_physical_router_xml = ''
                meta = Metadata('global-system-config-physical-router' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = global_system_config_physical_router_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_router_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-router'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                global_system_config_virtual_router_xml = ''
                meta = Metadata('global-system-config-virtual-router' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = global_system_config_virtual_router_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_global_system_config_set

    def _ifmap_global_system_config_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['global-system-config']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_global_system_config_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_global_system_config_create


    def _ifmap_global_system_config_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_global_system_config_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'global-system-config'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:global-system-config-bgp-router'):
            result['bgp_router_refs'] = []
            result_elems = metas['contrail:global-system-config-bgp-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('bgp_router', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['bgp_router_refs'].append(ref_info)


        if metas.has_key('contrail:autonomous-system'):
            result_elems = metas['contrail:autonomous-system']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['autonomous_system'] = obj_val

        if metas.has_key('contrail:config-version'):
            result_elems = metas['contrail:config-version']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['config_version'] = obj_val

        if metas.has_key('contrail:plugin-tuning'):
            result_elems = metas['contrail:plugin-tuning']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = PluginProperties()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['plugin_tuning'] = obj_val

        if metas.has_key('contrail:ibgp-auto-mesh'):
            result_elems = metas['contrail:ibgp-auto-mesh']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['ibgp_auto_mesh'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:global-system-config-global-vrouter-config'):
            result['global_vrouter_configs'] = []
            result_elems = metas['contrail:global-system-config-global-vrouter-config']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('global_vrouter_config', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['global_vrouter_configs'].append(has_info)


        if metas.has_key('contrail:global-system-config-physical-router'):
            result['physical_routers'] = []
            result_elems = metas['contrail:global-system-config-physical-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('physical_router', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['physical_routers'].append(has_info)


        if metas.has_key('contrail:global-system-config-virtual-router'):
            result['virtual_routers'] = []
            result_elems = metas['contrail:global-system-config-virtual-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('virtual_router', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['virtual_routers'].append(has_info)


        return (True, result)
    #end _ifmap_global_system_config_read

    def _ifmap_global_system_config_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'global-system-config-bgp-router', u'autonomous-system', u'config-version', u'plugin-tuning', u'ibgp-auto-mesh', u'id-perms', u'display-name', u'global-system-config-global-vrouter-config', u'global-system-config-physical-router', u'global-system-config-virtual-router']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('bgp_router_refs'):
                req_metas.append('global-system-config-bgp-router')
            if field_names.has_key('autonomous_system'):
                req_metas.append('autonomous-system')
            if field_names.has_key('config_version'):
                req_metas.append('config-version')
            if field_names.has_key('plugin_tuning'):
                req_metas.append('plugin-tuning')
            if field_names.has_key('ibgp_auto_mesh'):
                req_metas.append('ibgp-auto-mesh')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('global_system_config_global_vrouter_config'):
                req_metas.append('global-system-config-global-vrouter-config')
            if field_names.has_key('global_system_config_physical_router'):
                req_metas.append('global-system-config-physical-router')
            if field_names.has_key('global_system_config_virtual_router'):
                req_metas.append('global-system-config-virtual-router')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_global-system-config_read_to_meta_index

    def _ifmap_global_system_config_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_global_system_config_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('autonomous-system' in existing_metas) and ('autonomous_system' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:autonomous-system')

        if ('config-version' in existing_metas) and ('config_version' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:config-version')

        if ('plugin-tuning' in existing_metas) and ('plugin_tuning' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:plugin-tuning')

        if ('ibgp-auto-mesh' in existing_metas) and ('ibgp_auto_mesh' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:ibgp-auto-mesh')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('global-system-config-bgp-router' in existing_metas):
             for meta_info in existing_metas['global-system-config-bgp-router']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('bgp_router_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['bgp_router_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('bgp-router', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:global-system-config-bgp-router')

        (ok, result) = self._ifmap_global_system_config_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_global_system_config_update

    def _ifmap_global_system_config_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['global-system-config']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_global_system_config_list

    def _ifmap_global_system_config_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:global-system-config-bgp-router'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_global_system_config_delete

    def _ifmap_loadbalancer_member_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.loadbalancer_member_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_loadbalancer_member_alloc

    def _ifmap_loadbalancer_member_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('loadbalancer_member_properties', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['loadbalancer_member_properties']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                LoadbalancerMemberType(**field).exportChildren(buf, level = 1, name_ = 'loadbalancer-member-properties', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'loadbalancer-member-properties', pretty_print = False)
            loadbalancer_member_properties_xml = buf.getvalue()
            buf.close()
            meta = Metadata('loadbalancer-member-properties' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = loadbalancer_member_properties_xml)

            if (existing_metas and 'loadbalancer-member-properties' in existing_metas and
                str(existing_metas['loadbalancer-member-properties'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_loadbalancer_member_set

    def _ifmap_loadbalancer_member_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['loadbalancer-member']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_loadbalancer_member_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_loadbalancer_member_create


    def _ifmap_loadbalancer_member_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_loadbalancer_member_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'loadbalancer-member'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:loadbalancer-member-properties'):
            result_elems = metas['contrail:loadbalancer-member-properties']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = LoadbalancerMemberType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['loadbalancer_member_properties'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_loadbalancer_member_read

    def _ifmap_loadbalancer_member_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'loadbalancer-member-properties', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('loadbalancer_member_properties'):
                req_metas.append('loadbalancer-member-properties')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_loadbalancer-member_read_to_meta_index

    def _ifmap_loadbalancer_member_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_loadbalancer_member_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('loadbalancer-member-properties' in existing_metas) and ('loadbalancer_member_properties' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:loadbalancer-member-properties')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        (ok, result) = self._ifmap_loadbalancer_member_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_loadbalancer_member_update

    def _ifmap_loadbalancer_member_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['loadbalancer-member']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_loadbalancer_member_list

    def _ifmap_loadbalancer_member_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_loadbalancer_member_delete

    def _ifmap_service_instance_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.service_instance_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_service_instance_alloc

    def _ifmap_service_instance_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('service_instance_properties', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['service_instance_properties']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                ServiceInstanceType(**field).exportChildren(buf, level = 1, name_ = 'service-instance-properties', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'service-instance-properties', pretty_print = False)
            service_instance_properties_xml = buf.getvalue()
            buf.close()
            meta = Metadata('service-instance-properties' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = service_instance_properties_xml)

            if (existing_metas and 'service-instance-properties' in existing_metas and
                str(existing_metas['service-instance-properties'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('service_template_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'service-template'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                service_instance_service_template_xml = ''
                meta = Metadata('service-instance-service-template' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = service_instance_service_template_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_service_instance_set

    def _ifmap_service_instance_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['service-instance']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_service_instance_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_service_instance_create


    def _ifmap_service_instance_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_service_instance_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'service-instance'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:service-instance-service-template'):
            result['service_template_refs'] = []
            result_elems = metas['contrail:service-instance-service-template']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('service_template', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['service_template_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-machine-service-instance'):
            result['virtual_machine_back_refs'] = []
            result_elems = metas['contrail:virtual-machine-service-instance']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_machine', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_machine_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:logical-router-service-instance'):
            result['logical_router_back_refs'] = []
            result_elems = metas['contrail:logical-router-service-instance']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('logical_router', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['logical_router_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:loadbalancer-pool-service-instance'):
            result['loadbalancer_pool_back_refs'] = []
            result_elems = metas['contrail:loadbalancer-pool-service-instance']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('loadbalancer_pool', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['loadbalancer_pool_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:service-instance-properties'):
            result_elems = metas['contrail:service-instance-properties']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = ServiceInstanceType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['service_instance_properties'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_service_instance_read

    def _ifmap_service_instance_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'service-instance-service-template', u'virtual-machine-service-instance', u'logical-router-service-instance', u'loadbalancer-pool-service-instance', u'service-instance-properties', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('service_template_refs'):
                req_metas.append('service-instance-service-template')
            if field_names.has_key('virtual_machine_back_refs'):
                req_metas.append('virtual-machine-service-instance')
            if field_names.has_key('logical_router_back_refs'):
                req_metas.append('logical-router-service-instance')
            if field_names.has_key('loadbalancer_pool_back_refs'):
                req_metas.append('loadbalancer-pool-service-instance')
            if field_names.has_key('service_instance_properties'):
                req_metas.append('service-instance-properties')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_service-instance_read_to_meta_index

    def _ifmap_service_instance_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_service_instance_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('service-instance-properties' in existing_metas) and ('service_instance_properties' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:service-instance-properties')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('service-instance-service-template' in existing_metas):
             for meta_info in existing_metas['service-instance-service-template']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('service_template_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['service_template_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('service-template', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:service-instance-service-template')

        (ok, result) = self._ifmap_service_instance_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_service_instance_update

    def _ifmap_service_instance_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['service-instance']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_service_instance_list

    def _ifmap_service_instance_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:service-instance-service-template'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_service_instance_delete

    def _ifmap_namespace_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.namespace_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_namespace_alloc

    def _ifmap_namespace_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('namespace_cidr', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['namespace_cidr']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                SubnetType(**field).exportChildren(buf, level = 1, name_ = 'namespace-cidr', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'namespace-cidr', pretty_print = False)
            namespace_cidr_xml = buf.getvalue()
            buf.close()
            meta = Metadata('namespace-cidr' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = namespace_cidr_xml)

            if (existing_metas and 'namespace-cidr' in existing_metas and
                str(existing_metas['namespace-cidr'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_namespace_set

    def _ifmap_namespace_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['namespace']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_namespace_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_namespace_create


    def _ifmap_namespace_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_namespace_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'namespace'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:project-namespace'):
            result['project_back_refs'] = []
            result_elems = metas['contrail:project-namespace']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = SubnetType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('project', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['project_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:namespace-cidr'):
            result_elems = metas['contrail:namespace-cidr']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = SubnetType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['namespace_cidr'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_namespace_read

    def _ifmap_namespace_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'project-namespace', u'namespace-cidr', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('project_back_refs'):
                req_metas.append('project-namespace')
            if field_names.has_key('namespace_cidr'):
                req_metas.append('namespace-cidr')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_namespace_read_to_meta_index

    def _ifmap_namespace_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_namespace_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('namespace-cidr' in existing_metas) and ('namespace_cidr' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:namespace-cidr')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        (ok, result) = self._ifmap_namespace_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_namespace_update

    def _ifmap_namespace_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['namespace']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_namespace_list

    def _ifmap_namespace_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_namespace_delete

    def _ifmap_route_table_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.route_table_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_route_table_alloc

    def _ifmap_route_table_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('routes', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['routes']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                RouteTableType(**field).exportChildren(buf, level = 1, name_ = 'routes', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'routes', pretty_print = False)
            routes_xml = buf.getvalue()
            buf.close()
            meta = Metadata('routes' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = routes_xml)

            if (existing_metas and 'routes' in existing_metas and
                str(existing_metas['routes'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_route_table_set

    def _ifmap_route_table_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['route-table']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_route_table_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_route_table_create


    def _ifmap_route_table_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_route_table_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'route-table'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:virtual-network-route-table'):
            result['virtual_network_back_refs'] = []
            result_elems = metas['contrail:virtual-network-route-table']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_network', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_network_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:routes'):
            result_elems = metas['contrail:routes']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = RouteTableType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['routes'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_route_table_read

    def _ifmap_route_table_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'virtual-network-route-table', u'routes', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_network_back_refs'):
                req_metas.append('virtual-network-route-table')
            if field_names.has_key('routes'):
                req_metas.append('routes')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_route-table_read_to_meta_index

    def _ifmap_route_table_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_route_table_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('routes' in existing_metas) and ('routes' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:routes')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        (ok, result) = self._ifmap_route_table_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_route_table_update

    def _ifmap_route_table_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['route-table']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_route_table_list

    def _ifmap_route_table_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_route_table_delete

    def _ifmap_physical_interface_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.physical_interface_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_physical_interface_alloc

    def _ifmap_physical_interface_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('logical_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'logical-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                physical_interface_logical_interface_xml = ''
                meta = Metadata('physical-interface-logical-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = physical_interface_logical_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_physical_interface_set

    def _ifmap_physical_interface_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['physical-interface']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_physical_interface_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_physical_interface_create


    def _ifmap_physical_interface_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_physical_interface_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'physical-interface'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:physical-interface-logical-interface'):
            result['logical_interfaces'] = []
            result_elems = metas['contrail:physical-interface-logical-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('logical_interface', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['logical_interfaces'].append(has_info)


        return (True, result)
    #end _ifmap_physical_interface_read

    def _ifmap_physical_interface_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'id-perms', u'display-name', u'physical-interface-logical-interface']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('physical_interface_logical_interface'):
                req_metas.append('physical-interface-logical-interface')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_physical-interface_read_to_meta_index

    def _ifmap_physical_interface_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_physical_interface_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        (ok, result) = self._ifmap_physical_interface_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_physical_interface_update

    def _ifmap_physical_interface_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['physical-interface']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_physical_interface_list

    def _ifmap_physical_interface_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_physical_interface_delete

    def _ifmap_access_control_list_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.access_control_list_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_access_control_list_alloc

    def _ifmap_access_control_list_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('access_control_list_entries', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['access_control_list_entries']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                AclEntriesType(**field).exportChildren(buf, level = 1, name_ = 'access-control-list-entries', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'access-control-list-entries', pretty_print = False)
            access_control_list_entries_xml = buf.getvalue()
            buf.close()
            meta = Metadata('access-control-list-entries' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = access_control_list_entries_xml)

            if (existing_metas and 'access-control-list-entries' in existing_metas and
                str(existing_metas['access-control-list-entries'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_access_control_list_set

    def _ifmap_access_control_list_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['access-control-list']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_access_control_list_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_access_control_list_create


    def _ifmap_access_control_list_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_access_control_list_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'access-control-list'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:access-control-list-entries'):
            result_elems = metas['contrail:access-control-list-entries']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = AclEntriesType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['access_control_list_entries'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_access_control_list_read

    def _ifmap_access_control_list_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'access-control-list-entries', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('access_control_list_entries'):
                req_metas.append('access-control-list-entries')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_access-control-list_read_to_meta_index

    def _ifmap_access_control_list_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_access_control_list_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('access-control-list-entries' in existing_metas) and ('access_control_list_entries' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:access-control-list-entries')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        (ok, result) = self._ifmap_access_control_list_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_access_control_list_update

    def _ifmap_access_control_list_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['access-control-list']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_access_control_list_list

    def _ifmap_access_control_list_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_access_control_list_delete

    def _ifmap_virtual_DNS_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.virtual_DNS_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_virtual_DNS_alloc

    def _ifmap_virtual_DNS_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('virtual_DNS_data', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['virtual_DNS_data']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                VirtualDnsType(**field).exportChildren(buf, level = 1, name_ = 'virtual-DNS-data', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'virtual-DNS-data', pretty_print = False)
            virtual_DNS_data_xml = buf.getvalue()
            buf.close()
            meta = Metadata('virtual-DNS-data' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = virtual_DNS_data_xml)

            if (existing_metas and 'virtual-DNS-data' in existing_metas and
                str(existing_metas['virtual-DNS-data'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_DNS_record_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-DNS-record'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_DNS_virtual_DNS_record_xml = ''
                meta = Metadata('virtual-DNS-virtual-DNS-record' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_DNS_virtual_DNS_record_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_virtual_DNS_set

    def _ifmap_virtual_DNS_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['virtual-DNS']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_virtual_DNS_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_virtual_DNS_create


    def _ifmap_virtual_DNS_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_virtual_DNS_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'virtual-DNS'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:network-ipam-virtual-DNS'):
            result['network_ipam_back_refs'] = []
            result_elems = metas['contrail:network-ipam-virtual-DNS']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('network_ipam', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['network_ipam_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:virtual-DNS-data'):
            result_elems = metas['contrail:virtual-DNS-data']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = VirtualDnsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['virtual_DNS_data'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:virtual-DNS-virtual-DNS-record'):
            result['virtual_DNS_records'] = []
            result_elems = metas['contrail:virtual-DNS-virtual-DNS-record']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('virtual_DNS_record', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['virtual_DNS_records'].append(has_info)


        return (True, result)
    #end _ifmap_virtual_DNS_read

    def _ifmap_virtual_DNS_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'network-ipam-virtual-DNS', u'virtual-DNS-data', u'id-perms', u'display-name', u'virtual-DNS-virtual-DNS-record']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('network_ipam_back_refs'):
                req_metas.append('network-ipam-virtual-DNS')
            if field_names.has_key('virtual_DNS_data'):
                req_metas.append('virtual-DNS-data')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('virtual_DNS_virtual_DNS_record'):
                req_metas.append('virtual-DNS-virtual-DNS-record')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_virtual-DNS_read_to_meta_index

    def _ifmap_virtual_DNS_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_virtual_DNS_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('virtual-DNS-data' in existing_metas) and ('virtual_DNS_data' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-DNS-data')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        (ok, result) = self._ifmap_virtual_DNS_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_virtual_DNS_update

    def _ifmap_virtual_DNS_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['virtual-DNS']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_virtual_DNS_list

    def _ifmap_virtual_DNS_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_virtual_DNS_delete

    def _ifmap_customer_attachment_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.customer_attachment_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_customer_attachment_alloc

    def _ifmap_customer_attachment_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('attachment_address', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['attachment_address']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                AttachmentAddressType(**field).exportChildren(buf, level = 1, name_ = 'attachment-address', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'attachment-address', pretty_print = False)
            attachment_address_xml = buf.getvalue()
            buf.close()
            meta = Metadata('attachment-address' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = attachment_address_xml)

            if (existing_metas and 'attachment-address' in existing_metas and
                str(existing_metas['attachment-address'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                customer_attachment_virtual_machine_interface_xml = ''
                meta = Metadata('customer-attachment-virtual-machine-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = customer_attachment_virtual_machine_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('floating_ip_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'floating-ip'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                customer_attachment_floating_ip_xml = ''
                meta = Metadata('customer-attachment-floating-ip' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = customer_attachment_floating_ip_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('routing_instance_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'routing-instance'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                binding_xml = ''
                ref_data = ref['attr']
                if ref_data:
                    buf = cStringIO.StringIO()
                    BindingType(**ref_data).exportChildren(buf, level = 1, name_ = 'binding', pretty_print = False)
                    binding_xml = binding_xml + buf.getvalue()
                    buf.close()
                meta = Metadata('binding' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = binding_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('provider_attachment_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'provider-attachment'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                attachment_info_xml = ''
                ref_data = ref['attr']
                if ref_data:
                    buf = cStringIO.StringIO()
                    AttachmentInfoType(**ref_data).exportChildren(buf, level = 1, name_ = 'attachment-info', pretty_print = False)
                    attachment_info_xml = attachment_info_xml + buf.getvalue()
                    buf.close()
                meta = Metadata('attachment-info' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = attachment_info_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_customer_attachment_set

    def _ifmap_customer_attachment_create(self, obj_ids, obj_dict):
        (ok, result) = self._ifmap_customer_attachment_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_customer_attachment_create


    def _ifmap_customer_attachment_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_customer_attachment_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'customer-attachment'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:customer-attachment-virtual-machine-interface'):
            result['virtual_machine_interface_refs'] = []
            result_elems = metas['contrail:customer-attachment-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine_interface', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_interface_refs'].append(ref_info)


        if metas.has_key('contrail:customer-attachment-floating-ip'):
            result['floating_ip_refs'] = []
            result_elems = metas['contrail:customer-attachment-floating-ip']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('floating_ip', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['floating_ip_refs'].append(ref_info)


        if metas.has_key('contrail:attachment-address'):
            result_elems = metas['contrail:attachment-address']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = AttachmentAddressType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['attachment_address'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:binding'):
            result['routing_instances'] = []
            result_elems = metas['contrail:binding']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('routing_instance', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['routing_instances'].append(has_info)


        if metas.has_key('contrail:attachment-info'):
            result['provider_attachments'] = []
            result_elems = metas['contrail:attachment-info']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = AttachmentInfoType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('provider_attachment', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['provider_attachments'].append(has_info)


        return (True, result)
    #end _ifmap_customer_attachment_read

    def _ifmap_customer_attachment_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'customer-attachment-virtual-machine-interface', u'customer-attachment-floating-ip', u'attachment-address', u'id-perms', u'display-name', u'binding', u'attachment-info']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_machine_interface_refs'):
                req_metas.append('customer-attachment-virtual-machine-interface')
            if field_names.has_key('floating_ip_refs'):
                req_metas.append('customer-attachment-floating-ip')
            if field_names.has_key('attachment_address'):
                req_metas.append('attachment-address')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('binding'):
                req_metas.append('binding')
            if field_names.has_key('attachment_info'):
                req_metas.append('attachment-info')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_customer-attachment_read_to_meta_index

    def _ifmap_customer_attachment_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_customer_attachment_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('attachment-address' in existing_metas) and ('attachment_address' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:attachment-address')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('customer-attachment-virtual-machine-interface' in existing_metas):
             for meta_info in existing_metas['customer-attachment-virtual-machine-interface']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_machine_interface_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_machine_interface_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-machine-interface', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:customer-attachment-virtual-machine-interface')

        old_refs = []
        if ('customer-attachment-floating-ip' in existing_metas):
             for meta_info in existing_metas['customer-attachment-floating-ip']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('floating_ip_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['floating_ip_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('floating-ip', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:customer-attachment-floating-ip')

        (ok, result) = self._ifmap_customer_attachment_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_customer_attachment_update

    def _ifmap_customer_attachment_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        return (True, children_fq_names)
    #end _ifmap_customer_attachment_list

    def _ifmap_customer_attachment_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:customer-attachment-virtual-machine-interface', u'contrail:customer-attachment-floating-ip', u'contrail:binding', u'contrail:attachment-info'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_customer_attachment_delete

    def _ifmap_loadbalancer_pool_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.loadbalancer_pool_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_loadbalancer_pool_alloc

    def _ifmap_loadbalancer_pool_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('loadbalancer_pool_properties', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['loadbalancer_pool_properties']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                LoadbalancerPoolType(**field).exportChildren(buf, level = 1, name_ = 'loadbalancer-pool-properties', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'loadbalancer-pool-properties', pretty_print = False)
            loadbalancer_pool_properties_xml = buf.getvalue()
            buf.close()
            meta = Metadata('loadbalancer-pool-properties' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = loadbalancer_pool_properties_xml)

            if (existing_metas and 'loadbalancer-pool-properties' in existing_metas and
                str(existing_metas['loadbalancer-pool-properties'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('loadbalancer_pool_provider', None)
        if field is not None:
            norm_str = escape(str(obj_dict['loadbalancer_pool_provider']))
            meta = Metadata('loadbalancer-pool-provider', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'loadbalancer-pool-provider' in existing_metas and
                str(existing_metas['loadbalancer-pool-provider'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('service_instance_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'service-instance'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                loadbalancer_pool_service_instance_xml = ''
                meta = Metadata('loadbalancer-pool-service-instance' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = loadbalancer_pool_service_instance_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                loadbalancer_pool_virtual_machine_interface_xml = ''
                meta = Metadata('loadbalancer-pool-virtual-machine-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = loadbalancer_pool_virtual_machine_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('loadbalancer_member_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'loadbalancer-member'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                loadbalancer_pool_loadbalancer_member_xml = ''
                meta = Metadata('loadbalancer-pool-loadbalancer-member' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = loadbalancer_pool_loadbalancer_member_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('loadbalancer_healthmonitor_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'loadbalancer-healthmonitor'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                loadbalancer_pool_loadbalancer_healthmonitor_xml = ''
                meta = Metadata('loadbalancer-pool-loadbalancer-healthmonitor' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = loadbalancer_pool_loadbalancer_healthmonitor_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_loadbalancer_pool_set

    def _ifmap_loadbalancer_pool_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['loadbalancer-pool']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_loadbalancer_pool_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_loadbalancer_pool_create


    def _ifmap_loadbalancer_pool_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_loadbalancer_pool_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'loadbalancer-pool'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:loadbalancer-pool-service-instance'):
            result['service_instance_refs'] = []
            result_elems = metas['contrail:loadbalancer-pool-service-instance']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('service_instance', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['service_instance_refs'].append(ref_info)


        if metas.has_key('contrail:loadbalancer-pool-virtual-machine-interface'):
            result['virtual_machine_interface_refs'] = []
            result_elems = metas['contrail:loadbalancer-pool-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine_interface', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_interface_refs'].append(ref_info)


        if metas.has_key('contrail:loadbalancer-pool-loadbalancer-healthmonitor'):
            result['loadbalancer_healthmonitor_refs'] = []
            result_elems = metas['contrail:loadbalancer-pool-loadbalancer-healthmonitor']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('loadbalancer_healthmonitor', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['loadbalancer_healthmonitor_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-ip-loadbalancer-pool'):
            result['virtual_ip_back_refs'] = []
            result_elems = metas['contrail:virtual-ip-loadbalancer-pool']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_ip', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_ip_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:loadbalancer-pool-properties'):
            result_elems = metas['contrail:loadbalancer-pool-properties']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = LoadbalancerPoolType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['loadbalancer_pool_properties'] = obj_val

        if metas.has_key('contrail:loadbalancer-pool-provider'):
            result_elems = metas['contrail:loadbalancer-pool-provider']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['loadbalancer_pool_provider'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:loadbalancer-pool-loadbalancer-member'):
            result['loadbalancer_members'] = []
            result_elems = metas['contrail:loadbalancer-pool-loadbalancer-member']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('loadbalancer_member', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['loadbalancer_members'].append(has_info)


        return (True, result)
    #end _ifmap_loadbalancer_pool_read

    def _ifmap_loadbalancer_pool_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'loadbalancer-pool-service-instance', u'loadbalancer-pool-virtual-machine-interface', u'loadbalancer-pool-loadbalancer-healthmonitor', u'virtual-ip-loadbalancer-pool', u'loadbalancer-pool-properties', u'loadbalancer-pool-provider', u'id-perms', u'display-name', u'loadbalancer-pool-loadbalancer-member']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('service_instance_refs'):
                req_metas.append('loadbalancer-pool-service-instance')
            if field_names.has_key('virtual_machine_interface_refs'):
                req_metas.append('loadbalancer-pool-virtual-machine-interface')
            if field_names.has_key('loadbalancer_healthmonitor_refs'):
                req_metas.append('loadbalancer-pool-loadbalancer-healthmonitor')
            if field_names.has_key('virtual_ip_back_refs'):
                req_metas.append('virtual-ip-loadbalancer-pool')
            if field_names.has_key('loadbalancer_pool_properties'):
                req_metas.append('loadbalancer-pool-properties')
            if field_names.has_key('loadbalancer_pool_provider'):
                req_metas.append('loadbalancer-pool-provider')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('loadbalancer_pool_loadbalancer_member'):
                req_metas.append('loadbalancer-pool-loadbalancer-member')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_loadbalancer-pool_read_to_meta_index

    def _ifmap_loadbalancer_pool_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_loadbalancer_pool_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('loadbalancer-pool-properties' in existing_metas) and ('loadbalancer_pool_properties' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:loadbalancer-pool-properties')

        if ('loadbalancer-pool-provider' in existing_metas) and ('loadbalancer_pool_provider' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:loadbalancer-pool-provider')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('loadbalancer-pool-service-instance' in existing_metas):
             for meta_info in existing_metas['loadbalancer-pool-service-instance']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('service_instance_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['service_instance_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('service-instance', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:loadbalancer-pool-service-instance')

        old_refs = []
        if ('loadbalancer-pool-virtual-machine-interface' in existing_metas):
             for meta_info in existing_metas['loadbalancer-pool-virtual-machine-interface']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_machine_interface_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_machine_interface_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-machine-interface', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:loadbalancer-pool-virtual-machine-interface')

        old_refs = []
        if ('loadbalancer-pool-loadbalancer-healthmonitor' in existing_metas):
             for meta_info in existing_metas['loadbalancer-pool-loadbalancer-healthmonitor']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('loadbalancer_healthmonitor_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['loadbalancer_healthmonitor_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('loadbalancer-healthmonitor', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:loadbalancer-pool-loadbalancer-healthmonitor')

        (ok, result) = self._ifmap_loadbalancer_pool_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_loadbalancer_pool_update

    def _ifmap_loadbalancer_pool_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['loadbalancer-pool']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_loadbalancer_pool_list

    def _ifmap_loadbalancer_pool_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:loadbalancer-pool-service-instance', u'contrail:loadbalancer-pool-virtual-machine-interface', u'contrail:loadbalancer-pool-loadbalancer-healthmonitor'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_loadbalancer_pool_delete

    def _ifmap_virtual_machine_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.virtual_machine_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_virtual_machine_alloc

    def _ifmap_virtual_machine_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_machine_virtual_machine_interface_xml = ''
                meta = Metadata('virtual-machine-virtual-machine-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_machine_virtual_machine_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('service_instance_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'service-instance'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_machine_service_instance_xml = ''
                meta = Metadata('virtual-machine-service-instance' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_machine_service_instance_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_virtual_machine_set

    def _ifmap_virtual_machine_create(self, obj_ids, obj_dict):
        (ok, result) = self._ifmap_virtual_machine_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_virtual_machine_create


    def _ifmap_virtual_machine_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_virtual_machine_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'virtual-machine'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:virtual-machine-service-instance'):
            result['service_instance_refs'] = []
            result_elems = metas['contrail:virtual-machine-service-instance']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('service_instance', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['service_instance_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-machine-interface-virtual-machine'):
            result['virtual_machine_interface_back_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-virtual-machine']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_machine_interface', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_machine_interface_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:virtual-router-virtual-machine'):
            result['virtual_router_back_refs'] = []
            result_elems = metas['contrail:virtual-router-virtual-machine']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_router', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_router_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:virtual-machine-virtual-machine-interface'):
            result['virtual_machine_interfaces'] = []
            result_elems = metas['contrail:virtual-machine-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('virtual_machine_interface', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['virtual_machine_interfaces'].append(has_info)


        return (True, result)
    #end _ifmap_virtual_machine_read

    def _ifmap_virtual_machine_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'virtual-machine-service-instance', u'virtual-machine-interface-virtual-machine', u'virtual-router-virtual-machine', u'id-perms', u'display-name', u'virtual-machine-virtual-machine-interface']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('service_instance_refs'):
                req_metas.append('virtual-machine-service-instance')
            if field_names.has_key('virtual_machine_interface_back_refs'):
                req_metas.append('virtual-machine-interface-virtual-machine')
            if field_names.has_key('virtual_router_back_refs'):
                req_metas.append('virtual-router-virtual-machine')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('virtual_machine_virtual_machine_interface'):
                req_metas.append('virtual-machine-virtual-machine-interface')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_virtual-machine_read_to_meta_index

    def _ifmap_virtual_machine_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_virtual_machine_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('virtual-machine-service-instance' in existing_metas):
             for meta_info in existing_metas['virtual-machine-service-instance']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('service_instance_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['service_instance_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('service-instance', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-machine-service-instance')

        (ok, result) = self._ifmap_virtual_machine_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_virtual_machine_update

    def _ifmap_virtual_machine_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        return (True, children_fq_names)
    #end _ifmap_virtual_machine_list

    def _ifmap_virtual_machine_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:virtual-machine-service-instance'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_virtual_machine_delete

    def _ifmap_interface_route_table_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.interface_route_table_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_interface_route_table_alloc

    def _ifmap_interface_route_table_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('interface_route_table_routes', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['interface_route_table_routes']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                RouteTableType(**field).exportChildren(buf, level = 1, name_ = 'interface-route-table-routes', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'interface-route-table-routes', pretty_print = False)
            interface_route_table_routes_xml = buf.getvalue()
            buf.close()
            meta = Metadata('interface-route-table-routes' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = interface_route_table_routes_xml)

            if (existing_metas and 'interface-route-table-routes' in existing_metas and
                str(existing_metas['interface-route-table-routes'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_interface_route_table_set

    def _ifmap_interface_route_table_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['interface-route-table']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_interface_route_table_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_interface_route_table_create


    def _ifmap_interface_route_table_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_interface_route_table_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'interface-route-table'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:virtual-machine-interface-route-table'):
            result['virtual_machine_interface_back_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-route-table']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_machine_interface', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_machine_interface_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:interface-route-table-routes'):
            result_elems = metas['contrail:interface-route-table-routes']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = RouteTableType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['interface_route_table_routes'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_interface_route_table_read

    def _ifmap_interface_route_table_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'virtual-machine-interface-route-table', u'interface-route-table-routes', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_machine_interface_back_refs'):
                req_metas.append('virtual-machine-interface-route-table')
            if field_names.has_key('interface_route_table_routes'):
                req_metas.append('interface-route-table-routes')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_interface-route-table_read_to_meta_index

    def _ifmap_interface_route_table_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_interface_route_table_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('interface-route-table-routes' in existing_metas) and ('interface_route_table_routes' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:interface-route-table-routes')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        (ok, result) = self._ifmap_interface_route_table_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_interface_route_table_update

    def _ifmap_interface_route_table_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['interface-route-table']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_interface_route_table_list

    def _ifmap_interface_route_table_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_interface_route_table_delete

    def _ifmap_service_template_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.service_template_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_service_template_alloc

    def _ifmap_service_template_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('service_template_properties', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['service_template_properties']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                ServiceTemplateType(**field).exportChildren(buf, level = 1, name_ = 'service-template-properties', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'service-template-properties', pretty_print = False)
            service_template_properties_xml = buf.getvalue()
            buf.close()
            meta = Metadata('service-template-properties' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = service_template_properties_xml)

            if (existing_metas and 'service-template-properties' in existing_metas and
                str(existing_metas['service-template-properties'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_service_template_set

    def _ifmap_service_template_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['service-template']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_service_template_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_service_template_create


    def _ifmap_service_template_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_service_template_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'service-template'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:service-instance-service-template'):
            result['service_instance_back_refs'] = []
            result_elems = metas['contrail:service-instance-service-template']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('service_instance', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['service_instance_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:service-template-properties'):
            result_elems = metas['contrail:service-template-properties']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = ServiceTemplateType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['service_template_properties'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_service_template_read

    def _ifmap_service_template_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'service-instance-service-template', u'service-template-properties', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('service_instance_back_refs'):
                req_metas.append('service-instance-service-template')
            if field_names.has_key('service_template_properties'):
                req_metas.append('service-template-properties')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_service-template_read_to_meta_index

    def _ifmap_service_template_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_service_template_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('service-template-properties' in existing_metas) and ('service_template_properties' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:service-template-properties')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        (ok, result) = self._ifmap_service_template_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_service_template_update

    def _ifmap_service_template_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['service-template']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_service_template_list

    def _ifmap_service_template_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_service_template_delete

    def _ifmap_virtual_ip_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.virtual_ip_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_virtual_ip_alloc

    def _ifmap_virtual_ip_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('virtual_ip_properties', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['virtual_ip_properties']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                VirtualIpType(**field).exportChildren(buf, level = 1, name_ = 'virtual-ip-properties', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'virtual-ip-properties', pretty_print = False)
            virtual_ip_properties_xml = buf.getvalue()
            buf.close()
            meta = Metadata('virtual-ip-properties' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = virtual_ip_properties_xml)

            if (existing_metas and 'virtual-ip-properties' in existing_metas and
                str(existing_metas['virtual-ip-properties'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('loadbalancer_pool_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'loadbalancer-pool'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_ip_loadbalancer_pool_xml = ''
                meta = Metadata('virtual-ip-loadbalancer-pool' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_ip_loadbalancer_pool_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_ip_virtual_machine_interface_xml = ''
                meta = Metadata('virtual-ip-virtual-machine-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_ip_virtual_machine_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_virtual_ip_set

    def _ifmap_virtual_ip_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['virtual-ip']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_virtual_ip_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_virtual_ip_create


    def _ifmap_virtual_ip_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_virtual_ip_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'virtual-ip'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:virtual-ip-loadbalancer-pool'):
            result['loadbalancer_pool_refs'] = []
            result_elems = metas['contrail:virtual-ip-loadbalancer-pool']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('loadbalancer_pool', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['loadbalancer_pool_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-ip-virtual-machine-interface'):
            result['virtual_machine_interface_refs'] = []
            result_elems = metas['contrail:virtual-ip-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine_interface', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_interface_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-ip-properties'):
            result_elems = metas['contrail:virtual-ip-properties']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = VirtualIpType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['virtual_ip_properties'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_virtual_ip_read

    def _ifmap_virtual_ip_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'virtual-ip-loadbalancer-pool', u'virtual-ip-virtual-machine-interface', u'virtual-ip-properties', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('loadbalancer_pool_refs'):
                req_metas.append('virtual-ip-loadbalancer-pool')
            if field_names.has_key('virtual_machine_interface_refs'):
                req_metas.append('virtual-ip-virtual-machine-interface')
            if field_names.has_key('virtual_ip_properties'):
                req_metas.append('virtual-ip-properties')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_virtual-ip_read_to_meta_index

    def _ifmap_virtual_ip_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_virtual_ip_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('virtual-ip-properties' in existing_metas) and ('virtual_ip_properties' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-ip-properties')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('virtual-ip-loadbalancer-pool' in existing_metas):
             for meta_info in existing_metas['virtual-ip-loadbalancer-pool']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('loadbalancer_pool_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['loadbalancer_pool_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('loadbalancer-pool', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-ip-loadbalancer-pool')

        old_refs = []
        if ('virtual-ip-virtual-machine-interface' in existing_metas):
             for meta_info in existing_metas['virtual-ip-virtual-machine-interface']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_machine_interface_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_machine_interface_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-machine-interface', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-ip-virtual-machine-interface')

        (ok, result) = self._ifmap_virtual_ip_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_virtual_ip_update

    def _ifmap_virtual_ip_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['virtual-ip']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_virtual_ip_list

    def _ifmap_virtual_ip_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:virtual-ip-loadbalancer-pool', u'contrail:virtual-ip-virtual-machine-interface'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_virtual_ip_delete

    def _ifmap_security_group_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.security_group_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_security_group_alloc

    def _ifmap_security_group_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('security_group_id', None)
        if field is not None:
            norm_str = escape(str(obj_dict['security_group_id']))
            meta = Metadata('security-group-id', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'security-group-id' in existing_metas and
                str(existing_metas['security-group-id'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('security_group_entries', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['security_group_entries']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                PolicyEntriesType(**field).exportChildren(buf, level = 1, name_ = 'security-group-entries', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'security-group-entries', pretty_print = False)
            security_group_entries_xml = buf.getvalue()
            buf.close()
            meta = Metadata('security-group-entries' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = security_group_entries_xml)

            if (existing_metas and 'security-group-entries' in existing_metas and
                str(existing_metas['security-group-entries'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('access_control_list_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'access-control-list'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                security_group_access_control_list_xml = ''
                meta = Metadata('security-group-access-control-list' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = security_group_access_control_list_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_security_group_set

    def _ifmap_security_group_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['security-group']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_security_group_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_security_group_create


    def _ifmap_security_group_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_security_group_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'security-group'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:virtual-machine-interface-security-group'):
            result['virtual_machine_interface_back_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-security-group']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_machine_interface', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_machine_interface_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:security-group-id'):
            result_elems = metas['contrail:security-group-id']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['security_group_id'] = obj_val

        if metas.has_key('contrail:security-group-entries'):
            result_elems = metas['contrail:security-group-entries']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = PolicyEntriesType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['security_group_entries'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:security-group-access-control-list'):
            result['access_control_lists'] = []
            result_elems = metas['contrail:security-group-access-control-list']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('access_control_list', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['access_control_lists'].append(has_info)


        return (True, result)
    #end _ifmap_security_group_read

    def _ifmap_security_group_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'virtual-machine-interface-security-group', u'security-group-id', u'security-group-entries', u'id-perms', u'display-name', u'security-group-access-control-list']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_machine_interface_back_refs'):
                req_metas.append('virtual-machine-interface-security-group')
            if field_names.has_key('security_group_id'):
                req_metas.append('security-group-id')
            if field_names.has_key('security_group_entries'):
                req_metas.append('security-group-entries')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('security_group_access_control_list'):
                req_metas.append('security-group-access-control-list')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_security-group_read_to_meta_index

    def _ifmap_security_group_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_security_group_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('security-group-id' in existing_metas) and ('security_group_id' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:security-group-id')

        if ('security-group-entries' in existing_metas) and ('security_group_entries' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:security-group-entries')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        (ok, result) = self._ifmap_security_group_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_security_group_update

    def _ifmap_security_group_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['security-group']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_security_group_list

    def _ifmap_security_group_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_security_group_delete

    def _ifmap_provider_attachment_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.provider_attachment_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_provider_attachment_alloc

    def _ifmap_provider_attachment_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_router_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-router'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                provider_attachment_virtual_router_xml = ''
                meta = Metadata('provider-attachment-virtual-router' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = provider_attachment_virtual_router_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_provider_attachment_set

    def _ifmap_provider_attachment_create(self, obj_ids, obj_dict):
        (ok, result) = self._ifmap_provider_attachment_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_provider_attachment_create


    def _ifmap_provider_attachment_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_provider_attachment_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'provider-attachment'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:provider-attachment-virtual-router'):
            result['virtual_router_refs'] = []
            result_elems = metas['contrail:provider-attachment-virtual-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_router', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_router_refs'].append(ref_info)


        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_provider_attachment_read

    def _ifmap_provider_attachment_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'provider-attachment-virtual-router', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_router_refs'):
                req_metas.append('provider-attachment-virtual-router')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_provider-attachment_read_to_meta_index

    def _ifmap_provider_attachment_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_provider_attachment_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('provider-attachment-virtual-router' in existing_metas):
             for meta_info in existing_metas['provider-attachment-virtual-router']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_router_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_router_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-router', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:provider-attachment-virtual-router')

        (ok, result) = self._ifmap_provider_attachment_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_provider_attachment_update

    def _ifmap_provider_attachment_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        return (True, children_fq_names)
    #end _ifmap_provider_attachment_list

    def _ifmap_provider_attachment_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:provider-attachment-virtual-router'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_provider_attachment_delete

    def _ifmap_network_ipam_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.network_ipam_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_network_ipam_alloc

    def _ifmap_network_ipam_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('network_ipam_mgmt', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['network_ipam_mgmt']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IpamType(**field).exportChildren(buf, level = 1, name_ = 'network-ipam-mgmt', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'network-ipam-mgmt', pretty_print = False)
            network_ipam_mgmt_xml = buf.getvalue()
            buf.close()
            meta = Metadata('network-ipam-mgmt' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = network_ipam_mgmt_xml)

            if (existing_metas and 'network-ipam-mgmt' in existing_metas and
                str(existing_metas['network-ipam-mgmt'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_DNS_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-DNS'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                network_ipam_virtual_DNS_xml = ''
                meta = Metadata('network-ipam-virtual-DNS' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = network_ipam_virtual_DNS_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_network_ipam_set

    def _ifmap_network_ipam_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['network-ipam']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_network_ipam_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_network_ipam_create


    def _ifmap_network_ipam_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_network_ipam_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'network-ipam'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:network-ipam-virtual-DNS'):
            result['virtual_DNS_refs'] = []
            result_elems = metas['contrail:network-ipam-virtual-DNS']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_DNS', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_DNS_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-network-network-ipam'):
            result['virtual_network_back_refs'] = []
            result_elems = metas['contrail:virtual-network-network-ipam']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = VnSubnetsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_network', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_network_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:network-ipam-mgmt'):
            result_elems = metas['contrail:network-ipam-mgmt']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IpamType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['network_ipam_mgmt'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_network_ipam_read

    def _ifmap_network_ipam_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'network-ipam-virtual-DNS', u'virtual-network-network-ipam', u'network-ipam-mgmt', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_DNS_refs'):
                req_metas.append('network-ipam-virtual-DNS')
            if field_names.has_key('virtual_network_back_refs'):
                req_metas.append('virtual-network-network-ipam')
            if field_names.has_key('network_ipam_mgmt'):
                req_metas.append('network-ipam-mgmt')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_network-ipam_read_to_meta_index

    def _ifmap_network_ipam_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_network_ipam_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('network-ipam-mgmt' in existing_metas) and ('network_ipam_mgmt' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:network-ipam-mgmt')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('network-ipam-virtual-DNS' in existing_metas):
             for meta_info in existing_metas['network-ipam-virtual-DNS']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_DNS_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_DNS_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-DNS', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:network-ipam-virtual-DNS')

        (ok, result) = self._ifmap_network_ipam_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_network_ipam_update

    def _ifmap_network_ipam_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['network-ipam']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_network_ipam_list

    def _ifmap_network_ipam_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:network-ipam-virtual-DNS'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_network_ipam_delete

    def _ifmap_loadbalancer_healthmonitor_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.loadbalancer_healthmonitor_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_loadbalancer_healthmonitor_alloc

    def _ifmap_loadbalancer_healthmonitor_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('loadbalancer_healthmonitor_properties', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['loadbalancer_healthmonitor_properties']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                LoadbalancerHealthmonitorType(**field).exportChildren(buf, level = 1, name_ = 'loadbalancer-healthmonitor-properties', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'loadbalancer-healthmonitor-properties', pretty_print = False)
            loadbalancer_healthmonitor_properties_xml = buf.getvalue()
            buf.close()
            meta = Metadata('loadbalancer-healthmonitor-properties' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = loadbalancer_healthmonitor_properties_xml)

            if (existing_metas and 'loadbalancer-healthmonitor-properties' in existing_metas and
                str(existing_metas['loadbalancer-healthmonitor-properties'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_loadbalancer_healthmonitor_set

    def _ifmap_loadbalancer_healthmonitor_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['loadbalancer-healthmonitor']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_loadbalancer_healthmonitor_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_loadbalancer_healthmonitor_create


    def _ifmap_loadbalancer_healthmonitor_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_loadbalancer_healthmonitor_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'loadbalancer-healthmonitor'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:loadbalancer-pool-loadbalancer-healthmonitor'):
            result['loadbalancer_pool_back_refs'] = []
            result_elems = metas['contrail:loadbalancer-pool-loadbalancer-healthmonitor']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('loadbalancer_pool', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['loadbalancer_pool_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:loadbalancer-healthmonitor-properties'):
            result_elems = metas['contrail:loadbalancer-healthmonitor-properties']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = LoadbalancerHealthmonitorType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['loadbalancer_healthmonitor_properties'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_loadbalancer_healthmonitor_read

    def _ifmap_loadbalancer_healthmonitor_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'loadbalancer-pool-loadbalancer-healthmonitor', u'loadbalancer-healthmonitor-properties', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('loadbalancer_pool_back_refs'):
                req_metas.append('loadbalancer-pool-loadbalancer-healthmonitor')
            if field_names.has_key('loadbalancer_healthmonitor_properties'):
                req_metas.append('loadbalancer-healthmonitor-properties')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_loadbalancer-healthmonitor_read_to_meta_index

    def _ifmap_loadbalancer_healthmonitor_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_loadbalancer_healthmonitor_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('loadbalancer-healthmonitor-properties' in existing_metas) and ('loadbalancer_healthmonitor_properties' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:loadbalancer-healthmonitor-properties')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        (ok, result) = self._ifmap_loadbalancer_healthmonitor_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_loadbalancer_healthmonitor_update

    def _ifmap_loadbalancer_healthmonitor_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['loadbalancer-healthmonitor']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_loadbalancer_healthmonitor_list

    def _ifmap_loadbalancer_healthmonitor_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))

        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_loadbalancer_healthmonitor_delete

    def _ifmap_virtual_network_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.virtual_network_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_virtual_network_alloc

    def _ifmap_virtual_network_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('virtual_network_properties', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['virtual_network_properties']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                VirtualNetworkType(**field).exportChildren(buf, level = 1, name_ = 'virtual-network-properties', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'virtual-network-properties', pretty_print = False)
            virtual_network_properties_xml = buf.getvalue()
            buf.close()
            meta = Metadata('virtual-network-properties' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = virtual_network_properties_xml)

            if (existing_metas and 'virtual-network-properties' in existing_metas and
                str(existing_metas['virtual-network-properties'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('route_target_list', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['route_target_list']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                RouteTargetList(**field).exportChildren(buf, level = 1, name_ = 'route-target-list', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'route-target-list', pretty_print = False)
            route_target_list_xml = buf.getvalue()
            buf.close()
            meta = Metadata('route-target-list' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = route_target_list_xml)

            if (existing_metas and 'route-target-list' in existing_metas and
                str(existing_metas['route-target-list'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('router_external', None)
        if field is not None:
            norm_str = escape(str(obj_dict['router_external']))
            meta = Metadata('router-external', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'router-external' in existing_metas and
                str(existing_metas['router-external'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('is_shared', None)
        if field is not None:
            norm_str = escape(str(obj_dict['is_shared']))
            meta = Metadata('is-shared', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'is-shared' in existing_metas and
                str(existing_metas['is-shared'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('network_ipam_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'network-ipam'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_network_network_ipam_xml = ''
                ref_data = ref['attr']
                if ref_data:
                    buf = cStringIO.StringIO()
                    VnSubnetsType(**ref_data).exportChildren(buf, level = 1, name_ = 'virtual-network-network-ipam', pretty_print = False)
                    virtual_network_network_ipam_xml = virtual_network_network_ipam_xml + buf.getvalue()
                    buf.close()
                meta = Metadata('virtual-network-network-ipam' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_network_network_ipam_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('network_policy_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'network-policy'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_network_network_policy_xml = ''
                ref_data = ref['attr']
                if ref_data:
                    buf = cStringIO.StringIO()
                    VirtualNetworkPolicyType(**ref_data).exportChildren(buf, level = 1, name_ = 'virtual-network-network-policy', pretty_print = False)
                    virtual_network_network_policy_xml = virtual_network_network_policy_xml + buf.getvalue()
                    buf.close()
                meta = Metadata('virtual-network-network-policy' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_network_network_policy_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('access_control_list_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'access-control-list'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_network_access_control_list_xml = ''
                meta = Metadata('virtual-network-access-control-list' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_network_access_control_list_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('floating_ip_pool_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'floating-ip-pool'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_network_floating_ip_pool_xml = ''
                meta = Metadata('virtual-network-floating-ip-pool' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_network_floating_ip_pool_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('routing_instance_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'routing-instance'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_network_routing_instance_xml = ''
                meta = Metadata('virtual-network-routing-instance' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_network_routing_instance_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('route_table_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'route-table'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_network_route_table_xml = ''
                meta = Metadata('virtual-network-route-table' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_network_route_table_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_virtual_network_set

    def _ifmap_virtual_network_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['virtual-network']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_virtual_network_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_virtual_network_create


    def _ifmap_virtual_network_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_virtual_network_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'virtual-network'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:virtual-network-network-ipam'):
            result['network_ipam_refs'] = []
            result_elems = metas['contrail:virtual-network-network-ipam']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = VnSubnetsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('network_ipam', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['network_ipam_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-network-network-policy'):
            result['network_policy_refs'] = []
            result_elems = metas['contrail:virtual-network-network-policy']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = VirtualNetworkPolicyType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('network_policy', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['network_policy_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-network-route-table'):
            result['route_table_refs'] = []
            result_elems = metas['contrail:virtual-network-route-table']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('route_table', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['route_table_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-machine-interface-virtual-network'):
            result['virtual_machine_interface_back_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-virtual-network']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_machine_interface', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_machine_interface_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:instance-ip-virtual-network'):
            result['instance_ip_back_refs'] = []
            result_elems = metas['contrail:instance-ip-virtual-network']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('instance_ip', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['instance_ip_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:physical-router-virtual-network'):
            result['physical_router_back_refs'] = []
            result_elems = metas['contrail:physical-router-virtual-network']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('physical_router', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['physical_router_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:logical-router-gateway'):
            result['logical_router_back_refs'] = []
            result_elems = metas['contrail:logical-router-gateway']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('logical_router', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['logical_router_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:virtual-network-properties'):
            result_elems = metas['contrail:virtual-network-properties']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = VirtualNetworkType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['virtual_network_properties'] = obj_val

        if metas.has_key('contrail:route-target-list'):
            result_elems = metas['contrail:route-target-list']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = RouteTargetList()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['route_target_list'] = obj_val

        if metas.has_key('contrail:router-external'):
            result_elems = metas['contrail:router-external']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['router_external'] = obj_val

        if metas.has_key('contrail:is-shared'):
            result_elems = metas['contrail:is-shared']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['is_shared'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:virtual-network-access-control-list'):
            result['access_control_lists'] = []
            result_elems = metas['contrail:virtual-network-access-control-list']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('access_control_list', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['access_control_lists'].append(has_info)


        if metas.has_key('contrail:virtual-network-floating-ip-pool'):
            result['floating_ip_pools'] = []
            result_elems = metas['contrail:virtual-network-floating-ip-pool']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('floating_ip_pool', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['floating_ip_pools'].append(has_info)


        if metas.has_key('contrail:virtual-network-routing-instance'):
            result['routing_instances'] = []
            result_elems = metas['contrail:virtual-network-routing-instance']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('routing_instance', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['routing_instances'].append(has_info)


        return (True, result)
    #end _ifmap_virtual_network_read

    def _ifmap_virtual_network_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'virtual-network-network-ipam', u'virtual-network-network-policy', u'virtual-network-route-table', u'virtual-machine-interface-virtual-network', u'instance-ip-virtual-network', u'physical-router-virtual-network', u'logical-router-gateway', u'virtual-network-properties', u'route-target-list', u'router-external', u'is-shared', u'id-perms', u'display-name', u'virtual-network-access-control-list', u'virtual-network-floating-ip-pool', u'virtual-network-routing-instance']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('network_ipam_refs'):
                req_metas.append('virtual-network-network-ipam')
            if field_names.has_key('network_policy_refs'):
                req_metas.append('virtual-network-network-policy')
            if field_names.has_key('route_table_refs'):
                req_metas.append('virtual-network-route-table')
            if field_names.has_key('virtual_machine_interface_back_refs'):
                req_metas.append('virtual-machine-interface-virtual-network')
            if field_names.has_key('instance_ip_back_refs'):
                req_metas.append('instance-ip-virtual-network')
            if field_names.has_key('physical_router_back_refs'):
                req_metas.append('physical-router-virtual-network')
            if field_names.has_key('logical_router_back_refs'):
                req_metas.append('logical-router-gateway')
            if field_names.has_key('virtual_network_properties'):
                req_metas.append('virtual-network-properties')
            if field_names.has_key('route_target_list'):
                req_metas.append('route-target-list')
            if field_names.has_key('router_external'):
                req_metas.append('router-external')
            if field_names.has_key('is_shared'):
                req_metas.append('is-shared')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('virtual_network_access_control_list'):
                req_metas.append('virtual-network-access-control-list')
            if field_names.has_key('virtual_network_floating_ip_pool'):
                req_metas.append('virtual-network-floating-ip-pool')
            if field_names.has_key('virtual_network_routing_instance'):
                req_metas.append('virtual-network-routing-instance')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_virtual-network_read_to_meta_index

    def _ifmap_virtual_network_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_virtual_network_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('virtual-network-properties' in existing_metas) and ('virtual_network_properties' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-network-properties')

        if ('route-target-list' in existing_metas) and ('route_target_list' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:route-target-list')

        if ('router-external' in existing_metas) and ('router_external' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:router-external')

        if ('is-shared' in existing_metas) and ('is_shared' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:is-shared')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('virtual-network-network-ipam' in existing_metas):
             for meta_info in existing_metas['virtual-network-network-ipam']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('network_ipam_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['network_ipam_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('network-ipam', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-network-network-ipam')

        old_refs = []
        if ('virtual-network-network-policy' in existing_metas):
             for meta_info in existing_metas['virtual-network-network-policy']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('network_policy_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['network_policy_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('network-policy', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-network-network-policy')

        old_refs = []
        if ('virtual-network-route-table' in existing_metas):
             for meta_info in existing_metas['virtual-network-route-table']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('route_table_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['route_table_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('route-table', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-network-route-table')

        (ok, result) = self._ifmap_virtual_network_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_virtual_network_update

    def _ifmap_virtual_network_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['virtual-network']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_virtual_network_list

    def _ifmap_virtual_network_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:virtual-network-network-ipam', u'contrail:virtual-network-network-policy', u'contrail:virtual-network-route-table'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_virtual_network_delete

    def _ifmap_project_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.project_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_project_alloc

    def _ifmap_project_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('quota', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['quota']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                QuotaType(**field).exportChildren(buf, level = 1, name_ = 'quota', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'quota', pretty_print = False)
            quota_xml = buf.getvalue()
            buf.close()
            meta = Metadata('quota' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = quota_xml)

            if (existing_metas and 'quota' in existing_metas and
                str(existing_metas['quota'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('namespace_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'namespace'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_namespace_xml = ''
                ref_data = ref['attr']
                if ref_data:
                    buf = cStringIO.StringIO()
                    SubnetType(**ref_data).exportChildren(buf, level = 1, name_ = 'project-namespace', pretty_print = False)
                    project_namespace_xml = project_namespace_xml + buf.getvalue()
                    buf.close()
                meta = Metadata('project-namespace' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_namespace_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('security_group_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'security-group'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_security_group_xml = ''
                meta = Metadata('project-security-group' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_security_group_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_network_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-network'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_virtual_network_xml = ''
                meta = Metadata('project-virtual-network' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_virtual_network_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('network_ipam_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'network-ipam'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_network_ipam_xml = ''
                meta = Metadata('project-network-ipam' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_network_ipam_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('network_policy_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'network-policy'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_network_policy_xml = ''
                meta = Metadata('project-network-policy' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_network_policy_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_virtual_machine_interface_xml = ''
                meta = Metadata('project-virtual-machine-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_virtual_machine_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('floating_ip_pool_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'floating-ip-pool'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_floating_ip_pool_xml = ''
                meta = Metadata('project-floating-ip-pool' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_floating_ip_pool_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('service_instance_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'service-instance'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_service_instance_xml = ''
                meta = Metadata('project-service-instance' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_service_instance_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('route_table_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'route-table'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_route_table_xml = ''
                meta = Metadata('project-route-table' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_route_table_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('interface_route_table_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'interface-route-table'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_interface_route_table_xml = ''
                meta = Metadata('project-interface-route-table' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_interface_route_table_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('logical_router_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'logical-router'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_logical_router_xml = ''
                meta = Metadata('project-logical-router' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_logical_router_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('loadbalancer_pool_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'loadbalancer-pool'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_loadbalancer_pool_xml = ''
                meta = Metadata('project-loadbalancer-pool' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_loadbalancer_pool_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('loadbalancer_healthmonitor_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'loadbalancer-healthmonitor'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_loadbalancer_healthmonitor_xml = ''
                meta = Metadata('project-loadbalancer-healthmonitor' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_loadbalancer_healthmonitor_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_ip_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-ip'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                project_virtual_ip_xml = ''
                meta = Metadata('project-virtual-ip' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = project_virtual_ip_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_project_set

    def _ifmap_project_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['project']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_project_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_project_create


    def _ifmap_project_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_project_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'project'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:project-namespace'):
            result['namespace_refs'] = []
            result_elems = metas['contrail:project-namespace']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = SubnetType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('namespace', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['namespace_refs'].append(ref_info)


        if metas.has_key('contrail:project-floating-ip-pool'):
            result['floating_ip_pool_refs'] = []
            result_elems = metas['contrail:project-floating-ip-pool']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('floating_ip_pool', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['floating_ip_pool_refs'].append(ref_info)


        if metas.has_key('contrail:floating-ip-project'):
            result['floating_ip_back_refs'] = []
            result_elems = metas['contrail:floating-ip-project']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('floating_ip', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['floating_ip_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:quota'):
            result_elems = metas['contrail:quota']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = QuotaType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['quota'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:project-security-group'):
            result['security_groups'] = []
            result_elems = metas['contrail:project-security-group']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('security_group', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['security_groups'].append(has_info)


        if metas.has_key('contrail:project-virtual-network'):
            result['virtual_networks'] = []
            result_elems = metas['contrail:project-virtual-network']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('virtual_network', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['virtual_networks'].append(has_info)


        if metas.has_key('contrail:project-network-ipam'):
            result['network_ipams'] = []
            result_elems = metas['contrail:project-network-ipam']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('network_ipam', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['network_ipams'].append(has_info)


        if metas.has_key('contrail:project-network-policy'):
            result['network_policys'] = []
            result_elems = metas['contrail:project-network-policy']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('network_policy', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['network_policys'].append(has_info)


        if metas.has_key('contrail:project-virtual-machine-interface'):
            result['virtual_machine_interfaces'] = []
            result_elems = metas['contrail:project-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('virtual_machine_interface', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['virtual_machine_interfaces'].append(has_info)


        if metas.has_key('contrail:project-service-instance'):
            result['service_instances'] = []
            result_elems = metas['contrail:project-service-instance']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('service_instance', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['service_instances'].append(has_info)


        if metas.has_key('contrail:project-route-table'):
            result['route_tables'] = []
            result_elems = metas['contrail:project-route-table']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('route_table', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['route_tables'].append(has_info)


        if metas.has_key('contrail:project-interface-route-table'):
            result['interface_route_tables'] = []
            result_elems = metas['contrail:project-interface-route-table']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('interface_route_table', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['interface_route_tables'].append(has_info)


        if metas.has_key('contrail:project-logical-router'):
            result['logical_routers'] = []
            result_elems = metas['contrail:project-logical-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('logical_router', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['logical_routers'].append(has_info)


        if metas.has_key('contrail:project-loadbalancer-pool'):
            result['loadbalancer_pools'] = []
            result_elems = metas['contrail:project-loadbalancer-pool']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('loadbalancer_pool', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['loadbalancer_pools'].append(has_info)


        if metas.has_key('contrail:project-loadbalancer-healthmonitor'):
            result['loadbalancer_healthmonitors'] = []
            result_elems = metas['contrail:project-loadbalancer-healthmonitor']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('loadbalancer_healthmonitor', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['loadbalancer_healthmonitors'].append(has_info)


        if metas.has_key('contrail:project-virtual-ip'):
            result['virtual_ips'] = []
            result_elems = metas['contrail:project-virtual-ip']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('virtual_ip', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['virtual_ips'].append(has_info)


        return (True, result)
    #end _ifmap_project_read

    def _ifmap_project_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'project-namespace', u'project-floating-ip-pool', u'floating-ip-project', u'quota', u'id-perms', u'display-name', u'project-security-group', u'project-virtual-network', u'project-network-ipam', u'project-network-policy', u'project-virtual-machine-interface', u'project-service-instance', u'project-route-table', u'project-interface-route-table', u'project-logical-router', u'project-loadbalancer-pool', u'project-loadbalancer-healthmonitor', u'project-virtual-ip']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('namespace_refs'):
                req_metas.append('project-namespace')
            if field_names.has_key('floating_ip_pool_refs'):
                req_metas.append('project-floating-ip-pool')
            if field_names.has_key('floating_ip_back_refs'):
                req_metas.append('floating-ip-project')
            if field_names.has_key('quota'):
                req_metas.append('quota')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('project_security_group'):
                req_metas.append('project-security-group')
            if field_names.has_key('project_virtual_network'):
                req_metas.append('project-virtual-network')
            if field_names.has_key('project_network_ipam'):
                req_metas.append('project-network-ipam')
            if field_names.has_key('project_network_policy'):
                req_metas.append('project-network-policy')
            if field_names.has_key('project_virtual_machine_interface'):
                req_metas.append('project-virtual-machine-interface')
            if field_names.has_key('project_service_instance'):
                req_metas.append('project-service-instance')
            if field_names.has_key('project_route_table'):
                req_metas.append('project-route-table')
            if field_names.has_key('project_interface_route_table'):
                req_metas.append('project-interface-route-table')
            if field_names.has_key('project_logical_router'):
                req_metas.append('project-logical-router')
            if field_names.has_key('project_loadbalancer_pool'):
                req_metas.append('project-loadbalancer-pool')
            if field_names.has_key('project_loadbalancer_healthmonitor'):
                req_metas.append('project-loadbalancer-healthmonitor')
            if field_names.has_key('project_virtual_ip'):
                req_metas.append('project-virtual-ip')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_project_read_to_meta_index

    def _ifmap_project_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_project_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('quota' in existing_metas) and ('quota' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:quota')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('project-namespace' in existing_metas):
             for meta_info in existing_metas['project-namespace']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('namespace_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['namespace_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('namespace', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:project-namespace')

        old_refs = []
        if ('project-floating-ip-pool' in existing_metas):
             for meta_info in existing_metas['project-floating-ip-pool']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('floating_ip_pool_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['floating_ip_pool_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('floating-ip-pool', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:project-floating-ip-pool')

        (ok, result) = self._ifmap_project_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_project_update

    def _ifmap_project_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['project']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_project_list

    def _ifmap_project_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:project-namespace', u'contrail:project-floating-ip-pool'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_project_delete

    def _ifmap_logical_interface_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.logical_interface_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_logical_interface_alloc

    def _ifmap_logical_interface_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('logical_interface_vlan_tag', None)
        if field is not None:
            norm_str = escape(str(obj_dict['logical_interface_vlan_tag']))
            meta = Metadata('logical-interface-vlan-tag', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'logical-interface-vlan-tag' in existing_metas and
                str(existing_metas['logical-interface-vlan-tag'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('logical_interface_type', None)
        if field is not None:
            norm_str = escape(str(obj_dict['logical_interface_type']))
            meta = Metadata('logical-interface-type', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'logical-interface-type' in existing_metas and
                str(existing_metas['logical-interface-type'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                logical_interface_virtual_machine_interface_xml = ''
                meta = Metadata('logical-interface-virtual-machine-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = logical_interface_virtual_machine_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_logical_interface_set

    def _ifmap_logical_interface_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['logical-interface']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_logical_interface_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_logical_interface_create


    def _ifmap_logical_interface_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_logical_interface_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'logical-interface'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:logical-interface-virtual-machine-interface'):
            result['virtual_machine_interface_refs'] = []
            result_elems = metas['contrail:logical-interface-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine_interface', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_interface_refs'].append(ref_info)


        if metas.has_key('contrail:logical-interface-vlan-tag'):
            result_elems = metas['contrail:logical-interface-vlan-tag']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['logical_interface_vlan_tag'] = obj_val

        if metas.has_key('contrail:logical-interface-type'):
            result_elems = metas['contrail:logical-interface-type']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['logical_interface_type'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_logical_interface_read

    def _ifmap_logical_interface_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'logical-interface-virtual-machine-interface', u'logical-interface-vlan-tag', u'logical-interface-type', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_machine_interface_refs'):
                req_metas.append('logical-interface-virtual-machine-interface')
            if field_names.has_key('logical_interface_vlan_tag'):
                req_metas.append('logical-interface-vlan-tag')
            if field_names.has_key('logical_interface_type'):
                req_metas.append('logical-interface-type')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_logical-interface_read_to_meta_index

    def _ifmap_logical_interface_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_logical_interface_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('logical-interface-vlan-tag' in existing_metas) and ('logical_interface_vlan_tag' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:logical-interface-vlan-tag')

        if ('logical-interface-type' in existing_metas) and ('logical_interface_type' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:logical-interface-type')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('logical-interface-virtual-machine-interface' in existing_metas):
             for meta_info in existing_metas['logical-interface-virtual-machine-interface']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_machine_interface_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_machine_interface_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-machine-interface', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:logical-interface-virtual-machine-interface')

        (ok, result) = self._ifmap_logical_interface_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_logical_interface_update

    def _ifmap_logical_interface_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['logical-interface']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_logical_interface_list

    def _ifmap_logical_interface_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:logical-interface-virtual-machine-interface'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_logical_interface_delete

    def _ifmap_routing_instance_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.routing_instance_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_routing_instance_alloc

    def _ifmap_routing_instance_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('service_chain_information', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['service_chain_information']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                ServiceChainInfo(**field).exportChildren(buf, level = 1, name_ = 'service-chain-information', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'service-chain-information', pretty_print = False)
            service_chain_information_xml = buf.getvalue()
            buf.close()
            meta = Metadata('service-chain-information' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = service_chain_information_xml)

            if (existing_metas and 'service-chain-information' in existing_metas and
                str(existing_metas['service-chain-information'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('routing_instance_is_default', None)
        if field is not None:
            norm_str = escape(str(obj_dict['routing_instance_is_default']))
            meta = Metadata('routing-instance-is-default', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'routing-instance-is-default' in existing_metas and
                str(existing_metas['routing-instance-is-default'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('static_route_entries', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['static_route_entries']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                StaticRouteEntriesType(**field).exportChildren(buf, level = 1, name_ = 'static-route-entries', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'static-route-entries', pretty_print = False)
            static_route_entries_xml = buf.getvalue()
            buf.close()
            meta = Metadata('static-route-entries' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = static_route_entries_xml)

            if (existing_metas and 'static-route-entries' in existing_metas and
                str(existing_metas['static-route-entries'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('default_ce_protocol', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['default_ce_protocol']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                DefaultProtocolType(**field).exportChildren(buf, level = 1, name_ = 'default-ce-protocol', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'default-ce-protocol', pretty_print = False)
            default_ce_protocol_xml = buf.getvalue()
            buf.close()
            meta = Metadata('default-ce-protocol' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = default_ce_protocol_xml)

            if (existing_metas and 'default-ce-protocol' in existing_metas and
                str(existing_metas['default-ce-protocol'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('bgp_router_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'bgp-router'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                instance_bgp_router_xml = ''
                meta = Metadata('instance-bgp-router' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = instance_bgp_router_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('routing_instance_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'routing-instance'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                connection_xml = ''
                ref_data = ref['attr']
                if ref_data:
                    buf = cStringIO.StringIO()
                    ConnectionType(**ref_data).exportChildren(buf, level = 1, name_ = 'connection', pretty_print = False)
                    connection_xml = connection_xml + buf.getvalue()
                    buf.close()
                meta = Metadata('connection' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = connection_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('route_target_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'route-target'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                instance_target_xml = ''
                ref_data = ref['attr']
                if ref_data:
                    buf = cStringIO.StringIO()
                    InstanceTargetType(**ref_data).exportChildren(buf, level = 1, name_ = 'instance-target', pretty_print = False)
                    instance_target_xml = instance_target_xml + buf.getvalue()
                    buf.close()
                meta = Metadata('instance-target' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = instance_target_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_routing_instance_set

    def _ifmap_routing_instance_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['routing-instance']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_routing_instance_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_routing_instance_create


    def _ifmap_routing_instance_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_routing_instance_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'routing-instance'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:connection'):
            result['routing_instance_refs'] = []
            result_elems = metas['contrail:connection']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('routing_instance', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['routing_instance_refs'].append(ref_info)


        if metas.has_key('contrail:instance-target'):
            result['route_target_refs'] = []
            result_elems = metas['contrail:instance-target']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = InstanceTargetType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('route_target', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['route_target_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-machine-interface-routing-instance'):
            result['virtual_machine_interface_back_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-routing-instance']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = PolicyBasedForwardingRuleType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_machine_interface', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_machine_interface_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:connection'):
            result['routing_instance_refs'] = []
            result_elems = metas['contrail:connection']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('routing_instance', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['routing_instance_refs'].append(ref_info)


        if metas.has_key('contrail:service-chain-information'):
            result_elems = metas['contrail:service-chain-information']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = ServiceChainInfo()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['service_chain_information'] = obj_val

        if metas.has_key('contrail:routing-instance-is-default'):
            result_elems = metas['contrail:routing-instance-is-default']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['routing_instance_is_default'] = obj_val

        if metas.has_key('contrail:static-route-entries'):
            result_elems = metas['contrail:static-route-entries']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = StaticRouteEntriesType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['static_route_entries'] = obj_val

        if metas.has_key('contrail:default-ce-protocol'):
            result_elems = metas['contrail:default-ce-protocol']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = DefaultProtocolType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['default_ce_protocol'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        if metas.has_key('contrail:instance-bgp-router'):
            result['bgp_routers'] = []
            result_elems = metas['contrail:instance-bgp-router']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                has_ifmap_id = r_elem[1].attrib['name']

                try:
                    has_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for child ' + has_ifmap_id
                    return (False, err_msg)
                try:
                    has_uuid = self._db_client_mgr.ifmap_id_to_uuid(has_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for child ' + has_ifmap_id
                    return (False, err_msg)

                has_url = self._generate_url('bgp_router', has_uuid)
                has_info = {}
                has_info['to'] = has_fq_name
                has_info['attr'] = obj_val
                has_info['href'] = has_url
                has_info['uuid'] = has_uuid
                result['bgp_routers'].append(has_info)


        return (True, result)
    #end _ifmap_routing_instance_read

    def _ifmap_routing_instance_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'connection', u'instance-target', u'virtual-machine-interface-routing-instance', u'connection', u'service-chain-information', u'routing-instance-is-default', u'static-route-entries', u'default-ce-protocol', u'id-perms', u'display-name', u'instance-bgp-router']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('routing_instance_refs'):
                req_metas.append('connection')
            if field_names.has_key('route_target_refs'):
                req_metas.append('instance-target')
            if field_names.has_key('virtual_machine_interface_back_refs'):
                req_metas.append('virtual-machine-interface-routing-instance')
            if field_names.has_key('routing_instance_refs'):
                req_metas.append('connection')
            if field_names.has_key('service_chain_information'):
                req_metas.append('service-chain-information')
            if field_names.has_key('routing_instance_is_default'):
                req_metas.append('routing-instance-is-default')
            if field_names.has_key('static_route_entries'):
                req_metas.append('static-route-entries')
            if field_names.has_key('default_ce_protocol'):
                req_metas.append('default-ce-protocol')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')
            if field_names.has_key('instance_bgp_router'):
                req_metas.append('instance-bgp-router')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_routing-instance_read_to_meta_index

    def _ifmap_routing_instance_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_routing_instance_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('service-chain-information' in existing_metas) and ('service_chain_information' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:service-chain-information')

        if ('routing-instance-is-default' in existing_metas) and ('routing_instance_is_default' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:routing-instance-is-default')

        if ('static-route-entries' in existing_metas) and ('static_route_entries' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:static-route-entries')

        if ('default-ce-protocol' in existing_metas) and ('default_ce_protocol' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:default-ce-protocol')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('connection' in existing_metas):
             for meta_info in existing_metas['connection']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('routing_instance_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['routing_instance_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('routing-instance', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:connection')

        old_refs = []
        if ('instance-target' in existing_metas):
             for meta_info in existing_metas['instance-target']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('route_target_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['route_target_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('route-target', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:instance-target')

        (ok, result) = self._ifmap_routing_instance_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_routing_instance_update

    def _ifmap_routing_instance_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['routing-instance']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_routing_instance_list

    def _ifmap_routing_instance_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:connection', u'contrail:instance-target'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_routing_instance_delete

    def _ifmap_virtual_machine_interface_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.virtual_machine_interface_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_virtual_machine_interface_alloc

    def _ifmap_virtual_machine_interface_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('virtual_machine_interface_mac_addresses', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['virtual_machine_interface_mac_addresses']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                MacAddressesType(**field).exportChildren(buf, level = 1, name_ = 'virtual-machine-interface-mac-addresses', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'virtual-machine-interface-mac-addresses', pretty_print = False)
            virtual_machine_interface_mac_addresses_xml = buf.getvalue()
            buf.close()
            meta = Metadata('virtual-machine-interface-mac-addresses' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = virtual_machine_interface_mac_addresses_xml)

            if (existing_metas and 'virtual-machine-interface-mac-addresses' in existing_metas and
                str(existing_metas['virtual-machine-interface-mac-addresses'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('virtual_machine_interface_dhcp_option_list', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['virtual_machine_interface_dhcp_option_list']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                DhcpOptionsListType(**field).exportChildren(buf, level = 1, name_ = 'virtual-machine-interface-dhcp-option-list', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'virtual-machine-interface-dhcp-option-list', pretty_print = False)
            virtual_machine_interface_dhcp_option_list_xml = buf.getvalue()
            buf.close()
            meta = Metadata('virtual-machine-interface-dhcp-option-list' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = virtual_machine_interface_dhcp_option_list_xml)

            if (existing_metas and 'virtual-machine-interface-dhcp-option-list' in existing_metas and
                str(existing_metas['virtual-machine-interface-dhcp-option-list'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('virtual_machine_interface_host_routes', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['virtual_machine_interface_host_routes']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                RouteTableType(**field).exportChildren(buf, level = 1, name_ = 'virtual-machine-interface-host-routes', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'virtual-machine-interface-host-routes', pretty_print = False)
            virtual_machine_interface_host_routes_xml = buf.getvalue()
            buf.close()
            meta = Metadata('virtual-machine-interface-host-routes' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = virtual_machine_interface_host_routes_xml)

            if (existing_metas and 'virtual-machine-interface-host-routes' in existing_metas and
                str(existing_metas['virtual-machine-interface-host-routes'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('virtual_machine_interface_allowed_address_pairs', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['virtual_machine_interface_allowed_address_pairs']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                AllowedAddressPairs(**field).exportChildren(buf, level = 1, name_ = 'virtual-machine-interface-allowed-address-pairs', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'virtual-machine-interface-allowed-address-pairs', pretty_print = False)
            virtual_machine_interface_allowed_address_pairs_xml = buf.getvalue()
            buf.close()
            meta = Metadata('virtual-machine-interface-allowed-address-pairs' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = virtual_machine_interface_allowed_address_pairs_xml)

            if (existing_metas and 'virtual-machine-interface-allowed-address-pairs' in existing_metas and
                str(existing_metas['virtual-machine-interface-allowed-address-pairs'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('vrf_assign_table', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['vrf_assign_table']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                VrfAssignTableType(**field).exportChildren(buf, level = 1, name_ = 'vrf-assign-table', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'vrf-assign-table', pretty_print = False)
            vrf_assign_table_xml = buf.getvalue()
            buf.close()
            meta = Metadata('vrf-assign-table' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = vrf_assign_table_xml)

            if (existing_metas and 'vrf-assign-table' in existing_metas and
                str(existing_metas['vrf-assign-table'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('virtual_machine_interface_device_owner', None)
        if field is not None:
            norm_str = escape(str(obj_dict['virtual_machine_interface_device_owner']))
            meta = Metadata('virtual-machine-interface-device-owner', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'virtual-machine-interface-device-owner' in existing_metas and
                str(existing_metas['virtual-machine-interface-device-owner'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('virtual_machine_interface_properties', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['virtual_machine_interface_properties']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                VirtualMachineInterfacePropertiesType(**field).exportChildren(buf, level = 1, name_ = 'virtual-machine-interface-properties', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'virtual-machine-interface-properties', pretty_print = False)
            virtual_machine_interface_properties_xml = buf.getvalue()
            buf.close()
            meta = Metadata('virtual-machine-interface-properties' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = virtual_machine_interface_properties_xml)

            if (existing_metas and 'virtual-machine-interface-properties' in existing_metas and
                str(existing_metas['virtual-machine-interface-properties'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('security_group_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'security-group'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_machine_interface_security_group_xml = ''
                meta = Metadata('virtual-machine-interface-security-group' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_machine_interface_security_group_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_machine_interface_sub_interface_xml = ''
                meta = Metadata('virtual-machine-interface-sub-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_machine_interface_sub_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_machine_interface_virtual_machine_xml = ''
                meta = Metadata('virtual-machine-interface-virtual-machine' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_machine_interface_virtual_machine_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_network_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-network'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_machine_interface_virtual_network_xml = ''
                meta = Metadata('virtual-machine-interface-virtual-network' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_machine_interface_virtual_network_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('routing_instance_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'routing-instance'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_machine_interface_routing_instance_xml = ''
                ref_data = ref['attr']
                if ref_data:
                    buf = cStringIO.StringIO()
                    PolicyBasedForwardingRuleType(**ref_data).exportChildren(buf, level = 1, name_ = 'virtual-machine-interface-routing-instance', pretty_print = False)
                    virtual_machine_interface_routing_instance_xml = virtual_machine_interface_routing_instance_xml + buf.getvalue()
                    buf.close()
                meta = Metadata('virtual-machine-interface-routing-instance' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_machine_interface_routing_instance_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('interface_route_table_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'interface-route-table'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                virtual_machine_interface_route_table_xml = ''
                meta = Metadata('virtual-machine-interface-route-table' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = virtual_machine_interface_route_table_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_virtual_machine_interface_set

    def _ifmap_virtual_machine_interface_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['virtual-machine-interface']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_virtual_machine_interface_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_virtual_machine_interface_create


    def _ifmap_virtual_machine_interface_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_virtual_machine_interface_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'virtual-machine-interface'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:virtual-machine-interface-security-group'):
            result['security_group_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-security-group']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('security_group', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['security_group_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-machine-interface-sub-interface'):
            result['virtual_machine_interface_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-sub-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine_interface', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_interface_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-machine-interface-virtual-machine'):
            result['virtual_machine_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-virtual-machine']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-machine-interface-virtual-network'):
            result['virtual_network_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-virtual-network']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_network', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_network_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-machine-interface-routing-instance'):
            result['routing_instance_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-routing-instance']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = PolicyBasedForwardingRuleType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('routing_instance', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['routing_instance_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-machine-interface-route-table'):
            result['interface_route_table_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-route-table']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('interface_route_table', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['interface_route_table_refs'].append(ref_info)


        if metas.has_key('contrail:virtual-machine-interface-sub-interface'):
            result['virtual_machine_interface_refs'] = []
            result_elems = metas['contrail:virtual-machine-interface-sub-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine_interface', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_interface_refs'].append(ref_info)


        if metas.has_key('contrail:instance-ip-virtual-machine-interface'):
            result['instance_ip_back_refs'] = []
            result_elems = metas['contrail:instance-ip-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('instance_ip', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['instance_ip_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:subnet-virtual-machine-interface'):
            result['subnet_back_refs'] = []
            result_elems = metas['contrail:subnet-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('subnet', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['subnet_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:floating-ip-virtual-machine-interface'):
            result['floating_ip_back_refs'] = []
            result_elems = metas['contrail:floating-ip-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('floating_ip', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['floating_ip_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:logical-interface-virtual-machine-interface'):
            result['logical_interface_back_refs'] = []
            result_elems = metas['contrail:logical-interface-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('logical_interface', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['logical_interface_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:customer-attachment-virtual-machine-interface'):
            result['customer_attachment_back_refs'] = []
            result_elems = metas['contrail:customer-attachment-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('customer_attachment', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['customer_attachment_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:logical-router-interface'):
            result['logical_router_back_refs'] = []
            result_elems = metas['contrail:logical-router-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('logical_router', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['logical_router_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:loadbalancer-pool-virtual-machine-interface'):
            result['loadbalancer_pool_back_refs'] = []
            result_elems = metas['contrail:loadbalancer-pool-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('loadbalancer_pool', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['loadbalancer_pool_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:virtual-ip-virtual-machine-interface'):
            result['virtual_ip_back_refs'] = []
            result_elems = metas['contrail:virtual-ip-virtual-machine-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                back_ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    back_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for backref ' + back_ref_ifmap_id
                    return (False, err_msg)
                try:
                    back_ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(back_ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for backref ' + back_ref_ifmap_id
                    return (False, err_msg)

                back_ref_url = self._generate_url('virtual_ip', back_ref_uuid)
                back_ref_info = {}
                back_ref_info['to'] = back_ref_fq_name
                back_ref_info['attr'] = obj_val
                back_ref_info['href'] = back_ref_url
                back_ref_info['uuid'] = back_ref_uuid
                result['virtual_ip_back_refs'].append(back_ref_info)


        if metas.has_key('contrail:virtual-machine-interface-mac-addresses'):
            result_elems = metas['contrail:virtual-machine-interface-mac-addresses']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = MacAddressesType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['virtual_machine_interface_mac_addresses'] = obj_val

        if metas.has_key('contrail:virtual-machine-interface-dhcp-option-list'):
            result_elems = metas['contrail:virtual-machine-interface-dhcp-option-list']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = DhcpOptionsListType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['virtual_machine_interface_dhcp_option_list'] = obj_val

        if metas.has_key('contrail:virtual-machine-interface-host-routes'):
            result_elems = metas['contrail:virtual-machine-interface-host-routes']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = RouteTableType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['virtual_machine_interface_host_routes'] = obj_val

        if metas.has_key('contrail:virtual-machine-interface-allowed-address-pairs'):
            result_elems = metas['contrail:virtual-machine-interface-allowed-address-pairs']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = AllowedAddressPairs()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['virtual_machine_interface_allowed_address_pairs'] = obj_val

        if metas.has_key('contrail:vrf-assign-table'):
            result_elems = metas['contrail:vrf-assign-table']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = VrfAssignTableType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['vrf_assign_table'] = obj_val

        if metas.has_key('contrail:virtual-machine-interface-device-owner'):
            result_elems = metas['contrail:virtual-machine-interface-device-owner']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['virtual_machine_interface_device_owner'] = obj_val

        if metas.has_key('contrail:virtual-machine-interface-properties'):
            result_elems = metas['contrail:virtual-machine-interface-properties']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = VirtualMachineInterfacePropertiesType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['virtual_machine_interface_properties'] = obj_val

        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_virtual_machine_interface_read

    def _ifmap_virtual_machine_interface_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'virtual-machine-interface-security-group', u'virtual-machine-interface-sub-interface', u'virtual-machine-interface-virtual-machine', u'virtual-machine-interface-virtual-network', u'virtual-machine-interface-routing-instance', u'virtual-machine-interface-route-table', u'virtual-machine-interface-sub-interface', u'instance-ip-virtual-machine-interface', u'subnet-virtual-machine-interface', u'floating-ip-virtual-machine-interface', u'logical-interface-virtual-machine-interface', u'customer-attachment-virtual-machine-interface', u'logical-router-interface', u'loadbalancer-pool-virtual-machine-interface', u'virtual-ip-virtual-machine-interface', u'virtual-machine-interface-mac-addresses', u'virtual-machine-interface-dhcp-option-list', u'virtual-machine-interface-host-routes', u'virtual-machine-interface-allowed-address-pairs', u'vrf-assign-table', u'virtual-machine-interface-device-owner', u'virtual-machine-interface-properties', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('security_group_refs'):
                req_metas.append('virtual-machine-interface-security-group')
            if field_names.has_key('virtual_machine_interface_refs'):
                req_metas.append('virtual-machine-interface-sub-interface')
            if field_names.has_key('virtual_machine_refs'):
                req_metas.append('virtual-machine-interface-virtual-machine')
            if field_names.has_key('virtual_network_refs'):
                req_metas.append('virtual-machine-interface-virtual-network')
            if field_names.has_key('routing_instance_refs'):
                req_metas.append('virtual-machine-interface-routing-instance')
            if field_names.has_key('interface_route_table_refs'):
                req_metas.append('virtual-machine-interface-route-table')
            if field_names.has_key('virtual_machine_interface_refs'):
                req_metas.append('virtual-machine-interface-sub-interface')
            if field_names.has_key('instance_ip_back_refs'):
                req_metas.append('instance-ip-virtual-machine-interface')
            if field_names.has_key('subnet_back_refs'):
                req_metas.append('subnet-virtual-machine-interface')
            if field_names.has_key('floating_ip_back_refs'):
                req_metas.append('floating-ip-virtual-machine-interface')
            if field_names.has_key('logical_interface_back_refs'):
                req_metas.append('logical-interface-virtual-machine-interface')
            if field_names.has_key('customer_attachment_back_refs'):
                req_metas.append('customer-attachment-virtual-machine-interface')
            if field_names.has_key('logical_router_back_refs'):
                req_metas.append('logical-router-interface')
            if field_names.has_key('loadbalancer_pool_back_refs'):
                req_metas.append('loadbalancer-pool-virtual-machine-interface')
            if field_names.has_key('virtual_ip_back_refs'):
                req_metas.append('virtual-ip-virtual-machine-interface')
            if field_names.has_key('virtual_machine_interface_mac_addresses'):
                req_metas.append('virtual-machine-interface-mac-addresses')
            if field_names.has_key('virtual_machine_interface_dhcp_option_list'):
                req_metas.append('virtual-machine-interface-dhcp-option-list')
            if field_names.has_key('virtual_machine_interface_host_routes'):
                req_metas.append('virtual-machine-interface-host-routes')
            if field_names.has_key('virtual_machine_interface_allowed_address_pairs'):
                req_metas.append('virtual-machine-interface-allowed-address-pairs')
            if field_names.has_key('vrf_assign_table'):
                req_metas.append('vrf-assign-table')
            if field_names.has_key('virtual_machine_interface_device_owner'):
                req_metas.append('virtual-machine-interface-device-owner')
            if field_names.has_key('virtual_machine_interface_properties'):
                req_metas.append('virtual-machine-interface-properties')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_virtual-machine-interface_read_to_meta_index

    def _ifmap_virtual_machine_interface_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_virtual_machine_interface_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('virtual-machine-interface-mac-addresses' in existing_metas) and ('virtual_machine_interface_mac_addresses' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-machine-interface-mac-addresses')

        if ('virtual-machine-interface-dhcp-option-list' in existing_metas) and ('virtual_machine_interface_dhcp_option_list' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-machine-interface-dhcp-option-list')

        if ('virtual-machine-interface-host-routes' in existing_metas) and ('virtual_machine_interface_host_routes' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-machine-interface-host-routes')

        if ('virtual-machine-interface-allowed-address-pairs' in existing_metas) and ('virtual_machine_interface_allowed_address_pairs' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-machine-interface-allowed-address-pairs')

        if ('vrf-assign-table' in existing_metas) and ('vrf_assign_table' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:vrf-assign-table')

        if ('virtual-machine-interface-device-owner' in existing_metas) and ('virtual_machine_interface_device_owner' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-machine-interface-device-owner')

        if ('virtual-machine-interface-properties' in existing_metas) and ('virtual_machine_interface_properties' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:virtual-machine-interface-properties')

        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('virtual-machine-interface-security-group' in existing_metas):
             for meta_info in existing_metas['virtual-machine-interface-security-group']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('security_group_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['security_group_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('security-group', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-machine-interface-security-group')

        old_refs = []
        if ('virtual-machine-interface-sub-interface' in existing_metas):
             for meta_info in existing_metas['virtual-machine-interface-sub-interface']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_machine_interface_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_machine_interface_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-machine-interface', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-machine-interface-sub-interface')

        old_refs = []
        if ('virtual-machine-interface-virtual-machine' in existing_metas):
             for meta_info in existing_metas['virtual-machine-interface-virtual-machine']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_machine_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_machine_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-machine', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-machine-interface-virtual-machine')

        old_refs = []
        if ('virtual-machine-interface-virtual-network' in existing_metas):
             for meta_info in existing_metas['virtual-machine-interface-virtual-network']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_network_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_network_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-network', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-machine-interface-virtual-network')

        old_refs = []
        if ('virtual-machine-interface-routing-instance' in existing_metas):
             for meta_info in existing_metas['virtual-machine-interface-routing-instance']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('routing_instance_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['routing_instance_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('routing-instance', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-machine-interface-routing-instance')

        old_refs = []
        if ('virtual-machine-interface-route-table' in existing_metas):
             for meta_info in existing_metas['virtual-machine-interface-route-table']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('interface_route_table_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['interface_route_table_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('interface-route-table', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:virtual-machine-interface-route-table')

        (ok, result) = self._ifmap_virtual_machine_interface_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_virtual_machine_interface_update

    def _ifmap_virtual_machine_interface_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['virtual-machine-interface']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_virtual_machine_interface_list

    def _ifmap_virtual_machine_interface_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:virtual-machine-interface-security-group', u'contrail:virtual-machine-interface-sub-interface', u'contrail:virtual-machine-interface-virtual-machine', u'contrail:virtual-machine-interface-virtual-network', u'contrail:virtual-machine-interface-routing-instance', u'contrail:virtual-machine-interface-route-table'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_virtual_machine_interface_delete

    def _ifmap_logical_router_alloc(self, parent_type, fq_name):
        imid = self._imid_handler
        (my_imid, parent_imid) = \
            imid.logical_router_alloc_ifmap_id(parent_type, fq_name)
        return (True, (my_imid, parent_imid))
    #end _ifmap_logical_router_alloc

    def _ifmap_logical_router_set(self, my_imid, existing_metas, obj_dict):
        # Properties Meta
        update = {}
        field = obj_dict.get('id_perms', None)
        if field is not None:
            # construct object of xsd-type and get its xml repr
            field = obj_dict['id_perms']
            buf = cStringIO.StringIO()
            # perms might be inserted at server as obj.
            # obj construction diff from dict construction.
            if isinstance(field, dict):
                IdPermsType(**field).exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            else: # object
                field.exportChildren(buf, level = 1, name_ = 'id-perms', pretty_print = False)
            id_perms_xml = buf.getvalue()
            buf.close()
            meta = Metadata('id-perms' , '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                   elements = id_perms_xml)

            if (existing_metas and 'id-perms' in existing_metas and
                str(existing_metas['id-perms'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        field = obj_dict.get('display_name', None)
        if field is not None:
            norm_str = escape(str(obj_dict['display_name']))
            meta = Metadata('display-name', norm_str,
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')

            if (existing_metas and 'display-name' in existing_metas and
                str(existing_metas['display-name'][0]['meta']) == str(meta)):
                # no change
                pass
            else:
                self._update_id_self_meta(update, meta)

        # Ref Link Metas
        imid = self._imid_handler

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_machine_interface_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-machine-interface'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                logical_router_interface_xml = ''
                meta = Metadata('logical-router-interface' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = logical_router_interface_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('route_target_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'route-target'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                logical_router_target_xml = ''
                meta = Metadata('logical-router-target' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = logical_router_target_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('virtual_network_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'virtual-network'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                logical_router_gateway_xml = ''
                meta = Metadata('logical-router-gateway' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = logical_router_gateway_xml)
                self._update_id_pair_meta(update, to_imid, meta)

        # construct object of xsd-type and get its xml repr
        refs = obj_dict.get('service_instance_refs', None)
        if refs:
            for ref in refs:
                ref_fq_name = ref['to']
                obj_type = 'service-instance'
                to_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(obj_type, ref_fq_name)
                logical_router_service_instance_xml = ''
                meta = Metadata('logical-router-service-instance' , '',
                       {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail',
                       elements = logical_router_service_instance_xml)
                self._update_id_pair_meta(update, to_imid, meta)


        self._publish_update(my_imid, update)
        return (True, '')
    #end _ifmap_logical_router_set

    def _ifmap_logical_router_create(self, obj_ids, obj_dict):
        if not 'parent_type' in obj_dict:
            # parent is config-root
            parent_type = 'config-root'
            parent_imid = 'contrail:config-root:root'
        else:
            parent_type = obj_dict['parent_type']
            parent_imid = obj_ids.get('parent_imid', None)

        # Parent Link Meta
        update = {}
        parent_link_meta = self._parent_metas[parent_type]['logical-router']
        meta = Metadata(parent_link_meta, '',
                   {'ifmap-cardinality':'singleValue'}, ns_prefix = 'contrail')
        self._update_id_pair_meta(update, obj_ids['imid'], meta)
        self._publish_update(parent_imid, update)

        (ok, result) = self._ifmap_logical_router_set(obj_ids['imid'], None, obj_dict)
        return (ok, result)
    #end _ifmap_logical_router_create


    def _ifmap_logical_router_read(self, ifmap_id, field_names = None):
        metas = self._ifmap_logical_router_read_to_meta_index(ifmap_id, field_names)
         # Construct dicts from xml
        result = {}
        result['_type'] = 'logical-router'
        fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ifmap_id)
        result['fq_name'] = fq_name
        result['name'] = fq_name[-1]
        if metas.has_key('contrail:logical-router-interface'):
            result['virtual_machine_interface_refs'] = []
            result_elems = metas['contrail:logical-router-interface']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_machine_interface', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_machine_interface_refs'].append(ref_info)


        if metas.has_key('contrail:logical-router-target'):
            result['route_target_refs'] = []
            result_elems = metas['contrail:logical-router-target']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('route_target', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['route_target_refs'].append(ref_info)


        if metas.has_key('contrail:logical-router-gateway'):
            result['virtual_network_refs'] = []
            result_elems = metas['contrail:logical-router-gateway']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('virtual_network', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['virtual_network_refs'].append(ref_info)


        if metas.has_key('contrail:logical-router-service-instance'):
            result['service_instance_refs'] = []
            result_elems = metas['contrail:logical-router-service-instance']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = {}
                # in case of refs between same types of identifiers,
                # the referred ident could appears at idx 0 or 1
                if ifmap_id == r_elem[0].attrib['name']:
                    ref_ifmap_id = r_elem[1].attrib['name']
                else:
                    ref_ifmap_id = r_elem[0].attrib['name']

                try:
                    ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown fq_name for ref ' + ref_ifmap_id
                    return (False, err_msg)
                try:
                    ref_uuid = self._db_client_mgr.ifmap_id_to_uuid(ref_ifmap_id)
                except cfgm_common.exceptions.NoIdError:
                    err_msg = 'Unknown uuid for ref ' + ref_ifmap_id
                    return (False, err_msg)

                ref_url = self._generate_url('service_instance', ref_uuid)
                ref_info = {}
                ref_info['to'] = ref_fq_name
                ref_info['attr'] = obj_val
                ref_info['href'] = ref_url
                ref_info['uuid'] = ref_uuid
                result['service_instance_refs'].append(ref_info)


        if metas.has_key('contrail:id-perms'):
            result_elems = metas['contrail:id-perms']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                # need to preserve type eg. int for prefix-len
                # for now, xml -> obj -> json -> dict
                # TODO TypeGenerator.py should have xml to dict method
                obj = IdPermsType()
                obj.build(meta)
                obj_json = ''
                obj_json = json.dumps(obj,
                    default=lambda o: dict((k, v) for k, v in o.__dict__.iteritems()))
                obj_val = json.loads(obj_json)
                result['id_perms'] = obj_val

        if metas.has_key('contrail:display-name'):
            result_elems = metas['contrail:display-name']
            # metas[key] is always a list
            for r_elem in result_elems:
                meta = r_elem[2]
                obj_val = meta.text
                result['display_name'] = obj_val

        return (True, result)
    #end _ifmap_logical_router_read

    def _ifmap_logical_router_read_to_meta_index(self, ifmap_id, field_names = None):
        # field_names = None means all fields will be read
        imid = self._imid_handler
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist

        all_metas = [u'logical-router-interface', u'logical-router-target', u'logical-router-gateway', u'logical-router-service-instance', u'id-perms', u'display-name']
        if not field_names:
            metas_to_read = all_metas
        else: # read only requested fields
            req_metas = []
            if field_names.has_key('virtual_machine_interface_refs'):
                req_metas.append('logical-router-interface')
            if field_names.has_key('route_target_refs'):
                req_metas.append('logical-router-target')
            if field_names.has_key('virtual_network_refs'):
                req_metas.append('logical-router-gateway')
            if field_names.has_key('service_instance_refs'):
                req_metas.append('logical-router-service-instance')
            if field_names.has_key('id_perms'):
                req_metas.append('id-perms')
            if field_names.has_key('display_name'):
                req_metas.append('display-name')

            metas_to_read = set(all_metas) & set(req_metas)

        # metas is a dict where key is meta-name and val is list of dict
        # of form [{'meta':meta}, {'id':id1, 'meta':meta}, {'id':id2, 'meta':meta}]
        metas = {}
        for meta_name in metas_to_read:
            if meta_name in self._id_to_metas[ifmap_id]:
                metas[meta_name] = self._id_to_metas[ifmap_id][meta_name]
        return metas
    #end _ifmap_logical-router_read_to_meta_index

    def _ifmap_logical_router_update(self, ifmap_id, new_obj_dict):
        # read in refs from ifmap to determine which ones become inactive after update
        existing_metas = self._ifmap_logical_router_read_to_meta_index(ifmap_id)

        # remove properties that are no longer active
        if ('id-perms' in existing_metas) and ('id_perms' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:id-perms')

        if ('display-name' in existing_metas) and ('display_name' not in new_obj_dict):
            self._delete_id_self_meta(ifmap_id, 'contrail:display-name')

        # remove refs that are no longer active
        old_refs = []
        if ('logical-router-interface' in existing_metas):
             for meta_info in existing_metas['logical-router-interface']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_machine_interface_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_machine_interface_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-machine-interface', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:logical-router-interface')

        old_refs = []
        if ('logical-router-target' in existing_metas):
             for meta_info in existing_metas['logical-router-target']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('route_target_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['route_target_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('route-target', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:logical-router-target')

        old_refs = []
        if ('logical-router-gateway' in existing_metas):
             for meta_info in existing_metas['logical-router-gateway']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('virtual_network_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['virtual_network_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('virtual-network', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:logical-router-gateway')

        old_refs = []
        if ('logical-router-service-instance' in existing_metas):
             for meta_info in existing_metas['logical-router-service-instance']:
                 ref_imid = meta_info['id']
                 meta = meta_info['meta']
                 old_ref_fq_name = self._db_client_mgr.ifmap_id_to_fq_name(ref_imid)
                 old_refs.append(old_ref_fq_name)
        old_set = set([tuple(ref) for ref in old_refs])

        new_refs = []
        if ('service_instance_refs' in new_obj_dict):
            new_refs = [tuple(ref['to']) for ref in new_obj_dict['service_instance_refs']]

        new_set = set(new_refs)
        inact_refs = old_set - new_set
        for inact_ref in inact_refs:
            to_imid = self.fq_name_to_ifmap_id('service-instance', inact_ref)
            self._delete_id_pair_meta(ifmap_id, to_imid, 'contrail:logical-router-service-instance')

        (ok, result) = self._ifmap_logical_router_set(ifmap_id, existing_metas, new_obj_dict)
        return (ok, result)
    #end _ifmap_logical_router_update

    def _ifmap_logical_router_list(self, parent_type, parent_fq_name):
        children_fq_names = []
        mapclient = self._mapclient
        parent_imid = cfgm_common.imid.get_ifmap_id_from_fq_name(parent_type, parent_fq_name)
        start_id = str(Identity(name = parent_imid, type = 'other',
                        other_type = 'extended'))
        # if id-perms missing, identity doesn't exist
        parent_link_meta = self._parent_metas[parent_type]['logical-router']
        srch_meta = 'contrail:' + parent_link_meta
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, parent_imid)
        for r_elem in result_list:
            child_ident = r_elem[1]
            if child_ident is None:
                continue
            child_imid = child_ident.attrib['name']
            fq_name = self._db_client_mgr.ifmap_id_to_fq_name(child_imid)
            children_fq_names.append(fq_name)


        return (True, children_fq_names)
    #end _ifmap_logical_router_list

    def _ifmap_logical_router_delete(self, obj_ids):
        ifmap_id = obj_ids['imid']
        parent_imid = obj_ids.get('parent_imid', None)
        start_id = str(Identity(name = ifmap_id, type = 'other',
                        other_type = 'extended'))
        # Delete links to our references, first find them
        srch_meta = ' or '.join([u'contrail:logical-router-interface', u'contrail:logical-router-target', u'contrail:logical-router-gateway', u'contrail:logical-router-service-instance'])
        srch_result = self._search(start_id, srch_meta)

        result_list = self.parse_result_items(srch_result, ifmap_id)
        for r_elem in result_list:
            to_ident = r_elem[1]
            if to_ident is None:
                continue
            to_imid = to_ident.attrib['name']
            r_meta = r_elem[2]
            meta_name = r_meta.prefix + ':' + re.sub('{.*}', '', r_meta.tag)
            # For symmetrical links, to_ident could be same as ifmap_id, so pass r_elem[0]
            self._delete_id_pair_meta(r_elem[0].attrib['name'], to_imid, meta_name)


        if parent_imid:
            # Remove link from parent
            self._delete_id_pair_meta(parent_imid, ifmap_id, None)

        # Remove all property metadata associated with this ident
        self._delete_id_self_meta(ifmap_id, None)

        return (True, '')
    #end _ifmap_logical_router_delete

#end class VncIfmapClientGen

class ImidGen(object):
    def domain_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:domain:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end domain_alloc_ifmap_id

    def global_vrouter_config_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:global-vrouter-config:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end global_vrouter_config_alloc_ifmap_id

    def instance_ip_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:instance-ip:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end instance_ip_alloc_ifmap_id

    def network_policy_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:network-policy:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end network_policy_alloc_ifmap_id

    def virtual_DNS_record_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:virtual-DNS-record:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end virtual_DNS_record_alloc_ifmap_id

    def route_target_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:route-target:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end route_target_alloc_ifmap_id

    def floating_ip_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:floating-ip:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end floating_ip_alloc_ifmap_id

    def floating_ip_pool_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:floating-ip-pool:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end floating_ip_pool_alloc_ifmap_id

    def physical_router_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:physical-router:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end physical_router_alloc_ifmap_id

    def bgp_router_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:bgp-router:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end bgp_router_alloc_ifmap_id

    def virtual_router_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:virtual-router:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end virtual_router_alloc_ifmap_id

    def config_root_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:config-root:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end config_root_alloc_ifmap_id

    def subnet_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:subnet:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end subnet_alloc_ifmap_id

    def global_system_config_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:global-system-config:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end global_system_config_alloc_ifmap_id

    def loadbalancer_member_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:loadbalancer-member:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end loadbalancer_member_alloc_ifmap_id

    def service_instance_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:service-instance:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end service_instance_alloc_ifmap_id

    def namespace_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:namespace:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end namespace_alloc_ifmap_id

    def route_table_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:route-table:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end route_table_alloc_ifmap_id

    def physical_interface_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:physical-interface:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end physical_interface_alloc_ifmap_id

    def access_control_list_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:access-control-list:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end access_control_list_alloc_ifmap_id

    def virtual_DNS_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:virtual-DNS:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end virtual_DNS_alloc_ifmap_id

    def customer_attachment_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:customer-attachment:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end customer_attachment_alloc_ifmap_id

    def loadbalancer_pool_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:loadbalancer-pool:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end loadbalancer_pool_alloc_ifmap_id

    def virtual_machine_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:virtual-machine:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end virtual_machine_alloc_ifmap_id

    def interface_route_table_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:interface-route-table:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end interface_route_table_alloc_ifmap_id

    def service_template_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:service-template:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end service_template_alloc_ifmap_id

    def virtual_ip_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:virtual-ip:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end virtual_ip_alloc_ifmap_id

    def security_group_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:security-group:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end security_group_alloc_ifmap_id

    def provider_attachment_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:provider-attachment:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end provider_attachment_alloc_ifmap_id

    def network_ipam_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:network-ipam:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end network_ipam_alloc_ifmap_id

    def loadbalancer_healthmonitor_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:loadbalancer-healthmonitor:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end loadbalancer_healthmonitor_alloc_ifmap_id

    def virtual_network_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:virtual-network:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end virtual_network_alloc_ifmap_id

    def project_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:project:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end project_alloc_ifmap_id

    def logical_interface_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:logical-interface:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end logical_interface_alloc_ifmap_id

    def routing_instance_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:routing-instance:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end routing_instance_alloc_ifmap_id

    def virtual_machine_interface_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:virtual-machine-interface:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end virtual_machine_interface_alloc_ifmap_id

    def logical_router_alloc_ifmap_id(self, parent_type, fq_name):
        my_fqn = ':'.join(fq_name)
        parent_fqn = ':'.join(fq_name[:-1])

        my_imid = 'contrail:logical-router:' + my_fqn
        if parent_fqn:
            parent_imid = 'contrail:' + parent_type + ':' + parent_fqn
        else: # parent is config-root
            parent_imid = 'contrail:config-root:root'

        # Normalize/escape special chars
        my_imid = escape(my_imid)
        parent_imid = escape(parent_imid)

        return (my_imid, parent_imid)
    #end logical_router_alloc_ifmap_id


link_name_to_xsd_type = {
    "project-namespace":"SubnetType",
    "connection":"ConnectionType",
    "bgp-peering":"BgpPeeringAttributes",
    "virtual-machine-interface-routing-instance":"PolicyBasedForwardingRuleType",
    "virtual-network-network-policy":"VirtualNetworkPolicyType",
    "instance-target":"InstanceTargetType",
    "virtual-network-network-ipam":"VnSubnetsType"
}

