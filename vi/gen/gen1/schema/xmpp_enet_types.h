
// autogenerated file --- DO NOT EDIT ---
#include <iostream>
#include <string.h>
#include <vector>

#include <boost/dynamic_bitset.hpp>
#include <boost/crc.hpp>      // for boost::crc_32_type
namespace pugi {
class xml_node;
class xml_document;
}  // namespace pugi

#include "ifmap/autogen.h"

namespace autogen {


struct EnetAddressType : public AutogenProperty {
    virtual ~EnetAddressType();
    int af;
    int safi;
    int ethernet_tag;
    std::string mac;
    std::string address;

    void Clear();
    void Copy(const EnetAddressType &rhs);
    bool XmlParse(const pugi::xml_node &node);
    static bool XmlParseProperty(const pugi::xml_node &node,
                                 std::auto_ptr<AutogenProperty> *resultp);
    void Encode(pugi::xml_node *node) const;
    void CalculateCrc(boost::crc_32_type *crc) const;
};

struct EnetTunnelEncapsulationListType : public AutogenProperty {
    virtual ~EnetTunnelEncapsulationListType();

    typedef std::vector<std::string>::const_iterator const_iterator;
    const_iterator begin() const { return tunnel_encapsulation.begin(); }
    const_iterator end() const { return tunnel_encapsulation.end(); }
    std::vector<std::string> tunnel_encapsulation;

    void Clear();
    void Copy(const EnetTunnelEncapsulationListType &rhs);
    bool XmlParse(const pugi::xml_node &node);
    static bool XmlParseProperty(const pugi::xml_node &node,
                                 std::auto_ptr<AutogenProperty> *resultp);
    void Encode(pugi::xml_node *node) const;
    void CalculateCrc(boost::crc_32_type *crc) const;
};

struct EnetNextHopType : public AutogenProperty {
    virtual ~EnetNextHopType();
    int af;
    std::string address;
    int label;
    EnetTunnelEncapsulationListType tunnel_encapsulation_list;

    void Clear();
    void Copy(const EnetNextHopType &rhs);
    bool XmlParse(const pugi::xml_node &node);
    static bool XmlParseProperty(const pugi::xml_node &node,
                                 std::auto_ptr<AutogenProperty> *resultp);
    void Encode(pugi::xml_node *node) const;
    void CalculateCrc(boost::crc_32_type *crc) const;
};

struct EnetNextHopListType : public AutogenProperty {
    virtual ~EnetNextHopListType();

    typedef std::vector<EnetNextHopType>::const_iterator const_iterator;
    const_iterator begin() const { return next_hop.begin(); }
    const_iterator end() const { return next_hop.end(); }
    std::vector<EnetNextHopType> next_hop;

    void Clear();
    void Copy(const EnetNextHopListType &rhs);
    bool XmlParse(const pugi::xml_node &node);
    static bool XmlParseProperty(const pugi::xml_node &node,
                                 std::auto_ptr<AutogenProperty> *resultp);
    void Encode(pugi::xml_node *node) const;
    void CalculateCrc(boost::crc_32_type *crc) const;
};

struct EnetOlistType : public AutogenProperty {
    virtual ~EnetOlistType();

    typedef std::vector<EnetNextHopType>::const_iterator const_iterator;
    const_iterator begin() const { return next_hop.begin(); }
    const_iterator end() const { return next_hop.end(); }
    std::vector<EnetNextHopType> next_hop;

    void Clear();
    void Copy(const EnetOlistType &rhs);
    bool XmlParse(const pugi::xml_node &node);
    static bool XmlParseProperty(const pugi::xml_node &node,
                                 std::auto_ptr<AutogenProperty> *resultp);
    void Encode(pugi::xml_node *node) const;
    void CalculateCrc(boost::crc_32_type *crc) const;
};

struct EnetSecurityGroupListType : public AutogenProperty {
    virtual ~EnetSecurityGroupListType();

    typedef std::vector<int>::const_iterator const_iterator;
    const_iterator begin() const { return security_group.begin(); }
    const_iterator end() const { return security_group.end(); }
    std::vector<int> security_group;

    void Clear();
    void Copy(const EnetSecurityGroupListType &rhs);
    bool XmlParse(const pugi::xml_node &node);
    static bool XmlParseProperty(const pugi::xml_node &node,
                                 std::auto_ptr<AutogenProperty> *resultp);
    void Encode(pugi::xml_node *node) const;
    void CalculateCrc(boost::crc_32_type *crc) const;
};

struct EnetEntryType : public AutogenProperty {
    virtual ~EnetEntryType();
    EnetAddressType nlri;
    EnetNextHopListType next_hops;
    EnetOlistType olist;
    std::string virtual_network;
    int sequence_number;
    EnetSecurityGroupListType security_group_list;
    int local_preference;
    bool edge_replication_not_supported;

    void Clear();
    void Copy(const EnetEntryType &rhs);
    bool XmlParse(const pugi::xml_node &node);
    static bool XmlParseProperty(const pugi::xml_node &node,
                                 std::auto_ptr<AutogenProperty> *resultp);
    void Encode(pugi::xml_node *node) const;
    void CalculateCrc(boost::crc_32_type *crc) const;
};

struct EnetItemType : public AutogenProperty {
    virtual ~EnetItemType();
    EnetEntryType entry;

    void Clear();
    void Copy(const EnetItemType &rhs);
    bool XmlParse(const pugi::xml_node &node);
    static bool XmlParseProperty(const pugi::xml_node &node,
                                 std::auto_ptr<AutogenProperty> *resultp);
    void Encode(pugi::xml_node *node) const;
    void CalculateCrc(boost::crc_32_type *crc) const;
};

struct EnetItemsType : public AutogenProperty {
    virtual ~EnetItemsType();

    typedef std::vector<EnetItemType>::const_iterator const_iterator;
    const_iterator begin() const { return item.begin(); }
    const_iterator end() const { return item.end(); }
    std::vector<EnetItemType> item;

    void Clear();
    void Copy(const EnetItemsType &rhs);
    bool XmlParse(const pugi::xml_node &node);
    static bool XmlParseProperty(const pugi::xml_node &node,
                                 std::auto_ptr<AutogenProperty> *resultp);
    void Encode(pugi::xml_node *node) const;
    void CalculateCrc(boost::crc_32_type *crc) const;
};
}  // namespace autogen
