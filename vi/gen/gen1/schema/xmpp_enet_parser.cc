
#include "/opt/stack/contrail/build/production/schema/xmpp_enet_types.h"

#include <stdint.h>
#include <sstream>
#include <boost/algorithm/string/trim.hpp>
#include <pugixml/pugixml.hpp>
#include <time.h>

using namespace pugi;
using namespace std;

#include "base/compiler.h"
#if defined(__GNUC__) && (__GCC_HAS_PRAGMA > 0)
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

namespace autogen {
static bool ParseInteger(const pugi::xml_node &node, int *valuep) {
    char *endp;
    *valuep = strtoul(node.child_value(), &endp, 10);
    while (isspace(*endp)) endp++;
    return endp[0] == '\0';
}

static bool ParseUnsignedLong(const pugi::xml_node &node, uint64_t *valuep) {
    char *endp;
    *valuep = strtoull(node.child_value(), &endp, 10);
    while (isspace(*endp)) endp++;
    return endp[0] == '\0';
}

static bool ParseBoolean(const pugi::xml_node &node, bool *valuep) {
    if (strcmp(node.child_value(), "true") ==0) 
        *valuep = true;
    else
        *valuep = false;
    return true;
}

static bool ParseDateTime(const pugi::xml_node &node, time_t *valuep) {
    string value(node.child_value());
    boost::trim(value);
    struct tm tm;
    char *endp;
    memset(&tm, 0, sizeof(tm));
    if (value.size() == 0) return true;
    endp = strptime(value.c_str(), "%FT%T", &tm);
    if (!endp) return false;
    *valuep = timegm(&tm);
    return true;
}
static bool ParseTime(const pugi::xml_node &node, time_t *valuep) {
    string value(node.child_value());
    boost::trim(value);
    struct tm tm;
    char *endp;
    endp = strptime(value.c_str(), "%T", &tm);
    if (!endp) return false;
    *valuep = timegm(&tm);
    return true;
}
static std::string FormatDateTime(const time_t *valuep) {
    struct tm tm;
    char result[100];
    gmtime_r(valuep, &tm);
    strftime(result, sizeof(result), "%FT%T", &tm);
    return std::string(result);
}
static std::string FormatTime(const time_t *valuep) {
    struct tm tm;
    char result[100];
    gmtime_r(valuep, &tm);
    strftime(result, sizeof(result), "%T", &tm);
    return std::string(result);
}

bool EnetNextHopType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "af") == 0) {
            if (!ParseInteger(node, &af)) return false;
        }
        if (strcmp(node.name(), "address") == 0) {
            address = node.child_value();
            boost::trim(address);
        }
        if (strcmp(node.name(), "label") == 0) {
            if (!ParseInteger(node, &label)) return false;
        }
        if (strcmp(node.name(), "tunnel-encapsulation-list") == 0) {
            if (!tunnel_encapsulation_list.XmlParse(node)) return false;
        }
    }
    return true;
}

bool EnetNextHopType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    EnetNextHopType *ptr = new EnetNextHopType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void EnetNextHopType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("af");
    node_c.text().set(EnetNextHopType::af);

    // Add child node 
    node_c = node_p->append_child("address");
    node_c.text().set(EnetNextHopType::address.c_str());

    // Add child node 
    node_c = node_p->append_child("label");
    node_c.text().set(EnetNextHopType::label);

    // Add complex node 
    node_c = node_p->append_child("tunnel-encapsulation-list");
    tunnel_encapsulation_list.Encode(&node_c); 
}

bool EnetItemsType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "item") == 0) {

            EnetItemType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            item.push_back(var);
        }
    }
    return true;
}

bool EnetItemsType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    EnetItemsType *ptr = new EnetItemsType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void EnetItemsType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<EnetItemType>::const_iterator iter = item.begin();
         iter != item.end(); ++iter) {
        node_c = node_p->append_child("item");
        iter->Encode(&node_c);
    }
}

bool EnetAddressType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "af") == 0) {
            if (!ParseInteger(node, &af)) return false;
        }
        if (strcmp(node.name(), "safi") == 0) {
            if (!ParseInteger(node, &safi)) return false;
        }
        if (strcmp(node.name(), "ethernet-tag") == 0) {
            if (!ParseInteger(node, &ethernet_tag)) return false;
        }
        if (strcmp(node.name(), "mac") == 0) {
            mac = node.child_value();
            boost::trim(mac);
        }
        if (strcmp(node.name(), "address") == 0) {
            address = node.child_value();
            boost::trim(address);
        }
    }
    return true;
}

bool EnetAddressType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    EnetAddressType *ptr = new EnetAddressType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void EnetAddressType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("af");
    node_c.text().set(EnetAddressType::af);

    // Add child node 
    node_c = node_p->append_child("safi");
    node_c.text().set(EnetAddressType::safi);

    // Add child node 
    node_c = node_p->append_child("ethernet-tag");
    node_c.text().set(EnetAddressType::ethernet_tag);

    // Add child node 
    node_c = node_p->append_child("mac");
    node_c.text().set(EnetAddressType::mac.c_str());

    // Add child node 
    node_c = node_p->append_child("address");
    node_c.text().set(EnetAddressType::address.c_str());

}

bool EnetTunnelEncapsulationListType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "tunnel-encapsulation") == 0) {

            string var(node.child_value());
            boost::trim(var);
            tunnel_encapsulation.push_back(var);
        }
    }
    return true;
}

bool EnetTunnelEncapsulationListType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    EnetTunnelEncapsulationListType *ptr = new EnetTunnelEncapsulationListType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void EnetTunnelEncapsulationListType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<std::string>::const_iterator iter = tunnel_encapsulation.begin();
         iter != tunnel_encapsulation.end(); ++iter) {
        node_c = node_p->append_child("tunnel-encapsulation");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
}

bool EnetSecurityGroupListType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "security-group") == 0) {

            int var;
            if (!ParseInteger(node, &var)) return false;
            security_group.push_back(var);
        }
    }
    return true;
}

bool EnetSecurityGroupListType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    EnetSecurityGroupListType *ptr = new EnetSecurityGroupListType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void EnetSecurityGroupListType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<int>::const_iterator iter = security_group.begin();
         iter != security_group.end(); ++iter) {
        node_c = node_p->append_child("security-group");
        node_c.text().set(*iter);
    }
}

bool EnetOlistType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "next-hop") == 0) {

            EnetNextHopType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            next_hop.push_back(var);
        }
    }
    return true;
}

bool EnetOlistType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    EnetOlistType *ptr = new EnetOlistType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void EnetOlistType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<EnetNextHopType>::const_iterator iter = next_hop.begin();
         iter != next_hop.end(); ++iter) {
        node_c = node_p->append_child("next-hop");
        iter->Encode(&node_c);
    }
}

bool EnetNextHopListType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "next-hop") == 0) {

            EnetNextHopType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            next_hop.push_back(var);
        }
    }
    return true;
}

bool EnetNextHopListType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    EnetNextHopListType *ptr = new EnetNextHopListType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void EnetNextHopListType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<EnetNextHopType>::const_iterator iter = next_hop.begin();
         iter != next_hop.end(); ++iter) {
        node_c = node_p->append_child("next-hop");
        iter->Encode(&node_c);
    }
}

bool EnetEntryType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "nlri") == 0) {
            if (!nlri.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "next-hops") == 0) {
            if (!next_hops.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "olist") == 0) {
            if (!olist.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "virtual-network") == 0) {
            virtual_network = node.child_value();
            boost::trim(virtual_network);
        }
        if (strcmp(node.name(), "sequence-number") == 0) {
            if (!ParseInteger(node, &sequence_number)) return false;
        }
        if (strcmp(node.name(), "security-group-list") == 0) {
            if (!security_group_list.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "local-preference") == 0) {
            if (!ParseInteger(node, &local_preference)) return false;
        }
        if (strcmp(node.name(), "edge-replication-not-supported") == 0) {
            if (!ParseBoolean(node, &edge_replication_not_supported)) return false;
        }
    }
    return true;
}

bool EnetEntryType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    EnetEntryType *ptr = new EnetEntryType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void EnetEntryType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("nlri");
    nlri.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("next-hops");
    next_hops.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("olist");
    olist.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("virtual-network");
    node_c.text().set(EnetEntryType::virtual_network.c_str());

    // Add child node 
    node_c = node_p->append_child("sequence-number");
    node_c.text().set(EnetEntryType::sequence_number);

    // Add complex node 
    node_c = node_p->append_child("security-group-list");
    security_group_list.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("local-preference");
    node_c.text().set(EnetEntryType::local_preference);

    // Add child node 
    node_c = node_p->append_child("edge-replication-not-supported");
    node_c.text().set(EnetEntryType::edge_replication_not_supported);

}

bool EnetItemType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "entry") == 0) {
            if (!entry.XmlParse(node)) return false;
        }
    }
    return true;
}

bool EnetItemType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    EnetItemType *ptr = new EnetItemType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void EnetItemType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("entry");
    entry.Encode(&node_c); 
}
}  // namespace autogen
