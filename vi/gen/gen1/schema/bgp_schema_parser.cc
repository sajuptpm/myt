
#include "/opt/stack/contrail/build/production/schema/bgp_schema_types.h"

#include <stdint.h>
#include <sstream>
#include <boost/algorithm/string/trim.hpp>
#include <pugixml/pugixml.hpp>

#include "ifmap/ifmap_agent_parser.h"
#include "ifmap/ifmap_server_parser.h"
#include "ifmap/ifmap_table.h"

using namespace pugi;
using namespace std;

#include "base/compiler.h"
#if defined(__GNUC__) && (__GCC_HAS_PRAGMA > 0)
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

namespace autogen {
static bool ParseInteger(const pugi::xml_node &node, int *valuep) {
    char *endp;
    *valuep = strtoul(node.child_value(), &endp, 10);
    while (isspace(*endp)) endp++;
    return endp[0] == '\0';
}

static bool ParseUnsignedLong(const pugi::xml_node &node, uint64_t *valuep) {
    char *endp;
    *valuep = strtoull(node.child_value(), &endp, 10);
    while (isspace(*endp)) endp++;
    return endp[0] == '\0';
}

static bool ParseBoolean(const pugi::xml_node &node, bool *valuep) {
    if (strcmp(node.child_value(), "true") ==0) 
        *valuep = true;
    else
        *valuep = false;
    return true;
}
static bool ParseDateTime(const pugi::xml_node &node, time_t *valuep) {
    string value(node.child_value());
    boost::trim(value);
    struct tm tm;
    memset(&tm, 0, sizeof(tm));
    if (value.size() == 0) return true;
    char *endp;
    endp = strptime(value.c_str(), "%FT%T", &tm);
    if (!endp) return false;
    *valuep = timegm(&tm);
    return true;
}
static bool ParseTime(const pugi::xml_node &node, time_t *valuep) {
    string value(node.child_value());
    boost::trim(value);
    struct tm tm;
    char *endp;
    endp = strptime(value.c_str(), "%T", &tm);
    if (!endp) return false;
    *valuep = timegm(&tm);
    return true;
}
static std::string FormatDateTime(const time_t *valuep) {
    struct tm tm;
    char result[100];
    gmtime_r(valuep, &tm);
    strftime(result, sizeof(result), "%FT%T", &tm);
    return std::string(result);
}
static std::string FormatTime(const time_t *valuep) {
    struct tm tm;
    char result[100];
    gmtime_r(valuep, &tm);
    strftime(result, sizeof(result), "%T", &tm);
    return std::string(result);
}

bool ProtocolOspfType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "area") == 0) {
            if (!ParseInteger(node, &area)) return false;
        }
    }
    return true;
}

bool ProtocolOspfType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ProtocolOspfType *ptr = new ProtocolOspfType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ProtocolOspfType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("area");
    node_c.text().set(ProtocolOspfType::area);

}

bool BgpSession::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "uuid") == 0) {
            uuid = node.child_value();
            boost::trim(uuid);
        }
        if (strcmp(node.name(), "attributes") == 0) {

            BgpSessionAttributes var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            attributes.push_back(var);
        }
    }
    return true;
}

bool BgpSession::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    BgpSession *ptr = new BgpSession();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void BgpSession::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("uuid");
    node_c.text().set(BgpSession::uuid.c_str());


    for (std::vector<BgpSessionAttributes>::const_iterator iter = attributes.begin();
         iter != attributes.end(); ++iter) {
        node_c = node_p->append_child("attributes");
        iter->Encode(&node_c);
    }
}

bool DefaultProtocolType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "bgp") == 0) {
            if (!bgp.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "ospf") == 0) {
            if (!ospf.XmlParse(node)) return false;
        }
    }
    return true;
}

bool DefaultProtocolType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    DefaultProtocolType *ptr = new DefaultProtocolType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void DefaultProtocolType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("bgp");
    bgp.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("ospf");
    ospf.Encode(&node_c); 
}

bool BgpRouterParams::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "vendor") == 0) {
            vendor = node.child_value();
            boost::trim(vendor);
        }
        if (strcmp(node.name(), "vnc-managed") == 0) {
            if (!ParseBoolean(node, &vnc_managed)) return false;
        }
        if (strcmp(node.name(), "autonomous-system") == 0) {
            if (!ParseInteger(node, &autonomous_system)) return false;
        }
        if (strcmp(node.name(), "identifier") == 0) {
            identifier = node.child_value();
            boost::trim(identifier);
        }
        if (strcmp(node.name(), "address") == 0) {
            address = node.child_value();
            boost::trim(address);
        }
        if (strcmp(node.name(), "port") == 0) {
            if (!ParseInteger(node, &port)) return false;
        }
        if (strcmp(node.name(), "hold-time") == 0) {
            if (!ParseInteger(node, &hold_time)) return false;
        }
        if (strcmp(node.name(), "address-families") == 0) {
            if (!address_families.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "auth-key-chain") == 0) {
            if (!auth_key_chain.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "local-autonomous-system") == 0) {
            if (!ParseInteger(node, &local_autonomous_system)) return false;
        }
    }
    return true;
}

bool BgpRouterParams::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    BgpRouterParams *ptr = new BgpRouterParams();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void BgpRouterParams::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("vendor");
    node_c.text().set(BgpRouterParams::vendor.c_str());

    // Add child node 
    node_c = node_p->append_child("vnc-managed");
    node_c.text().set(BgpRouterParams::vnc_managed);

    // Add child node 
    node_c = node_p->append_child("autonomous-system");
    node_c.text().set(BgpRouterParams::autonomous_system);

    // Add child node 
    node_c = node_p->append_child("identifier");
    node_c.text().set(BgpRouterParams::identifier.c_str());

    // Add child node 
    node_c = node_p->append_child("address");
    node_c.text().set(BgpRouterParams::address.c_str());

    // Add child node 
    node_c = node_p->append_child("port");
    node_c.text().set(BgpRouterParams::port);

    // Add child node 
    node_c = node_p->append_child("hold-time");
    node_c.text().set(BgpRouterParams::hold_time);

    // Add complex node 
    node_c = node_p->append_child("address-families");
    address_families.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("auth-key-chain");
    auth_key_chain.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("local-autonomous-system");
    node_c.text().set(BgpRouterParams::local_autonomous_system);

}

bool InstanceTargetType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "import-export") == 0) {
            import_export = node.child_value();
            boost::trim(import_export);
        }
    }
    return true;
}

bool InstanceTargetType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    InstanceTargetType *ptr = new InstanceTargetType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void InstanceTargetType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("import-export");
    node_c.text().set(InstanceTargetType::import_export.c_str());

}

bool StaticRouteEntriesType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "route") == 0) {

            StaticRouteType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            route.push_back(var);
        }
    }
    return true;
}

bool StaticRouteEntriesType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    StaticRouteEntriesType *ptr = new StaticRouteEntriesType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void StaticRouteEntriesType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<StaticRouteType>::const_iterator iter = route.begin();
         iter != route.end(); ++iter) {
        node_c = node_p->append_child("route");
        iter->Encode(&node_c);
    }
}

bool ServiceChainInfo::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "routing-instance") == 0) {
            routing_instance = node.child_value();
            boost::trim(routing_instance);
        }
        if (strcmp(node.name(), "prefix") == 0) {

            string var(node.child_value());
            boost::trim(var);
            prefix.push_back(var);
        }
        if (strcmp(node.name(), "service-chain-address") == 0) {
            service_chain_address = node.child_value();
            boost::trim(service_chain_address);
        }
        if (strcmp(node.name(), "service-instance") == 0) {
            service_instance = node.child_value();
            boost::trim(service_instance);
        }
        if (strcmp(node.name(), "source-routing-instance") == 0) {
            source_routing_instance = node.child_value();
            boost::trim(source_routing_instance);
        }
    }
    return true;
}

bool ServiceChainInfo::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ServiceChainInfo *ptr = new ServiceChainInfo();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ServiceChainInfo::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("routing-instance");
    node_c.text().set(ServiceChainInfo::routing_instance.c_str());


    for (std::vector<std::string>::const_iterator iter = prefix.begin();
         iter != prefix.end(); ++iter) {
        node_c = node_p->append_child("prefix");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
    // Add child node 
    node_c = node_p->append_child("service-chain-address");
    node_c.text().set(ServiceChainInfo::service_chain_address.c_str());

    // Add child node 
    node_c = node_p->append_child("service-instance");
    node_c.text().set(ServiceChainInfo::service_instance.c_str());

    // Add child node 
    node_c = node_p->append_child("source-routing-instance");
    node_c.text().set(ServiceChainInfo::source_routing_instance.c_str());

}

bool AuthenticationKeyChain::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "auth-key-items") == 0) {

            AuthenticationKeyItem var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            auth_key_items.push_back(var);
        }
    }
    return true;
}

bool AuthenticationKeyChain::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    AuthenticationKeyChain *ptr = new AuthenticationKeyChain();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void AuthenticationKeyChain::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<AuthenticationKeyItem>::const_iterator iter = auth_key_items.begin();
         iter != auth_key_items.end(); ++iter) {
        node_c = node_p->append_child("auth-key-items");
        iter->Encode(&node_c);
    }
}

bool StaticRouteType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "prefix") == 0) {
            prefix = node.child_value();
            boost::trim(prefix);
        }
        if (strcmp(node.name(), "next-hop") == 0) {
            next_hop = node.child_value();
            boost::trim(next_hop);
        }
        if (strcmp(node.name(), "route-target") == 0) {

            string var(node.child_value());
            boost::trim(var);
            route_target.push_back(var);
        }
    }
    return true;
}

bool StaticRouteType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    StaticRouteType *ptr = new StaticRouteType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void StaticRouteType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("prefix");
    node_c.text().set(StaticRouteType::prefix.c_str());

    // Add child node 
    node_c = node_p->append_child("next-hop");
    node_c.text().set(StaticRouteType::next_hop.c_str());


    for (std::vector<std::string>::const_iterator iter = route_target.begin();
         iter != route_target.end(); ++iter) {
        node_c = node_p->append_child("route-target");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
}

bool AddressFamilies::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "family") == 0) {

            string var(node.child_value());
            boost::trim(var);
            family.push_back(var);
        }
    }
    return true;
}

bool AddressFamilies::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    AddressFamilies *ptr = new AddressFamilies();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void AddressFamilies::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<std::string>::const_iterator iter = family.begin();
         iter != family.end(); ++iter) {
        node_c = node_p->append_child("family");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
}

bool AttachmentAddressType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
    }
    return true;
}

bool AttachmentAddressType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    AttachmentAddressType *ptr = new AttachmentAddressType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void AttachmentAddressType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

}

bool AttachmentInfoType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "static") == 0) {
            if (!_static.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "bgp") == 0) {
            if (!bgp.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "ospf") == 0) {
            if (!ospf.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "state") == 0) {
            state = node.child_value();
            boost::trim(state);
        }
    }
    return true;
}

bool AttachmentInfoType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    AttachmentInfoType *ptr = new AttachmentInfoType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void AttachmentInfoType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("static");
    _static.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("bgp");
    bgp.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("ospf");
    ospf.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("state");
    node_c.text().set(AttachmentInfoType::state.c_str());

}

bool BgpSessionAttributes::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "bgp-router") == 0) {
            bgp_router = node.child_value();
            boost::trim(bgp_router);
        }
        if (strcmp(node.name(), "address-families") == 0) {
            if (!address_families.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "auth-key-chain") == 0) {
            if (!auth_key_chain.XmlParse(node)) return false;
        }
    }
    return true;
}

bool BgpSessionAttributes::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    BgpSessionAttributes *ptr = new BgpSessionAttributes();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void BgpSessionAttributes::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("bgp-router");
    node_c.text().set(BgpSessionAttributes::bgp_router.c_str());

    // Add complex node 
    node_c = node_p->append_child("address-families");
    address_families.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("auth-key-chain");
    auth_key_chain.Encode(&node_c); 
}

bool ProtocolBgpType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
    }
    return true;
}

bool ProtocolBgpType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ProtocolBgpType *ptr = new ProtocolBgpType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ProtocolBgpType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

}

bool AuthenticationKeyItem::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "key-id") == 0) {
            key_id = node.child_value();
            boost::trim(key_id);
        }
        if (strcmp(node.name(), "key-type") == 0) {
            key_type = node.child_value();
            boost::trim(key_type);
        }
        if (strcmp(node.name(), "key") == 0) {
            key = node.child_value();
            boost::trim(key);
        }
        if (strcmp(node.name(), "start-time") == 0) {
            if (!ParseDateTime(node, &start_time)) return false;
        }
    }
    return true;
}

bool AuthenticationKeyItem::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    AuthenticationKeyItem *ptr = new AuthenticationKeyItem();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void AuthenticationKeyItem::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("key-id");
    node_c.text().set(AuthenticationKeyItem::key_id.c_str());

    // Add child node 
    node_c = node_p->append_child("key-type");
    node_c.text().set(AuthenticationKeyItem::key_type.c_str());

    // Add child node 
    node_c = node_p->append_child("key");
    node_c.text().set(AuthenticationKeyItem::key.c_str());

    // Add child node 
    node_c = node_p->append_child("start-time");
    node_c.text().set(FormatDateTime(&start_time).c_str());

}

bool ProtocolStaticType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "route") == 0) {

            string var(node.child_value());
            boost::trim(var);
            route.push_back(var);
        }
    }
    return true;
}

bool ProtocolStaticType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ProtocolStaticType *ptr = new ProtocolStaticType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ProtocolStaticType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<std::string>::const_iterator iter = route.begin();
         iter != route.end(); ++iter) {
        node_c = node_p->append_child("route");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
}

bool BgpPeeringAttributes::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "session") == 0) {

            BgpSession var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            session.push_back(var);
        }
    }
    return true;
}

bool BgpPeeringAttributes::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    BgpPeeringAttributes *ptr = new BgpPeeringAttributes();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void BgpPeeringAttributes::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<BgpSession>::const_iterator iter = session.begin();
         iter != session.end(); ++iter) {
        node_c = node_p->append_child("session");
        iter->Encode(&node_c);
    }
}

void BgpRouter::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(PARAMETERS)) {
        xml_node p = node->append_child("bgp-router-parameters");
        parameters_.Encode(&p);
    }
}

bool BgpRouter::Decode(const xml_node &parent, std::string *id_name,
                       BgpRouter *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "bgp-router-parameters") == 0) {

            bool success = ptr->parameters_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(PARAMETERS);
        }
    }
    return true;
}

void RoutingInstance::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(SERVICE_CHAIN_INFORMATION)) {
        xml_node p = node->append_child("service-chain-information");
        service_chain_information_.Encode(&p);
    }

    if (property_set_.test(IS_DEFAULT)) {
        xml_node p = node->append_child("routing-instance-is-default");
        p.text().set(is_default_);
    }

    if (property_set_.test(STATIC_ROUTE_ENTRIES)) {
        xml_node p = node->append_child("static-route-entries");

        for (std::vector<StaticRouteType>::const_iterator iter = static_route_entries_.begin();
             iter != static_route_entries_.end(); ++iter) {
            xml_node s = p.append_child("route");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(DEFAULT_CE_PROTOCOL)) {
        xml_node p = node->append_child("default-ce-protocol");
        default_ce_protocol_.Encode(&p);
    }
}

bool RoutingInstance::Decode(const xml_node &parent, std::string *id_name,
                       RoutingInstance *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "service-chain-information") == 0) {

            bool success = ptr->service_chain_information_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(SERVICE_CHAIN_INFORMATION);
        }
        if (strcmp(node.name(), "routing-instance-is-default") == 0) {
            ptr->is_default_ = node.text().as_bool();
            ptr->property_set_.set(IS_DEFAULT);
        }
        if (strcmp(node.name(), "static-route-entries") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "route") != 0) {
                    continue;
                }

                StaticRouteType var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->static_route_entries_.push_back(var);
                ptr->property_set_.set(STATIC_ROUTE_ENTRIES);
            }
        }
        if (strcmp(node.name(), "default-ce-protocol") == 0) {

            bool success = ptr->default_ce_protocol_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(DEFAULT_CE_PROTOCOL);
        }
    }
    return true;
}

void ProviderAttachment::EncodeUpdate(xml_node *node) const {
}

bool ProviderAttachment::Decode(const xml_node &parent, std::string *id_name,
                       ProviderAttachment *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
    }
    return true;
}

void CustomerAttachment::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ATTACHMENT_ADDRESS)) {
        xml_node p = node->append_child("attachment-address");
        attachment_address_.Encode(&p);
    }
}

bool CustomerAttachment::Decode(const xml_node &parent, std::string *id_name,
                       CustomerAttachment *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "attachment-address") == 0) {

            bool success = ptr->attachment_address_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ATTACHMENT_ADDRESS);
        }
    }
    return true;
}

void RouteTarget::EncodeUpdate(xml_node *node) const {
}

bool RouteTarget::Decode(const xml_node &parent, std::string *id_name,
                       RouteTarget *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
    }
    return true;
}

void BgpPeering::EncodeUpdate(xml_node *node) const {
    xml_node data = node->append_child("value");
    data_.Encode(&data);
}

bool BgpPeering::Decode(const xml_node &parent, std::string *id_name,
                       BgpPeering *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }

        if (strcmp(node.name(), "value") == 0) {
            bool success = ptr->data_.XmlParse(node);
            if (!success) {
                return false;
            }
        }
        }
    return true;
}

void AttachmentInfo::EncodeUpdate(xml_node *node) const {
    xml_node data = node->append_child("value");
    data_.Encode(&data);
}

bool AttachmentInfo::Decode(const xml_node &parent, std::string *id_name,
                       AttachmentInfo *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }

        if (strcmp(node.name(), "value") == 0) {
            bool success = ptr->data_.XmlParse(node);
            if (!success) {
                return false;
            }
        }
        }
    return true;
}

void InstanceTarget::EncodeUpdate(xml_node *node) const {
    xml_node data = node->append_child("value");
    data_.Encode(&data);
}

bool InstanceTarget::Decode(const xml_node &parent, std::string *id_name,
                       InstanceTarget *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }

        if (strcmp(node.name(), "value") == 0) {
            bool success = ptr->data_.XmlParse(node);
            if (!success) {
                return false;
            }
        }
        }
    return true;
}
}  // namespace autogen
