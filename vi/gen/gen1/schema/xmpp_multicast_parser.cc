
#include "/opt/stack/contrail/build/production/schema/xmpp_multicast_types.h"

#include <stdint.h>
#include <sstream>
#include <boost/algorithm/string/trim.hpp>
#include <pugixml/pugixml.hpp>
#include <time.h>

using namespace pugi;
using namespace std;

#include "base/compiler.h"
#if defined(__GNUC__) && (__GCC_HAS_PRAGMA > 0)
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

namespace autogen {
static bool ParseInteger(const pugi::xml_node &node, int *valuep) {
    char *endp;
    *valuep = strtoul(node.child_value(), &endp, 10);
    while (isspace(*endp)) endp++;
    return endp[0] == '\0';
}

static bool ParseUnsignedLong(const pugi::xml_node &node, uint64_t *valuep) {
    char *endp;
    *valuep = strtoull(node.child_value(), &endp, 10);
    while (isspace(*endp)) endp++;
    return endp[0] == '\0';
}

static bool ParseBoolean(const pugi::xml_node &node, bool *valuep) {
    if (strcmp(node.child_value(), "true") ==0) 
        *valuep = true;
    else
        *valuep = false;
    return true;
}

static bool ParseDateTime(const pugi::xml_node &node, time_t *valuep) {
    string value(node.child_value());
    boost::trim(value);
    struct tm tm;
    char *endp;
    memset(&tm, 0, sizeof(tm));
    if (value.size() == 0) return true;
    endp = strptime(value.c_str(), "%FT%T", &tm);
    if (!endp) return false;
    *valuep = timegm(&tm);
    return true;
}
static bool ParseTime(const pugi::xml_node &node, time_t *valuep) {
    string value(node.child_value());
    boost::trim(value);
    struct tm tm;
    char *endp;
    endp = strptime(value.c_str(), "%T", &tm);
    if (!endp) return false;
    *valuep = timegm(&tm);
    return true;
}
static std::string FormatDateTime(const time_t *valuep) {
    struct tm tm;
    char result[100];
    gmtime_r(valuep, &tm);
    strftime(result, sizeof(result), "%FT%T", &tm);
    return std::string(result);
}
static std::string FormatTime(const time_t *valuep) {
    struct tm tm;
    char result[100];
    gmtime_r(valuep, &tm);
    strftime(result, sizeof(result), "%T", &tm);
    return std::string(result);
}

bool McastItemsType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "item") == 0) {

            McastItemType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            item.push_back(var);
        }
    }
    return true;
}

bool McastItemsType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    McastItemsType *ptr = new McastItemsType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void McastItemsType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<McastItemType>::const_iterator iter = item.begin();
         iter != item.end(); ++iter) {
        node_c = node_p->append_child("item");
        iter->Encode(&node_c);
    }
}

bool McastNlriType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "af") == 0) {
            if (!ParseInteger(node, &af)) return false;
        }
        if (strcmp(node.name(), "safi") == 0) {
            if (!ParseInteger(node, &safi)) return false;
        }
        if (strcmp(node.name(), "group") == 0) {
            group = node.child_value();
            boost::trim(group);
        }
        if (strcmp(node.name(), "source") == 0) {
            source = node.child_value();
            boost::trim(source);
        }
        if (strcmp(node.name(), "source-label") == 0) {
            if (!ParseInteger(node, &source_label)) return false;
        }
    }
    return true;
}

bool McastNlriType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    McastNlriType *ptr = new McastNlriType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void McastNlriType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("af");
    node_c.text().set(McastNlriType::af);

    // Add child node 
    node_c = node_p->append_child("safi");
    node_c.text().set(McastNlriType::safi);

    // Add child node 
    node_c = node_p->append_child("group");
    node_c.text().set(McastNlriType::group.c_str());

    // Add child node 
    node_c = node_p->append_child("source");
    node_c.text().set(McastNlriType::source.c_str());

    // Add child node 
    node_c = node_p->append_child("source-label");
    node_c.text().set(McastNlriType::source_label);

}

bool McastTunnelEncapsulationListType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "tunnel-encapsulation") == 0) {

            string var(node.child_value());
            boost::trim(var);
            tunnel_encapsulation.push_back(var);
        }
    }
    return true;
}

bool McastTunnelEncapsulationListType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    McastTunnelEncapsulationListType *ptr = new McastTunnelEncapsulationListType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void McastTunnelEncapsulationListType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<std::string>::const_iterator iter = tunnel_encapsulation.begin();
         iter != tunnel_encapsulation.end(); ++iter) {
        node_c = node_p->append_child("tunnel-encapsulation");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
}

bool McastItemType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "entry") == 0) {
            if (!entry.XmlParse(node)) return false;
        }
    }
    return true;
}

bool McastItemType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    McastItemType *ptr = new McastItemType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void McastItemType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("entry");
    entry.Encode(&node_c); 
}

bool McastEntryType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "nlri") == 0) {
            if (!nlri.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "next-hops") == 0) {
            if (!next_hops.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "olist") == 0) {
            if (!olist.XmlParse(node)) return false;
        }
    }
    return true;
}

bool McastEntryType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    McastEntryType *ptr = new McastEntryType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void McastEntryType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("nlri");
    nlri.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("next-hops");
    next_hops.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("olist");
    olist.Encode(&node_c); 
}

bool OlistType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "next-hop") == 0) {

            McastNextHopType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            next_hop.push_back(var);
        }
    }
    return true;
}

bool OlistType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    OlistType *ptr = new OlistType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void OlistType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<McastNextHopType>::const_iterator iter = next_hop.begin();
         iter != next_hop.end(); ++iter) {
        node_c = node_p->append_child("next-hop");
        iter->Encode(&node_c);
    }
}

bool McastNextHopsType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "next-hop") == 0) {

            McastNextHopType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            next_hop.push_back(var);
        }
    }
    return true;
}

bool McastNextHopsType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    McastNextHopsType *ptr = new McastNextHopsType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void McastNextHopsType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<McastNextHopType>::const_iterator iter = next_hop.begin();
         iter != next_hop.end(); ++iter) {
        node_c = node_p->append_child("next-hop");
        iter->Encode(&node_c);
    }
}

bool McastNextHopType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "af") == 0) {
            if (!ParseInteger(node, &af)) return false;
        }
        if (strcmp(node.name(), "address") == 0) {
            address = node.child_value();
            boost::trim(address);
        }
        if (strcmp(node.name(), "label") == 0) {
            label = node.child_value();
            boost::trim(label);
        }
        if (strcmp(node.name(), "tunnel-encapsulation-list") == 0) {
            if (!tunnel_encapsulation_list.XmlParse(node)) return false;
        }
    }
    return true;
}

bool McastNextHopType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    McastNextHopType *ptr = new McastNextHopType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void McastNextHopType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("af");
    node_c.text().set(McastNextHopType::af);

    // Add child node 
    node_c = node_p->append_child("address");
    node_c.text().set(McastNextHopType::address.c_str());

    // Add child node 
    node_c = node_p->append_child("label");
    node_c.text().set(McastNextHopType::label.c_str());

    // Add complex node 
    node_c = node_p->append_child("tunnel-encapsulation-list");
    tunnel_encapsulation_list.Encode(&node_c); 
}
}  // namespace autogen
