
#include "/opt/stack/contrail/build/production/schema/xmpp_unicast_types.h"

#include <stdint.h>
#include <sstream>
#include <boost/algorithm/string/trim.hpp>
#include <pugixml/pugixml.hpp>
#include <time.h>

using namespace pugi;
using namespace std;

#include "base/compiler.h"
#if defined(__GNUC__) && (__GCC_HAS_PRAGMA > 0)
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

namespace autogen {
static bool ParseInteger(const pugi::xml_node &node, int *valuep) {
    char *endp;
    *valuep = strtoul(node.child_value(), &endp, 10);
    while (isspace(*endp)) endp++;
    return endp[0] == '\0';
}

static bool ParseUnsignedLong(const pugi::xml_node &node, uint64_t *valuep) {
    char *endp;
    *valuep = strtoull(node.child_value(), &endp, 10);
    while (isspace(*endp)) endp++;
    return endp[0] == '\0';
}

static bool ParseBoolean(const pugi::xml_node &node, bool *valuep) {
    if (strcmp(node.child_value(), "true") ==0) 
        *valuep = true;
    else
        *valuep = false;
    return true;
}

static bool ParseDateTime(const pugi::xml_node &node, time_t *valuep) {
    string value(node.child_value());
    boost::trim(value);
    struct tm tm;
    char *endp;
    memset(&tm, 0, sizeof(tm));
    if (value.size() == 0) return true;
    endp = strptime(value.c_str(), "%FT%T", &tm);
    if (!endp) return false;
    *valuep = timegm(&tm);
    return true;
}
static bool ParseTime(const pugi::xml_node &node, time_t *valuep) {
    string value(node.child_value());
    boost::trim(value);
    struct tm tm;
    char *endp;
    endp = strptime(value.c_str(), "%T", &tm);
    if (!endp) return false;
    *valuep = timegm(&tm);
    return true;
}
static std::string FormatDateTime(const time_t *valuep) {
    struct tm tm;
    char result[100];
    gmtime_r(valuep, &tm);
    strftime(result, sizeof(result), "%FT%T", &tm);
    return std::string(result);
}
static std::string FormatTime(const time_t *valuep) {
    struct tm tm;
    char result[100];
    gmtime_r(valuep, &tm);
    strftime(result, sizeof(result), "%T", &tm);
    return std::string(result);
}

bool IPAddressType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "af") == 0) {
            if (!ParseInteger(node, &af)) return false;
        }
        if (strcmp(node.name(), "safi") == 0) {
            if (!ParseInteger(node, &safi)) return false;
        }
        if (strcmp(node.name(), "address") == 0) {
            address = node.child_value();
            boost::trim(address);
        }
    }
    return true;
}

bool IPAddressType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    IPAddressType *ptr = new IPAddressType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void IPAddressType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("af");
    node_c.text().set(IPAddressType::af);

    // Add child node 
    node_c = node_p->append_child("safi");
    node_c.text().set(IPAddressType::safi);

    // Add child node 
    node_c = node_p->append_child("address");
    node_c.text().set(IPAddressType::address.c_str());

}

bool TunnelEncapsulationListType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "tunnel-encapsulation") == 0) {

            string var(node.child_value());
            boost::trim(var);
            tunnel_encapsulation.push_back(var);
        }
    }
    return true;
}

bool TunnelEncapsulationListType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    TunnelEncapsulationListType *ptr = new TunnelEncapsulationListType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void TunnelEncapsulationListType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<std::string>::const_iterator iter = tunnel_encapsulation.begin();
         iter != tunnel_encapsulation.end(); ++iter) {
        node_c = node_p->append_child("tunnel-encapsulation");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
}

bool ItemsType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "item") == 0) {

            ItemType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            item.push_back(var);
        }
    }
    return true;
}

bool ItemsType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ItemsType *ptr = new ItemsType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ItemsType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<ItemType>::const_iterator iter = item.begin();
         iter != item.end(); ++iter) {
        node_c = node_p->append_child("item");
        iter->Encode(&node_c);
    }
}

bool NextHopType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "af") == 0) {
            if (!ParseInteger(node, &af)) return false;
        }
        if (strcmp(node.name(), "address") == 0) {
            address = node.child_value();
            boost::trim(address);
        }
        if (strcmp(node.name(), "label") == 0) {
            if (!ParseInteger(node, &label)) return false;
        }
        if (strcmp(node.name(), "tunnel-encapsulation-list") == 0) {
            if (!tunnel_encapsulation_list.XmlParse(node)) return false;
        }
    }
    return true;
}

bool NextHopType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    NextHopType *ptr = new NextHopType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void NextHopType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("af");
    node_c.text().set(NextHopType::af);

    // Add child node 
    node_c = node_p->append_child("address");
    node_c.text().set(NextHopType::address.c_str());

    // Add child node 
    node_c = node_p->append_child("label");
    node_c.text().set(NextHopType::label);

    // Add complex node 
    node_c = node_p->append_child("tunnel-encapsulation-list");
    tunnel_encapsulation_list.Encode(&node_c); 
}

bool NextHopListType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "next-hop") == 0) {

            NextHopType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            next_hop.push_back(var);
        }
    }
    return true;
}

bool NextHopListType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    NextHopListType *ptr = new NextHopListType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void NextHopListType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<NextHopType>::const_iterator iter = next_hop.begin();
         iter != next_hop.end(); ++iter) {
        node_c = node_p->append_child("next-hop");
        iter->Encode(&node_c);
    }
}

bool SecurityGroupListType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "security-group") == 0) {

            int var;
            if (!ParseInteger(node, &var)) return false;
            security_group.push_back(var);
        }
    }
    return true;
}

bool SecurityGroupListType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    SecurityGroupListType *ptr = new SecurityGroupListType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void SecurityGroupListType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<int>::const_iterator iter = security_group.begin();
         iter != security_group.end(); ++iter) {
        node_c = node_p->append_child("security-group");
        node_c.text().set(*iter);
    }
}

bool EntryType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "nlri") == 0) {
            if (!nlri.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "next-hops") == 0) {
            if (!next_hops.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "version") == 0) {
            if (!ParseInteger(node, &version)) return false;
        }
        if (strcmp(node.name(), "virtual-network") == 0) {
            virtual_network = node.child_value();
            boost::trim(virtual_network);
        }
        if (strcmp(node.name(), "sequence-number") == 0) {
            if (!ParseInteger(node, &sequence_number)) return false;
        }
        if (strcmp(node.name(), "security-group-list") == 0) {
            if (!security_group_list.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "local-preference") == 0) {
            if (!ParseInteger(node, &local_preference)) return false;
        }
    }
    return true;
}

bool EntryType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    EntryType *ptr = new EntryType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void EntryType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("nlri");
    nlri.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("next-hops");
    next_hops.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("version");
    node_c.text().set(EntryType::version);

    // Add child node 
    node_c = node_p->append_child("virtual-network");
    node_c.text().set(EntryType::virtual_network.c_str());

    // Add child node 
    node_c = node_p->append_child("sequence-number");
    node_c.text().set(EntryType::sequence_number);

    // Add complex node 
    node_c = node_p->append_child("security-group-list");
    security_group_list.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("local-preference");
    node_c.text().set(EntryType::local_preference);

}

bool ItemType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "entry") == 0) {
            if (!entry.XmlParse(node)) return false;
        }
    }
    return true;
}

bool ItemType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ItemType *ptr = new ItemType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ItemType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("entry");
    entry.Encode(&node_c); 
}
}  // namespace autogen
