
#include "/opt/stack/contrail/build/production/schema/vnc_cfg_types.h"

#include <stdint.h>
#include <sstream>
#include <boost/algorithm/string/trim.hpp>
#include <pugixml/pugixml.hpp>

#include "ifmap/ifmap_agent_parser.h"
#include "ifmap/ifmap_server_parser.h"
#include "ifmap/ifmap_table.h"

using namespace pugi;
using namespace std;

#include "base/compiler.h"
#if defined(__GNUC__) && (__GCC_HAS_PRAGMA > 0)
#pragma GCC diagnostic ignored "-Wunused-function"
#endif

namespace autogen {
static bool ParseInteger(const pugi::xml_node &node, int *valuep) {
    char *endp;
    *valuep = strtoul(node.child_value(), &endp, 10);
    while (isspace(*endp)) endp++;
    return endp[0] == '\0';
}

static bool ParseUnsignedLong(const pugi::xml_node &node, uint64_t *valuep) {
    char *endp;
    *valuep = strtoull(node.child_value(), &endp, 10);
    while (isspace(*endp)) endp++;
    return endp[0] == '\0';
}

static bool ParseBoolean(const pugi::xml_node &node, bool *valuep) {
    if (strcmp(node.child_value(), "true") ==0) 
        *valuep = true;
    else
        *valuep = false;
    return true;
}
static bool ParseDateTime(const pugi::xml_node &node, time_t *valuep) {
    string value(node.child_value());
    boost::trim(value);
    struct tm tm;
    memset(&tm, 0, sizeof(tm));
    if (value.size() == 0) return true;
    char *endp;
    endp = strptime(value.c_str(), "%FT%T", &tm);
    if (!endp) return false;
    *valuep = timegm(&tm);
    return true;
}
static bool ParseTime(const pugi::xml_node &node, time_t *valuep) {
    string value(node.child_value());
    boost::trim(value);
    struct tm tm;
    char *endp;
    endp = strptime(value.c_str(), "%T", &tm);
    if (!endp) return false;
    *valuep = timegm(&tm);
    return true;
}
static std::string FormatDateTime(const time_t *valuep) {
    struct tm tm;
    char result[100];
    gmtime_r(valuep, &tm);
    strftime(result, sizeof(result), "%FT%T", &tm);
    return std::string(result);
}
static std::string FormatTime(const time_t *valuep) {
    struct tm tm;
    char result[100];
    gmtime_r(valuep, &tm);
    strftime(result, sizeof(result), "%T", &tm);
    return std::string(result);
}

bool AclEntriesType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "dynamic") == 0) {
            if (!ParseBoolean(node, &dynamic)) return false;
        }
        if (strcmp(node.name(), "acl-rule") == 0) {

            AclRuleType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            acl_rule.push_back(var);
        }
    }
    return true;
}

bool AclEntriesType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    AclEntriesType *ptr = new AclEntriesType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void AclEntriesType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("dynamic");
    node_c.text().set(AclEntriesType::dynamic);


    for (std::vector<AclRuleType>::const_iterator iter = acl_rule.begin();
         iter != acl_rule.end(); ++iter) {
        node_c = node_p->append_child("acl-rule");
        iter->Encode(&node_c);
    }
}

bool ServiceScaleOutType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "max-instances") == 0) {
            if (!ParseInteger(node, &max_instances)) return false;
        }
        if (strcmp(node.name(), "auto-scale") == 0) {
            if (!ParseBoolean(node, &auto_scale)) return false;
        }
    }
    return true;
}

bool ServiceScaleOutType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ServiceScaleOutType *ptr = new ServiceScaleOutType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ServiceScaleOutType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("max-instances");
    node_c.text().set(ServiceScaleOutType::max_instances);

    // Add child node 
    node_c = node_p->append_child("auto-scale");
    node_c.text().set(ServiceScaleOutType::auto_scale);

}

bool VrfAssignTableType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "vrf-assign-rule") == 0) {

            VrfAssignRuleType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            vrf_assign_rule.push_back(var);
        }
    }
    return true;
}

bool VrfAssignTableType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    VrfAssignTableType *ptr = new VrfAssignTableType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void VrfAssignTableType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<VrfAssignRuleType>::const_iterator iter = vrf_assign_rule.begin();
         iter != vrf_assign_rule.end(); ++iter) {
        node_c = node_p->append_child("vrf-assign-rule");
        iter->Encode(&node_c);
    }
}

bool VirtualMachineInterfacePropertiesType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "service-interface-type") == 0) {
            service_interface_type = node.child_value();
            boost::trim(service_interface_type);
        }
        if (strcmp(node.name(), "interface-mirror") == 0) {
            if (!interface_mirror.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "local-preference") == 0) {
            if (!ParseInteger(node, &local_preference)) return false;
        }
        if (strcmp(node.name(), "sub-interface-vlan-tag") == 0) {
            if (!ParseInteger(node, &sub_interface_vlan_tag)) return false;
        }
    }
    return true;
}

bool VirtualMachineInterfacePropertiesType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    VirtualMachineInterfacePropertiesType *ptr = new VirtualMachineInterfacePropertiesType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void VirtualMachineInterfacePropertiesType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("service-interface-type");
    node_c.text().set(VirtualMachineInterfacePropertiesType::service_interface_type.c_str());

    // Add complex node 
    node_c = node_p->append_child("interface-mirror");
    interface_mirror.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("local-preference");
    node_c.text().set(VirtualMachineInterfacePropertiesType::local_preference);

    // Add child node 
    node_c = node_p->append_child("sub-interface-vlan-tag");
    node_c.text().set(VirtualMachineInterfacePropertiesType::sub_interface_vlan_tag);

}

bool PolicyEntriesType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "policy-rule") == 0) {

            PolicyRuleType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            policy_rule.push_back(var);
        }
    }
    return true;
}

bool PolicyEntriesType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    PolicyEntriesType *ptr = new PolicyEntriesType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void PolicyEntriesType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<PolicyRuleType>::const_iterator iter = policy_rule.begin();
         iter != policy_rule.end(); ++iter) {
        node_c = node_p->append_child("policy-rule");
        iter->Encode(&node_c);
    }
}

bool VrfAssignRuleType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "match-condition") == 0) {
            if (!match_condition.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "vlan-tag") == 0) {
            if (!ParseInteger(node, &vlan_tag)) return false;
        }
        if (strcmp(node.name(), "routing-instance") == 0) {
            routing_instance = node.child_value();
            boost::trim(routing_instance);
        }
        if (strcmp(node.name(), "ignore-acl") == 0) {
            if (!ParseBoolean(node, &ignore_acl)) return false;
        }
    }
    return true;
}

bool VrfAssignRuleType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    VrfAssignRuleType *ptr = new VrfAssignRuleType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void VrfAssignRuleType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("match-condition");
    match_condition.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("vlan-tag");
    node_c.text().set(VrfAssignRuleType::vlan_tag);

    // Add child node 
    node_c = node_p->append_child("routing-instance");
    node_c.text().set(VrfAssignRuleType::routing_instance.c_str());

    // Add child node 
    node_c = node_p->append_child("ignore-acl");
    node_c.text().set(VrfAssignRuleType::ignore_acl);

}

bool AllowedAddressPair::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "ip") == 0) {
            if (!ip.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "mac") == 0) {
            mac = node.child_value();
            boost::trim(mac);
        }
        if (strcmp(node.name(), "address-mode") == 0) {
            address_mode = node.child_value();
            boost::trim(address_mode);
        }
    }
    return true;
}

bool AllowedAddressPair::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    AllowedAddressPair *ptr = new AllowedAddressPair();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void AllowedAddressPair::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("ip");
    ip.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("mac");
    node_c.text().set(AllowedAddressPair::mac.c_str());

    // Add child node 
    node_c = node_p->append_child("address-mode");
    node_c.text().set(AllowedAddressPair::address_mode.c_str());

}

bool DhcpOptionType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "dhcp-option-name") == 0) {
            dhcp_option_name = node.child_value();
            boost::trim(dhcp_option_name);
        }
        if (strcmp(node.name(), "dhcp-option-value") == 0) {
            dhcp_option_value = node.child_value();
            boost::trim(dhcp_option_value);
        }
    }
    return true;
}

bool DhcpOptionType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    DhcpOptionType *ptr = new DhcpOptionType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void DhcpOptionType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("dhcp-option-name");
    node_c.text().set(DhcpOptionType::dhcp_option_name.c_str());

    // Add child node 
    node_c = node_p->append_child("dhcp-option-value");
    node_c.text().set(DhcpOptionType::dhcp_option_value.c_str());

}

bool IdPermsType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "permissions") == 0) {
            if (!permissions.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "uuid") == 0) {
            if (!uuid.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "enable") == 0) {
            if (!ParseBoolean(node, &enable)) return false;
        }
        if (strcmp(node.name(), "created") == 0) {
            if (!ParseDateTime(node, &created)) return false;
        }
        if (strcmp(node.name(), "last-modified") == 0) {
            if (!ParseDateTime(node, &last_modified)) return false;
        }
        if (strcmp(node.name(), "description") == 0) {
            description = node.child_value();
            boost::trim(description);
        }
        if (strcmp(node.name(), "user-visible") == 0) {
            if (!ParseBoolean(node, &user_visible)) return false;
        }
        if (strcmp(node.name(), "creator") == 0) {
            creator = node.child_value();
            boost::trim(creator);
        }
    }
    return true;
}

bool IdPermsType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    IdPermsType *ptr = new IdPermsType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void IdPermsType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("permissions");
    permissions.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("uuid");
    uuid.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("enable");
    node_c.text().set(IdPermsType::enable);

    // Add child node 
    node_c = node_p->append_child("created");
    node_c.text().set(FormatDateTime(&created).c_str());

    // Add child node 
    node_c = node_p->append_child("last-modified");
    node_c.text().set(FormatDateTime(&last_modified).c_str());

    // Add child node 
    node_c = node_p->append_child("description");
    node_c.text().set(IdPermsType::description.c_str());

    // Add child node 
    node_c = node_p->append_child("user-visible");
    node_c.text().set(IdPermsType::user_visible);

    // Add child node 
    node_c = node_p->append_child("creator");
    node_c.text().set(IdPermsType::creator.c_str());

}

bool RouteTargetList::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "route-target") == 0) {

            string var(node.child_value());
            boost::trim(var);
            route_target.push_back(var);
        }
    }
    return true;
}

bool RouteTargetList::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    RouteTargetList *ptr = new RouteTargetList();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void RouteTargetList::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<std::string>::const_iterator iter = route_target.begin();
         iter != route_target.end(); ++iter) {
        node_c = node_p->append_child("route-target");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
}

bool LoadbalancerPoolType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "status") == 0) {
            status = node.child_value();
            boost::trim(status);
        }
        if (strcmp(node.name(), "status-description") == 0) {
            status_description = node.child_value();
            boost::trim(status_description);
        }
        if (strcmp(node.name(), "admin-state") == 0) {
            if (!ParseBoolean(node, &admin_state)) return false;
        }
        if (strcmp(node.name(), "protocol") == 0) {
            protocol = node.child_value();
            boost::trim(protocol);
        }
        if (strcmp(node.name(), "loadbalancer-method") == 0) {
            loadbalancer_method = node.child_value();
            boost::trim(loadbalancer_method);
        }
        if (strcmp(node.name(), "subnet-id") == 0) {
            subnet_id = node.child_value();
            boost::trim(subnet_id);
        }
    }
    return true;
}

bool LoadbalancerPoolType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    LoadbalancerPoolType *ptr = new LoadbalancerPoolType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void LoadbalancerPoolType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("status");
    node_c.text().set(LoadbalancerPoolType::status.c_str());

    // Add child node 
    node_c = node_p->append_child("status-description");
    node_c.text().set(LoadbalancerPoolType::status_description.c_str());

    // Add child node 
    node_c = node_p->append_child("admin-state");
    node_c.text().set(LoadbalancerPoolType::admin_state);

    // Add child node 
    node_c = node_p->append_child("protocol");
    node_c.text().set(LoadbalancerPoolType::protocol.c_str());

    // Add child node 
    node_c = node_p->append_child("loadbalancer-method");
    node_c.text().set(LoadbalancerPoolType::loadbalancer_method.c_str());

    // Add child node 
    node_c = node_p->append_child("subnet-id");
    node_c.text().set(LoadbalancerPoolType::subnet_id.c_str());

}

bool ServiceInstanceType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "auto-policy") == 0) {
            if (!ParseBoolean(node, &auto_policy)) return false;
        }
        if (strcmp(node.name(), "availability-zone") == 0) {
            availability_zone = node.child_value();
            boost::trim(availability_zone);
        }
        if (strcmp(node.name(), "management-virtual-network") == 0) {
            management_virtual_network = node.child_value();
            boost::trim(management_virtual_network);
        }
        if (strcmp(node.name(), "left-virtual-network") == 0) {
            left_virtual_network = node.child_value();
            boost::trim(left_virtual_network);
        }
        if (strcmp(node.name(), "left-ip-address") == 0) {
            left_ip_address = node.child_value();
            boost::trim(left_ip_address);
        }
        if (strcmp(node.name(), "right-virtual-network") == 0) {
            right_virtual_network = node.child_value();
            boost::trim(right_virtual_network);
        }
        if (strcmp(node.name(), "right-ip-address") == 0) {
            right_ip_address = node.child_value();
            boost::trim(right_ip_address);
        }
        if (strcmp(node.name(), "interface-list") == 0) {

            ServiceInstanceInterfaceType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            interface_list.push_back(var);
        }
        if (strcmp(node.name(), "scale-out") == 0) {
            if (!scale_out.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "ha-mode") == 0) {
            ha_mode = node.child_value();
            boost::trim(ha_mode);
        }
        if (strcmp(node.name(), "virtual-router-id") == 0) {
            virtual_router_id = node.child_value();
            boost::trim(virtual_router_id);
        }
    }
    return true;
}

bool ServiceInstanceType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ServiceInstanceType *ptr = new ServiceInstanceType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ServiceInstanceType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("auto-policy");
    node_c.text().set(ServiceInstanceType::auto_policy);

    // Add child node 
    node_c = node_p->append_child("availability-zone");
    node_c.text().set(ServiceInstanceType::availability_zone.c_str());

    // Add child node 
    node_c = node_p->append_child("management-virtual-network");
    node_c.text().set(ServiceInstanceType::management_virtual_network.c_str());

    // Add child node 
    node_c = node_p->append_child("left-virtual-network");
    node_c.text().set(ServiceInstanceType::left_virtual_network.c_str());

    // Add child node 
    node_c = node_p->append_child("left-ip-address");
    node_c.text().set(ServiceInstanceType::left_ip_address.c_str());

    // Add child node 
    node_c = node_p->append_child("right-virtual-network");
    node_c.text().set(ServiceInstanceType::right_virtual_network.c_str());

    // Add child node 
    node_c = node_p->append_child("right-ip-address");
    node_c.text().set(ServiceInstanceType::right_ip_address.c_str());


    for (std::vector<ServiceInstanceInterfaceType>::const_iterator iter = interface_list.begin();
         iter != interface_list.end(); ++iter) {
        node_c = node_p->append_child("interface-list");
        iter->Encode(&node_c);
    }
    // Add complex node 
    node_c = node_p->append_child("scale-out");
    scale_out.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("ha-mode");
    node_c.text().set(ServiceInstanceType::ha_mode.c_str());

    // Add child node 
    node_c = node_p->append_child("virtual-router-id");
    node_c.text().set(ServiceInstanceType::virtual_router_id.c_str());

}

bool InterfaceMirrorType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "traffic-direction") == 0) {
            traffic_direction = node.child_value();
            boost::trim(traffic_direction);
        }
        if (strcmp(node.name(), "mirror-to") == 0) {
            if (!mirror_to.XmlParse(node)) return false;
        }
    }
    return true;
}

bool InterfaceMirrorType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    InterfaceMirrorType *ptr = new InterfaceMirrorType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void InterfaceMirrorType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("traffic-direction");
    node_c.text().set(InterfaceMirrorType::traffic_direction.c_str());

    // Add complex node 
    node_c = node_p->append_child("mirror-to");
    mirror_to.Encode(&node_c); 
}

bool IpamType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "ipam-method") == 0) {
            ipam_method = node.child_value();
            boost::trim(ipam_method);
        }
        if (strcmp(node.name(), "ipam-dns-method") == 0) {
            ipam_dns_method = node.child_value();
            boost::trim(ipam_dns_method);
        }
        if (strcmp(node.name(), "ipam-dns-server") == 0) {
            if (!ipam_dns_server.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "dhcp-option-list") == 0) {
            if (!dhcp_option_list.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "cidr-block") == 0) {
            if (!cidr_block.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "host-routes") == 0) {
            if (!host_routes.XmlParse(node)) return false;
        }
    }
    return true;
}

bool IpamType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    IpamType *ptr = new IpamType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void IpamType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("ipam-method");
    node_c.text().set(IpamType::ipam_method.c_str());

    // Add child node 
    node_c = node_p->append_child("ipam-dns-method");
    node_c.text().set(IpamType::ipam_dns_method.c_str());

    // Add complex node 
    node_c = node_p->append_child("ipam-dns-server");
    ipam_dns_server.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("dhcp-option-list");
    dhcp_option_list.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("cidr-block");
    cidr_block.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("host-routes");
    host_routes.Encode(&node_c); 
}

bool ServiceTemplateType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "service-mode") == 0) {
            service_mode = node.child_value();
            boost::trim(service_mode);
        }
        if (strcmp(node.name(), "service-type") == 0) {
            service_type = node.child_value();
            boost::trim(service_type);
        }
        if (strcmp(node.name(), "image-name") == 0) {
            image_name = node.child_value();
            boost::trim(image_name);
        }
        if (strcmp(node.name(), "service-scaling") == 0) {
            if (!ParseBoolean(node, &service_scaling)) return false;
        }
        if (strcmp(node.name(), "interface-type") == 0) {

            ServiceTemplateInterfaceType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            interface_type.push_back(var);
        }
        if (strcmp(node.name(), "flavor") == 0) {
            flavor = node.child_value();
            boost::trim(flavor);
        }
        if (strcmp(node.name(), "ordered-interfaces") == 0) {
            if (!ParseBoolean(node, &ordered_interfaces)) return false;
        }
        if (strcmp(node.name(), "service-virtualization-type") == 0) {
            service_virtualization_type = node.child_value();
            boost::trim(service_virtualization_type);
        }
        if (strcmp(node.name(), "availability-zone-enable") == 0) {
            if (!ParseBoolean(node, &availability_zone_enable)) return false;
        }
        if (strcmp(node.name(), "vrouter-instance-type") == 0) {
            vrouter_instance_type = node.child_value();
            boost::trim(vrouter_instance_type);
        }
        if (strcmp(node.name(), "instance-data") == 0) {
            instance_data = node.child_value();
            boost::trim(instance_data);
        }
    }
    return true;
}

bool ServiceTemplateType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ServiceTemplateType *ptr = new ServiceTemplateType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ServiceTemplateType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("service-mode");
    node_c.text().set(ServiceTemplateType::service_mode.c_str());

    // Add child node 
    node_c = node_p->append_child("service-type");
    node_c.text().set(ServiceTemplateType::service_type.c_str());

    // Add child node 
    node_c = node_p->append_child("image-name");
    node_c.text().set(ServiceTemplateType::image_name.c_str());

    // Add child node 
    node_c = node_p->append_child("service-scaling");
    node_c.text().set(ServiceTemplateType::service_scaling);


    for (std::vector<ServiceTemplateInterfaceType>::const_iterator iter = interface_type.begin();
         iter != interface_type.end(); ++iter) {
        node_c = node_p->append_child("interface-type");
        iter->Encode(&node_c);
    }
    // Add child node 
    node_c = node_p->append_child("flavor");
    node_c.text().set(ServiceTemplateType::flavor.c_str());

    // Add child node 
    node_c = node_p->append_child("ordered-interfaces");
    node_c.text().set(ServiceTemplateType::ordered_interfaces);

    // Add child node 
    node_c = node_p->append_child("service-virtualization-type");
    node_c.text().set(ServiceTemplateType::service_virtualization_type.c_str());

    // Add child node 
    node_c = node_p->append_child("availability-zone-enable");
    node_c.text().set(ServiceTemplateType::availability_zone_enable);

    // Add child node 
    node_c = node_p->append_child("vrouter-instance-type");
    node_c.text().set(ServiceTemplateType::vrouter_instance_type.c_str());

    // Add child node 
    node_c = node_p->append_child("instance-data");
    node_c.text().set(ServiceTemplateType::instance_data.c_str());

}

bool PolicyRuleType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "rule-sequence") == 0) {
            if (!rule_sequence.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "rule-uuid") == 0) {
            rule_uuid = node.child_value();
            boost::trim(rule_uuid);
        }
        if (strcmp(node.name(), "direction") == 0) {
            direction = node.child_value();
            boost::trim(direction);
        }
        if (strcmp(node.name(), "protocol") == 0) {
            protocol = node.child_value();
            boost::trim(protocol);
        }
        if (strcmp(node.name(), "src-addresses") == 0) {

            AddressType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            src_addresses.push_back(var);
        }
        if (strcmp(node.name(), "src-ports") == 0) {

            PortType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            src_ports.push_back(var);
        }
        if (strcmp(node.name(), "application") == 0) {

            string var(node.child_value());
            boost::trim(var);
            application.push_back(var);
        }
        if (strcmp(node.name(), "dst-addresses") == 0) {

            AddressType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            dst_addresses.push_back(var);
        }
        if (strcmp(node.name(), "dst-ports") == 0) {

            PortType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            dst_ports.push_back(var);
        }
        if (strcmp(node.name(), "action-list") == 0) {
            if (!action_list.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "ethertype") == 0) {
            ethertype = node.child_value();
            boost::trim(ethertype);
        }
    }
    return true;
}

bool PolicyRuleType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    PolicyRuleType *ptr = new PolicyRuleType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void PolicyRuleType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("rule-sequence");
    rule_sequence.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("rule-uuid");
    node_c.text().set(PolicyRuleType::rule_uuid.c_str());

    // Add child node 
    node_c = node_p->append_child("direction");
    node_c.text().set(PolicyRuleType::direction.c_str());

    // Add child node 
    node_c = node_p->append_child("protocol");
    node_c.text().set(PolicyRuleType::protocol.c_str());


    for (std::vector<AddressType>::const_iterator iter = src_addresses.begin();
         iter != src_addresses.end(); ++iter) {
        node_c = node_p->append_child("src-addresses");
        iter->Encode(&node_c);
    }

    for (std::vector<PortType>::const_iterator iter = src_ports.begin();
         iter != src_ports.end(); ++iter) {
        node_c = node_p->append_child("src-ports");
        iter->Encode(&node_c);
    }

    for (std::vector<std::string>::const_iterator iter = application.begin();
         iter != application.end(); ++iter) {
        node_c = node_p->append_child("application");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }

    for (std::vector<AddressType>::const_iterator iter = dst_addresses.begin();
         iter != dst_addresses.end(); ++iter) {
        node_c = node_p->append_child("dst-addresses");
        iter->Encode(&node_c);
    }

    for (std::vector<PortType>::const_iterator iter = dst_ports.begin();
         iter != dst_ports.end(); ++iter) {
        node_c = node_p->append_child("dst-ports");
        iter->Encode(&node_c);
    }
    // Add complex node 
    node_c = node_p->append_child("action-list");
    action_list.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("ethertype");
    node_c.text().set(PolicyRuleType::ethertype.c_str());

}

bool AddressType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "subnet") == 0) {
            if (!subnet.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "virtual-network") == 0) {
            virtual_network = node.child_value();
            boost::trim(virtual_network);
        }
        if (strcmp(node.name(), "security-group") == 0) {
            security_group = node.child_value();
            boost::trim(security_group);
        }
        if (strcmp(node.name(), "network-policy") == 0) {
            network_policy = node.child_value();
            boost::trim(network_policy);
        }
    }
    return true;
}

bool AddressType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    AddressType *ptr = new AddressType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void AddressType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("subnet");
    subnet.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("virtual-network");
    node_c.text().set(AddressType::virtual_network.c_str());

    // Add child node 
    node_c = node_p->append_child("security-group");
    node_c.text().set(AddressType::security_group.c_str());

    // Add child node 
    node_c = node_p->append_child("network-policy");
    node_c.text().set(AddressType::network_policy.c_str());

}

bool VirtualDnsRecordType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "record-name") == 0) {
            record_name = node.child_value();
            boost::trim(record_name);
        }
        if (strcmp(node.name(), "record-type") == 0) {
            record_type = node.child_value();
            boost::trim(record_type);
        }
        if (strcmp(node.name(), "record-class") == 0) {
            record_class = node.child_value();
            boost::trim(record_class);
        }
        if (strcmp(node.name(), "record-data") == 0) {
            record_data = node.child_value();
            boost::trim(record_data);
        }
        if (strcmp(node.name(), "record-ttl-seconds") == 0) {
            if (!ParseInteger(node, &record_ttl_seconds)) return false;
        }
        if (strcmp(node.name(), "record-mx-preference") == 0) {
            if (!ParseInteger(node, &record_mx_preference)) return false;
        }
    }
    return true;
}

bool VirtualDnsRecordType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    VirtualDnsRecordType *ptr = new VirtualDnsRecordType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void VirtualDnsRecordType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("record-name");
    node_c.text().set(VirtualDnsRecordType::record_name.c_str());

    // Add child node 
    node_c = node_p->append_child("record-type");
    node_c.text().set(VirtualDnsRecordType::record_type.c_str());

    // Add child node 
    node_c = node_p->append_child("record-class");
    node_c.text().set(VirtualDnsRecordType::record_class.c_str());

    // Add child node 
    node_c = node_p->append_child("record-data");
    node_c.text().set(VirtualDnsRecordType::record_data.c_str());

    // Add child node 
    node_c = node_p->append_child("record-ttl-seconds");
    node_c.text().set(VirtualDnsRecordType::record_ttl_seconds);

    // Add child node 
    node_c = node_p->append_child("record-mx-preference");
    node_c.text().set(VirtualDnsRecordType::record_mx_preference);

}

bool IpamDnsAddressType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "tenant-dns-server-address") == 0) {
            if (!tenant_dns_server_address.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "virtual-dns-server-name") == 0) {
            virtual_dns_server_name = node.child_value();
            boost::trim(virtual_dns_server_name);
        }
    }
    return true;
}

bool IpamDnsAddressType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    IpamDnsAddressType *ptr = new IpamDnsAddressType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void IpamDnsAddressType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("tenant-dns-server-address");
    tenant_dns_server_address.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("virtual-dns-server-name");
    node_c.text().set(IpamDnsAddressType::virtual_dns_server_name.c_str());

}

bool TimerType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "start-time") == 0) {
            if (!ParseDateTime(node, &start_time)) return false;
        }
        if (strcmp(node.name(), "on-interval") == 0) {
            if (!ParseTime(node, &on_interval)) return false;
        }
        if (strcmp(node.name(), "off-interval") == 0) {
            if (!ParseTime(node, &off_interval)) return false;
        }
        if (strcmp(node.name(), "end-time") == 0) {
            if (!ParseDateTime(node, &end_time)) return false;
        }
    }
    return true;
}

bool TimerType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    TimerType *ptr = new TimerType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void TimerType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("start-time");
    node_c.text().set(FormatDateTime(&start_time).c_str());

    // Add child node 
    node_c = node_p->append_child("on-interval");
    node_c.text().set(FormatTime(&on_interval).c_str());

    // Add child node 
    node_c = node_p->append_child("off-interval");
    node_c.text().set(FormatTime(&off_interval).c_str());

    // Add child node 
    node_c = node_p->append_child("end-time");
    node_c.text().set(FormatDateTime(&end_time).c_str());

}

bool VirtualNetworkType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "allow-transit") == 0) {
            if (!ParseBoolean(node, &allow_transit)) return false;
        }
        if (strcmp(node.name(), "network-id") == 0) {
            if (!ParseInteger(node, &network_id)) return false;
        }
        if (strcmp(node.name(), "vxlan-network-identifier") == 0) {
            if (!ParseInteger(node, &vxlan_network_identifier)) return false;
        }
        if (strcmp(node.name(), "forwarding-mode") == 0) {
            forwarding_mode = node.child_value();
            boost::trim(forwarding_mode);
        }
        if (strcmp(node.name(), "rpf") == 0) {
            rpf = node.child_value();
            boost::trim(rpf);
        }
    }
    return true;
}

bool VirtualNetworkType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    VirtualNetworkType *ptr = new VirtualNetworkType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void VirtualNetworkType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("allow-transit");
    node_c.text().set(VirtualNetworkType::allow_transit);

    // Add child node 
    node_c = node_p->append_child("network-id");
    node_c.text().set(VirtualNetworkType::network_id);

    // Add child node 
    node_c = node_p->append_child("vxlan-network-identifier");
    node_c.text().set(VirtualNetworkType::vxlan_network_identifier);

    // Add child node 
    node_c = node_p->append_child("forwarding-mode");
    node_c.text().set(VirtualNetworkType::forwarding_mode.c_str());

    // Add child node 
    node_c = node_p->append_child("rpf");
    node_c.text().set(VirtualNetworkType::rpf.c_str());

}

bool ServiceInstanceInterfaceType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "virtual-network") == 0) {
            virtual_network = node.child_value();
            boost::trim(virtual_network);
        }
        if (strcmp(node.name(), "ip-address") == 0) {
            ip_address = node.child_value();
            boost::trim(ip_address);
        }
        if (strcmp(node.name(), "static-routes") == 0) {
            if (!static_routes.XmlParse(node)) return false;
        }
    }
    return true;
}

bool ServiceInstanceInterfaceType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ServiceInstanceInterfaceType *ptr = new ServiceInstanceInterfaceType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ServiceInstanceInterfaceType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("virtual-network");
    node_c.text().set(ServiceInstanceInterfaceType::virtual_network.c_str());

    // Add child node 
    node_c = node_p->append_child("ip-address");
    node_c.text().set(ServiceInstanceInterfaceType::ip_address.c_str());

    // Add complex node 
    node_c = node_p->append_child("static-routes");
    static_routes.Encode(&node_c); 
}

bool PluginProperty::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "property") == 0) {
            property = node.child_value();
            boost::trim(property);
        }
        if (strcmp(node.name(), "value") == 0) {
            value = node.child_value();
            boost::trim(value);
        }
    }
    return true;
}

bool PluginProperty::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    PluginProperty *ptr = new PluginProperty();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void PluginProperty::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("property");
    node_c.text().set(PluginProperty::property.c_str());

    // Add child node 
    node_c = node_p->append_child("value");
    node_c.text().set(PluginProperty::value.c_str());

}

bool MirrorActionType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "analyzer-name") == 0) {
            analyzer_name = node.child_value();
            boost::trim(analyzer_name);
        }
        if (strcmp(node.name(), "encapsulation") == 0) {
            encapsulation = node.child_value();
            boost::trim(encapsulation);
        }
        if (strcmp(node.name(), "analyzer-ip-address") == 0) {
            analyzer_ip_address = node.child_value();
            boost::trim(analyzer_ip_address);
        }
        if (strcmp(node.name(), "routing-instance") == 0) {
            routing_instance = node.child_value();
            boost::trim(routing_instance);
        }
        if (strcmp(node.name(), "udp-port") == 0) {
            if (!ParseInteger(node, &udp_port)) return false;
        }
    }
    return true;
}

bool MirrorActionType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    MirrorActionType *ptr = new MirrorActionType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void MirrorActionType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("analyzer-name");
    node_c.text().set(MirrorActionType::analyzer_name.c_str());

    // Add child node 
    node_c = node_p->append_child("encapsulation");
    node_c.text().set(MirrorActionType::encapsulation.c_str());

    // Add child node 
    node_c = node_p->append_child("analyzer-ip-address");
    node_c.text().set(MirrorActionType::analyzer_ip_address.c_str());

    // Add child node 
    node_c = node_p->append_child("routing-instance");
    node_c.text().set(MirrorActionType::routing_instance.c_str());

    // Add child node 
    node_c = node_p->append_child("udp-port");
    node_c.text().set(MirrorActionType::udp_port);

}

bool MatchConditionType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "protocol") == 0) {
            protocol = node.child_value();
            boost::trim(protocol);
        }
        if (strcmp(node.name(), "src-address") == 0) {
            if (!src_address.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "src-port") == 0) {
            if (!src_port.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "dst-address") == 0) {
            if (!dst_address.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "dst-port") == 0) {
            if (!dst_port.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "ethertype") == 0) {
            ethertype = node.child_value();
            boost::trim(ethertype);
        }
    }
    return true;
}

bool MatchConditionType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    MatchConditionType *ptr = new MatchConditionType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void MatchConditionType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("protocol");
    node_c.text().set(MatchConditionType::protocol.c_str());

    // Add complex node 
    node_c = node_p->append_child("src-address");
    src_address.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("src-port");
    src_port.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("dst-address");
    dst_address.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("dst-port");
    dst_port.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("ethertype");
    node_c.text().set(MatchConditionType::ethertype.c_str());

}

bool SNMPCredentials::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "version") == 0) {
            if (!ParseInteger(node, &version)) return false;
        }
        if (strcmp(node.name(), "local-port") == 0) {
            if (!ParseInteger(node, &local_port)) return false;
        }
        if (strcmp(node.name(), "retries") == 0) {
            if (!ParseInteger(node, &retries)) return false;
        }
        if (strcmp(node.name(), "timeout") == 0) {
            if (!ParseInteger(node, &timeout)) return false;
        }
        if (strcmp(node.name(), "v2-community") == 0) {
            v2_community = node.child_value();
            boost::trim(v2_community);
        }
        if (strcmp(node.name(), "v3-security-name") == 0) {
            v3_security_name = node.child_value();
            boost::trim(v3_security_name);
        }
        if (strcmp(node.name(), "v3-security-level") == 0) {
            v3_security_level = node.child_value();
            boost::trim(v3_security_level);
        }
        if (strcmp(node.name(), "v3-security-engine-id") == 0) {
            v3_security_engine_id = node.child_value();
            boost::trim(v3_security_engine_id);
        }
        if (strcmp(node.name(), "v3-context") == 0) {
            v3_context = node.child_value();
            boost::trim(v3_context);
        }
        if (strcmp(node.name(), "v3-context-engine-id") == 0) {
            v3_context_engine_id = node.child_value();
            boost::trim(v3_context_engine_id);
        }
        if (strcmp(node.name(), "v3-authentication-protocol") == 0) {
            v3_authentication_protocol = node.child_value();
            boost::trim(v3_authentication_protocol);
        }
        if (strcmp(node.name(), "v3-authentication-password") == 0) {
            v3_authentication_password = node.child_value();
            boost::trim(v3_authentication_password);
        }
        if (strcmp(node.name(), "v3-privacy-protocol") == 0) {
            v3_privacy_protocol = node.child_value();
            boost::trim(v3_privacy_protocol);
        }
        if (strcmp(node.name(), "v3-privacy-password") == 0) {
            v3_privacy_password = node.child_value();
            boost::trim(v3_privacy_password);
        }
        if (strcmp(node.name(), "v3-engine-id") == 0) {
            v3_engine_id = node.child_value();
            boost::trim(v3_engine_id);
        }
        if (strcmp(node.name(), "v3-engine-boots") == 0) {
            if (!ParseInteger(node, &v3_engine_boots)) return false;
        }
        if (strcmp(node.name(), "v3-engine-time") == 0) {
            if (!ParseInteger(node, &v3_engine_time)) return false;
        }
    }
    return true;
}

bool SNMPCredentials::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    SNMPCredentials *ptr = new SNMPCredentials();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void SNMPCredentials::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("version");
    node_c.text().set(SNMPCredentials::version);

    // Add child node 
    node_c = node_p->append_child("local-port");
    node_c.text().set(SNMPCredentials::local_port);

    // Add child node 
    node_c = node_p->append_child("retries");
    node_c.text().set(SNMPCredentials::retries);

    // Add child node 
    node_c = node_p->append_child("timeout");
    node_c.text().set(SNMPCredentials::timeout);

    // Add child node 
    node_c = node_p->append_child("v2-community");
    node_c.text().set(SNMPCredentials::v2_community.c_str());

    // Add child node 
    node_c = node_p->append_child("v3-security-name");
    node_c.text().set(SNMPCredentials::v3_security_name.c_str());

    // Add child node 
    node_c = node_p->append_child("v3-security-level");
    node_c.text().set(SNMPCredentials::v3_security_level.c_str());

    // Add child node 
    node_c = node_p->append_child("v3-security-engine-id");
    node_c.text().set(SNMPCredentials::v3_security_engine_id.c_str());

    // Add child node 
    node_c = node_p->append_child("v3-context");
    node_c.text().set(SNMPCredentials::v3_context.c_str());

    // Add child node 
    node_c = node_p->append_child("v3-context-engine-id");
    node_c.text().set(SNMPCredentials::v3_context_engine_id.c_str());

    // Add child node 
    node_c = node_p->append_child("v3-authentication-protocol");
    node_c.text().set(SNMPCredentials::v3_authentication_protocol.c_str());

    // Add child node 
    node_c = node_p->append_child("v3-authentication-password");
    node_c.text().set(SNMPCredentials::v3_authentication_password.c_str());

    // Add child node 
    node_c = node_p->append_child("v3-privacy-protocol");
    node_c.text().set(SNMPCredentials::v3_privacy_protocol.c_str());

    // Add child node 
    node_c = node_p->append_child("v3-privacy-password");
    node_c.text().set(SNMPCredentials::v3_privacy_password.c_str());

    // Add child node 
    node_c = node_p->append_child("v3-engine-id");
    node_c.text().set(SNMPCredentials::v3_engine_id.c_str());

    // Add child node 
    node_c = node_p->append_child("v3-engine-boots");
    node_c.text().set(SNMPCredentials::v3_engine_boots);

    // Add child node 
    node_c = node_p->append_child("v3-engine-time");
    node_c.text().set(SNMPCredentials::v3_engine_time);

}

bool VirtualNetworkPolicyType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "sequence") == 0) {
            if (!sequence.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "timer") == 0) {
            if (!timer.XmlParse(node)) return false;
        }
    }
    return true;
}

bool VirtualNetworkPolicyType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    VirtualNetworkPolicyType *ptr = new VirtualNetworkPolicyType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void VirtualNetworkPolicyType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("sequence");
    sequence.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("timer");
    timer.Encode(&node_c); 
}

bool AllowedAddressPairs::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "allowed-address-pair") == 0) {

            AllowedAddressPair var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            allowed_address_pair.push_back(var);
        }
    }
    return true;
}

bool AllowedAddressPairs::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    AllowedAddressPairs *ptr = new AllowedAddressPairs();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void AllowedAddressPairs::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<AllowedAddressPair>::const_iterator iter = allowed_address_pair.begin();
         iter != allowed_address_pair.end(); ++iter) {
        node_c = node_p->append_child("allowed-address-pair");
        iter->Encode(&node_c);
    }
}

bool LinklocalServicesTypes::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "linklocal-service-entry") == 0) {

            LinklocalServiceEntryType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            linklocal_service_entry.push_back(var);
        }
    }
    return true;
}

bool LinklocalServicesTypes::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    LinklocalServicesTypes *ptr = new LinklocalServicesTypes();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void LinklocalServicesTypes::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<LinklocalServiceEntryType>::const_iterator iter = linklocal_service_entry.begin();
         iter != linklocal_service_entry.end(); ++iter) {
        node_c = node_p->append_child("linklocal-service-entry");
        iter->Encode(&node_c);
    }
}

bool ApiAccessType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "api-name") == 0) {
            api_name = node.child_value();
            boost::trim(api_name);
        }
        if (strcmp(node.name(), "permissions") == 0) {
            if (!permissions.XmlParse(node)) return false;
        }
    }
    return true;
}

bool ApiAccessType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ApiAccessType *ptr = new ApiAccessType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ApiAccessType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("api-name");
    node_c.text().set(ApiAccessType::api_name.c_str());

    // Add complex node 
    node_c = node_p->append_child("permissions");
    permissions.Encode(&node_c); 
}

bool FloatingIpPoolType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "subnet") == 0) {

            SubnetType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            subnet.push_back(var);
        }
    }
    return true;
}

bool FloatingIpPoolType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    FloatingIpPoolType *ptr = new FloatingIpPoolType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void FloatingIpPoolType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<SubnetType>::const_iterator iter = subnet.begin();
         iter != subnet.end(); ++iter) {
        node_c = node_p->append_child("subnet");
        iter->Encode(&node_c);
    }
}

bool PolicyBasedForwardingRuleType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "direction") == 0) {
            direction = node.child_value();
            boost::trim(direction);
        }
        if (strcmp(node.name(), "vlan-tag") == 0) {
            if (!ParseInteger(node, &vlan_tag)) return false;
        }
        if (strcmp(node.name(), "src-mac") == 0) {
            src_mac = node.child_value();
            boost::trim(src_mac);
        }
        if (strcmp(node.name(), "dst-mac") == 0) {
            dst_mac = node.child_value();
            boost::trim(dst_mac);
        }
        if (strcmp(node.name(), "mpls-label") == 0) {
            if (!ParseInteger(node, &mpls_label)) return false;
        }
        if (strcmp(node.name(), "service-chain-address") == 0) {
            service_chain_address = node.child_value();
            boost::trim(service_chain_address);
        }
        if (strcmp(node.name(), "protocol") == 0) {
            protocol = node.child_value();
            boost::trim(protocol);
        }
    }
    return true;
}

bool PolicyBasedForwardingRuleType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    PolicyBasedForwardingRuleType *ptr = new PolicyBasedForwardingRuleType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void PolicyBasedForwardingRuleType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("direction");
    node_c.text().set(PolicyBasedForwardingRuleType::direction.c_str());

    // Add child node 
    node_c = node_p->append_child("vlan-tag");
    node_c.text().set(PolicyBasedForwardingRuleType::vlan_tag);

    // Add child node 
    node_c = node_p->append_child("src-mac");
    node_c.text().set(PolicyBasedForwardingRuleType::src_mac.c_str());

    // Add child node 
    node_c = node_p->append_child("dst-mac");
    node_c.text().set(PolicyBasedForwardingRuleType::dst_mac.c_str());

    // Add child node 
    node_c = node_p->append_child("mpls-label");
    node_c.text().set(PolicyBasedForwardingRuleType::mpls_label);

    // Add child node 
    node_c = node_p->append_child("service-chain-address");
    node_c.text().set(PolicyBasedForwardingRuleType::service_chain_address.c_str());

    // Add child node 
    node_c = node_p->append_child("protocol");
    node_c.text().set(PolicyBasedForwardingRuleType::protocol.c_str());

}

bool IpamSubnetType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "subnet") == 0) {
            if (!subnet.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "default-gateway") == 0) {
            default_gateway = node.child_value();
            boost::trim(default_gateway);
        }
        if (strcmp(node.name(), "dns-server-address") == 0) {
            dns_server_address = node.child_value();
            boost::trim(dns_server_address);
        }
        if (strcmp(node.name(), "subnet-uuid") == 0) {
            subnet_uuid = node.child_value();
            boost::trim(subnet_uuid);
        }
        if (strcmp(node.name(), "enable-dhcp") == 0) {
            if (!ParseBoolean(node, &enable_dhcp)) return false;
        }
        if (strcmp(node.name(), "dns-nameservers") == 0) {

            string var(node.child_value());
            boost::trim(var);
            dns_nameservers.push_back(var);
        }
        if (strcmp(node.name(), "allocation-pools") == 0) {

            AllocationPoolType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            allocation_pools.push_back(var);
        }
        if (strcmp(node.name(), "addr_from_start") == 0) {
            if (!ParseBoolean(node, &addr_from_start)) return false;
        }
        if (strcmp(node.name(), "dhcp-option-list") == 0) {
            if (!dhcp_option_list.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "host-routes") == 0) {
            if (!host_routes.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "subnet-name") == 0) {
            subnet_name = node.child_value();
            boost::trim(subnet_name);
        }
    }
    return true;
}

bool IpamSubnetType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    IpamSubnetType *ptr = new IpamSubnetType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void IpamSubnetType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("subnet");
    subnet.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("default-gateway");
    node_c.text().set(IpamSubnetType::default_gateway.c_str());

    // Add child node 
    node_c = node_p->append_child("dns-server-address");
    node_c.text().set(IpamSubnetType::dns_server_address.c_str());

    // Add child node 
    node_c = node_p->append_child("subnet-uuid");
    node_c.text().set(IpamSubnetType::subnet_uuid.c_str());

    // Add child node 
    node_c = node_p->append_child("enable-dhcp");
    node_c.text().set(IpamSubnetType::enable_dhcp);


    for (std::vector<std::string>::const_iterator iter = dns_nameservers.begin();
         iter != dns_nameservers.end(); ++iter) {
        node_c = node_p->append_child("dns-nameservers");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }

    for (std::vector<AllocationPoolType>::const_iterator iter = allocation_pools.begin();
         iter != allocation_pools.end(); ++iter) {
        node_c = node_p->append_child("allocation-pools");
        iter->Encode(&node_c);
    }
    // Add child node 
    node_c = node_p->append_child("addr_from_start");
    node_c.text().set(IpamSubnetType::addr_from_start);

    // Add complex node 
    node_c = node_p->append_child("dhcp-option-list");
    dhcp_option_list.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("host-routes");
    host_routes.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("subnet-name");
    node_c.text().set(IpamSubnetType::subnet_name.c_str());

}

bool UserCredentials::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "username") == 0) {
            username = node.child_value();
            boost::trim(username);
        }
        if (strcmp(node.name(), "password") == 0) {
            password = node.child_value();
            boost::trim(password);
        }
    }
    return true;
}

bool UserCredentials::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    UserCredentials *ptr = new UserCredentials();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void UserCredentials::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("username");
    node_c.text().set(UserCredentials::username.c_str());

    // Add child node 
    node_c = node_p->append_child("password");
    node_c.text().set(UserCredentials::password.c_str());

}

bool VirtualIpType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "address") == 0) {
            address = node.child_value();
            boost::trim(address);
        }
        if (strcmp(node.name(), "status") == 0) {
            status = node.child_value();
            boost::trim(status);
        }
        if (strcmp(node.name(), "status-description") == 0) {
            status_description = node.child_value();
            boost::trim(status_description);
        }
        if (strcmp(node.name(), "admin-state") == 0) {
            if (!ParseBoolean(node, &admin_state)) return false;
        }
        if (strcmp(node.name(), "protocol") == 0) {
            protocol = node.child_value();
            boost::trim(protocol);
        }
        if (strcmp(node.name(), "protocol-port") == 0) {
            if (!ParseInteger(node, &protocol_port)) return false;
        }
        if (strcmp(node.name(), "connection-limit") == 0) {
            if (!ParseInteger(node, &connection_limit)) return false;
        }
        if (strcmp(node.name(), "subnet-id") == 0) {
            subnet_id = node.child_value();
            boost::trim(subnet_id);
        }
        if (strcmp(node.name(), "persistence-cookie-name") == 0) {
            persistence_cookie_name = node.child_value();
            boost::trim(persistence_cookie_name);
        }
        if (strcmp(node.name(), "persistence-type") == 0) {
            persistence_type = node.child_value();
            boost::trim(persistence_type);
        }
    }
    return true;
}

bool VirtualIpType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    VirtualIpType *ptr = new VirtualIpType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void VirtualIpType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("address");
    node_c.text().set(VirtualIpType::address.c_str());

    // Add child node 
    node_c = node_p->append_child("status");
    node_c.text().set(VirtualIpType::status.c_str());

    // Add child node 
    node_c = node_p->append_child("status-description");
    node_c.text().set(VirtualIpType::status_description.c_str());

    // Add child node 
    node_c = node_p->append_child("admin-state");
    node_c.text().set(VirtualIpType::admin_state);

    // Add child node 
    node_c = node_p->append_child("protocol");
    node_c.text().set(VirtualIpType::protocol.c_str());

    // Add child node 
    node_c = node_p->append_child("protocol-port");
    node_c.text().set(VirtualIpType::protocol_port);

    // Add child node 
    node_c = node_p->append_child("connection-limit");
    node_c.text().set(VirtualIpType::connection_limit);

    // Add child node 
    node_c = node_p->append_child("subnet-id");
    node_c.text().set(VirtualIpType::subnet_id.c_str());

    // Add child node 
    node_c = node_p->append_child("persistence-cookie-name");
    node_c.text().set(VirtualIpType::persistence_cookie_name.c_str());

    // Add child node 
    node_c = node_p->append_child("persistence-type");
    node_c.text().set(VirtualIpType::persistence_type.c_str());

}

bool VirtualDnsType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "domain-name") == 0) {
            domain_name = node.child_value();
            boost::trim(domain_name);
        }
        if (strcmp(node.name(), "dynamic-records-from-client") == 0) {
            if (!ParseBoolean(node, &dynamic_records_from_client)) return false;
        }
        if (strcmp(node.name(), "record-order") == 0) {
            record_order = node.child_value();
            boost::trim(record_order);
        }
        if (strcmp(node.name(), "default-ttl-seconds") == 0) {
            if (!ParseInteger(node, &default_ttl_seconds)) return false;
        }
        if (strcmp(node.name(), "next-virtual-DNS") == 0) {
            next_virtual_DNS = node.child_value();
            boost::trim(next_virtual_DNS);
        }
        if (strcmp(node.name(), "floating-ip-record") == 0) {
            floating_ip_record = node.child_value();
            boost::trim(floating_ip_record);
        }
    }
    return true;
}

bool VirtualDnsType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    VirtualDnsType *ptr = new VirtualDnsType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void VirtualDnsType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("domain-name");
    node_c.text().set(VirtualDnsType::domain_name.c_str());

    // Add child node 
    node_c = node_p->append_child("dynamic-records-from-client");
    node_c.text().set(VirtualDnsType::dynamic_records_from_client);

    // Add child node 
    node_c = node_p->append_child("record-order");
    node_c.text().set(VirtualDnsType::record_order.c_str());

    // Add child node 
    node_c = node_p->append_child("default-ttl-seconds");
    node_c.text().set(VirtualDnsType::default_ttl_seconds);

    // Add child node 
    node_c = node_p->append_child("next-virtual-DNS");
    node_c.text().set(VirtualDnsType::next_virtual_DNS.c_str());

    // Add child node 
    node_c = node_p->append_child("floating-ip-record");
    node_c.text().set(VirtualDnsType::floating_ip_record.c_str());

}

bool LoadbalancerHealthmonitorType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "admin-state") == 0) {
            if (!ParseBoolean(node, &admin_state)) return false;
        }
        if (strcmp(node.name(), "monitor-type") == 0) {
            monitor_type = node.child_value();
            boost::trim(monitor_type);
        }
        if (strcmp(node.name(), "delay") == 0) {
            if (!ParseInteger(node, &delay)) return false;
        }
        if (strcmp(node.name(), "timeout") == 0) {
            if (!ParseInteger(node, &timeout)) return false;
        }
        if (strcmp(node.name(), "max-retries") == 0) {
            if (!ParseInteger(node, &max_retries)) return false;
        }
        if (strcmp(node.name(), "http-method") == 0) {
            http_method = node.child_value();
            boost::trim(http_method);
        }
        if (strcmp(node.name(), "url-path") == 0) {
            url_path = node.child_value();
            boost::trim(url_path);
        }
        if (strcmp(node.name(), "expected-codes") == 0) {
            expected_codes = node.child_value();
            boost::trim(expected_codes);
        }
    }
    return true;
}

bool LoadbalancerHealthmonitorType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    LoadbalancerHealthmonitorType *ptr = new LoadbalancerHealthmonitorType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void LoadbalancerHealthmonitorType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("admin-state");
    node_c.text().set(LoadbalancerHealthmonitorType::admin_state);

    // Add child node 
    node_c = node_p->append_child("monitor-type");
    node_c.text().set(LoadbalancerHealthmonitorType::monitor_type.c_str());

    // Add child node 
    node_c = node_p->append_child("delay");
    node_c.text().set(LoadbalancerHealthmonitorType::delay);

    // Add child node 
    node_c = node_p->append_child("timeout");
    node_c.text().set(LoadbalancerHealthmonitorType::timeout);

    // Add child node 
    node_c = node_p->append_child("max-retries");
    node_c.text().set(LoadbalancerHealthmonitorType::max_retries);

    // Add child node 
    node_c = node_p->append_child("http-method");
    node_c.text().set(LoadbalancerHealthmonitorType::http_method.c_str());

    // Add child node 
    node_c = node_p->append_child("url-path");
    node_c.text().set(LoadbalancerHealthmonitorType::url_path.c_str());

    // Add child node 
    node_c = node_p->append_child("expected-codes");
    node_c.text().set(LoadbalancerHealthmonitorType::expected_codes.c_str());

}

bool DomainLimitsType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "project-limit") == 0) {
            if (!ParseInteger(node, &project_limit)) return false;
        }
        if (strcmp(node.name(), "virtual-network-limit") == 0) {
            if (!ParseInteger(node, &virtual_network_limit)) return false;
        }
        if (strcmp(node.name(), "security-group-limit") == 0) {
            if (!ParseInteger(node, &security_group_limit)) return false;
        }
    }
    return true;
}

bool DomainLimitsType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    DomainLimitsType *ptr = new DomainLimitsType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void DomainLimitsType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("project-limit");
    node_c.text().set(DomainLimitsType::project_limit);

    // Add child node 
    node_c = node_p->append_child("virtual-network-limit");
    node_c.text().set(DomainLimitsType::virtual_network_limit);

    // Add child node 
    node_c = node_p->append_child("security-group-limit");
    node_c.text().set(DomainLimitsType::security_group_limit);

}

bool RouteType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "prefix") == 0) {
            prefix = node.child_value();
            boost::trim(prefix);
        }
        if (strcmp(node.name(), "next-hop") == 0) {
            next_hop = node.child_value();
            boost::trim(next_hop);
        }
        if (strcmp(node.name(), "next-hop-type") == 0) {
            next_hop_type = node.child_value();
            boost::trim(next_hop_type);
        }
    }
    return true;
}

bool RouteType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    RouteType *ptr = new RouteType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void RouteType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("prefix");
    node_c.text().set(RouteType::prefix.c_str());

    // Add child node 
    node_c = node_p->append_child("next-hop");
    node_c.text().set(RouteType::next_hop.c_str());

    // Add child node 
    node_c = node_p->append_child("next-hop-type");
    node_c.text().set(RouteType::next_hop_type.c_str());

}

bool PortType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "start-port") == 0) {
            if (!ParseInteger(node, &start_port)) return false;
        }
        if (strcmp(node.name(), "end-port") == 0) {
            if (!ParseInteger(node, &end_port)) return false;
        }
    }
    return true;
}

bool PortType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    PortType *ptr = new PortType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void PortType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("start-port");
    node_c.text().set(PortType::start_port);

    // Add child node 
    node_c = node_p->append_child("end-port");
    node_c.text().set(PortType::end_port);

}

bool SequenceType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "major") == 0) {
            if (!ParseInteger(node, &major)) return false;
        }
        if (strcmp(node.name(), "minor") == 0) {
            if (!ParseInteger(node, &minor)) return false;
        }
    }
    return true;
}

bool SequenceType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    SequenceType *ptr = new SequenceType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void SequenceType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("major");
    node_c.text().set(SequenceType::major);

    // Add child node 
    node_c = node_p->append_child("minor");
    node_c.text().set(SequenceType::minor);

}

bool ActionListType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "simple-action") == 0) {
            simple_action = node.child_value();
            boost::trim(simple_action);
        }
        if (strcmp(node.name(), "gateway-name") == 0) {
            gateway_name = node.child_value();
            boost::trim(gateway_name);
        }
        if (strcmp(node.name(), "apply-service") == 0) {

            string var(node.child_value());
            boost::trim(var);
            apply_service.push_back(var);
        }
        if (strcmp(node.name(), "mirror-to") == 0) {
            if (!mirror_to.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "assign-routing-instance") == 0) {
            assign_routing_instance = node.child_value();
            boost::trim(assign_routing_instance);
        }
    }
    return true;
}

bool ActionListType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ActionListType *ptr = new ActionListType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ActionListType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("simple-action");
    node_c.text().set(ActionListType::simple_action.c_str());

    // Add child node 
    node_c = node_p->append_child("gateway-name");
    node_c.text().set(ActionListType::gateway_name.c_str());


    for (std::vector<std::string>::const_iterator iter = apply_service.begin();
         iter != apply_service.end(); ++iter) {
        node_c = node_p->append_child("apply-service");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
    // Add complex node 
    node_c = node_p->append_child("mirror-to");
    mirror_to.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("assign-routing-instance");
    node_c.text().set(ActionListType::assign_routing_instance.c_str());

}

bool AclRuleType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "match-condition") == 0) {
            if (!match_condition.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "action-list") == 0) {
            if (!action_list.XmlParse(node)) return false;
        }
        if (strcmp(node.name(), "rule-uuid") == 0) {
            rule_uuid = node.child_value();
            boost::trim(rule_uuid);
        }
    }
    return true;
}

bool AclRuleType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    AclRuleType *ptr = new AclRuleType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void AclRuleType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add complex node 
    node_c = node_p->append_child("match-condition");
    match_condition.Encode(&node_c); 
    // Add complex node 
    node_c = node_p->append_child("action-list");
    action_list.Encode(&node_c); 
    // Add child node 
    node_c = node_p->append_child("rule-uuid");
    node_c.text().set(AclRuleType::rule_uuid.c_str());

}

bool UuidType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "uuid-mslong") == 0) {
            if (!ParseUnsignedLong(node, &uuid_mslong)) return false;
        }
        if (strcmp(node.name(), "uuid-lslong") == 0) {
            if (!ParseUnsignedLong(node, &uuid_lslong)) return false;
        }
    }
    return true;
}

bool UuidType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    UuidType *ptr = new UuidType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void UuidType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    node_c = node_p->append_child("uuid-mslong");
    {
        ostringstream oss;
        oss << uuid_mslong;
        node_c.text().set(oss.str().c_str());
    }

    node_c = node_p->append_child("uuid-lslong");
    {
        ostringstream oss;
        oss << uuid_lslong;
        node_c.text().set(oss.str().c_str());
    }
}

bool PermType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "owner") == 0) {
            owner = node.child_value();
            boost::trim(owner);
        }
        if (strcmp(node.name(), "owner-access") == 0) {
            if (!ParseInteger(node, &owner_access)) return false;
        }
        if (strcmp(node.name(), "group") == 0) {
            group = node.child_value();
            boost::trim(group);
        }
        if (strcmp(node.name(), "group-access") == 0) {
            if (!ParseInteger(node, &group_access)) return false;
        }
        if (strcmp(node.name(), "other-access") == 0) {
            if (!ParseInteger(node, &other_access)) return false;
        }
    }
    return true;
}

bool PermType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    PermType *ptr = new PermType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void PermType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("owner");
    node_c.text().set(PermType::owner.c_str());

    // Add child node 
    node_c = node_p->append_child("owner-access");
    node_c.text().set(PermType::owner_access);

    // Add child node 
    node_c = node_p->append_child("group");
    node_c.text().set(PermType::group.c_str());

    // Add child node 
    node_c = node_p->append_child("group-access");
    node_c.text().set(PermType::group_access);

    // Add child node 
    node_c = node_p->append_child("other-access");
    node_c.text().set(PermType::other_access);

}

bool ApiAccessListType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "api-access") == 0) {

            ApiAccessType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            api_access.push_back(var);
        }
    }
    return true;
}

bool ApiAccessListType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ApiAccessListType *ptr = new ApiAccessListType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ApiAccessListType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<ApiAccessType>::const_iterator iter = api_access.begin();
         iter != api_access.end(); ++iter) {
        node_c = node_p->append_child("api-access");
        iter->Encode(&node_c);
    }
}

bool AllocationPoolType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "start") == 0) {
            start = node.child_value();
            boost::trim(start);
        }
        if (strcmp(node.name(), "end") == 0) {
            end = node.child_value();
            boost::trim(end);
        }
    }
    return true;
}

bool AllocationPoolType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    AllocationPoolType *ptr = new AllocationPoolType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void AllocationPoolType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("start");
    node_c.text().set(AllocationPoolType::start.c_str());

    // Add child node 
    node_c = node_p->append_child("end");
    node_c.text().set(AllocationPoolType::end.c_str());

}

bool VnSubnetsType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "ipam-subnets") == 0) {

            IpamSubnetType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            ipam_subnets.push_back(var);
        }
        if (strcmp(node.name(), "host-routes") == 0) {
            if (!host_routes.XmlParse(node)) return false;
        }
    }
    return true;
}

bool VnSubnetsType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    VnSubnetsType *ptr = new VnSubnetsType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void VnSubnetsType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<IpamSubnetType>::const_iterator iter = ipam_subnets.begin();
         iter != ipam_subnets.end(); ++iter) {
        node_c = node_p->append_child("ipam-subnets");
        iter->Encode(&node_c);
    }
    // Add complex node 
    node_c = node_p->append_child("host-routes");
    host_routes.Encode(&node_c); 
}

bool IpAddressesType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "ip-address") == 0) {

            string var(node.child_value());
            boost::trim(var);
            ip_address.push_back(var);
        }
    }
    return true;
}

bool IpAddressesType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    IpAddressesType *ptr = new IpAddressesType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void IpAddressesType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<std::string>::const_iterator iter = ip_address.begin();
         iter != ip_address.end(); ++iter) {
        node_c = node_p->append_child("ip-address");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
}

bool LinklocalServiceEntryType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "linklocal-service-name") == 0) {
            linklocal_service_name = node.child_value();
            boost::trim(linklocal_service_name);
        }
        if (strcmp(node.name(), "linklocal-service-ip") == 0) {
            linklocal_service_ip = node.child_value();
            boost::trim(linklocal_service_ip);
        }
        if (strcmp(node.name(), "linklocal-service-port") == 0) {
            if (!ParseInteger(node, &linklocal_service_port)) return false;
        }
        if (strcmp(node.name(), "ip-fabric-DNS-service-name") == 0) {
            ip_fabric_DNS_service_name = node.child_value();
            boost::trim(ip_fabric_DNS_service_name);
        }
        if (strcmp(node.name(), "ip-fabric-service-port") == 0) {
            if (!ParseInteger(node, &ip_fabric_service_port)) return false;
        }
        if (strcmp(node.name(), "ip-fabric-service-ip") == 0) {

            string var(node.child_value());
            boost::trim(var);
            ip_fabric_service_ip.push_back(var);
        }
    }
    return true;
}

bool LinklocalServiceEntryType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    LinklocalServiceEntryType *ptr = new LinklocalServiceEntryType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void LinklocalServiceEntryType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("linklocal-service-name");
    node_c.text().set(LinklocalServiceEntryType::linklocal_service_name.c_str());

    // Add child node 
    node_c = node_p->append_child("linklocal-service-ip");
    node_c.text().set(LinklocalServiceEntryType::linklocal_service_ip.c_str());

    // Add child node 
    node_c = node_p->append_child("linklocal-service-port");
    node_c.text().set(LinklocalServiceEntryType::linklocal_service_port);

    // Add child node 
    node_c = node_p->append_child("ip-fabric-DNS-service-name");
    node_c.text().set(LinklocalServiceEntryType::ip_fabric_DNS_service_name.c_str());

    // Add child node 
    node_c = node_p->append_child("ip-fabric-service-port");
    node_c.text().set(LinklocalServiceEntryType::ip_fabric_service_port);


    for (std::vector<std::string>::const_iterator iter = ip_fabric_service_ip.begin();
         iter != ip_fabric_service_ip.end(); ++iter) {
        node_c = node_p->append_child("ip-fabric-service-ip");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
}

bool DhcpOptionsListType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "dhcp-option") == 0) {

            DhcpOptionType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            dhcp_option.push_back(var);
        }
    }
    return true;
}

bool DhcpOptionsListType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    DhcpOptionsListType *ptr = new DhcpOptionsListType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void DhcpOptionsListType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<DhcpOptionType>::const_iterator iter = dhcp_option.begin();
         iter != dhcp_option.end(); ++iter) {
        node_c = node_p->append_child("dhcp-option");
        iter->Encode(&node_c);
    }
}

bool PluginProperties::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "plugin-property") == 0) {

            PluginProperty var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            plugin_property.push_back(var);
        }
    }
    return true;
}

bool PluginProperties::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    PluginProperties *ptr = new PluginProperties();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void PluginProperties::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<PluginProperty>::const_iterator iter = plugin_property.begin();
         iter != plugin_property.end(); ++iter) {
        node_c = node_p->append_child("plugin-property");
        iter->Encode(&node_c);
    }
}

bool ServiceTemplateInterfaceType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "service-interface-type") == 0) {
            service_interface_type = node.child_value();
            boost::trim(service_interface_type);
        }
        if (strcmp(node.name(), "shared-ip") == 0) {
            if (!ParseBoolean(node, &shared_ip)) return false;
        }
        if (strcmp(node.name(), "static-route-enable") == 0) {
            if (!ParseBoolean(node, &static_route_enable)) return false;
        }
    }
    return true;
}

bool ServiceTemplateInterfaceType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    ServiceTemplateInterfaceType *ptr = new ServiceTemplateInterfaceType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void ServiceTemplateInterfaceType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("service-interface-type");
    node_c.text().set(ServiceTemplateInterfaceType::service_interface_type.c_str());

    // Add child node 
    node_c = node_p->append_child("shared-ip");
    node_c.text().set(ServiceTemplateInterfaceType::shared_ip);

    // Add child node 
    node_c = node_p->append_child("static-route-enable");
    node_c.text().set(ServiceTemplateInterfaceType::static_route_enable);

}

bool EncapsulationPrioritiesType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "encapsulation") == 0) {

            string var(node.child_value());
            boost::trim(var);
            encapsulation.push_back(var);
        }
    }
    return true;
}

bool EncapsulationPrioritiesType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    EncapsulationPrioritiesType *ptr = new EncapsulationPrioritiesType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void EncapsulationPrioritiesType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<std::string>::const_iterator iter = encapsulation.begin();
         iter != encapsulation.end(); ++iter) {
        node_c = node_p->append_child("encapsulation");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
}

bool SubnetType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "ip-prefix") == 0) {
            ip_prefix = node.child_value();
            boost::trim(ip_prefix);
        }
        if (strcmp(node.name(), "ip-prefix-len") == 0) {
            if (!ParseInteger(node, &ip_prefix_len)) return false;
        }
    }
    return true;
}

bool SubnetType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    SubnetType *ptr = new SubnetType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void SubnetType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("ip-prefix");
    node_c.text().set(SubnetType::ip_prefix.c_str());

    // Add child node 
    node_c = node_p->append_child("ip-prefix-len");
    node_c.text().set(SubnetType::ip_prefix_len);

}

bool MacAddressesType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "mac-address") == 0) {

            string var(node.child_value());
            boost::trim(var);
            mac_address.push_back(var);
        }
    }
    return true;
}

bool MacAddressesType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    MacAddressesType *ptr = new MacAddressesType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void MacAddressesType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<std::string>::const_iterator iter = mac_address.begin();
         iter != mac_address.end(); ++iter) {
        node_c = node_p->append_child("mac-address");
        std::string str = *iter;
        node_c.text().set(str.c_str());
    }
}

bool QuotaType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "defaults") == 0) {
            if (!ParseInteger(node, &defaults)) return false;
        }
        if (strcmp(node.name(), "floating-ip") == 0) {
            if (!ParseInteger(node, &floating_ip)) return false;
        }
        if (strcmp(node.name(), "instance-ip") == 0) {
            if (!ParseInteger(node, &instance_ip)) return false;
        }
        if (strcmp(node.name(), "virtual-machine-interface") == 0) {
            if (!ParseInteger(node, &virtual_machine_interface)) return false;
        }
        if (strcmp(node.name(), "virtual-network") == 0) {
            if (!ParseInteger(node, &virtual_network)) return false;
        }
        if (strcmp(node.name(), "virtual-router") == 0) {
            if (!ParseInteger(node, &virtual_router)) return false;
        }
        if (strcmp(node.name(), "virtual-DNS") == 0) {
            if (!ParseInteger(node, &virtual_DNS)) return false;
        }
        if (strcmp(node.name(), "virtual-DNS-record") == 0) {
            if (!ParseInteger(node, &virtual_DNS_record)) return false;
        }
        if (strcmp(node.name(), "bgp-router") == 0) {
            if (!ParseInteger(node, &bgp_router)) return false;
        }
        if (strcmp(node.name(), "network-ipam") == 0) {
            if (!ParseInteger(node, &network_ipam)) return false;
        }
        if (strcmp(node.name(), "access-control-list") == 0) {
            if (!ParseInteger(node, &access_control_list)) return false;
        }
        if (strcmp(node.name(), "floating-ip-pool") == 0) {
            if (!ParseInteger(node, &floating_ip_pool)) return false;
        }
        if (strcmp(node.name(), "service-template") == 0) {
            if (!ParseInteger(node, &service_template)) return false;
        }
        if (strcmp(node.name(), "service-instance") == 0) {
            if (!ParseInteger(node, &service_instance)) return false;
        }
        if (strcmp(node.name(), "logical-router") == 0) {
            if (!ParseInteger(node, &logical_router)) return false;
        }
        if (strcmp(node.name(), "security-group") == 0) {
            if (!ParseInteger(node, &security_group)) return false;
        }
        if (strcmp(node.name(), "security-group-rule") == 0) {
            if (!ParseInteger(node, &security_group_rule)) return false;
        }
        if (strcmp(node.name(), "subnet") == 0) {
            if (!ParseInteger(node, &subnet)) return false;
        }
        if (strcmp(node.name(), "global-vrouter-config") == 0) {
            if (!ParseInteger(node, &global_vrouter_config)) return false;
        }
        if (strcmp(node.name(), "loadbalancer-pool") == 0) {
            if (!ParseInteger(node, &loadbalancer_pool)) return false;
        }
        if (strcmp(node.name(), "loadbalancer-member") == 0) {
            if (!ParseInteger(node, &loadbalancer_member)) return false;
        }
        if (strcmp(node.name(), "loadbalancer-healthmonitor") == 0) {
            if (!ParseInteger(node, &loadbalancer_healthmonitor)) return false;
        }
        if (strcmp(node.name(), "virtual-ip") == 0) {
            if (!ParseInteger(node, &virtual_ip)) return false;
        }
    }
    return true;
}

bool QuotaType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    QuotaType *ptr = new QuotaType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void QuotaType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("defaults");
    node_c.text().set(QuotaType::defaults);

    // Add child node 
    node_c = node_p->append_child("floating-ip");
    node_c.text().set(QuotaType::floating_ip);

    // Add child node 
    node_c = node_p->append_child("instance-ip");
    node_c.text().set(QuotaType::instance_ip);

    // Add child node 
    node_c = node_p->append_child("virtual-machine-interface");
    node_c.text().set(QuotaType::virtual_machine_interface);

    // Add child node 
    node_c = node_p->append_child("virtual-network");
    node_c.text().set(QuotaType::virtual_network);

    // Add child node 
    node_c = node_p->append_child("virtual-router");
    node_c.text().set(QuotaType::virtual_router);

    // Add child node 
    node_c = node_p->append_child("virtual-DNS");
    node_c.text().set(QuotaType::virtual_DNS);

    // Add child node 
    node_c = node_p->append_child("virtual-DNS-record");
    node_c.text().set(QuotaType::virtual_DNS_record);

    // Add child node 
    node_c = node_p->append_child("bgp-router");
    node_c.text().set(QuotaType::bgp_router);

    // Add child node 
    node_c = node_p->append_child("network-ipam");
    node_c.text().set(QuotaType::network_ipam);

    // Add child node 
    node_c = node_p->append_child("access-control-list");
    node_c.text().set(QuotaType::access_control_list);

    // Add child node 
    node_c = node_p->append_child("floating-ip-pool");
    node_c.text().set(QuotaType::floating_ip_pool);

    // Add child node 
    node_c = node_p->append_child("service-template");
    node_c.text().set(QuotaType::service_template);

    // Add child node 
    node_c = node_p->append_child("service-instance");
    node_c.text().set(QuotaType::service_instance);

    // Add child node 
    node_c = node_p->append_child("logical-router");
    node_c.text().set(QuotaType::logical_router);

    // Add child node 
    node_c = node_p->append_child("security-group");
    node_c.text().set(QuotaType::security_group);

    // Add child node 
    node_c = node_p->append_child("security-group-rule");
    node_c.text().set(QuotaType::security_group_rule);

    // Add child node 
    node_c = node_p->append_child("subnet");
    node_c.text().set(QuotaType::subnet);

    // Add child node 
    node_c = node_p->append_child("global-vrouter-config");
    node_c.text().set(QuotaType::global_vrouter_config);

    // Add child node 
    node_c = node_p->append_child("loadbalancer-pool");
    node_c.text().set(QuotaType::loadbalancer_pool);

    // Add child node 
    node_c = node_p->append_child("loadbalancer-member");
    node_c.text().set(QuotaType::loadbalancer_member);

    // Add child node 
    node_c = node_p->append_child("loadbalancer-healthmonitor");
    node_c.text().set(QuotaType::loadbalancer_healthmonitor);

    // Add child node 
    node_c = node_p->append_child("virtual-ip");
    node_c.text().set(QuotaType::virtual_ip);

}

bool RouteTableType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "route") == 0) {

            RouteType var;
            var.Clear();
            if (!var.XmlParse(node)) return false;
            route.push_back(var);
        }
    }
    return true;
}

bool RouteTableType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    RouteTableType *ptr = new RouteTableType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void RouteTableType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;


    for (std::vector<RouteType>::const_iterator iter = route.begin();
         iter != route.end(); ++iter) {
        node_c = node_p->append_child("route");
        iter->Encode(&node_c);
    }
}

bool LoadbalancerMemberType::XmlParse(const xml_node &parent) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "admin-state") == 0) {
            if (!ParseBoolean(node, &admin_state)) return false;
        }
        if (strcmp(node.name(), "status") == 0) {
            status = node.child_value();
            boost::trim(status);
        }
        if (strcmp(node.name(), "status-description") == 0) {
            status_description = node.child_value();
            boost::trim(status_description);
        }
        if (strcmp(node.name(), "protocol-port") == 0) {
            if (!ParseInteger(node, &protocol_port)) return false;
        }
        if (strcmp(node.name(), "weight") == 0) {
            if (!ParseInteger(node, &weight)) return false;
        }
        if (strcmp(node.name(), "address") == 0) {
            address = node.child_value();
            boost::trim(address);
        }
    }
    return true;
}

bool LoadbalancerMemberType::XmlParseProperty(const xml_node &parent,
        auto_ptr<AutogenProperty> *resultp) {
    LoadbalancerMemberType *ptr = new LoadbalancerMemberType();
    resultp->reset(ptr);
    if (!ptr->XmlParse(parent)) {
        return false;
    }
    return true;
}

void LoadbalancerMemberType::Encode(xml_node *node_p) const {
    pugi::xml_node node_c;

    // Add child node 
    node_c = node_p->append_child("admin-state");
    node_c.text().set(LoadbalancerMemberType::admin_state);

    // Add child node 
    node_c = node_p->append_child("status");
    node_c.text().set(LoadbalancerMemberType::status.c_str());

    // Add child node 
    node_c = node_p->append_child("status-description");
    node_c.text().set(LoadbalancerMemberType::status_description.c_str());

    // Add child node 
    node_c = node_p->append_child("protocol-port");
    node_c.text().set(LoadbalancerMemberType::protocol_port);

    // Add child node 
    node_c = node_p->append_child("weight");
    node_c.text().set(LoadbalancerMemberType::weight);

    // Add child node 
    node_c = node_p->append_child("address");
    node_c.text().set(LoadbalancerMemberType::address.c_str());

}

void Domain::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(LIMITS)) {
        xml_node p = node->append_child("domain-limits");
        limits_.Encode(&p);
    }

    if (property_set_.test(API_ACCESS_LIST)) {
        xml_node p = node->append_child("api-access-list");

        for (std::vector<ApiAccessType>::const_iterator iter = api_access_list_.begin();
             iter != api_access_list_.end(); ++iter) {
            xml_node s = p.append_child("api-access");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool Domain::Decode(const xml_node &parent, std::string *id_name,
                       Domain *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "domain-limits") == 0) {

            bool success = ptr->limits_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(LIMITS);
        }
        if (strcmp(node.name(), "api-access-list") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "api-access") != 0) {
                    continue;
                }

                ApiAccessType var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->api_access_list_.push_back(var);
                ptr->property_set_.set(API_ACCESS_LIST);
            }
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void GlobalVrouterConfig::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(LINKLOCAL_SERVICES)) {
        xml_node p = node->append_child("linklocal-services");

        for (std::vector<LinklocalServiceEntryType>::const_iterator iter = linklocal_services_.begin();
             iter != linklocal_services_.end(); ++iter) {
            xml_node s = p.append_child("linklocal-service-entry");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(ENCAPSULATION_PRIORITIES)) {
        xml_node p = node->append_child("encapsulation-priorities");

        for (std::vector<std::string>::const_iterator iter = encapsulation_priorities_.begin();
             iter != encapsulation_priorities_.end(); ++iter) {
            xml_node s = p.append_child("encapsulation");
            s.text().set(iter->c_str());
        }
    }

    if (property_set_.test(VXLAN_NETWORK_IDENTIFIER_MODE)) {
        xml_node p = node->append_child("vxlan-network-identifier-mode");
        p.text().set(vxlan_network_identifier_mode_.c_str());
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool GlobalVrouterConfig::Decode(const xml_node &parent, std::string *id_name,
                       GlobalVrouterConfig *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "linklocal-services") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "linklocal-service-entry") != 0) {
                    continue;
                }

                LinklocalServiceEntryType var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->linklocal_services_.push_back(var);
                ptr->property_set_.set(LINKLOCAL_SERVICES);
            }
        }
        if (strcmp(node.name(), "encapsulation-priorities") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "encapsulation") != 0) {
                    continue;
                }
                string var(item.child_value());
                boost::trim(var);
                ptr->encapsulation_priorities_.push_back(var);
                ptr->property_set_.set(ENCAPSULATION_PRIORITIES);
            }
        }
        if (strcmp(node.name(), "vxlan-network-identifier-mode") == 0) {
            ptr->vxlan_network_identifier_mode_ = node.child_value();
            ptr->property_set_.set(VXLAN_NETWORK_IDENTIFIER_MODE);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void InstanceIp::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ADDRESS)) {
        xml_node p = node->append_child("instance-ip-address");
        p.text().set(address_.c_str());
    }

    if (property_set_.test(FAMILY)) {
        xml_node p = node->append_child("instance-ip-family");
        p.text().set(family_.c_str());
    }

    if (property_set_.test(MODE)) {
        xml_node p = node->append_child("instance-ip-mode");
        p.text().set(mode_.c_str());
    }

    if (property_set_.test(SUBNET_UUID)) {
        xml_node p = node->append_child("subnet-uuid");
        p.text().set(subnet_uuid_.c_str());
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool InstanceIp::Decode(const xml_node &parent, std::string *id_name,
                       InstanceIp *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "instance-ip-address") == 0) {
            ptr->address_ = node.child_value();
            ptr->property_set_.set(ADDRESS);
        }
        if (strcmp(node.name(), "instance-ip-family") == 0) {
            ptr->family_ = node.child_value();
            ptr->property_set_.set(FAMILY);
        }
        if (strcmp(node.name(), "instance-ip-mode") == 0) {
            ptr->mode_ = node.child_value();
            ptr->property_set_.set(MODE);
        }
        if (strcmp(node.name(), "subnet-uuid") == 0) {
            ptr->subnet_uuid_ = node.child_value();
            ptr->property_set_.set(SUBNET_UUID);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void NetworkPolicy::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ENTRIES)) {
        xml_node p = node->append_child("network-policy-entries");

        for (std::vector<PolicyRuleType>::const_iterator iter = entries_.begin();
             iter != entries_.end(); ++iter) {
            xml_node s = p.append_child("policy-rule");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool NetworkPolicy::Decode(const xml_node &parent, std::string *id_name,
                       NetworkPolicy *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "network-policy-entries") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "policy-rule") != 0) {
                    continue;
                }

                PolicyRuleType var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->entries_.push_back(var);
                ptr->property_set_.set(ENTRIES);
            }
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void VirtualDnsRecord::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(DATA)) {
        xml_node p = node->append_child("virtual-DNS-record-data");
        data_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool VirtualDnsRecord::Decode(const xml_node &parent, std::string *id_name,
                       VirtualDnsRecord *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "virtual-DNS-record-data") == 0) {

            bool success = ptr->data_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(DATA);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void FloatingIp::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ADDRESS)) {
        xml_node p = node->append_child("floating-ip-address");
        p.text().set(address_.c_str());
    }

    if (property_set_.test(IS_VIRTUAL_IP)) {
        xml_node p = node->append_child("floating-ip-is-virtual-ip");
        p.text().set(is_virtual_ip_);
    }

    if (property_set_.test(FIXED_IP_ADDRESS)) {
        xml_node p = node->append_child("floating-ip-fixed-ip-address");
        p.text().set(fixed_ip_address_.c_str());
    }

    if (property_set_.test(ADDRESS_FAMILY)) {
        xml_node p = node->append_child("floating-ip-address-family");
        p.text().set(address_family_.c_str());
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool FloatingIp::Decode(const xml_node &parent, std::string *id_name,
                       FloatingIp *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "floating-ip-address") == 0) {
            ptr->address_ = node.child_value();
            ptr->property_set_.set(ADDRESS);
        }
        if (strcmp(node.name(), "floating-ip-is-virtual-ip") == 0) {
            ptr->is_virtual_ip_ = node.text().as_bool();
            ptr->property_set_.set(IS_VIRTUAL_IP);
        }
        if (strcmp(node.name(), "floating-ip-fixed-ip-address") == 0) {
            ptr->fixed_ip_address_ = node.child_value();
            ptr->property_set_.set(FIXED_IP_ADDRESS);
        }
        if (strcmp(node.name(), "floating-ip-address-family") == 0) {
            ptr->address_family_ = node.child_value();
            ptr->property_set_.set(ADDRESS_FAMILY);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void FloatingIpPool::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(PREFIXES)) {
        xml_node p = node->append_child("floating-ip-pool-prefixes");

        for (std::vector<SubnetType>::const_iterator iter = prefixes_.begin();
             iter != prefixes_.end(); ++iter) {
            xml_node s = p.append_child("subnet");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool FloatingIpPool::Decode(const xml_node &parent, std::string *id_name,
                       FloatingIpPool *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "floating-ip-pool-prefixes") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "subnet") != 0) {
                    continue;
                }

                SubnetType var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->prefixes_.push_back(var);
                ptr->property_set_.set(PREFIXES);
            }
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void PhysicalRouter::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(MANAGEMENT_IP)) {
        xml_node p = node->append_child("physical-router-management-ip");
        p.text().set(management_ip_.c_str());
    }

    if (property_set_.test(DATAPLANE_IP)) {
        xml_node p = node->append_child("physical-router-dataplane-ip");
        p.text().set(dataplane_ip_.c_str());
    }

    if (property_set_.test(VENDOR_NAME)) {
        xml_node p = node->append_child("physical-router-vendor-name");
        p.text().set(vendor_name_.c_str());
    }

    if (property_set_.test(USER_CREDENTIALS)) {
        xml_node p = node->append_child("physical-router-user-credentials");
        user_credentials_.Encode(&p);
    }

    if (property_set_.test(SNMP_CREDENTIALS)) {
        xml_node p = node->append_child("physical-router-snmp-credentials");
        snmp_credentials_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool PhysicalRouter::Decode(const xml_node &parent, std::string *id_name,
                       PhysicalRouter *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "physical-router-management-ip") == 0) {
            ptr->management_ip_ = node.child_value();
            ptr->property_set_.set(MANAGEMENT_IP);
        }
        if (strcmp(node.name(), "physical-router-dataplane-ip") == 0) {
            ptr->dataplane_ip_ = node.child_value();
            ptr->property_set_.set(DATAPLANE_IP);
        }
        if (strcmp(node.name(), "physical-router-vendor-name") == 0) {
            ptr->vendor_name_ = node.child_value();
            ptr->property_set_.set(VENDOR_NAME);
        }
        if (strcmp(node.name(), "physical-router-user-credentials") == 0) {

            bool success = ptr->user_credentials_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(USER_CREDENTIALS);
        }
        if (strcmp(node.name(), "physical-router-snmp-credentials") == 0) {

            bool success = ptr->snmp_credentials_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(SNMP_CREDENTIALS);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void VirtualRouter::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(TYPE)) {
        xml_node p = node->append_child("virtual-router-type");
        p.text().set(type_.c_str());
    }

    if (property_set_.test(IP_ADDRESS)) {
        xml_node p = node->append_child("virtual-router-ip-address");
        p.text().set(ip_address_.c_str());
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool VirtualRouter::Decode(const xml_node &parent, std::string *id_name,
                       VirtualRouter *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "virtual-router-type") == 0) {
            ptr->type_ = node.child_value();
            ptr->property_set_.set(TYPE);
        }
        if (strcmp(node.name(), "virtual-router-ip-address") == 0) {
            ptr->ip_address_ = node.child_value();
            ptr->property_set_.set(IP_ADDRESS);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void ConfigRoot::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool ConfigRoot::Decode(const xml_node &parent, std::string *id_name,
                       ConfigRoot *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void Subnet::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(IP_PREFIX)) {
        xml_node p = node->append_child("subnet-ip-prefix");
        ip_prefix_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool Subnet::Decode(const xml_node &parent, std::string *id_name,
                       Subnet *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "subnet-ip-prefix") == 0) {

            bool success = ptr->ip_prefix_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(IP_PREFIX);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void GlobalSystemConfig::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(AUTONOMOUS_SYSTEM)) {
        xml_node p = node->append_child("autonomous-system");
        p.text().set(autonomous_system_);
    }

    if (property_set_.test(CONFIG_VERSION)) {
        xml_node p = node->append_child("config-version");
        p.text().set(config_version_.c_str());
    }

    if (property_set_.test(PLUGIN_TUNING)) {
        xml_node p = node->append_child("plugin-tuning");

        for (std::vector<PluginProperty>::const_iterator iter = plugin_tuning_.begin();
             iter != plugin_tuning_.end(); ++iter) {
            xml_node s = p.append_child("plugin-property");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(IBGP_AUTO_MESH)) {
        xml_node p = node->append_child("ibgp-auto-mesh");
        p.text().set(ibgp_auto_mesh_);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool GlobalSystemConfig::Decode(const xml_node &parent, std::string *id_name,
                       GlobalSystemConfig *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "autonomous-system") == 0) {
            ptr->autonomous_system_ = atoi(node.child_value());
            ptr->property_set_.set(AUTONOMOUS_SYSTEM);
        }
        if (strcmp(node.name(), "config-version") == 0) {
            ptr->config_version_ = node.child_value();
            ptr->property_set_.set(CONFIG_VERSION);
        }
        if (strcmp(node.name(), "plugin-tuning") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "plugin-property") != 0) {
                    continue;
                }

                PluginProperty var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->plugin_tuning_.push_back(var);
                ptr->property_set_.set(PLUGIN_TUNING);
            }
        }
        if (strcmp(node.name(), "ibgp-auto-mesh") == 0) {
            ptr->ibgp_auto_mesh_ = node.text().as_bool();
            ptr->property_set_.set(IBGP_AUTO_MESH);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void LoadbalancerMember::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(PROPERTIES)) {
        xml_node p = node->append_child("loadbalancer-member-properties");
        properties_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool LoadbalancerMember::Decode(const xml_node &parent, std::string *id_name,
                       LoadbalancerMember *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "loadbalancer-member-properties") == 0) {

            bool success = ptr->properties_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(PROPERTIES);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void ServiceInstance::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(PROPERTIES)) {
        xml_node p = node->append_child("service-instance-properties");
        properties_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool ServiceInstance::Decode(const xml_node &parent, std::string *id_name,
                       ServiceInstance *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "service-instance-properties") == 0) {

            bool success = ptr->properties_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(PROPERTIES);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void Namespace::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(CIDR)) {
        xml_node p = node->append_child("namespace-cidr");
        cidr_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool Namespace::Decode(const xml_node &parent, std::string *id_name,
                       Namespace *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "namespace-cidr") == 0) {

            bool success = ptr->cidr_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(CIDR);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void RouteTable::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ROUTES)) {
        xml_node p = node->append_child("routes");

        for (std::vector<RouteType>::const_iterator iter = routes_.begin();
             iter != routes_.end(); ++iter) {
            xml_node s = p.append_child("route");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool RouteTable::Decode(const xml_node &parent, std::string *id_name,
                       RouteTable *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "routes") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "route") != 0) {
                    continue;
                }

                RouteType var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->routes_.push_back(var);
                ptr->property_set_.set(ROUTES);
            }
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void PhysicalInterface::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool PhysicalInterface::Decode(const xml_node &parent, std::string *id_name,
                       PhysicalInterface *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void AccessControlList::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ENTRIES)) {
        xml_node p = node->append_child("access-control-list-entries");
        entries_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool AccessControlList::Decode(const xml_node &parent, std::string *id_name,
                       AccessControlList *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "access-control-list-entries") == 0) {

            bool success = ptr->entries_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ENTRIES);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void VirtualDns::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(DATA)) {
        xml_node p = node->append_child("virtual-DNS-data");
        data_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool VirtualDns::Decode(const xml_node &parent, std::string *id_name,
                       VirtualDns *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "virtual-DNS-data") == 0) {

            bool success = ptr->data_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(DATA);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void LoadbalancerPool::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(PROPERTIES)) {
        xml_node p = node->append_child("loadbalancer-pool-properties");
        properties_.Encode(&p);
    }

    if (property_set_.test(PROVIDER)) {
        xml_node p = node->append_child("loadbalancer-pool-provider");
        p.text().set(provider_.c_str());
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool LoadbalancerPool::Decode(const xml_node &parent, std::string *id_name,
                       LoadbalancerPool *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "loadbalancer-pool-properties") == 0) {

            bool success = ptr->properties_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(PROPERTIES);
        }
        if (strcmp(node.name(), "loadbalancer-pool-provider") == 0) {
            ptr->provider_ = node.child_value();
            ptr->property_set_.set(PROVIDER);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void VirtualMachine::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool VirtualMachine::Decode(const xml_node &parent, std::string *id_name,
                       VirtualMachine *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void InterfaceRouteTable::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ROUTES)) {
        xml_node p = node->append_child("interface-route-table-routes");

        for (std::vector<RouteType>::const_iterator iter = routes_.begin();
             iter != routes_.end(); ++iter) {
            xml_node s = p.append_child("route");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool InterfaceRouteTable::Decode(const xml_node &parent, std::string *id_name,
                       InterfaceRouteTable *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "interface-route-table-routes") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "route") != 0) {
                    continue;
                }

                RouteType var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->routes_.push_back(var);
                ptr->property_set_.set(ROUTES);
            }
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void ServiceTemplate::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(PROPERTIES)) {
        xml_node p = node->append_child("service-template-properties");
        properties_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool ServiceTemplate::Decode(const xml_node &parent, std::string *id_name,
                       ServiceTemplate *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "service-template-properties") == 0) {

            bool success = ptr->properties_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(PROPERTIES);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void VirtualIp::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(PROPERTIES)) {
        xml_node p = node->append_child("virtual-ip-properties");
        properties_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool VirtualIp::Decode(const xml_node &parent, std::string *id_name,
                       VirtualIp *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "virtual-ip-properties") == 0) {

            bool success = ptr->properties_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(PROPERTIES);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void SecurityGroup::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ID)) {
        xml_node p = node->append_child("security-group-id");
        p.text().set(id_.c_str());
    }

    if (property_set_.test(ENTRIES)) {
        xml_node p = node->append_child("security-group-entries");

        for (std::vector<PolicyRuleType>::const_iterator iter = entries_.begin();
             iter != entries_.end(); ++iter) {
            xml_node s = p.append_child("policy-rule");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool SecurityGroup::Decode(const xml_node &parent, std::string *id_name,
                       SecurityGroup *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "security-group-id") == 0) {
            ptr->id_ = node.child_value();
            ptr->property_set_.set(ID);
        }
        if (strcmp(node.name(), "security-group-entries") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "policy-rule") != 0) {
                    continue;
                }

                PolicyRuleType var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->entries_.push_back(var);
                ptr->property_set_.set(ENTRIES);
            }
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void NetworkIpam::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(MGMT)) {
        xml_node p = node->append_child("network-ipam-mgmt");
        mgmt_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool NetworkIpam::Decode(const xml_node &parent, std::string *id_name,
                       NetworkIpam *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "network-ipam-mgmt") == 0) {

            bool success = ptr->mgmt_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(MGMT);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void LoadbalancerHealthmonitor::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(PROPERTIES)) {
        xml_node p = node->append_child("loadbalancer-healthmonitor-properties");
        properties_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool LoadbalancerHealthmonitor::Decode(const xml_node &parent, std::string *id_name,
                       LoadbalancerHealthmonitor *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "loadbalancer-healthmonitor-properties") == 0) {

            bool success = ptr->properties_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(PROPERTIES);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void VirtualNetwork::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(PROPERTIES)) {
        xml_node p = node->append_child("virtual-network-properties");
        properties_.Encode(&p);
    }

    if (property_set_.test(ROUTE_TARGET_LIST)) {
        xml_node p = node->append_child("route-target-list");

        for (std::vector<std::string>::const_iterator iter = route_target_list_.begin();
             iter != route_target_list_.end(); ++iter) {
            xml_node s = p.append_child("route-target");
            s.text().set(iter->c_str());
        }
    }

    if (property_set_.test(ROUTER_EXTERNAL)) {
        xml_node p = node->append_child("router-external");
        p.text().set(router_external_);
    }

    if (property_set_.test(IS_SHARED)) {
        xml_node p = node->append_child("is-shared");
        p.text().set(is_shared_);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool VirtualNetwork::Decode(const xml_node &parent, std::string *id_name,
                       VirtualNetwork *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "virtual-network-properties") == 0) {

            bool success = ptr->properties_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(PROPERTIES);
        }
        if (strcmp(node.name(), "route-target-list") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "route-target") != 0) {
                    continue;
                }
                string var(item.child_value());
                boost::trim(var);
                ptr->route_target_list_.push_back(var);
                ptr->property_set_.set(ROUTE_TARGET_LIST);
            }
        }
        if (strcmp(node.name(), "router-external") == 0) {
            ptr->router_external_ = node.text().as_bool();
            ptr->property_set_.set(ROUTER_EXTERNAL);
        }
        if (strcmp(node.name(), "is-shared") == 0) {
            ptr->is_shared_ = node.text().as_bool();
            ptr->property_set_.set(IS_SHARED);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void Project::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(QUOTA)) {
        xml_node p = node->append_child("quota");
        quota_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool Project::Decode(const xml_node &parent, std::string *id_name,
                       Project *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "quota") == 0) {

            bool success = ptr->quota_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(QUOTA);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void LogicalInterface::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(VLAN_TAG)) {
        xml_node p = node->append_child("logical-interface-vlan-tag");
        p.text().set(vlan_tag_);
    }

    if (property_set_.test(TYPE)) {
        xml_node p = node->append_child("logical-interface-type");
        p.text().set(type_.c_str());
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool LogicalInterface::Decode(const xml_node &parent, std::string *id_name,
                       LogicalInterface *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "logical-interface-vlan-tag") == 0) {
            ptr->vlan_tag_ = atoi(node.child_value());
            ptr->property_set_.set(VLAN_TAG);
        }
        if (strcmp(node.name(), "logical-interface-type") == 0) {
            ptr->type_ = node.child_value();
            ptr->property_set_.set(TYPE);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void VirtualMachineInterface::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(MAC_ADDRESSES)) {
        xml_node p = node->append_child("virtual-machine-interface-mac-addresses");

        for (std::vector<std::string>::const_iterator iter = mac_addresses_.begin();
             iter != mac_addresses_.end(); ++iter) {
            xml_node s = p.append_child("mac-address");
            s.text().set(iter->c_str());
        }
    }

    if (property_set_.test(DHCP_OPTION_LIST)) {
        xml_node p = node->append_child("virtual-machine-interface-dhcp-option-list");

        for (std::vector<DhcpOptionType>::const_iterator iter = dhcp_option_list_.begin();
             iter != dhcp_option_list_.end(); ++iter) {
            xml_node s = p.append_child("dhcp-option");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(HOST_ROUTES)) {
        xml_node p = node->append_child("virtual-machine-interface-host-routes");

        for (std::vector<RouteType>::const_iterator iter = host_routes_.begin();
             iter != host_routes_.end(); ++iter) {
            xml_node s = p.append_child("route");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(ALLOWED_ADDRESS_PAIRS)) {
        xml_node p = node->append_child("virtual-machine-interface-allowed-address-pairs");

        for (std::vector<AllowedAddressPair>::const_iterator iter = allowed_address_pairs_.begin();
             iter != allowed_address_pairs_.end(); ++iter) {
            xml_node s = p.append_child("allowed-address-pair");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(VRF_ASSIGN_TABLE)) {
        xml_node p = node->append_child("vrf-assign-table");

        for (std::vector<VrfAssignRuleType>::const_iterator iter = vrf_assign_table_.begin();
             iter != vrf_assign_table_.end(); ++iter) {
            xml_node s = p.append_child("vrf-assign-rule");
            iter->Encode(&s);
        }
    }

    if (property_set_.test(DEVICE_OWNER)) {
        xml_node p = node->append_child("virtual-machine-interface-device-owner");
        p.text().set(device_owner_.c_str());
    }

    if (property_set_.test(PROPERTIES)) {
        xml_node p = node->append_child("virtual-machine-interface-properties");
        properties_.Encode(&p);
    }

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool VirtualMachineInterface::Decode(const xml_node &parent, std::string *id_name,
                       VirtualMachineInterface *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "virtual-machine-interface-mac-addresses") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "mac-address") != 0) {
                    continue;
                }
                string var(item.child_value());
                boost::trim(var);
                ptr->mac_addresses_.push_back(var);
                ptr->property_set_.set(MAC_ADDRESSES);
            }
        }
        if (strcmp(node.name(), "virtual-machine-interface-dhcp-option-list") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "dhcp-option") != 0) {
                    continue;
                }

                DhcpOptionType var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->dhcp_option_list_.push_back(var);
                ptr->property_set_.set(DHCP_OPTION_LIST);
            }
        }
        if (strcmp(node.name(), "virtual-machine-interface-host-routes") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "route") != 0) {
                    continue;
                }

                RouteType var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->host_routes_.push_back(var);
                ptr->property_set_.set(HOST_ROUTES);
            }
        }
        if (strcmp(node.name(), "virtual-machine-interface-allowed-address-pairs") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "allowed-address-pair") != 0) {
                    continue;
                }

                AllowedAddressPair var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->allowed_address_pairs_.push_back(var);
                ptr->property_set_.set(ALLOWED_ADDRESS_PAIRS);
            }
        }
        if (strcmp(node.name(), "vrf-assign-table") == 0) {

            for (xml_node item = node.first_child(); item;
                item = item.next_sibling()) {
                if (strcmp(item.name(), "vrf-assign-rule") != 0) {
                    continue;
                }

                VrfAssignRuleType var;
                var.Clear();
                bool success = var.XmlParse(item);
                if (!success) {
                    return false;
                }
                ptr->vrf_assign_table_.push_back(var);
                ptr->property_set_.set(VRF_ASSIGN_TABLE);
            }
        }
        if (strcmp(node.name(), "virtual-machine-interface-device-owner") == 0) {
            ptr->device_owner_ = node.child_value();
            ptr->property_set_.set(DEVICE_OWNER);
        }
        if (strcmp(node.name(), "virtual-machine-interface-properties") == 0) {

            bool success = ptr->properties_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(PROPERTIES);
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void LogicalRouter::EncodeUpdate(xml_node *node) const {

    if (property_set_.test(ID_PERMS)) {
        xml_node p = node->append_child("id-perms");
        id_perms_.Encode(&p);
    }

    if (property_set_.test(DISPLAY_NAME)) {
        xml_node p = node->append_child("display-name");
        p.text().set(display_name_.c_str());
    }
}

bool LogicalRouter::Decode(const xml_node &parent, std::string *id_name,
                       LogicalRouter *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }
        if (strcmp(node.name(), "id-perms") == 0) {

            bool success = ptr->id_perms_.XmlParse(node);
            if (!success) {
                return false;
            }
            ptr->property_set_.set(ID_PERMS);
        }
        if (strcmp(node.name(), "display-name") == 0) {
            ptr->display_name_ = node.child_value();
            ptr->property_set_.set(DISPLAY_NAME);
        }
    }
    return true;
}

void ProjectNamespace::EncodeUpdate(xml_node *node) const {
    xml_node data = node->append_child("value");
    data_.Encode(&data);
}

bool ProjectNamespace::Decode(const xml_node &parent, std::string *id_name,
                       ProjectNamespace *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }

        if (strcmp(node.name(), "value") == 0) {
            bool success = ptr->data_.XmlParse(node);
            if (!success) {
                return false;
            }
        }
        }
    return true;
}

void VirtualNetworkNetworkPolicy::EncodeUpdate(xml_node *node) const {
    xml_node data = node->append_child("value");
    data_.Encode(&data);
}

bool VirtualNetworkNetworkPolicy::Decode(const xml_node &parent, std::string *id_name,
                       VirtualNetworkNetworkPolicy *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }

        if (strcmp(node.name(), "value") == 0) {
            bool success = ptr->data_.XmlParse(node);
            if (!success) {
                return false;
            }
        }
        }
    return true;
}

void VirtualNetworkNetworkIpam::EncodeUpdate(xml_node *node) const {
    xml_node data = node->append_child("value");
    data_.Encode(&data);
}

bool VirtualNetworkNetworkIpam::Decode(const xml_node &parent, std::string *id_name,
                       VirtualNetworkNetworkIpam *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }

        if (strcmp(node.name(), "value") == 0) {
            bool success = ptr->data_.XmlParse(node);
            if (!success) {
                return false;
            }
        }
        }
    return true;
}

void VirtualMachineInterfaceRoutingInstance::EncodeUpdate(xml_node *node) const {
    xml_node data = node->append_child("value");
    data_.Encode(&data);
}

bool VirtualMachineInterfaceRoutingInstance::Decode(const xml_node &parent, std::string *id_name,
                       VirtualMachineInterfaceRoutingInstance *ptr) {
    for (xml_node node = parent.first_child(); node;
         node = node.next_sibling()) {
        if (strcmp(node.name(), "name") == 0) {
            *id_name = node.child_value();
        }

        if (strcmp(node.name(), "value") == 0) {
            bool success = ptr->data_.XmlParse(node);
            if (!success) {
                return false;
            }
        }
        }
    return true;
}
}  // namespace autogen
