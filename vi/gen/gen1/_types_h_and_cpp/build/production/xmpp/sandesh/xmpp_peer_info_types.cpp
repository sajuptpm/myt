/**
 * Autogenerated by Sandesh Compiler (1.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 * Copyright (c) 2013 Juniper Networks, Inc. All rights reserved.
 */
#include <boost/date_time/posix_time/posix_time.hpp>

#include <base/logging.h>

#include <sandesh/sandesh_types.h>
#include <sandesh/sandesh_constants.h>
#include <sandesh/sandesh.h>
#include <sandesh/sandesh_uve.h>
#include <sandesh/sandesh_http.h>
#include <sandesh/sandesh_trace.h>
#include <curl/curl.h>

#include "xmpp_peer_info_types.h"



const char* XmppPeerInfoData::ascii_fingerprint = "8A329758E794E263D3CEF0102D630B30";
const uint8_t XmppPeerInfoData::binary_fingerprint[16] = {0x8A,0x32,0x97,0x58,0xE7,0x94,0xE2,0x63,0xD3,0xCE,0xF0,0x10,0x2D,0x63,0x0B,0x30};

int32_t XmppPeerInfoData::read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot) {

  int32_t xfer = 0, ret;
  std::string fname;
  ::contrail::sandesh::protocol::TType ftype;
  int16_t fid;

  if ((ret = iprot->readStructBegin(fname)) < 0) {
    return ret;
  }
  xfer += ret;


  while (true)
  {
    if ((ret = iprot->readFieldBegin(fname, ftype, fid)) < 0) {
      return ret;
    }
    xfer += ret;
    if (ftype == ::contrail::sandesh::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::contrail::sandesh::protocol::T_STRING) {
          if ((ret = iprot->readString(this->name)) < 0) {
            return ret;
          }
          xfer += ret;

          this->__isset.name = true;
        } else {
          if ((ret = iprot->skip(ftype)) < 0) {
            return ret;
          }
          xfer += ret;
        }
        break;
      case 2:
        if (ftype == ::contrail::sandesh::protocol::T_BOOL) {
          if ((ret = iprot->readBool(this->deleted)) < 0) {
            return ret;
          }
          xfer += ret;

          this->__isset.deleted = true;
        } else {
          if ((ret = iprot->skip(ftype)) < 0) {
            return ret;
          }
          xfer += ret;
        }
        break;
      case 3:
        if (ftype == ::contrail::sandesh::protocol::T_STRING) {
          if ((ret = iprot->readString(this->send_state)) < 0) {
            return ret;
          }
          xfer += ret;

          this->__isset.send_state = true;
        } else {
          if ((ret = iprot->skip(ftype)) < 0) {
            return ret;
          }
          xfer += ret;
        }
        break;
      case 4:
        if (ftype == ::contrail::sandesh::protocol::T_LIST) {
          {
            this->routing_tables.clear();
            uint32_t _size0;
            ::contrail::sandesh::protocol::TType _etype3;
            if ((ret = iprot->readListBegin(_etype3, _size0)) < 0) {
              return ret;
            }
            xfer += ret;
            this->routing_tables.resize(_size0);
            uint32_t _i4;
            for (_i4 = 0; _i4 < _size0; ++_i4)
            {
              if ((ret = iprot->readContainerElementBegin()) < 0) {
                return ret;
              }
              xfer += ret;
              if ((ret = iprot->readString(this->routing_tables[_i4])) < 0) {
                return ret;
              }
              xfer += ret;

              if ((ret = iprot->readContainerElementEnd()) < 0) {
                return ret;
              }
              xfer += ret;
            }
            if ((ret = iprot->readListEnd()) < 0) {
              return ret;
            }
            xfer += ret;
          }
          this->__isset.routing_tables = true;
        } else {
          if ((ret = iprot->skip(ftype)) < 0) {
            return ret;
          }
          xfer += ret;
        }
        break;
      case 5:
        if (ftype == ::contrail::sandesh::protocol::T_STRUCT) {
          if ((ret = this->flap_info.read(iprot)) < 0) {
            return ret;
          }
          xfer += ret;
          this->__isset.flap_info = true;
        } else {
          if ((ret = iprot->skip(ftype)) < 0) {
            return ret;
          }
          xfer += ret;
        }
        break;
      case 6:
        if (ftype == ::contrail::sandesh::protocol::T_STRUCT) {
          if ((ret = this->state_info.read(iprot)) < 0) {
            return ret;
          }
          xfer += ret;
          this->__isset.state_info = true;
        } else {
          if ((ret = iprot->skip(ftype)) < 0) {
            return ret;
          }
          xfer += ret;
        }
        break;
      case 7:
        if (ftype == ::contrail::sandesh::protocol::T_STRUCT) {
          if ((ret = this->event_info.read(iprot)) < 0) {
            return ret;
          }
          xfer += ret;
          this->__isset.event_info = true;
        } else {
          if ((ret = iprot->skip(ftype)) < 0) {
            return ret;
          }
          xfer += ret;
        }
        break;
      case 8:
        if (ftype == ::contrail::sandesh::protocol::T_STRUCT) {
          if ((ret = this->peer_stats_info.read(iprot)) < 0) {
            return ret;
          }
          xfer += ret;
          this->__isset.peer_stats_info = true;
        } else {
          if ((ret = iprot->skip(ftype)) < 0) {
            return ret;
          }
          xfer += ret;
        }
        break;
      case 9:
        if (ftype == ::contrail::sandesh::protocol::T_STRING) {
          if ((ret = iprot->readString(this->close_reason)) < 0) {
            return ret;
          }
          xfer += ret;

          this->__isset.close_reason = true;
        } else {
          if ((ret = iprot->skip(ftype)) < 0) {
            return ret;
          }
          xfer += ret;
        }
        break;
      case 10:
        if (ftype == ::contrail::sandesh::protocol::T_STRING) {
          if ((ret = iprot->readString(this->identifier)) < 0) {
            return ret;
          }
          xfer += ret;

          this->__isset.identifier = true;
        } else {
          if ((ret = iprot->skip(ftype)) < 0) {
            return ret;
          }
          xfer += ret;
        }
        break;
      default:
        if ((ret = iprot->skip(ftype)) < 0) {
          return ret;
        }
        xfer += ret;
        break;
    }
    if ((ret = iprot->readFieldEnd()) < 0) {
      return ret;
    }
    xfer += ret;
  }

  if ((ret = iprot->readStructEnd()) < 0) {
    return ret;
  }
  xfer += ret;

  return xfer;
}

int32_t XmppPeerInfoData::write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const {
  int32_t xfer = 0, ret;
  if ((ret = oprot->writeStructBegin("XmppPeerInfoData")) < 0) {
    return ret;
  }
  xfer += ret;
  {
    std::map<std::string, std::string> annotations_;
    annotations_.insert(std::make_pair("key", "ObjectXmppPeerInfo"));
    if ((ret = oprot->writeFieldBegin("name", ::contrail::sandesh::protocol::T_STRING, 1, &annotations_)) < 0) {
      return ret;
    }
    xfer += ret;
  }
  if ((ret = oprot->writeString(this->name)) < 0) {
    return ret;
  }
  xfer += ret;

  if ((ret = oprot->writeFieldEnd()) < 0) {
    return ret;
  }
  xfer += ret;
  if (this->__isset.deleted) {
    if ((ret = oprot->writeFieldBegin("deleted", ::contrail::sandesh::protocol::T_BOOL, 2)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = oprot->writeBool(this->deleted)) < 0) {
      return ret;
    }
    xfer += ret;

    if ((ret = oprot->writeFieldEnd()) < 0) {
      return ret;
    }
    xfer += ret;
  }
  if (this->__isset.send_state) {
    if ((ret = oprot->writeFieldBegin("send_state", ::contrail::sandesh::protocol::T_STRING, 3)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = oprot->writeString(this->send_state)) < 0) {
      return ret;
    }
    xfer += ret;

    if ((ret = oprot->writeFieldEnd()) < 0) {
      return ret;
    }
    xfer += ret;
  }
  if (this->__isset.routing_tables) {
    if ((ret = oprot->writeFieldBegin("routing_tables", ::contrail::sandesh::protocol::T_LIST, 4)) < 0) {
      return ret;
    }
    xfer += ret;
    {
      if ((ret = oprot->writeListBegin(::contrail::sandesh::protocol::T_STRING, static_cast<uint32_t>(this->routing_tables.size()))) < 0) {
        return ret;
      }
      xfer += ret;
      std::vector<std::string> ::const_iterator _iter5;
      for (_iter5 = this->routing_tables.begin(); _iter5 != this->routing_tables.end(); ++_iter5)
      {
        if ((ret = oprot->writeContainerElementBegin()) < 0) {
          return ret;
        }
        xfer += ret;
        if ((ret = oprot->writeString((*_iter5))) < 0) {
          return ret;
        }
        xfer += ret;

        if ((ret = oprot->writeContainerElementEnd()) < 0) {
          return ret;
        }
        xfer += ret;
      }
      if ((ret = oprot->writeListEnd()) < 0) {
        return ret;
      }
      xfer += ret;
    }
    if ((ret = oprot->writeFieldEnd()) < 0) {
      return ret;
    }
    xfer += ret;
  }
  if (this->__isset.flap_info) {
    if ((ret = oprot->writeFieldBegin("flap_info", ::contrail::sandesh::protocol::T_STRUCT, 5)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = this->flap_info.write(oprot)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = oprot->writeFieldEnd()) < 0) {
      return ret;
    }
    xfer += ret;
  }
  if (this->__isset.state_info) {
    if ((ret = oprot->writeFieldBegin("state_info", ::contrail::sandesh::protocol::T_STRUCT, 6)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = this->state_info.write(oprot)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = oprot->writeFieldEnd()) < 0) {
      return ret;
    }
    xfer += ret;
  }
  if (this->__isset.event_info) {
    if ((ret = oprot->writeFieldBegin("event_info", ::contrail::sandesh::protocol::T_STRUCT, 7)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = this->event_info.write(oprot)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = oprot->writeFieldEnd()) < 0) {
      return ret;
    }
    xfer += ret;
  }
  if (this->__isset.peer_stats_info) {
    if ((ret = oprot->writeFieldBegin("peer_stats_info", ::contrail::sandesh::protocol::T_STRUCT, 8)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = this->peer_stats_info.write(oprot)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = oprot->writeFieldEnd()) < 0) {
      return ret;
    }
    xfer += ret;
  }
  if (this->__isset.close_reason) {
    if ((ret = oprot->writeFieldBegin("close_reason", ::contrail::sandesh::protocol::T_STRING, 9)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = oprot->writeString(this->close_reason)) < 0) {
      return ret;
    }
    xfer += ret;

    if ((ret = oprot->writeFieldEnd()) < 0) {
      return ret;
    }
    xfer += ret;
  }
  if (this->__isset.identifier) {
    if ((ret = oprot->writeFieldBegin("identifier", ::contrail::sandesh::protocol::T_STRING, 10)) < 0) {
      return ret;
    }
    xfer += ret;
    if ((ret = oprot->writeString(this->identifier)) < 0) {
      return ret;
    }
    xfer += ret;

    if ((ret = oprot->writeFieldEnd()) < 0) {
      return ret;
    }
    xfer += ret;
  }
  if ((ret = oprot->writeFieldStop()) < 0) {
    return ret;
  }
  xfer += ret;
  if ((ret = oprot->writeStructEnd()) < 0) {
    return ret;
  }
  xfer += ret;
  return xfer;
}

std::string XmppPeerInfoData::log() const {
  std::stringstream Xbuf;
  Xbuf << "name" << " = " << name;
  if (__isset.deleted) {
    Xbuf << " " << "deleted" << " = " << integerToString(deleted);
  }
  if (__isset.send_state) {
    Xbuf << " " << "send_state" << " = " << send_state;
  }
  if (__isset.routing_tables) {
    Xbuf << " " << "routing_tables" << "= [ ";
    {
      Xbuf << " " << "[";
      std::vector<std::string> ::const_iterator _iter6;
      for (_iter6 = routing_tables.begin(); _iter6 != routing_tables.end(); ++_iter6)
      {
        Xbuf << " " << "(*_iter6)" << " = " << (*_iter6);
        Xbuf << ", ";
      }
      Xbuf << " " << "]";
    }
    Xbuf << " ]";
  }
  if (__isset.flap_info) {
    Xbuf << " " << "flap_info" << "= [ ";
    Xbuf << " " << flap_info.log();
    Xbuf << " ]";
  }
  if (__isset.state_info) {
    Xbuf << " " << "state_info" << "= [ ";
    Xbuf << " " << state_info.log();
    Xbuf << " ]";
  }
  if (__isset.event_info) {
    Xbuf << " " << "event_info" << "= [ ";
    Xbuf << " " << event_info.log();
    Xbuf << " ]";
  }
  if (__isset.peer_stats_info) {
    Xbuf << " " << "peer_stats_info" << "= [ ";
    Xbuf << " " << peer_stats_info.log();
    Xbuf << " ]";
  }
  if (__isset.close_reason) {
    Xbuf << " " << "close_reason" << " = " << close_reason;
  }
  if (__isset.identifier) {
    Xbuf << " " << "identifier" << " = " << identifier;
  }
  return Xbuf.str();
}

const char* XMPPPeerInfo::ascii_fingerprint = "854E4DED008E7BD78DAA3E92F030EF87";
const uint8_t XMPPPeerInfo::binary_fingerprint[16] = {0x85,0x4E,0x4D,0xED,0x00,0x8E,0x7B,0xD7,0x8D,0xAA,0x3E,0x92,0xF0,0x30,0xEF,0x87};

uint32_t XMPPPeerInfo::versionsig_ = 3981266565U;

SANDESH_UVE_DEF(XMPPPeerInfo,XmppPeerInfoData);
void XMPPPeerInfo::Send(const XmppPeerInfoData& data, bool seq, uint32_t seqno, std::string ctx, bool more) {
  XmppPeerInfoData & cdata = const_cast<XmppPeerInfoData &>(data);
  XMPPPeerInfo *snh;
  if (seq) snh = new XMPPPeerInfo(seqno, cdata);
  else snh = new XMPPPeerInfo(lseqnum_++, cdata);
  snh->set_context(ctx); snh->set_more(more);
  if (!seq) uvemapXMPPPeerInfo.UpdateUVE(snh);
  snh->Dispatch();
}

int32_t XMPPPeerInfo::Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot) {

  int32_t xfer = 0, ret;
  std::string fname;
  ::contrail::sandesh::protocol::TType ftype;
  int16_t fid;

  if ((ret = iprot->readSandeshBegin(fname)) < 0) {
    return ret;
  }
  xfer += ret;


  while (true)
  {
    if ((ret = iprot->readFieldBegin(fname, ftype, fid)) < 0) {
      return ret;
    }
    xfer += ret;
    if (ftype == ::contrail::sandesh::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::contrail::sandesh::protocol::T_STRUCT) {
          if ((ret = this->data.read(iprot)) < 0) {
            return ret;
          }
          xfer += ret;
          this->__isset.data = true;
        } else {
          if ((ret = iprot->skip(ftype)) < 0) {
            return ret;
          }
          xfer += ret;
        }
        break;
      default:
        if ((ret = iprot->skip(ftype)) < 0) {
          return ret;
        }
        xfer += ret;
        break;
    }
    if ((ret = iprot->readFieldEnd()) < 0) {
      return ret;
    }
    xfer += ret;
  }

  if ((ret = iprot->readSandeshEnd()) < 0) {
    return ret;
  }
  xfer += ret;

  return xfer;
}

int32_t XMPPPeerInfo::Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const {
  int32_t xfer = 0, ret;
  if ((ret = oprot->writeSandeshBegin("XMPPPeerInfo")) < 0) {
    return ret;
  }
  xfer += ret;
  if ((ret = oprot->writeFieldBegin("data", ::contrail::sandesh::protocol::T_STRUCT, 1)) < 0) {
    return ret;
  }
  xfer += ret;
  if ((ret = this->data.write(oprot)) < 0) {
    return ret;
  }
  xfer += ret;
  if ((ret = oprot->writeFieldEnd()) < 0) {
    return ret;
  }
  xfer += ret;
  if ((ret = oprot->writeFieldStop()) < 0) {
    return ret;
  }
  xfer += ret;
  if ((ret = oprot->writeSandeshEnd()) < 0) {
    return ret;
  }
  xfer += ret;
  return xfer;
}

void XMPPPeerInfo::Log() const {
  log4cplus::Logger Xlogger = Sandesh::logger();
  log4cplus::LogLevel Xlog4level(SandeshLevelTolog4Level(Sandesh::level()));
  if (!Xlogger.isEnabledFor(Xlog4level)) {
    return;
  }
  log4cplus::tostringstream Xbuf;
  Xbuf << "XMPPPeerInfo: ";
  Xbuf << "data" << "= [ ";
  Xbuf << data.log();
  Xbuf << " ]";
  Xlogger.forcedLog(Xlog4level, Xbuf.str());
}

void XMPPPeerInfo::ForcedLog() const {
  log4cplus::Logger Xlogger = Sandesh::logger();
  log4cplus::LogLevel Xlog4level(SandeshLevelTolog4Level(Sandesh::level()));
  log4cplus::tostringstream Xbuf;
  Xbuf << "XMPPPeerInfo: ";
  Xbuf << "data" << "= [ ";
  Xbuf << data.log();
  Xbuf << " ]";
  Xlogger.forcedLog(Xlog4level, Xbuf.str());
}

std::string XMPPPeerInfo::ToString() const {
  std::stringstream Xbuf;
  Xbuf << integerToString(timestamp()) << " ";
  Xbuf << "XMPPPeerInfo: ";
  Xbuf << "data" << "= [ ";
  Xbuf << data.log();
  Xbuf << " ]";
  return Xbuf.str();
}

uint32_t XMPPPeerInfo::lseqnum_ = 1;
void XMPPPeerInfo::UpdateUVE(XmppPeerInfoData & tdata) {
  if (data.__isset.name)
    tdata.set_name(data.get_name());

  if (data.__isset.deleted)
    tdata.set_deleted(data.get_deleted());

  if (data.__isset.send_state)
    tdata.set_send_state(data.get_send_state());

  if (data.__isset.routing_tables)
    tdata.set_routing_tables(data.get_routing_tables());

  if (data.__isset.flap_info)
    tdata.set_flap_info(data.get_flap_info());

  if (data.__isset.state_info)
    tdata.set_state_info(data.get_state_info());

  if (data.__isset.event_info)
    tdata.set_event_info(data.get_event_info());

  if (data.__isset.peer_stats_info)
    tdata.set_peer_stats_info(data.get_peer_stats_info());

  if (data.__isset.close_reason)
    tdata.set_close_reason(data.get_close_reason());

  if (data.__isset.identifier)
    tdata.set_identifier(data.get_identifier());

}


