/**
 * Autogenerated by Sandesh Compiler (1.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 * Copyright (c) 2013 Juniper Networks, Inc. All rights reserved.
 */
#ifndef xmpp_trace_sandesh_TYPES_H
#define xmpp_trace_sandesh_TYPES_H

#include <sandesh/Thrift.h>
#include <base/trace.h>
#include <sandesh/sandesh_constants.h>
#include <sandesh/protocol/TProtocol.h>
#include <sandesh/transport/TTransport.h>





typedef struct _XmppPayloadRead__isset {
  _XmppPayloadRead__isset() : f1(false), f2(false), file(false), line(false) {}
  bool f1;
  bool f2;
  bool file;
  bool line;
} _XmppPayloadRead__isset;

class XmppPayloadRead : public SandeshTrace {
 public:
  static const char* ascii_fingerprint; // = "A5436E5E8C09927C014838016A19A4A7";
  static const uint8_t binary_fingerprint[16]; // = {0xA5,0x43,0x6E,0x5E,0x8C,0x09,0x92,0x7C,0x01,0x48,0x38,0x01,0x6A,0x19,0xA4,0xA7};


  void set_f1(const int32_t val) {
    f1 = val;
  }

  const int32_t get_f1() const {
    return f1;
  }

  void set_f2(const std::string& val) {
    f2 = val;
  }

  const std::string& get_f2() const {
    return f2;
  }

  void set_file(const std::string& val) {
    file = val;
  }

  const std::string& get_file() const {
    return file;
  }

  void set_line(const int32_t val) {
    line = val;
  }

  const int32_t get_line() const {
    return line;
  }

  bool operator == (const XmppPayloadRead & rhs) const
  {
    if (!(str1 == rhs.str1))
      return false;
    if (!(f1 == rhs.f1))
      return false;
    if (!(str3 == rhs.str3))
      return false;
    if (!(f2 == rhs.f2))
      return false;
    if (!(file == rhs.file))
      return false;
    if (!(line == rhs.line))
      return false;
    return true;
  }
  bool operator != (const XmppPayloadRead &rhs) const {
    return !(*this == rhs);
  }


  virtual ~XmppPayloadRead() {}


  _XmppPayloadRead__isset __isset;
  virtual std::string ModuleName() const { return "xmpp_trace_sandesh"; }

  virtual void SendTrace(const std::string& tcontext, bool more) {
    set_context(tcontext);
    set_more(more);
    XmppPayloadRead * snh = new XmppPayloadRead(*this);
    snh->Dispatch();
  }


  static void TraceMsg(SandeshTraceBufferPtr tracebuf, std::string file, int32_t line, int32_t f1, std::string f2);

  #define XMPP_PAYLOAD_READ_TRACE(_trace_buf, _f1, _f2)\
    XmppPayloadRead::TraceMsg((_trace_buf), __FILE__, __LINE__, (_f1), (_f2))
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  std::string ToString() const;

private:

  explicit XmppPayloadRead(uint32_t seqno, int32_t f1, std::string f2, std::string file, int32_t line) : SandeshTrace("XmppPayloadRead",seqno), f1(f1), f2(f2), file(file), line(line) {
    set_seqnum(0);
    set_hints(0);
  }
  static std::string str1;
  int32_t f1;
  static std::string str3;
  std::string f2;
  std::string file;
  int32_t line;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _XmppTxStream__isset {
  _XmppTxStream__isset() : IPaddress(false), port(false), size(false), packet(false), file(false), line(false) {}
  bool IPaddress;
  bool port;
  bool size;
  bool packet;
  bool file;
  bool line;
} _XmppTxStream__isset;

class XmppTxStream : public SandeshTrace {
 public:
  static const char* ascii_fingerprint; // = "3E07942C35AE103D22E8A3FF6E55BADF";
  static const uint8_t binary_fingerprint[16]; // = {0x3E,0x07,0x94,0x2C,0x35,0xAE,0x10,0x3D,0x22,0xE8,0xA3,0xFF,0x6E,0x55,0xBA,0xDF};


  void set_IPaddress(const std::string& val) {
    IPaddress = val;
  }

  const std::string& get_IPaddress() const {
    return IPaddress;
  }

  void set_port(const int32_t val) {
    port = val;
  }

  const int32_t get_port() const {
    return port;
  }

  void set_size(const int32_t val) {
    size = val;
  }

  const int32_t get_size() const {
    return size;
  }

  void set_packet(const std::string& val) {
    packet = val;
  }

  const std::string& get_packet() const {
    return packet;
  }

  void set_file(const std::string& val) {
    file = val;
  }

  const std::string& get_file() const {
    return file;
  }

  void set_line(const int32_t val) {
    line = val;
  }

  const int32_t get_line() const {
    return line;
  }

  bool operator == (const XmppTxStream & rhs) const
  {
    if (!(str1 == rhs.str1))
      return false;
    if (!(IPaddress == rhs.IPaddress))
      return false;
    if (!(str3 == rhs.str3))
      return false;
    if (!(port == rhs.port))
      return false;
    if (!(str5 == rhs.str5))
      return false;
    if (!(size == rhs.size))
      return false;
    if (!(str7 == rhs.str7))
      return false;
    if (!(packet == rhs.packet))
      return false;
    if (!(str9 == rhs.str9))
      return false;
    if (!(file == rhs.file))
      return false;
    if (!(line == rhs.line))
      return false;
    return true;
  }
  bool operator != (const XmppTxStream &rhs) const {
    return !(*this == rhs);
  }


  virtual ~XmppTxStream() {}


  _XmppTxStream__isset __isset;
  virtual std::string ModuleName() const { return "xmpp_trace_sandesh"; }

  virtual void SendTrace(const std::string& tcontext, bool more) {
    set_context(tcontext);
    set_more(more);
    XmppTxStream * snh = new XmppTxStream(*this);
    snh->Dispatch();
  }


  static void TraceMsg(SandeshTraceBufferPtr tracebuf, std::string file, int32_t line, std::string IPaddress, int32_t port, int32_t size, std::string packet);

  #define XMPP_TX_STREAM_TRACE(_trace_buf, _IPaddress, _port, _size, _packet)\
    XmppTxStream::TraceMsg((_trace_buf), __FILE__, __LINE__, (_IPaddress), (_port), (_size), (_packet))
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  std::string ToString() const;

private:

  explicit XmppTxStream(uint32_t seqno, std::string IPaddress, int32_t port, int32_t size, std::string packet, std::string file, int32_t line) : SandeshTrace("XmppTxStream",seqno), IPaddress(IPaddress), port(port), size(size), packet(packet), file(file), line(line) {
    set_seqnum(0);
    set_hints(0);
  }
  static std::string str1;
  std::string IPaddress;
  static std::string str3;
  int32_t port;
  static std::string str5;
  int32_t size;
  static std::string str7;
  std::string packet;
  static std::string str9;
  std::string file;
  int32_t line;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _XmppRxStream__isset {
  _XmppRxStream__isset() : IPaddress(false), port(false), size(false), packet(false), file(false), line(false) {}
  bool IPaddress;
  bool port;
  bool size;
  bool packet;
  bool file;
  bool line;
} _XmppRxStream__isset;

class XmppRxStream : public SandeshTrace {
 public:
  static const char* ascii_fingerprint; // = "3E07942C35AE103D22E8A3FF6E55BADF";
  static const uint8_t binary_fingerprint[16]; // = {0x3E,0x07,0x94,0x2C,0x35,0xAE,0x10,0x3D,0x22,0xE8,0xA3,0xFF,0x6E,0x55,0xBA,0xDF};


  void set_IPaddress(const std::string& val) {
    IPaddress = val;
  }

  const std::string& get_IPaddress() const {
    return IPaddress;
  }

  void set_port(const int32_t val) {
    port = val;
  }

  const int32_t get_port() const {
    return port;
  }

  void set_size(const int32_t val) {
    size = val;
  }

  const int32_t get_size() const {
    return size;
  }

  void set_packet(const std::string& val) {
    packet = val;
  }

  const std::string& get_packet() const {
    return packet;
  }

  void set_file(const std::string& val) {
    file = val;
  }

  const std::string& get_file() const {
    return file;
  }

  void set_line(const int32_t val) {
    line = val;
  }

  const int32_t get_line() const {
    return line;
  }

  bool operator == (const XmppRxStream & rhs) const
  {
    if (!(str1 == rhs.str1))
      return false;
    if (!(IPaddress == rhs.IPaddress))
      return false;
    if (!(str3 == rhs.str3))
      return false;
    if (!(port == rhs.port))
      return false;
    if (!(str5 == rhs.str5))
      return false;
    if (!(size == rhs.size))
      return false;
    if (!(str7 == rhs.str7))
      return false;
    if (!(packet == rhs.packet))
      return false;
    if (!(str9 == rhs.str9))
      return false;
    if (!(file == rhs.file))
      return false;
    if (!(line == rhs.line))
      return false;
    return true;
  }
  bool operator != (const XmppRxStream &rhs) const {
    return !(*this == rhs);
  }


  virtual ~XmppRxStream() {}


  _XmppRxStream__isset __isset;
  virtual std::string ModuleName() const { return "xmpp_trace_sandesh"; }

  virtual void SendTrace(const std::string& tcontext, bool more) {
    set_context(tcontext);
    set_more(more);
    XmppRxStream * snh = new XmppRxStream(*this);
    snh->Dispatch();
  }


  static void TraceMsg(SandeshTraceBufferPtr tracebuf, std::string file, int32_t line, std::string IPaddress, int32_t port, int32_t size, std::string packet);

  #define XMPP_RX_STREAM_TRACE(_trace_buf, _IPaddress, _port, _size, _packet)\
    XmppRxStream::TraceMsg((_trace_buf), __FILE__, __LINE__, (_IPaddress), (_port), (_size), (_packet))
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  std::string ToString() const;

private:

  explicit XmppRxStream(uint32_t seqno, std::string IPaddress, int32_t port, int32_t size, std::string packet, std::string file, int32_t line) : SandeshTrace("XmppRxStream",seqno), IPaddress(IPaddress), port(port), size(size), packet(packet), file(file), line(line) {
    set_seqnum(0);
    set_hints(0);
  }
  static std::string str1;
  std::string IPaddress;
  static std::string str3;
  int32_t port;
  static std::string str5;
  int32_t size;
  static std::string str7;
  std::string packet;
  static std::string str9;
  std::string file;
  int32_t line;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _XmppRxStreamInvalid__isset {
  _XmppRxStreamInvalid__isset() : IPaddress(false), port(false), size(false), packet(false), file(false), line(false) {}
  bool IPaddress;
  bool port;
  bool size;
  bool packet;
  bool file;
  bool line;
} _XmppRxStreamInvalid__isset;

class XmppRxStreamInvalid : public SandeshTrace {
 public:
  static const char* ascii_fingerprint; // = "3E07942C35AE103D22E8A3FF6E55BADF";
  static const uint8_t binary_fingerprint[16]; // = {0x3E,0x07,0x94,0x2C,0x35,0xAE,0x10,0x3D,0x22,0xE8,0xA3,0xFF,0x6E,0x55,0xBA,0xDF};


  void set_IPaddress(const std::string& val) {
    IPaddress = val;
  }

  const std::string& get_IPaddress() const {
    return IPaddress;
  }

  void set_port(const int32_t val) {
    port = val;
  }

  const int32_t get_port() const {
    return port;
  }

  void set_size(const int32_t val) {
    size = val;
  }

  const int32_t get_size() const {
    return size;
  }

  void set_packet(const std::string& val) {
    packet = val;
  }

  const std::string& get_packet() const {
    return packet;
  }

  void set_file(const std::string& val) {
    file = val;
  }

  const std::string& get_file() const {
    return file;
  }

  void set_line(const int32_t val) {
    line = val;
  }

  const int32_t get_line() const {
    return line;
  }

  bool operator == (const XmppRxStreamInvalid & rhs) const
  {
    if (!(str1 == rhs.str1))
      return false;
    if (!(IPaddress == rhs.IPaddress))
      return false;
    if (!(str3 == rhs.str3))
      return false;
    if (!(port == rhs.port))
      return false;
    if (!(str5 == rhs.str5))
      return false;
    if (!(size == rhs.size))
      return false;
    if (!(str7 == rhs.str7))
      return false;
    if (!(packet == rhs.packet))
      return false;
    if (!(str9 == rhs.str9))
      return false;
    if (!(file == rhs.file))
      return false;
    if (!(line == rhs.line))
      return false;
    return true;
  }
  bool operator != (const XmppRxStreamInvalid &rhs) const {
    return !(*this == rhs);
  }


  virtual ~XmppRxStreamInvalid() {}


  _XmppRxStreamInvalid__isset __isset;
  virtual std::string ModuleName() const { return "xmpp_trace_sandesh"; }

  virtual void SendTrace(const std::string& tcontext, bool more) {
    set_context(tcontext);
    set_more(more);
    XmppRxStreamInvalid * snh = new XmppRxStreamInvalid(*this);
    snh->Dispatch();
  }


  static void TraceMsg(SandeshTraceBufferPtr tracebuf, std::string file, int32_t line, std::string IPaddress, int32_t port, int32_t size, std::string packet);

  #define XMPP_RX_STREAM_INVALID_TRACE(_trace_buf, _IPaddress, _port, _size, _packet)\
    XmppRxStreamInvalid::TraceMsg((_trace_buf), __FILE__, __LINE__, (_IPaddress), (_port), (_size), (_packet))
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  std::string ToString() const;

private:

  explicit XmppRxStreamInvalid(uint32_t seqno, std::string IPaddress, int32_t port, int32_t size, std::string packet, std::string file, int32_t line) : SandeshTrace("XmppRxStreamInvalid",seqno), IPaddress(IPaddress), port(port), size(size), packet(packet), file(file), line(line) {
    set_seqnum(0);
    set_hints(0);
  }
  static std::string str1;
  std::string IPaddress;
  static std::string str3;
  int32_t port;
  static std::string str5;
  int32_t size;
  static std::string str7;
  std::string packet;
  static std::string str9;
  std::string file;
  int32_t line;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _XmppSendChatMessage__isset {
  _XmppSendChatMessage__isset() : f1(false), f2(false), file(false), line(false) {}
  bool f1;
  bool f2;
  bool file;
  bool line;
} _XmppSendChatMessage__isset;

class XmppSendChatMessage : public SandeshTrace {
 public:
  static const char* ascii_fingerprint; // = "D3CB269ACF2954A1DC7F4634CC68A594";
  static const uint8_t binary_fingerprint[16]; // = {0xD3,0xCB,0x26,0x9A,0xCF,0x29,0x54,0xA1,0xDC,0x7F,0x46,0x34,0xCC,0x68,0xA5,0x94};


  void set_f1(const std::string& val) {
    f1 = val;
  }

  const std::string& get_f1() const {
    return f1;
  }

  void set_f2(const std::string& val) {
    f2 = val;
  }

  const std::string& get_f2() const {
    return f2;
  }

  void set_file(const std::string& val) {
    file = val;
  }

  const std::string& get_file() const {
    return file;
  }

  void set_line(const int32_t val) {
    line = val;
  }

  const int32_t get_line() const {
    return line;
  }

  bool operator == (const XmppSendChatMessage & rhs) const
  {
    if (!(str1 == rhs.str1))
      return false;
    if (!(f1 == rhs.f1))
      return false;
    if (!(str3 == rhs.str3))
      return false;
    if (!(f2 == rhs.f2))
      return false;
    if (!(file == rhs.file))
      return false;
    if (!(line == rhs.line))
      return false;
    return true;
  }
  bool operator != (const XmppSendChatMessage &rhs) const {
    return !(*this == rhs);
  }


  virtual ~XmppSendChatMessage() {}


  _XmppSendChatMessage__isset __isset;
  virtual std::string ModuleName() const { return "xmpp_trace_sandesh"; }

  virtual void SendTrace(const std::string& tcontext, bool more) {
    set_context(tcontext);
    set_more(more);
    XmppSendChatMessage * snh = new XmppSendChatMessage(*this);
    snh->Dispatch();
  }


  static void TraceMsg(SandeshTraceBufferPtr tracebuf, std::string file, int32_t line, std::string f1, std::string f2);

  #define XMPP_SEND_CHAT_MESSAGE_TRACE(_trace_buf, _f1, _f2)\
    XmppSendChatMessage::TraceMsg((_trace_buf), __FILE__, __LINE__, (_f1), (_f2))
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  std::string ToString() const;

private:

  explicit XmppSendChatMessage(uint32_t seqno, std::string f1, std::string f2, std::string file, int32_t line) : SandeshTrace("XmppSendChatMessage",seqno), f1(f1), f2(f2), file(file), line(line) {
    set_seqnum(0);
    set_hints(0);
  }
  static std::string str1;
  std::string f1;
  static std::string str3;
  std::string f2;
  std::string file;
  int32_t line;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _XmppRxChatMessageClient__isset {
  _XmppRxChatMessageClient__isset() : f1(false), f2(false), file(false), line(false) {}
  bool f1;
  bool f2;
  bool file;
  bool line;
} _XmppRxChatMessageClient__isset;

class XmppRxChatMessageClient : public SandeshTrace {
 public:
  static const char* ascii_fingerprint; // = "D3CB269ACF2954A1DC7F4634CC68A594";
  static const uint8_t binary_fingerprint[16]; // = {0xD3,0xCB,0x26,0x9A,0xCF,0x29,0x54,0xA1,0xDC,0x7F,0x46,0x34,0xCC,0x68,0xA5,0x94};


  void set_f1(const std::string& val) {
    f1 = val;
  }

  const std::string& get_f1() const {
    return f1;
  }

  void set_f2(const std::string& val) {
    f2 = val;
  }

  const std::string& get_f2() const {
    return f2;
  }

  void set_file(const std::string& val) {
    file = val;
  }

  const std::string& get_file() const {
    return file;
  }

  void set_line(const int32_t val) {
    line = val;
  }

  const int32_t get_line() const {
    return line;
  }

  bool operator == (const XmppRxChatMessageClient & rhs) const
  {
    if (!(str1 == rhs.str1))
      return false;
    if (!(f1 == rhs.f1))
      return false;
    if (!(str3 == rhs.str3))
      return false;
    if (!(f2 == rhs.f2))
      return false;
    if (!(file == rhs.file))
      return false;
    if (!(line == rhs.line))
      return false;
    return true;
  }
  bool operator != (const XmppRxChatMessageClient &rhs) const {
    return !(*this == rhs);
  }


  virtual ~XmppRxChatMessageClient() {}


  _XmppRxChatMessageClient__isset __isset;
  virtual std::string ModuleName() const { return "xmpp_trace_sandesh"; }

  virtual void SendTrace(const std::string& tcontext, bool more) {
    set_context(tcontext);
    set_more(more);
    XmppRxChatMessageClient * snh = new XmppRxChatMessageClient(*this);
    snh->Dispatch();
  }


  static void TraceMsg(SandeshTraceBufferPtr tracebuf, std::string file, int32_t line, std::string f1, std::string f2);

  #define XMPP_RX_CHAT_MESSAGE_CLIENT_TRACE(_trace_buf, _f1, _f2)\
    XmppRxChatMessageClient::TraceMsg((_trace_buf), __FILE__, __LINE__, (_f1), (_f2))
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  std::string ToString() const;

private:

  explicit XmppRxChatMessageClient(uint32_t seqno, std::string f1, std::string f2, std::string file, int32_t line) : SandeshTrace("XmppRxChatMessageClient",seqno), f1(f1), f2(f2), file(file), line(line) {
    set_seqnum(0);
    set_hints(0);
  }
  static std::string str1;
  std::string f1;
  static std::string str3;
  std::string f2;
  std::string file;
  int32_t line;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _XmppRxChatMessageServer__isset {
  _XmppRxChatMessageServer__isset() : f1(false), f2(false), f3(false), file(false), line(false) {}
  bool f1;
  bool f2;
  bool f3;
  bool file;
  bool line;
} _XmppRxChatMessageServer__isset;

class XmppRxChatMessageServer : public SandeshTrace {
 public:
  static const char* ascii_fingerprint; // = "E358A9D5A83AC20CEE249FD45E7ADB45";
  static const uint8_t binary_fingerprint[16]; // = {0xE3,0x58,0xA9,0xD5,0xA8,0x3A,0xC2,0x0C,0xEE,0x24,0x9F,0xD4,0x5E,0x7A,0xDB,0x45};


  void set_f1(const std::string& val) {
    f1 = val;
  }

  const std::string& get_f1() const {
    return f1;
  }

  void set_f2(const std::string& val) {
    f2 = val;
  }

  const std::string& get_f2() const {
    return f2;
  }

  void set_f3(const std::string& val) {
    f3 = val;
  }

  const std::string& get_f3() const {
    return f3;
  }

  void set_file(const std::string& val) {
    file = val;
  }

  const std::string& get_file() const {
    return file;
  }

  void set_line(const int32_t val) {
    line = val;
  }

  const int32_t get_line() const {
    return line;
  }

  bool operator == (const XmppRxChatMessageServer & rhs) const
  {
    if (!(str1 == rhs.str1))
      return false;
    if (!(f1 == rhs.f1))
      return false;
    if (!(str3 == rhs.str3))
      return false;
    if (!(f2 == rhs.f2))
      return false;
    if (!(f3 == rhs.f3))
      return false;
    if (!(file == rhs.file))
      return false;
    if (!(line == rhs.line))
      return false;
    return true;
  }
  bool operator != (const XmppRxChatMessageServer &rhs) const {
    return !(*this == rhs);
  }


  virtual ~XmppRxChatMessageServer() {}


  _XmppRxChatMessageServer__isset __isset;
  virtual std::string ModuleName() const { return "xmpp_trace_sandesh"; }

  virtual void SendTrace(const std::string& tcontext, bool more) {
    set_context(tcontext);
    set_more(more);
    XmppRxChatMessageServer * snh = new XmppRxChatMessageServer(*this);
    snh->Dispatch();
  }


  static void TraceMsg(SandeshTraceBufferPtr tracebuf, std::string file, int32_t line, std::string f1, std::string f2, std::string f3);

  #define XMPP_RX_CHAT_MESSAGE_SERVER_TRACE(_trace_buf, _f1, _f2, _f3)\
    XmppRxChatMessageServer::TraceMsg((_trace_buf), __FILE__, __LINE__, (_f1), (_f2), (_f3))
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  std::string ToString() const;

private:

  explicit XmppRxChatMessageServer(uint32_t seqno, std::string f1, std::string f2, std::string f3, std::string file, int32_t line) : SandeshTrace("XmppRxChatMessageServer",seqno), f1(f1), f2(f2), f3(f3), file(file), line(line) {
    set_seqnum(0);
    set_hints(0);
  }
  static std::string str1;
  std::string f1;
  static std::string str3;
  std::string f2;
  std::string f3;
  std::string file;
  int32_t line;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};



#endif
