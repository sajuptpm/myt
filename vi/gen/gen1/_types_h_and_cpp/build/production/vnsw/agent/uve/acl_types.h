/**
 * Autogenerated by Sandesh Compiler (1.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 * Copyright (c) 2013 Juniper Networks, Inc. All rights reserved.
 */
#ifndef acl_TYPES_H
#define acl_TYPES_H

#include <sandesh/Thrift.h>
#include <base/trace.h>
#include <sandesh/sandesh_constants.h>
#include <sandesh/protocol/TProtocol.h>
#include <sandesh/transport/TTransport.h>





typedef struct _AclRuleToFlowCount__isset {
  _AclRuleToFlowCount__isset() : acl_major(false), acl_minor(false), flows(false) {}
  bool acl_major;
  bool acl_minor;
  bool flows;
} _AclRuleToFlowCount__isset;

class AclRuleToFlowCount {
 public:

  static const char* ascii_fingerprint; // = "49C4050C1193C0367CF93402E8A80EEA";
  static const uint8_t binary_fingerprint[16]; // = {0x49,0xC4,0x05,0x0C,0x11,0x93,0xC0,0x36,0x7C,0xF9,0x34,0x02,0xE8,0xA8,0x0E,0xEA};

  AclRuleToFlowCount() : acl_major(0), acl_minor(0), flows(0) {
  }

  virtual ~AclRuleToFlowCount() throw() {}

  int32_t acl_major;
  int32_t acl_minor;
  int64_t flows;

  _AclRuleToFlowCount__isset __isset;

  void set_acl_major(const int32_t val) {
    acl_major = val;
  }

  const int32_t get_acl_major() const {
    return acl_major;
  }

  void set_acl_minor(const int32_t val) {
    acl_minor = val;
  }

  const int32_t get_acl_minor() const {
    return acl_minor;
  }

  void set_flows(const int64_t val) {
    flows = val;
  }

  const int64_t get_flows() const {
    return flows;
  }

  bool operator == (const AclRuleToFlowCount & rhs) const
  {
    if (!(acl_major == rhs.acl_major))
      return false;
    if (!(acl_minor == rhs.acl_minor))
      return false;
    if (!(flows == rhs.flows))
      return false;
    return true;
  }
  bool operator != (const AclRuleToFlowCount &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const AclRuleToFlowCount & ) const;

  int32_t read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;
  std::string log() const;

};

typedef struct _UveAclAgent__isset {
  _UveAclAgent__isset() : acl_rule_flow_hits(false) {}
  bool acl_rule_flow_hits;
} _UveAclAgent__isset;

class UveAclAgent {
 public:

  static const char* ascii_fingerprint; // = "297FE4C7D27FF903E695AC51A168CD94";
  static const uint8_t binary_fingerprint[16]; // = {0x29,0x7F,0xE4,0xC7,0xD2,0x7F,0xF9,0x03,0xE6,0x95,0xAC,0x51,0xA1,0x68,0xCD,0x94};

  UveAclAgent() {
  }

  virtual ~UveAclAgent() throw() {}

  std::vector<AclRuleToFlowCount>  acl_rule_flow_hits;

  _UveAclAgent__isset __isset;

  void set_acl_rule_flow_hits(const std::vector<AclRuleToFlowCount> & val) {
    acl_rule_flow_hits = val;
    __isset.acl_rule_flow_hits = true;
  }

  const std::vector<AclRuleToFlowCount> & get_acl_rule_flow_hits() const {
    return acl_rule_flow_hits;
  }

  bool operator == (const UveAclAgent & rhs) const
  {
    if (__isset.acl_rule_flow_hits != rhs.__isset.acl_rule_flow_hits)
      return false;
    else if (__isset.acl_rule_flow_hits && !(acl_rule_flow_hits == rhs.acl_rule_flow_hits))
      return false;
    return true;
  }
  bool operator != (const UveAclAgent &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const UveAclAgent & ) const;

  int32_t read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;
  std::string log() const;

};

typedef struct _UveAclVirtualNetworkAgent__isset {
  _UveAclVirtualNetworkAgent__isset() : name(false), deleted(false), agent(false) {}
  bool name;
  bool deleted;
  bool agent;
} _UveAclVirtualNetworkAgent__isset;

class UveAclVirtualNetworkAgent {
 public:

  static const char* ascii_fingerprint; // = "30D31589B9744BB7E9A168520B7AFA02";
  static const uint8_t binary_fingerprint[16]; // = {0x30,0xD3,0x15,0x89,0xB9,0x74,0x4B,0xB7,0xE9,0xA1,0x68,0x52,0x0B,0x7A,0xFA,0x02};

  UveAclVirtualNetworkAgent() : name(""), deleted(0) {
  }

  virtual ~UveAclVirtualNetworkAgent() throw() {}

  std::string name;
  bool deleted;
  UveAclAgent agent;

  _UveAclVirtualNetworkAgent__isset __isset;

  void set_name(const std::string& val) {
    name = val;
  }

  const std::string& get_name() const {
    return name;
  }

  void set_deleted(const bool val) {
    deleted = val;
    __isset.deleted = true;
  }

  const bool get_deleted() const {
    return deleted;
  }

  void set_agent(const UveAclAgent& val) {
    agent = val;
    __isset.agent = true;
  }

  const UveAclAgent& get_agent() const {
    return agent;
  }

  bool operator == (const UveAclVirtualNetworkAgent & rhs) const
  {
    if (!(name == rhs.name))
      return false;
    if (__isset.deleted != rhs.__isset.deleted)
      return false;
    else if (__isset.deleted && !(deleted == rhs.deleted))
      return false;
    if (__isset.agent != rhs.__isset.agent)
      return false;
    else if (__isset.agent && !(agent == rhs.agent))
      return false;
    return true;
  }
  bool operator != (const UveAclVirtualNetworkAgent &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const UveAclVirtualNetworkAgent & ) const;

  int32_t read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;
  std::string log() const;

};

typedef struct _UveAclVirtualMachineAgent__isset {
  _UveAclVirtualMachineAgent__isset() : name(false), deleted(false), agent(false) {}
  bool name;
  bool deleted;
  bool agent;
} _UveAclVirtualMachineAgent__isset;

class UveAclVirtualMachineAgent {
 public:

  static const char* ascii_fingerprint; // = "30D31589B9744BB7E9A168520B7AFA02";
  static const uint8_t binary_fingerprint[16]; // = {0x30,0xD3,0x15,0x89,0xB9,0x74,0x4B,0xB7,0xE9,0xA1,0x68,0x52,0x0B,0x7A,0xFA,0x02};

  UveAclVirtualMachineAgent() : name(""), deleted(0) {
  }

  virtual ~UveAclVirtualMachineAgent() throw() {}

  std::string name;
  bool deleted;
  UveAclAgent agent;

  _UveAclVirtualMachineAgent__isset __isset;

  void set_name(const std::string& val) {
    name = val;
  }

  const std::string& get_name() const {
    return name;
  }

  void set_deleted(const bool val) {
    deleted = val;
    __isset.deleted = true;
  }

  const bool get_deleted() const {
    return deleted;
  }

  void set_agent(const UveAclAgent& val) {
    agent = val;
    __isset.agent = true;
  }

  const UveAclAgent& get_agent() const {
    return agent;
  }

  bool operator == (const UveAclVirtualMachineAgent & rhs) const
  {
    if (!(name == rhs.name))
      return false;
    if (__isset.deleted != rhs.__isset.deleted)
      return false;
    else if (__isset.deleted && !(deleted == rhs.deleted))
      return false;
    if (__isset.agent != rhs.__isset.agent)
      return false;
    else if (__isset.agent && !(agent == rhs.agent))
      return false;
    return true;
  }
  bool operator != (const UveAclVirtualMachineAgent &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const UveAclVirtualMachineAgent & ) const;

  int32_t read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;
  std::string log() const;

};

typedef struct _UveAclVirtualNetworkAgentTrace__isset {
  _UveAclVirtualNetworkAgentTrace__isset() : data(false) {}
  bool data;
} _UveAclVirtualNetworkAgentTrace__isset;

class UveAclVirtualNetworkAgentTrace : public SandeshUVE {
 public:
  static const char* ascii_fingerprint; // = "3AEA42E4AD13D80052E1F13341C9C291";
  static const uint8_t binary_fingerprint[16]; // = {0x3A,0xEA,0x42,0xE4,0xAD,0x13,0xD8,0x00,0x52,0xE1,0xF1,0x33,0x41,0xC9,0xC2,0x91};


  void set_data(const UveAclVirtualNetworkAgent& val) {
    data = val;
  }

  const UveAclVirtualNetworkAgent& get_data() const {
    return data;
  }

  bool operator == (const UveAclVirtualNetworkAgentTrace & rhs) const
  {
    if (!(data == rhs.data))
      return false;
    return true;
  }
  bool operator != (const UveAclVirtualNetworkAgentTrace &rhs) const {
    return !(*this == rhs);
  }


  virtual ~UveAclVirtualNetworkAgentTrace() {}


  _UveAclVirtualNetworkAgentTrace__isset __isset;
  virtual std::string ModuleName() const { return "acl"; }

  static void Send(const UveAclVirtualNetworkAgent& data, bool seq = false, uint32_t seqno = 0, std::string ctx = "", bool more = false);
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  static int32_t lseqnum() { return lseqnum_;}
  void UpdateUVE(UveAclVirtualNetworkAgent & tdata);
  std::string ToString() const;

private:

  explicit UveAclVirtualNetworkAgentTrace(uint32_t seqno, const UveAclVirtualNetworkAgent& data) : SandeshUVE("UveAclVirtualNetworkAgentTrace",seqno), data(data) {
    set_hints(0 | g_sandesh_constants.SANDESH_KEY_HINT);
  }
  UveAclVirtualNetworkAgent data;
  static uint32_t lseqnum_;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _UveAclVirtualMachineAgentTrace__isset {
  _UveAclVirtualMachineAgentTrace__isset() : data(false) {}
  bool data;
} _UveAclVirtualMachineAgentTrace__isset;

class UveAclVirtualMachineAgentTrace : public SandeshUVE {
 public:
  static const char* ascii_fingerprint; // = "3AEA42E4AD13D80052E1F13341C9C291";
  static const uint8_t binary_fingerprint[16]; // = {0x3A,0xEA,0x42,0xE4,0xAD,0x13,0xD8,0x00,0x52,0xE1,0xF1,0x33,0x41,0xC9,0xC2,0x91};


  void set_data(const UveAclVirtualMachineAgent& val) {
    data = val;
  }

  const UveAclVirtualMachineAgent& get_data() const {
    return data;
  }

  bool operator == (const UveAclVirtualMachineAgentTrace & rhs) const
  {
    if (!(data == rhs.data))
      return false;
    return true;
  }
  bool operator != (const UveAclVirtualMachineAgentTrace &rhs) const {
    return !(*this == rhs);
  }


  virtual ~UveAclVirtualMachineAgentTrace() {}


  _UveAclVirtualMachineAgentTrace__isset __isset;
  virtual std::string ModuleName() const { return "acl"; }

  static void Send(const UveAclVirtualMachineAgent& data, bool seq = false, uint32_t seqno = 0, std::string ctx = "", bool more = false);
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  static int32_t lseqnum() { return lseqnum_;}
  void UpdateUVE(UveAclVirtualMachineAgent & tdata);
  std::string ToString() const;

private:

  explicit UveAclVirtualMachineAgentTrace(uint32_t seqno, const UveAclVirtualMachineAgent& data) : SandeshUVE("UveAclVirtualMachineAgentTrace",seqno), data(data) {
    set_hints(0 | g_sandesh_constants.SANDESH_KEY_HINT);
  }
  UveAclVirtualMachineAgent data;
  static uint32_t lseqnum_;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};



#endif
