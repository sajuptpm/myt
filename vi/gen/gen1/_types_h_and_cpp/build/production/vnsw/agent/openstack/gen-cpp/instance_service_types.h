/**
 * Autogenerated by Thrift Compiler (0.8.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#ifndef instance_service_TYPES_H
#define instance_service_TYPES_H

#include <Thrift.h>
#include <TApplicationException.h>
#include <protocol/TProtocol.h>
#include <transport/TTransport.h>

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <async/TAsync.h>
#include <async/TFuture.h>
#include <concurrency/Mutex.h>





struct PortTypes {
  enum type {
    NovaVMPort = 0,
    NameSpacePort = 1
  };
};

extern const std::map<int, const char*> _PortTypes_VALUES_TO_NAMES;

typedef std::vector<int16_t>  tuuid;

typedef std::vector<class Port>  PortList;

typedef std::vector<class Subnet>  SubnetList;

typedef std::vector<class VirtualGatewayRequest>  VirtualGatewayRequestList;

typedef struct _Port__isset {
  _Port__isset() : display_name(false), hostname(false), host(false), vm_project_id(false), vlan_id(false), port_type(false), ip6_address(false), isolated_vlan_id(false) {}
  bool display_name;
  bool hostname;
  bool host;
  bool vm_project_id;
  bool vlan_id;
  bool port_type;
  bool ip6_address;
  bool isolated_vlan_id;
} _Port__isset;

class Port {
 public:

  static const char* ascii_fingerprint; // = "85B4874F1725ED416914A9040FB75536";
  static const uint8_t binary_fingerprint[16]; // = {0x85,0xB4,0x87,0x4F,0x17,0x25,0xED,0x41,0x69,0x14,0xA9,0x04,0x0F,0xB7,0x55,0x36};

  Port() : tap_name(""), ip_address(""), mac_address(""), display_name(""), hostname(""), host(""), vlan_id(0), port_type(0), ip6_address(""), isolated_vlan_id(0) {
  }

  virtual ~Port() throw() {}

  tuuid port_id;
  tuuid instance_id;
  std::string tap_name;
  std::string ip_address;
  tuuid vn_id;
  std::string mac_address;
  std::string display_name;
  std::string hostname;
  std::string host;
  tuuid vm_project_id;
  int16_t vlan_id;
  int16_t port_type;
  std::string ip6_address;
  int16_t isolated_vlan_id;

  _Port__isset __isset;

  void __set_port_id(const tuuid& val) {
    port_id = val;
  }

  void __set_instance_id(const tuuid& val) {
    instance_id = val;
  }

  void __set_tap_name(const std::string& val) {
    tap_name = val;
  }

  void __set_ip_address(const std::string& val) {
    ip_address = val;
  }

  void __set_vn_id(const tuuid& val) {
    vn_id = val;
  }

  void __set_mac_address(const std::string& val) {
    mac_address = val;
  }

  void __set_display_name(const std::string& val) {
    display_name = val;
    __isset.display_name = true;
  }

  void __set_hostname(const std::string& val) {
    hostname = val;
    __isset.hostname = true;
  }

  void __set_host(const std::string& val) {
    host = val;
    __isset.host = true;
  }

  void __set_vm_project_id(const tuuid& val) {
    vm_project_id = val;
    __isset.vm_project_id = true;
  }

  void __set_vlan_id(const int16_t val) {
    vlan_id = val;
    __isset.vlan_id = true;
  }

  void __set_port_type(const int16_t val) {
    port_type = val;
    __isset.port_type = true;
  }

  void __set_ip6_address(const std::string& val) {
    ip6_address = val;
    __isset.ip6_address = true;
  }

  void __set_isolated_vlan_id(const int16_t val) {
    isolated_vlan_id = val;
    __isset.isolated_vlan_id = true;
  }

  bool operator == (const Port & rhs) const
  {
    if (!(port_id == rhs.port_id))
      return false;
    if (!(instance_id == rhs.instance_id))
      return false;
    if (!(tap_name == rhs.tap_name))
      return false;
    if (!(ip_address == rhs.ip_address))
      return false;
    if (!(vn_id == rhs.vn_id))
      return false;
    if (!(mac_address == rhs.mac_address))
      return false;
    if (__isset.display_name != rhs.__isset.display_name)
      return false;
    else if (__isset.display_name && !(display_name == rhs.display_name))
      return false;
    if (__isset.hostname != rhs.__isset.hostname)
      return false;
    else if (__isset.hostname && !(hostname == rhs.hostname))
      return false;
    if (__isset.host != rhs.__isset.host)
      return false;
    else if (__isset.host && !(host == rhs.host))
      return false;
    if (__isset.vm_project_id != rhs.__isset.vm_project_id)
      return false;
    else if (__isset.vm_project_id && !(vm_project_id == rhs.vm_project_id))
      return false;
    if (__isset.vlan_id != rhs.__isset.vlan_id)
      return false;
    else if (__isset.vlan_id && !(vlan_id == rhs.vlan_id))
      return false;
    if (__isset.port_type != rhs.__isset.port_type)
      return false;
    else if (__isset.port_type && !(port_type == rhs.port_type))
      return false;
    if (__isset.ip6_address != rhs.__isset.ip6_address)
      return false;
    else if (__isset.ip6_address && !(ip6_address == rhs.ip6_address))
      return false;
    if (__isset.isolated_vlan_id != rhs.__isset.isolated_vlan_id)
      return false;
    else if (__isset.isolated_vlan_id && !(isolated_vlan_id == rhs.isolated_vlan_id))
      return false;
    return true;
  }
  bool operator != (const Port &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const Port & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};


class Subnet {
 public:

  static const char* ascii_fingerprint; // = "3628A1EB414F66736E1B2A082E79475F";
  static const uint8_t binary_fingerprint[16]; // = {0x36,0x28,0xA1,0xEB,0x41,0x4F,0x66,0x73,0x6E,0x1B,0x2A,0x08,0x2E,0x79,0x47,0x5F};

  Subnet() : prefix(""), plen(0) {
  }

  virtual ~Subnet() throw() {}

  std::string prefix;
  int16_t plen;

  void __set_prefix(const std::string& val) {
    prefix = val;
  }

  void __set_plen(const int16_t val) {
    plen = val;
  }

  bool operator == (const Subnet & rhs) const
  {
    if (!(prefix == rhs.prefix))
      return false;
    if (!(plen == rhs.plen))
      return false;
    return true;
  }
  bool operator != (const Subnet &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const Subnet & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};

typedef struct _VirtualGatewayRequest__isset {
  _VirtualGatewayRequest__isset() : routes(false) {}
  bool routes;
} _VirtualGatewayRequest__isset;

class VirtualGatewayRequest {
 public:

  static const char* ascii_fingerprint; // = "540EF366750D8935EB8D899414FCF01F";
  static const uint8_t binary_fingerprint[16]; // = {0x54,0x0E,0xF3,0x66,0x75,0x0D,0x89,0x35,0xEB,0x8D,0x89,0x94,0x14,0xFC,0xF0,0x1F};

  VirtualGatewayRequest() : interface_name(""), routing_instance("") {
  }

  virtual ~VirtualGatewayRequest() throw() {}

  std::string interface_name;
  std::string routing_instance;
  SubnetList subnets;
  SubnetList routes;

  _VirtualGatewayRequest__isset __isset;

  void __set_interface_name(const std::string& val) {
    interface_name = val;
  }

  void __set_routing_instance(const std::string& val) {
    routing_instance = val;
  }

  void __set_subnets(const SubnetList& val) {
    subnets = val;
  }

  void __set_routes(const SubnetList& val) {
    routes = val;
    __isset.routes = true;
  }

  bool operator == (const VirtualGatewayRequest & rhs) const
  {
    if (!(interface_name == rhs.interface_name))
      return false;
    if (!(routing_instance == rhs.routing_instance))
      return false;
    if (!(subnets == rhs.subnets))
      return false;
    if (__isset.routes != rhs.__isset.routes)
      return false;
    else if (__isset.routes && !(routes == rhs.routes))
      return false;
    return true;
  }
  bool operator != (const VirtualGatewayRequest &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const VirtualGatewayRequest & ) const;

  uint32_t read(::apache::thrift::protocol::TProtocol* iprot);
  uint32_t write(::apache::thrift::protocol::TProtocol* oprot) const;

};



#endif
