/**
 * Autogenerated by Sandesh Compiler (1.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 * Copyright (c) 2013 Juniper Networks, Inc. All rights reserved.
 */
#ifndef task_TYPES_H
#define task_TYPES_H

#include <sandesh/Thrift.h>
#include <base/trace.h>
#include <sandesh/sandesh_constants.h>
#include <sandesh/protocol/TProtocol.h>
#include <sandesh/transport/TTransport.h>





typedef struct _SandeshTaskGroupSummary__isset {
  _SandeshTaskGroupSummary__isset() : task_id(false), run_count(false) {}
  bool task_id;
  bool run_count;
} _SandeshTaskGroupSummary__isset;

class SandeshTaskGroupSummary {
 public:

  static const char* ascii_fingerprint; // = "5AC1E34C30839E9C93964E95C47C4A61";
  static const uint8_t binary_fingerprint[16]; // = {0x5A,0xC1,0xE3,0x4C,0x30,0x83,0x9E,0x9C,0x93,0x96,0x4E,0x95,0xC4,0x7C,0x4A,0x61};

  SandeshTaskGroupSummary() : task_id(0), run_count(0) {
  }

  virtual ~SandeshTaskGroupSummary() throw() {}

  uint32_t task_id;
  uint32_t run_count;

  _SandeshTaskGroupSummary__isset __isset;

  void set_task_id(const uint32_t val) {
    task_id = val;
  }

  const uint32_t get_task_id() const {
    return task_id;
  }

  void set_run_count(const uint32_t val) {
    run_count = val;
  }

  const uint32_t get_run_count() const {
    return run_count;
  }

  bool operator == (const SandeshTaskGroupSummary & rhs) const
  {
    if (!(task_id == rhs.task_id))
      return false;
    if (!(run_count == rhs.run_count))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskGroupSummary &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const SandeshTaskGroupSummary & ) const;

  int32_t read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;
  std::string log() const;

};

typedef struct _SandeshTaskGroupNameSummary__isset {
  _SandeshTaskGroupNameSummary__isset() : name(false), task_id(false), run_count(false) {}
  bool name;
  bool task_id;
  bool run_count;
} _SandeshTaskGroupNameSummary__isset;

class SandeshTaskGroupNameSummary {
 public:

  static const char* ascii_fingerprint; // = "BA073F018B1872F0E3A92D8826251972";
  static const uint8_t binary_fingerprint[16]; // = {0xBA,0x07,0x3F,0x01,0x8B,0x18,0x72,0xF0,0xE3,0xA9,0x2D,0x88,0x26,0x25,0x19,0x72};

  SandeshTaskGroupNameSummary() : name(""), task_id(0), run_count(0) {
  }

  virtual ~SandeshTaskGroupNameSummary() throw() {}

  std::string name;
  uint32_t task_id;
  uint32_t run_count;

  _SandeshTaskGroupNameSummary__isset __isset;

  void set_name(const std::string& val) {
    name = val;
  }

  const std::string& get_name() const {
    return name;
  }

  void set_task_id(const uint32_t val) {
    task_id = val;
  }

  const uint32_t get_task_id() const {
    return task_id;
  }

  void set_run_count(const uint32_t val) {
    run_count = val;
  }

  const uint32_t get_run_count() const {
    return run_count;
  }

  bool operator == (const SandeshTaskGroupNameSummary & rhs) const
  {
    if (!(name == rhs.name))
      return false;
    if (!(task_id == rhs.task_id))
      return false;
    if (!(run_count == rhs.run_count))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskGroupNameSummary &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const SandeshTaskGroupNameSummary & ) const;

  int32_t read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;
  std::string log() const;

};

typedef struct _SandeshTaskEntrySummary__isset {
  _SandeshTaskEntrySummary__isset() : task_entry_key(false), waitq_size(false), run_count(false) {}
  bool task_entry_key;
  bool waitq_size;
  bool run_count;
} _SandeshTaskEntrySummary__isset;

class SandeshTaskEntrySummary {
 public:

  static const char* ascii_fingerprint; // = "BA073F018B1872F0E3A92D8826251972";
  static const uint8_t binary_fingerprint[16]; // = {0xBA,0x07,0x3F,0x01,0x8B,0x18,0x72,0xF0,0xE3,0xA9,0x2D,0x88,0x26,0x25,0x19,0x72};

  SandeshTaskEntrySummary() : task_entry_key(""), waitq_size(0), run_count(0) {
  }

  virtual ~SandeshTaskEntrySummary() throw() {}

  std::string task_entry_key;
  uint32_t waitq_size;
  uint32_t run_count;

  _SandeshTaskEntrySummary__isset __isset;

  void set_task_entry_key(const std::string& val) {
    task_entry_key = val;
  }

  const std::string& get_task_entry_key() const {
    return task_entry_key;
  }

  void set_waitq_size(const uint32_t val) {
    waitq_size = val;
  }

  const uint32_t get_waitq_size() const {
    return waitq_size;
  }

  void set_run_count(const uint32_t val) {
    run_count = val;
  }

  const uint32_t get_run_count() const {
    return run_count;
  }

  bool operator == (const SandeshTaskEntrySummary & rhs) const
  {
    if (!(task_entry_key == rhs.task_entry_key))
      return false;
    if (!(waitq_size == rhs.waitq_size))
      return false;
    if (!(run_count == rhs.run_count))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskEntrySummary &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const SandeshTaskEntrySummary & ) const;

  int32_t read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;
  std::string log() const;

};

typedef struct _SandeshTaskSummary__isset {
  _SandeshTaskSummary__isset() : task_key(false), state(false) {}
  bool task_key;
  bool state;
} _SandeshTaskSummary__isset;

class SandeshTaskSummary {
 public:

  static const char* ascii_fingerprint; // = "07A9615F837F7D0A952B595DD3020972";
  static const uint8_t binary_fingerprint[16]; // = {0x07,0xA9,0x61,0x5F,0x83,0x7F,0x7D,0x0A,0x95,0x2B,0x59,0x5D,0xD3,0x02,0x09,0x72};

  SandeshTaskSummary() : task_key(""), state("") {
  }

  virtual ~SandeshTaskSummary() throw() {}

  std::string task_key;
  std::string state;

  _SandeshTaskSummary__isset __isset;

  void set_task_key(const std::string& val) {
    task_key = val;
  }

  const std::string& get_task_key() const {
    return task_key;
  }

  void set_state(const std::string& val) {
    state = val;
  }

  const std::string& get_state() const {
    return state;
  }

  bool operator == (const SandeshTaskSummary & rhs) const
  {
    if (!(task_key == rhs.task_key))
      return false;
    if (!(state == rhs.state))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskSummary &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const SandeshTaskSummary & ) const;

  int32_t read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;
  std::string log() const;

};

typedef struct _SandeshTaskStats__isset {
  _SandeshTaskStats__isset() : wait_count(false), run_count(false), defer_count(false) {}
  bool wait_count;
  bool run_count;
  bool defer_count;
} _SandeshTaskStats__isset;

class SandeshTaskStats {
 public:

  static const char* ascii_fingerprint; // = "60A08F71BAFDDD885F6CE2EDD0BBEFD0";
  static const uint8_t binary_fingerprint[16]; // = {0x60,0xA0,0x8F,0x71,0xBA,0xFD,0xDD,0x88,0x5F,0x6C,0xE2,0xED,0xD0,0xBB,0xEF,0xD0};

  SandeshTaskStats() : wait_count(0), run_count(0), defer_count(0) {
  }

  virtual ~SandeshTaskStats() throw() {}

  uint32_t wait_count;
  uint32_t run_count;
  uint32_t defer_count;

  _SandeshTaskStats__isset __isset;

  void set_wait_count(const uint32_t val) {
    wait_count = val;
  }

  const uint32_t get_wait_count() const {
    return wait_count;
  }

  void set_run_count(const uint32_t val) {
    run_count = val;
  }

  const uint32_t get_run_count() const {
    return run_count;
  }

  void set_defer_count(const uint32_t val) {
    defer_count = val;
  }

  const uint32_t get_defer_count() const {
    return defer_count;
  }

  bool operator == (const SandeshTaskStats & rhs) const
  {
    if (!(wait_count == rhs.wait_count))
      return false;
    if (!(run_count == rhs.run_count))
      return false;
    if (!(defer_count == rhs.defer_count))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskStats &rhs) const {
    return !(*this == rhs);
  }

  bool operator < (const SandeshTaskStats & ) const;

  int32_t read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;
  std::string log() const;

};


class SandeshTaskSchedulerReq : public SandeshRequest {
 public:
  static const char* ascii_fingerprint; // = "99914B932BD37A50B983C5E7C90AE93B";
  static const uint8_t binary_fingerprint[16]; // = {0x99,0x91,0x4B,0x93,0x2B,0xD3,0x7A,0x50,0xB9,0x83,0xC5,0xE7,0xC9,0x0A,0xE9,0x3B};


  bool operator == (const SandeshTaskSchedulerReq & /* rhs */) const
  {
    return true;
  }
  bool operator != (const SandeshTaskSchedulerReq &rhs) const {
    return !(*this == rhs);
  }


  virtual ~SandeshTaskSchedulerReq() {}

  virtual std::string ModuleName() const { return "task"; }

  SandeshTaskSchedulerReq() : SandeshRequest("SandeshTaskSchedulerReq",lseqnum_++) {
    set_hints(0);
    set_context("");
  }
  SANDESH_REGISTER_DEC_TYPE(SandeshTaskSchedulerReq);
  virtual void HandleRequest() const;
  virtual bool RequestFromHttp(const std::string& ctx, const std::string& snh_query);
  static void Request(const std::string& context, SandeshConnection * sconn = NULL) {
    SandeshTaskSchedulerReq * snh = new SandeshTaskSchedulerReq(lseqnum_++, context, sconn);
    snh->Dispatch(sconn);
  }

  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  static int32_t lseqnum() { return lseqnum_;}
  std::string ToString() const;

private:

  explicit SandeshTaskSchedulerReq(uint32_t seqno, const std::string& context, SandeshConnection * sconn = NULL) : SandeshRequest("SandeshTaskSchedulerReq",seqno) {
    set_context(context);
    if (context == "ctrl") set_hints(g_sandesh_constants.SANDESH_CONTROL_HINT);
    else set_hints(0);
    (void)sconn;
  }
  static uint32_t lseqnum_;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _SandeshTaskSchedulerResp__isset {
  _SandeshTaskSchedulerResp__isset() : running(false), seqno(false), thread_count(false), task_group_list(false), more(false) {}
  bool running;
  bool seqno;
  bool thread_count;
  bool task_group_list;
  bool more;
} _SandeshTaskSchedulerResp__isset;

class SandeshTaskSchedulerResp : public SandeshResponse {
 public:
  static const char* ascii_fingerprint; // = "9BEF229D1EA404735434A79FB3805538";
  static const uint8_t binary_fingerprint[16]; // = {0x9B,0xEF,0x22,0x9D,0x1E,0xA4,0x04,0x73,0x54,0x34,0xA7,0x9F,0xB3,0x80,0x55,0x38};


  void set_running(const bool val) {
    running = val;
  }

  const bool get_running() const {
    return running;
  }

  void set_seqno(const uint32_t val) {
    seqno = val;
  }

  const uint32_t get_seqno() const {
    return seqno;
  }

  void set_thread_count(const uint32_t val) {
    thread_count = val;
  }

  const uint32_t get_thread_count() const {
    return thread_count;
  }

  void set_task_group_list(const std::vector<SandeshTaskGroupNameSummary> & val) {
    task_group_list = val;
  }

  const std::vector<SandeshTaskGroupNameSummary> & get_task_group_list() const {
    return task_group_list;
  }

  void set_more(const bool val) {
    more = val;
  }

  const bool get_more() const {
    return more;
  }

  bool operator == (const SandeshTaskSchedulerResp & rhs) const
  {
    if (!(running == rhs.running))
      return false;
    if (!(seqno == rhs.seqno))
      return false;
    if (!(thread_count == rhs.thread_count))
      return false;
    if (!(task_group_list == rhs.task_group_list))
      return false;
    if (!(more == rhs.more))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskSchedulerResp &rhs) const {
    return !(*this == rhs);
  }


  virtual ~SandeshTaskSchedulerResp() {}


  _SandeshTaskSchedulerResp__isset __isset;
  virtual std::string ModuleName() const { return "task"; }

  SandeshTaskSchedulerResp() : SandeshResponse("SandeshTaskSchedulerResp",lseqnum_++), running(0), seqno(0), thread_count(0), more(0) {
    set_hints(0);
  }
  void Response() { Dispatch(); }
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  static int32_t lseqnum() { return lseqnum_;}
  std::string ToString() const;

private:

  bool running;
  uint32_t seqno;
  uint32_t thread_count;
  std::vector<SandeshTaskGroupNameSummary>  task_group_list;
  bool more;
  static uint32_t lseqnum_;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _SandeshTaskGroupReq__isset {
  _SandeshTaskGroupReq__isset() : task_id(false) {}
  bool task_id;
} _SandeshTaskGroupReq__isset;

class SandeshTaskGroupReq : public SandeshRequest {
 public:
  static const char* ascii_fingerprint; // = "080867256049BADC652E96903226AFD5";
  static const uint8_t binary_fingerprint[16]; // = {0x08,0x08,0x67,0x25,0x60,0x49,0xBA,0xDC,0x65,0x2E,0x96,0x90,0x32,0x26,0xAF,0xD5};


  void set_task_id(const uint32_t val) {
    task_id = val;
  }

  const uint32_t get_task_id() const {
    return task_id;
  }

  bool operator == (const SandeshTaskGroupReq & rhs) const
  {
    if (!(task_id == rhs.task_id))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskGroupReq &rhs) const {
    return !(*this == rhs);
  }


  virtual ~SandeshTaskGroupReq() {}


  _SandeshTaskGroupReq__isset __isset;
  virtual std::string ModuleName() const { return "task"; }

  SandeshTaskGroupReq() : SandeshRequest("SandeshTaskGroupReq",lseqnum_++), task_id(0) {
    set_hints(0);
    set_context("");
  }
  SANDESH_REGISTER_DEC_TYPE(SandeshTaskGroupReq);
  virtual void HandleRequest() const;
  virtual bool RequestFromHttp(const std::string& ctx, const std::string& snh_query);
  static void Request(uint32_t task_id, const std::string& context, SandeshConnection * sconn = NULL) {
    SandeshTaskGroupReq * snh = new SandeshTaskGroupReq(lseqnum_++, task_id, context, sconn);
    snh->Dispatch(sconn);
  }

  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  static int32_t lseqnum() { return lseqnum_;}
  std::string ToString() const;

private:

  explicit SandeshTaskGroupReq(uint32_t seqno, uint32_t task_id, const std::string& context, SandeshConnection * sconn = NULL) : SandeshRequest("SandeshTaskGroupReq",seqno), task_id(task_id) {
    set_context(context);
    if (context == "ctrl") set_hints(g_sandesh_constants.SANDESH_CONTROL_HINT);
    else set_hints(0);
    (void)sconn;
  }
  uint32_t task_id;
  static uint32_t lseqnum_;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _SandeshTaskGroupResp__isset {
  _SandeshTaskGroupResp__isset() : task_id(false), run_count(false), task_with_no_instance(false), policy_list(false), defer_list(false), task_entry_list(false), summary_stats(false), more(false) {}
  bool task_id;
  bool run_count;
  bool task_with_no_instance;
  bool policy_list;
  bool defer_list;
  bool task_entry_list;
  bool summary_stats;
  bool more;
} _SandeshTaskGroupResp__isset;

class SandeshTaskGroupResp : public SandeshResponse {
 public:
  static const char* ascii_fingerprint; // = "F6C820D8FAA258426DB27ECE78645038";
  static const uint8_t binary_fingerprint[16]; // = {0xF6,0xC8,0x20,0xD8,0xFA,0xA2,0x58,0x42,0x6D,0xB2,0x7E,0xCE,0x78,0x64,0x50,0x38};


  void set_task_id(const uint32_t val) {
    task_id = val;
  }

  const uint32_t get_task_id() const {
    return task_id;
  }

  void set_run_count(const uint32_t val) {
    run_count = val;
  }

  const uint32_t get_run_count() const {
    return run_count;
  }

  void set_task_with_no_instance(const SandeshTaskEntrySummary& val) {
    task_with_no_instance = val;
  }

  const SandeshTaskEntrySummary& get_task_with_no_instance() const {
    return task_with_no_instance;
  }

  void set_policy_list(const std::vector<SandeshTaskGroupSummary> & val) {
    policy_list = val;
  }

  const std::vector<SandeshTaskGroupSummary> & get_policy_list() const {
    return policy_list;
  }

  void set_defer_list(const std::vector<SandeshTaskEntrySummary> & val) {
    defer_list = val;
  }

  const std::vector<SandeshTaskEntrySummary> & get_defer_list() const {
    return defer_list;
  }

  void set_task_entry_list(const std::vector<SandeshTaskEntrySummary> & val) {
    task_entry_list = val;
  }

  const std::vector<SandeshTaskEntrySummary> & get_task_entry_list() const {
    return task_entry_list;
  }

  void set_summary_stats(const SandeshTaskStats& val) {
    summary_stats = val;
  }

  const SandeshTaskStats& get_summary_stats() const {
    return summary_stats;
  }

  void set_more(const bool val) {
    more = val;
  }

  const bool get_more() const {
    return more;
  }

  bool operator == (const SandeshTaskGroupResp & rhs) const
  {
    if (!(task_id == rhs.task_id))
      return false;
    if (!(run_count == rhs.run_count))
      return false;
    if (!(task_with_no_instance == rhs.task_with_no_instance))
      return false;
    if (!(policy_list == rhs.policy_list))
      return false;
    if (!(defer_list == rhs.defer_list))
      return false;
    if (!(task_entry_list == rhs.task_entry_list))
      return false;
    if (!(summary_stats == rhs.summary_stats))
      return false;
    if (!(more == rhs.more))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskGroupResp &rhs) const {
    return !(*this == rhs);
  }


  virtual ~SandeshTaskGroupResp() {}


  _SandeshTaskGroupResp__isset __isset;
  virtual std::string ModuleName() const { return "task"; }

  SandeshTaskGroupResp() : SandeshResponse("SandeshTaskGroupResp",lseqnum_++), task_id(0), run_count(0), more(0) {
    set_hints(0);
  }
  void Response() { Dispatch(); }
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  static int32_t lseqnum() { return lseqnum_;}
  std::string ToString() const;

private:

  uint32_t task_id;
  uint32_t run_count;
  SandeshTaskEntrySummary task_with_no_instance;
  std::vector<SandeshTaskGroupSummary>  policy_list;
  std::vector<SandeshTaskEntrySummary>  defer_list;
  std::vector<SandeshTaskEntrySummary>  task_entry_list;
  SandeshTaskStats summary_stats;
  bool more;
  static uint32_t lseqnum_;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _SandeshTaskEntryReq__isset {
  _SandeshTaskEntryReq__isset() : key(false) {}
  bool key;
} _SandeshTaskEntryReq__isset;

class SandeshTaskEntryReq : public SandeshRequest {
 public:
  static const char* ascii_fingerprint; // = "EFB929595D312AC8F305D5A794CFEDA1";
  static const uint8_t binary_fingerprint[16]; // = {0xEF,0xB9,0x29,0x59,0x5D,0x31,0x2A,0xC8,0xF3,0x05,0xD5,0xA7,0x94,0xCF,0xED,0xA1};


  void set_key(const std::string& val) {
    key = val;
  }

  const std::string& get_key() const {
    return key;
  }

  bool operator == (const SandeshTaskEntryReq & rhs) const
  {
    if (!(key == rhs.key))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskEntryReq &rhs) const {
    return !(*this == rhs);
  }


  virtual ~SandeshTaskEntryReq() {}


  _SandeshTaskEntryReq__isset __isset;
  virtual std::string ModuleName() const { return "task"; }

  SandeshTaskEntryReq() : SandeshRequest("SandeshTaskEntryReq",lseqnum_++), key("") {
    set_hints(0);
    set_context("");
  }
  SANDESH_REGISTER_DEC_TYPE(SandeshTaskEntryReq);
  virtual void HandleRequest() const;
  virtual bool RequestFromHttp(const std::string& ctx, const std::string& snh_query);
  static void Request(std::string key, const std::string& context, SandeshConnection * sconn = NULL) {
    SandeshTaskEntryReq * snh = new SandeshTaskEntryReq(lseqnum_++, key, context, sconn);
    snh->Dispatch(sconn);
  }

  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  static int32_t lseqnum() { return lseqnum_;}
  std::string ToString() const;

private:

  explicit SandeshTaskEntryReq(uint32_t seqno, std::string key, const std::string& context, SandeshConnection * sconn = NULL) : SandeshRequest("SandeshTaskEntryReq",seqno), key(key) {
    set_context(context);
    if (context == "ctrl") set_hints(g_sandesh_constants.SANDESH_CONTROL_HINT);
    else set_hints(0);
    (void)sconn;
  }
  std::string key;
  static uint32_t lseqnum_;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _SandeshTaskEntryResp__isset {
  _SandeshTaskEntryResp__isset() : task_id(false), instance_id(false), run_count(false), policy_list(false), run_task(false), wait_queue(false), defer_list(false), summary_stats(false), wait_task_entry(false), wait_task_group(false), more(false) {}
  bool task_id;
  bool instance_id;
  bool run_count;
  bool policy_list;
  bool run_task;
  bool wait_queue;
  bool defer_list;
  bool summary_stats;
  bool wait_task_entry;
  bool wait_task_group;
  bool more;
} _SandeshTaskEntryResp__isset;

class SandeshTaskEntryResp : public SandeshResponse {
 public:
  static const char* ascii_fingerprint; // = "81241E8E2B75CC6F4D1F0624E2FBBAFB";
  static const uint8_t binary_fingerprint[16]; // = {0x81,0x24,0x1E,0x8E,0x2B,0x75,0xCC,0x6F,0x4D,0x1F,0x06,0x24,0xE2,0xFB,0xBA,0xFB};


  void set_task_id(const uint32_t val) {
    task_id = val;
  }

  const uint32_t get_task_id() const {
    return task_id;
  }

  void set_instance_id(const uint32_t val) {
    instance_id = val;
  }

  const uint32_t get_instance_id() const {
    return instance_id;
  }

  void set_run_count(const uint32_t val) {
    run_count = val;
  }

  const uint32_t get_run_count() const {
    return run_count;
  }

  void set_policy_list(const std::vector<SandeshTaskEntrySummary> & val) {
    policy_list = val;
  }

  const std::vector<SandeshTaskEntrySummary> & get_policy_list() const {
    return policy_list;
  }

  void set_run_task(const SandeshTaskSummary& val) {
    run_task = val;
  }

  const SandeshTaskSummary& get_run_task() const {
    return run_task;
  }

  void set_wait_queue(const std::vector<SandeshTaskSummary> & val) {
    wait_queue = val;
  }

  const std::vector<SandeshTaskSummary> & get_wait_queue() const {
    return wait_queue;
  }

  void set_defer_list(const std::vector<SandeshTaskEntrySummary> & val) {
    defer_list = val;
  }

  const std::vector<SandeshTaskEntrySummary> & get_defer_list() const {
    return defer_list;
  }

  void set_summary_stats(const SandeshTaskStats& val) {
    summary_stats = val;
  }

  const SandeshTaskStats& get_summary_stats() const {
    return summary_stats;
  }

  void set_wait_task_entry(const SandeshTaskEntrySummary& val) {
    wait_task_entry = val;
  }

  const SandeshTaskEntrySummary& get_wait_task_entry() const {
    return wait_task_entry;
  }

  void set_wait_task_group(const SandeshTaskGroupSummary& val) {
    wait_task_group = val;
  }

  const SandeshTaskGroupSummary& get_wait_task_group() const {
    return wait_task_group;
  }

  void set_more(const bool val) {
    more = val;
  }

  const bool get_more() const {
    return more;
  }

  bool operator == (const SandeshTaskEntryResp & rhs) const
  {
    if (!(task_id == rhs.task_id))
      return false;
    if (!(instance_id == rhs.instance_id))
      return false;
    if (!(run_count == rhs.run_count))
      return false;
    if (!(policy_list == rhs.policy_list))
      return false;
    if (!(run_task == rhs.run_task))
      return false;
    if (!(wait_queue == rhs.wait_queue))
      return false;
    if (!(defer_list == rhs.defer_list))
      return false;
    if (!(summary_stats == rhs.summary_stats))
      return false;
    if (!(wait_task_entry == rhs.wait_task_entry))
      return false;
    if (!(wait_task_group == rhs.wait_task_group))
      return false;
    if (!(more == rhs.more))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskEntryResp &rhs) const {
    return !(*this == rhs);
  }


  virtual ~SandeshTaskEntryResp() {}


  _SandeshTaskEntryResp__isset __isset;
  virtual std::string ModuleName() const { return "task"; }

  SandeshTaskEntryResp() : SandeshResponse("SandeshTaskEntryResp",lseqnum_++), task_id(0), instance_id(0), run_count(0), more(0) {
    set_hints(0);
  }
  void Response() { Dispatch(); }
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  static int32_t lseqnum() { return lseqnum_;}
  std::string ToString() const;

private:

  uint32_t task_id;
  uint32_t instance_id;
  uint32_t run_count;
  std::vector<SandeshTaskEntrySummary>  policy_list;
  SandeshTaskSummary run_task;
  std::vector<SandeshTaskSummary>  wait_queue;
  std::vector<SandeshTaskEntrySummary>  defer_list;
  SandeshTaskStats summary_stats;
  SandeshTaskEntrySummary wait_task_entry;
  SandeshTaskGroupSummary wait_task_group;
  bool more;
  static uint32_t lseqnum_;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _SandeshTaskReq__isset {
  _SandeshTaskReq__isset() : key(false) {}
  bool key;
} _SandeshTaskReq__isset;

class SandeshTaskReq : public SandeshRequest {
 public:
  static const char* ascii_fingerprint; // = "EFB929595D312AC8F305D5A794CFEDA1";
  static const uint8_t binary_fingerprint[16]; // = {0xEF,0xB9,0x29,0x59,0x5D,0x31,0x2A,0xC8,0xF3,0x05,0xD5,0xA7,0x94,0xCF,0xED,0xA1};


  void set_key(const std::string& val) {
    key = val;
  }

  const std::string& get_key() const {
    return key;
  }

  bool operator == (const SandeshTaskReq & rhs) const
  {
    if (!(key == rhs.key))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskReq &rhs) const {
    return !(*this == rhs);
  }


  virtual ~SandeshTaskReq() {}


  _SandeshTaskReq__isset __isset;
  virtual std::string ModuleName() const { return "task"; }

  SandeshTaskReq() : SandeshRequest("SandeshTaskReq",lseqnum_++), key("") {
    set_hints(0);
    set_context("");
  }
  SANDESH_REGISTER_DEC_TYPE(SandeshTaskReq);
  virtual void HandleRequest() const;
  virtual bool RequestFromHttp(const std::string& ctx, const std::string& snh_query);
  static void Request(std::string key, const std::string& context, SandeshConnection * sconn = NULL) {
    SandeshTaskReq * snh = new SandeshTaskReq(lseqnum_++, key, context, sconn);
    snh->Dispatch(sconn);
  }

  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  static int32_t lseqnum() { return lseqnum_;}
  std::string ToString() const;

private:

  explicit SandeshTaskReq(uint32_t seqno, std::string key, const std::string& context, SandeshConnection * sconn = NULL) : SandeshRequest("SandeshTaskReq",seqno), key(key) {
    set_context(context);
    if (context == "ctrl") set_hints(g_sandesh_constants.SANDESH_CONTROL_HINT);
    else set_hints(0);
    (void)sconn;
  }
  std::string key;
  static uint32_t lseqnum_;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};

typedef struct _SandeshTaskResp__isset {
  _SandeshTaskResp__isset() : task_id(false), instance_id(false), seqno(false), state(false), recycle(false), cancel(false), task_spawned(false), more(false) {}
  bool task_id;
  bool instance_id;
  bool seqno;
  bool state;
  bool recycle;
  bool cancel;
  bool task_spawned;
  bool more;
} _SandeshTaskResp__isset;

class SandeshTaskResp : public SandeshResponse {
 public:
  static const char* ascii_fingerprint; // = "D3C7409D55F672F1E556A8658267F4D9";
  static const uint8_t binary_fingerprint[16]; // = {0xD3,0xC7,0x40,0x9D,0x55,0xF6,0x72,0xF1,0xE5,0x56,0xA8,0x65,0x82,0x67,0xF4,0xD9};


  void set_task_id(const uint32_t val) {
    task_id = val;
  }

  const uint32_t get_task_id() const {
    return task_id;
  }

  void set_instance_id(const uint32_t val) {
    instance_id = val;
  }

  const uint32_t get_instance_id() const {
    return instance_id;
  }

  void set_seqno(const uint32_t val) {
    seqno = val;
  }

  const uint32_t get_seqno() const {
    return seqno;
  }

  void set_state(const std::string& val) {
    state = val;
  }

  const std::string& get_state() const {
    return state;
  }

  void set_recycle(const bool val) {
    recycle = val;
  }

  const bool get_recycle() const {
    return recycle;
  }

  void set_cancel(const bool val) {
    cancel = val;
  }

  const bool get_cancel() const {
    return cancel;
  }

  void set_task_spawned(const bool val) {
    task_spawned = val;
  }

  const bool get_task_spawned() const {
    return task_spawned;
  }

  void set_more(const bool val) {
    more = val;
  }

  const bool get_more() const {
    return more;
  }

  bool operator == (const SandeshTaskResp & rhs) const
  {
    if (!(task_id == rhs.task_id))
      return false;
    if (!(instance_id == rhs.instance_id))
      return false;
    if (!(seqno == rhs.seqno))
      return false;
    if (!(state == rhs.state))
      return false;
    if (!(recycle == rhs.recycle))
      return false;
    if (!(cancel == rhs.cancel))
      return false;
    if (!(task_spawned == rhs.task_spawned))
      return false;
    if (!(more == rhs.more))
      return false;
    return true;
  }
  bool operator != (const SandeshTaskResp &rhs) const {
    return !(*this == rhs);
  }


  virtual ~SandeshTaskResp() {}


  _SandeshTaskResp__isset __isset;
  virtual std::string ModuleName() const { return "task"; }

  SandeshTaskResp() : SandeshResponse("SandeshTaskResp",lseqnum_++), task_id(0), instance_id(0), seqno(0), state(""), recycle(0), cancel(0), task_spawned(0), more(0) {
    set_hints(0);
  }
  void Response() { Dispatch(); }
  virtual const int32_t versionsig() const { return versionsig_;}
  static const int32_t sversionsig() { return versionsig_;}
  static int32_t lseqnum() { return lseqnum_;}
  std::string ToString() const;

private:

  uint32_t task_id;
  uint32_t instance_id;
  uint32_t seqno;
  std::string state;
  bool recycle;
  bool cancel;
  bool task_spawned;
  bool more;
  static uint32_t lseqnum_;
  static uint32_t versionsig_;
  static const char *name_;
  void Log() const;
  void ForcedLog() const;
  int32_t Read(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> iprot);
  int32_t Write(boost::shared_ptr<contrail::sandesh::protocol::TProtocol> oprot) const;

};



#endif
