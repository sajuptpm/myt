

#https://docs.datastax.com/en/cassandra/2.0/cassandra/operations/ops_monitoring_c.html
#http://stackoverflow.com/questions/2359159/cassandra-port-usage-how-are-the-ports-used
#https://docs.oracle.com/cd/E19159-01/819-7758/gcitp/index.html

# $ sudo apt-get install python-jpype
# $ locate libjvm.so
# $ jconsole 127.0.0.1:7199

import jpype
from jpype import java
from jpype import javax

CASSA_HOST="127.0.0.1"
CASSA_JMX_MONIT_PORT=7199
CASSA_THRIFT_API_PORT=9160
CASSA_USER=""
CASSA_PASSWORD=""

#JAVAJVM_LOC="/usr/lib/jvm/default-java/jre/lib/amd64/server/libjvm.so"
JAVAJVM_LOC="/usr/lib/jvm/java-8-oracle/jre/lib/amd64/server/libjvm.so"


def get_jmx_conn(host, port, user, passwd):
    URL = "service:jmx:rmi:///jndi/rmi://%s:%d/jmxrmi" % (host, port)
    jpype.startJVM(JAVAJVM_LOC)
    jhash = java.util.HashMap()
    jarray = jpype.JArray(java.lang.String)([user, passwd])
    jhash.put (javax.management.remote.JMXConnector.CREDENTIALS, jarray);
    jmxurl = javax.management.remote.JMXServiceURL(URL)
    jmxsoc = javax.management.remote.JMXConnectorFactory.connect(jmxurl, jhash)
    conn = jmxsoc.getMBeanServerConnection();
    return conn


conn = get_jmx_conn(CASSA_HOST, CASSA_JMX_MONIT_PORT, CASSA_USER, CASSA_PASSWORD)
print conn

def get_thread_count():
    object_name="java.lang:type=Threading"
    obj=javax.management.ObjectName(object_name)
    attribute="ThreadCount"
    return conn.getAttribute(obj, attribute)


def get_messaging_service_info():
    object_name="org.apache.cassandra.net:type=MessagingService"
    obj=javax.management.ObjectName(object_name)
    attributes=["RecentTimeoutsPerHost", "CommandDroppedTasks", 
                "CommandPendingTasks", "DroppedMessages", "TimeoutsPerHost"]
    for attribute in attributes:
        print attribute + ": ", conn.getAttribute(obj, attribute)


def get_cache_info():
    object_name="org.apache.cassandra.db:type=Caches"
    obj=javax.management.ObjectName(object_name)
    attributes=["KeyCacheCapacityInBytes", "KeyCacheCapacityInMB", 
                "KeyCacheEntries", "KeyCacheHits", "KeyCacheKeysToSave", 
                "KeyCacheRequests", "KeyCacheSize"]
    for attribute in attributes:
        print attribute + ": ", conn.getAttribute(obj, attribute)


def get_os_info():
    object_name="java.lang:type=OperatingSystem"
    obj=javax.management.ObjectName(object_name)
    attributes=["ProcessCpuLoad"]
    for attribute in attributes:
        print attribute + ": ", conn.getAttribute(obj, attribute)

def find_obj_fq_name_table_key_count():
    """
        Invoking an Operation without Parameters
        https://docs.oracle.com/cd/E19159-01/819-7758/gcitp/index.html

        Doc of MBeanServerConnection, invoke, getAttribute, etc---
        https://docs.oracle.com/javase/7/docs/api/javax/management/MBeanServerConnection.html

        How this Operation call working:
        "estimateKeys" method in ColumnFamilyStoreMBean.java
        https://github.com/apache/cassandra/blob/4a0d1caa262af3b6f2b6d329e45766b4df845a88/src/java/org/apache/cassandra/db/ColumnFamilyStoreMBean.java#L110
        "estimateKeys" method in ColumnFamilyStore.java
        https://github.com/apache/cassandra/blob/6a4d106287adaf64280dce96632066b6c8d7d194/src/java/org/apache/cassandra/db/ColumnFamilyStore.java#L2264

    """
    object_name="org.apache.cassandra.db:type=ColumnFamilies,keyspace=config_db_uuid,columnfamily=obj_fq_name_table"
    obj=javax.management.ObjectName(object_name)
    operation="estimateKeys"
    params = None
    signature = None
    print "obj_fq_name_table key count: ", conn.invoke(obj, operation, params, signature)

def find_obj_uuid_table_key_count():
    object_name="org.apache.cassandra.db:type=ColumnFamilies,keyspace=config_db_uuid,columnfamily=obj_uuid_table"
    obj=javax.management.ObjectName(object_name)
    operation="estimateKeys"
    params = None
    signature = None
    print "obj_uuid_table key count: ", conn.invoke(obj, operation, params, signature)


def take_snapshot():
    """
        Invoking an Operation with Parameters

        #https://github.com/apache/cassandra/blob/ee9e38b7dc20a3b98e5a68904be35d727ed9c09c/src/java/org/apache/cassandra/service/StorageServiceMBean.java#L224
        #https://github.com/apache/cassandra/blob/001bdd306043f951f27369f9887bda6935026900/src/java/org/apache/cassandra/service/StorageService.java#L2810

    """
    object_name = "org.apache.cassandra.db:type=StorageService"
    operation = "takeSnapshot"
    dbtable = jpype.JArray(java.lang.String)([])
    params = jpype.JArray(java.lang.Object)([bkpname, dbtable])
    signature = jpype.JArray(java.lang.String)(['java.lang.String', '[Ljava.lang.String;'])
    #conn.invoke(obj, operation, params, signature)



print "Current thread count: ", get_thread_count()
get_messaging_service_info()
get_cache_info()
get_os_info()
find_obj_fq_name_table_key_count()
find_obj_uuid_table_key_count()







