import sys, getopt
from pprint import pprint
from pycassa.pool import ConnectionPool
from pycassa.columnfamily import ColumnFamily

HOST_AND_PORT = ['127.0.0.1:9160']
KEYSPACE="config_db_uuid"
UUID_COLUMN_FAMILY_NAME="obj_uuid_table"
FQNAME_COLUMN_FAMILY_NAME="obj_fq_name_table"
DOMAIN_NAME="default-domain"
PROJECT_NAME="openstack"
NETWORK_NAME="sajupublic1"

try:
        PROJECT_NAME=sys.argv[1]
except IndexError as ex:
        pass
try:
        NETWORK_NAME=sys.argv[2]
except IndexError as ex:
        pass

pool = ConnectionPool(KEYSPACE, HOST_AND_PORT)
uuid_col_fam_conn = ColumnFamily(pool, UUID_COLUMN_FAMILY_NAME)
fqname_col_fam_conn = ColumnFamily(pool, FQNAME_COLUMN_FAMILY_NAME)

#row_keys = dict(col_fam_conn.get_range()).keys()
#print row_keys

row_key="virtual_network"
fqname="{DOMAIN_NAME}:{PROJECT_NAME}:{NETWORK_NAME}".format(DOMAIN_NAME=DOMAIN_NAME, 
				PROJECT_NAME=PROJECT_NAME, NETWORK_NAME=NETWORK_NAME)
col_start = '{fqname}:'.format(fqname=fqname)
col_fin='{fqname};'.format(fqname=fqname)
## ":" and ";" at the end is required.
data = fqname_col_fam_conn.xget(row_key, column_start=col_start, column_finish=col_fin)
data=dict(data)
#print data
data_list=zip(data)
uuid = data_list[0][0].split(":")[-1]
#print uuid


row_key=uuid
data = uuid_col_fam_conn.get(row_key)
data_dict = dict(data)
pprint(data_dict)






