import os
import time
import uuid
from vnc_api_client import get_vnc_conn##SM
from vnc_api import vnc_api
from pprint import pprint

resources = ['AccessControlList', 'BgpRouter', 'ConfigRoot', 'CustomerAttachment', 'Domain', 'FloatingIp', 'FloatingIpPool', 'GlobalSystemConfig', 'GlobalVrouterConfig', 'InstanceIp', 'InterfaceRouteTable', 'LoadbalancerHealthmonitor', 'LoadbalancerMember', 'LoadbalancerPool', 'LogicalInterface', 'LogicalRouter', 'Namespace', 'NetworkIpam', 'NetworkPolicy', 'PhysicalInterface', 'PhysicalRouter', 'Project', 'ProviderAttachment', 'RouteTable', 'RouteTarget', 'RoutingInstance', 'SecurityGroup', 'ServiceInstance', 'ServiceTemplate', 'Subnet', 'VirtualDns', 'VirtualDnsRecord', 'VirtualIp', 'VirtualMachine', 'VirtualMachineInterface', 'VirtualNetwork', 'VirtualRouter']

conn = get_vnc_conn()
lookup = vars(conn)['_type_to_class']
d = {}
for k, v in lookup.items():
	d[k] = v.create_uri

print pprint(d)


"""

{'access_control_list': u'/access-control-lists',
 'bgp_router': u'/bgp-routers',
 'config_root': '',
 'customer_attachment': u'/customer-attachments',
 'domain': u'/domains',
 'floating_ip': u'/floating-ips',
 'floating_ip_pool': u'/floating-ip-pools',
 'global_system_config': u'/global-system-configs',
 'global_vrouter_config': u'/global-vrouter-configs',
 'instance_ip': u'/instance-ips',
 'interface_route_table': u'/interface-route-tables',
 'loadbalancer_healthmonitor': u'/loadbalancer-healthmonitors',
 'loadbalancer_member': u'/loadbalancer-members',
 'loadbalancer_pool': u'/loadbalancer-pools',
 'logical_interface': u'/logical-interfaces',
 'logical_router': u'/logical-routers',
 'namespace': u'/namespaces',
 'network_ipam': u'/network-ipams',
 'network_policy': u'/network-policys',
 'physical_interface': u'/physical-interfaces',
 'physical_router': u'/physical-routers',
 'project': u'/projects',
 'provider_attachment': u'/provider-attachments',
 'route_table': u'/route-tables',
 'route_target': u'/route-targets',
 'routing_instance': u'/routing-instances',
 'security_group': u'/security-groups',
 'service_instance': u'/service-instances',
 'service_template': u'/service-templates',
 'subnet': u'/subnets',
 'virtual_DNS': u'/virtual-DNSs',
 'virtual_DNS_record': u'/virtual-DNS-records',
 'virtual_ip': u'/virtual-ips',
 'virtual_machine': u'/virtual-machines',
 'virtual_machine_interface': u'/virtual-machine-interfaces',
 'virtual_network': u'/virtual-networks',
 'virtual_router': u'/virtual-routers'}


"""


