
"""
export OS_USERNAME=admin
export OS_PASSWORD=secret123
export OS_TENANT_NAME=admin
export OS_AUTH_URL=http://127.0.0.1:35357/v2.0
export API_SERVER_IP=127.0.0.1

"""

import os
import time
import uuid
import requests
from vnc_api import vnc_api


def get_vnc_conn():

        auth_url= os.environ['OS_AUTH_URL']
        username= os.environ['OS_USERNAME']
        password= os.environ['OS_PASSWORD']
        tenant_name= os.environ['OS_TENANT_NAME']
        auth_protocol = auth_url.split(':')[0]
        auth_host = auth_url.split(':')[1].split("/")[2]
        auth_port = auth_url.split(':')[2].split("/")[0]

        #Contrail API Server IP and Port
        #api_server_ip = "10.140.218.12"
        api_server_ip = os.environ['API_SERVER_IP']
        api_server_port = 8082

        # Retry till a api-server is up
        while True:
                try:
                        vnc_conn = vnc_api.VncApi(username, password, tenant_name,
                                                api_server_ip, api_server_port,
                                                auth_host=auth_host, auth_port=auth_port, auth_protocol=auth_protocol)
                        return vnc_conn
                except requests.exceptions.RequestException as e:
                        time.sleep(3)


def add_interface_route_table_to_port(vnc_conn, port_id, route_prefix_list):
	"""
		Tested and working
	"""
	interface_rt_name = "interface_rt_"+port_id
	port_obj = vnc_conn.virtual_machine_interface_read(id=port_id)
	project_obj =  vnc_conn.project_read(id=port_obj.parent_uuid)

    	#Create RouteTableType object and set route as empty.
    	route_table = vnc_api.RouteTableType(interface_rt_name)
    	route_table.set_route([])

    	#Create InterfaceRouteTable object and set interface_route_table_routes
    	intf_route_table = vnc_api.InterfaceRouteTable(
			interface_route_table_routes=route_table,
                        parent_obj=project_obj,
                        name=interface_rt_name)

    	#Create InterfaceRouteTable by making vnc api call
    	intf_route_table_id = vnc_conn.interface_route_table_create(
                            	intf_route_table)
    	intf_route_table_obj = vnc_conn.interface_route_table_read(
                            	id=intf_route_table_id)

    	#Get routes from InterfaceRouteTable object.
    	rt_routes = intf_route_table_obj.get_interface_route_table_routes()
    	routes = rt_routes.get_route()
    	# delete any old routes
    	routes = []

    	#Set routes to InterfaceRouteTable
    	for prefix in route_prefix_list:
        	routes.append(vnc_api.RouteType(prefix=prefix))
    	rt_routes.set_route(routes)
    	intf_route_table_obj.set_interface_route_table_routes(rt_routes)
    	vnc_conn.interface_route_table_update(intf_route_table_obj)
    
    	#Add InterfaceRouteTable to port
    	port_obj.add_interface_route_table(intf_route_table_obj)
    	vnc_conn.virtual_machine_interface_update(port_obj)

def delete_interface_route_table(vnc_conn, port_id):
        """
                Tested and working
        """
	port_obj = vnc_conn.virtual_machine_interface_read(id=port_id)
	intf_rt_ref = port_obj.get_interface_route_table_refs()
	intf_rt_fq_name = intf_rt_ref[0]['to']
	intf_rt_obj = vnc_conn.interface_route_table_read(intf_rt_fq_name)
	port_obj.del_interface_route_table(intf_rt_obj)
	vnc_conn.virtual_machine_interface_update(port_obj)

PORT_ID = "cbdaa463-0590-4246-8f90-483b68c62367"
vnc_conn = get_vnc_conn()
route_prefix_list = ['12.0.0.0/24']
#Route1 = destination-12.0.0.0/24, next_hop=10.0.0.5
#Here '10.0.0.5' is the IP of the port and '12.0.0.0/24' is the cidr
#for whcih we need to redirect traffic to 10.0.0.5.

add_interface_route_table_to_port(vnc_conn, PORT_ID, route_prefix_list)

#delete_interface_route_table(vnc_conn, PORT_ID)


"""
Use "#neutron port-list" command to find port id, there you can see the IP atatched to the port also.

Once we add the interface route table, check "Interface Route table" in controller introspect,
there we should see new entry.

If that port is already attached to a VM, then we can find the added route in
VRF of the network where VM is provisioned.

if port is not attached to a VM then we could not able to see added route in VRF
of any networks.

We an also take a port which already attached to a VM for this experiment.

Interface Route table
http://ip-of-control-node:8083/Snh_IFMapTableShowReq?x=interface-route-table

VRF List
http://ip-of-host:8085/Snh_VrfListReq?name=


After adding this "interface route table" with "route prefix" "12.0.0.0/24" to port "10.0.0.5", try
to ping to "12.0.0.8" from a different VM in the same network and run "tcpdump" in interface attached
to the VM:10.0.0.5 (#sudo tcpdump -i tapcbdaa463-05), we can see the ICMP packets there, that means traffic destinct to 12.0.0.8 is 
redirected to port/VM:10.0.0.5.

If you have another network and is conneted to the network where the port:10.0.0.5 is, via a router, then you could able to
redirect trafic destict to "12.0.0.0/24" from vm in that second network to port:10.0.0.5 in the first network. This is because 
route entry goes to VRF of both the networks. You can check it via vrouter agent introspect http://ip-of-host:8085/Snh_VrfListReq?name=


"""


