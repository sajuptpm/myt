
#http://zookeeper.apache.org/doc/r3.3.1/zookeeperAdmin.html

import telnetlib

HOST="127.0.0.1"
PORT=2181

#tconn = telnetlib.Telnet(HOST, PORT)
commands=["srvr", "cons", "ruok", "conf", "stat", "wchs", "wchc", "mntr"]

#srvr : general information about the server
#cons : general information about the connection
#ruok : The server will respond with imok if it is running

for cmd in commands:
    tconn = telnetlib.Telnet(HOST, PORT)
    tconn.write(cmd)
    print cmd + ": \n", tconn.read_all(), "\n"
    tconn.close()


