import kazoo.client
import kazoo.handlers.gevent
from pprint import pprint

SERVER_LIST = "10.140.15.81:2181,10.10.140.15.80:2181,10.140.15.78:2181"
zk_con = kazoo.client.KazooClient(
                SERVER_LIST,
                timeout=400,
                handler=kazoo.handlers.gevent.SequentialGeventHandler())
zk_con.start()

res={}
for x in zk_con.get_children("/fq-name-to-uuid"):
	resource = x.split(':')[0]
	if res.has_key(resource):
		res[resource] += 1
	else:
		res[resource] = 1
pprint(res)

