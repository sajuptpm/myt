import kazoo.client
import kazoo.handlers.gevent
import netaddr
from pprint import pprint

SERVER_LIST = "10.140.15.81:2181,10.10.140.15.80:2181,10.140.15.78:2181"
DOMAIN="default-domain"
PROJECT="openstack"
NETWORK="sajupublic1"
SUBNET_CIDR="102.0.0.0/20"

zk_con = kazoo.client.KazooClient(
                SERVER_LIST,
                timeout=400,
                handler=kazoo.handlers.gevent.SequentialGeventHandler())
zk_con.start()

def get_data(node_list, base_path="/"):
        ips_uuid={}
        for n in node_list:
                uuid=None
                path = base_path + n
                data = zk_con.get(path)
                if data:
                	uuid = data[0]
                ips_uuid[str(netaddr.IPAddress(n))]=uuid
        print pprint(ips_uuid)

base_path="/api-server/subnets/{DOMAIN}:{PROJECT}:{NETWORK}:{SUBNET_CIDR}/".format(DOMAIN=DOMAIN, 
					PROJECT=PROJECT, NETWORK=NETWORK, SUBNET_CIDR=SUBNET_CIDR)
get_data(zk_con.get_children(base_path), base_path=base_path)


