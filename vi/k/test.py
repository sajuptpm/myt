import kazoo.client
import kazoo.handlers.gevent

SERVER_LIST = "10.140.214.51:2181,10.140.214.52:2181,10.140.214.55:2181"

zk_con = kazoo.client.KazooClient(
                SERVER_LIST,
                timeout=400,
                handler=kazoo.handlers.gevent.SequentialGeventHandler())
zk_con.start()


#printst_children(find_fq_name_to_uuid("float"), base_path="/fq-name-to-uuid/") zk_con.get_children("/api-server/subnets/default-domain:services:public:10.140.213.0/24")

#print zk_con.get_children("/fq-name-to-uuid")

#print zk_con.get_children("/id")


def find_fq_name_to_uuid(match):
    return [x for x in zk_con.get_children("/fq-name-to-uuid") if match in x]

def list_children(node_list, base_path="/"):
        for n in node_list:
                path = base_path + n
                print path
                print zk_con.get_children(path)

def get_data(node_list, base_path="/"):
        for n in node_list:
                path = base_path + n
                print path
                print zk_con.get(path)


#print list_children(find_fq_name_to_uuid("virtual_network"), base_path="/fq-name-to-uuid/")

print get_data(find_fq_name_to_uuid("float"), base_path="/fq-name-to-uuid/")


