#!/bin/bash
#nova hypervisor-list

hypervisor_hostname="cp2-test-build-contrail-r1-1-upg.node.consul"

vmids=$(nova hypervisor-servers $hypervisor_hostname | awk '{ if($2!="ID" && $2!="") print $2}')
vmids_array=($vmids)
echo "Number of Instances: " ${#vmids_array[@]}
#echo $vmids

for x in $vmids;
do
    echo "Stopping Server: " $x
    nova stop $x
done

