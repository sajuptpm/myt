#python-keystoneclient>=1.0.0
#python-novaclient>=2.18.0

import os
import novaclient
from novaclient.v2 import client as nova_client
import keystoneclient
from keystoneclient.v2_0 import client
from six.moves import _thread as thread

auth_url= os.environ['OS_AUTH_URL']
username= os.environ['OS_USERNAME']
password= os.environ['OS_PASSWORD']
tenant_name= os.environ['OS_TENANT_NAME']
project_id = "c216189f65a64175b12bd99b22a60336"

def get_keystoneclient():
    remote_addr = None
    conn = client.Client(username=username,
                        password=password,
                        tenant_id=project_id,
                        auth_url=auth_url,
                        original_ip=None,
                        insecure=False,
                        cacert=None,
                        debug=False)
    return conn

def get_token():
    conn = get_keystoneclient()
    try:
        return conn.auth_ref['token']['id']
    except Exception as ex:
        raise Exception('token not found')


def _url_for(service_type):
    catalog = get_keystoneclient().service_catalog.catalog
    service_catalog = catalog['serviceCatalog']
    for service in service_catalog:
        if service['type'] != service_type:
            continue
        for endpoint in service['endpoints']:
            if 'publicURL' in endpoint:
                return endpoint['publicURL']
        else:
            return None
    return None


def get_novaclient():
    #We need to set token, username, project_id, and catalog 
    #in user request object
    token = get_token()
    endpoint = _url_for("compute")
    if not endpoint:
        raise Exception("Endpoint is None")
    conn = nova_client.Client(username=username,
                            #we can set password and token inapi_key
                            api_key=token,
                            project_id=project_id,
                            auth_url=endpoint,
                            auth_token=token,
                            bypass_url=endpoint,
                            insecure=False,
                            cacert=None,
                            http_log_debug=False)
    return conn


def nova_list1():
    for x in range(1, 100):
        res = get_novaclient().servers.list()
        print len(res)

def get_all_vms():
    for x in range(1, 5):
        thread.start_new_thread(nova_list1, ())

#get_all_vms()
#nova_list1()




