#python-keystoneclient>=1.0.0
#python-novaclient>=2.18.0
#neutronclient apis APIParamsCall, https://github.com/openstack/python-neutronclient/blob/master/neutronclient/v2_0/client.py

import os
import neutronclient
from neutronclient.neutron import client as neutron_client
import keystoneclient
from keystoneclient.v2_0 import client

import thread
#from six.moves import _thread as thread

auth_url= os.environ['OS_AUTH_URL']
username= os.environ['OS_USERNAME']
password= os.environ['OS_PASSWORD']
tenant_name= os.environ['OS_TENANT_NAME']
project_id = "fab195f83e314f57bef958bb04337dd1"

def get_keystoneclient():
    remote_addr = None
    conn = client.Client(username=username,
                        password=password,
                        tenant_id=project_id,
                        auth_url=auth_url,
                        original_ip=None,
                        insecure=False,
                        cacert=None,
                        debug=False)
    return conn

def get_token():
    conn = get_keystoneclient()
    try:
        return conn.auth_ref['token']['id']
    except Exception as ex:
        raise Exception('token not found')


def _url_for(service_type):
    catalog = get_keystoneclient().service_catalog.catalog
    service_catalog = catalog['serviceCatalog']
    for service in service_catalog:
        if service['type'] != service_type:
            continue
        for endpoint in service['endpoints']:
            if 'publicURL' in endpoint:
                return endpoint['publicURL']
        else:
            return None
    return None


def get_neutronclient():
    #We need to set token, username, project_id, and catalog 
    #in user request object
    token = get_token()
    endpoint = _url_for("network")
    if not endpoint:
        raise Exception("Endpoint is None")
    conn = neutron_client.Client('2.0',
                                endpoint_url=endpoint,
                                token=token)
    return conn


neutron = get_neutronclient()
#print dir(neutron)

def create_network(i):
    #neutron = get_neutronclient()
    #res = neutron.list_networks()
    
    network_name = "networkk"+str(i)
    network_info = {"network": {'name': network_name, 
				'admin_state_up': True
				}
			}
    network = neutron.create_network(network_info)
    """
    #print network
    subnet_info = {"subnet": {"network_id": network['network']['id'], 
				"ip_version": 4, 
				"cidr": "11.0.0.0/24"}
			}
    subnet = neutron.create_subnet(subnet_info)    
    #print subnet    
    print "Created network: {network_name}".format(network_name=network_name)	
    """
    print i


def delete_network(i):
    network = neutron.delete_network(i)
    print i


def run_test():
    for i in range(0, 5):
        thread.start_new_thread(create_network, (i,))


def clean():
    networks = neutron.list_networks()
    for network in networks['networks']:
        #print network['name'], network['id']
        if "networkk" in network['name']:
            thread.start_new_thread(delete_network, (network['id'],))

clean()
#run_test()

while True:
    pass



