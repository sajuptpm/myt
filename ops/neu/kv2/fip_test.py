#How to run
#
#1)
#First source "openrc" file

#2)
#Create FIPs
#$python fip_test.py create <number-of_fips>

#3)
#List duplicate FIPs
#$python fip_test.py list

#4)
#Delete/Clean all FIPs
#$python fip_test.py clean
#
#
#python-keystoneclient>=1.0.0
#python-novaclient>=2.18.0
#neutronclient apis APIParamsCall, https://github.com/openstack/python-neutronclient/blob/master/neutronclient/v2_0/client.py

import os
import sys
import neutronclient
from neutronclient.neutron import client as neutron_client
import keystoneclient
from keystoneclient.v2_0 import client

import thread
#from six.moves import _thread as thread

auth_url= os.environ['OS_AUTH_URL']
username= os.environ['OS_USERNAME']
password= os.environ['OS_PASSWORD']
tenant_name= os.environ['OS_TENANT_NAME']
project_id = "ddf7a9659e5b445582593e51ece151e4"
#FLOATING_NETWORK_ID = "8c34c2bf-2e3f-45e3-b319-f1ea2e47f375"
FLOATING_NETWORK_ID = "b5d57e42-fdd3-43ef-bf58-a2a8f0361104"

def get_keystoneclient():
    remote_addr = None
    conn = client.Client(username=username,
                        password=password,
                        tenant_id=project_id,
                        auth_url=auth_url,
                        original_ip=None,
                        insecure=False,
                        cacert=None,
                        debug=False)
    return conn

def get_token():
    conn = get_keystoneclient()
    try:
        return conn.auth_ref['token']['id']
    except Exception as ex:
        raise Exception('token not found')


def _url_for(service_type):
    catalog = get_keystoneclient().service_catalog.catalog
    service_catalog = catalog['serviceCatalog']
    for service in service_catalog:
        if service['type'] != service_type:
            continue
        for endpoint in service['endpoints']:
            if 'publicURL' in endpoint:
                return endpoint['publicURL']
        else:
            return None
    return None


def get_neutronclient():
    #We need to set token, username, project_id, and catalog 
    #in user request object
    token = get_token()
    endpoint = _url_for("network")
    if not endpoint:
        raise Exception("Endpoint is None")
    conn = neutron_client.Client('2.0',
                                endpoint_url=endpoint,
                                token=token)
    return conn


neutron = get_neutronclient()
#print dir(neutron)

def create_fip(i):
	params = {"floatingip": {"floating_network_id": FLOATING_NETWORK_ID}}
	neutron.create_floatingip(params)
	print i

def delete_fip(i):
    network = neutron.delete_floatingip(i)
    print i


def create_fips():
    NUM_FIPS = 50 
    if len(sys.argv) == 3:
	NUM_FIPS = int(sys.argv[2])
    for i in range(0, NUM_FIPS):
        thread.start_new_thread(create_fip, (i,))


def clean_fips():
    fips = neutron.list_floatingips()
    for fip in fips['floatingips']:
        thread.start_new_thread(delete_fip, (fip['id'],))

def list_fips():
    ip_list = []
    fips = neutron.list_floatingips()
    for fip in fips['floatingips']:
    	ip_list.append(fip['floating_ip_address'])
    print len(ip_list), len(set(ip_list)), set([x for x in ip_list if ip_list.count(x) > 1])

if sys.argv[1] in ['clean']:
    clean_fips()
if sys.argv[1] in ['create']:
    create_fips()
if sys.argv[1] in ['list']:
    list_fips()

while True:
    pass



