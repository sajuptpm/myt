#python-keystoneclient>=1.0.0
#python-novaclient>=2.18.0

import os
import sys
import novaclient
from novaclient.v2 import client as nova_client
import keystoneclient
from keystoneclient.v2_0 import client
from six.moves import _thread as thread

auth_url= os.environ['OS_AUTH_URL']
username= os.environ['OS_USERNAME']
password= os.environ['OS_PASSWORD']
tenant_name= os.environ['OS_TENANT_NAME']
project_id = "ddf7a9659e5b445582593e51ece151e4"
IMAGE_ID = "2040470d-d302-4675-8d21-916ef5ee2d7d"
NW_ID = "2d6300a5-72c0-4659-862a-6996828cbeb7"
FLAVOR_ID = "1"


def get_keystoneclient():
    remote_addr = None
    conn = client.Client(username=username,
                        password=password,
                        tenant_id=project_id,
                        auth_url=auth_url,
                        original_ip=None,
                        insecure=False,
                        cacert=None,
                        debug=False)
    return conn

def get_token():
    conn = get_keystoneclient()
    try:
        return conn.auth_ref['token']['id']
    except Exception as ex:
        raise Exception('token not found')


def _url_for(service_type):
    catalog = get_keystoneclient().service_catalog.catalog
    service_catalog = catalog['serviceCatalog']
    for service in service_catalog:
        if service['type'] != service_type:
            continue
        for endpoint in service['endpoints']:
            if 'publicURL' in endpoint:
                return endpoint['publicURL']
        else:
            return None
    return None


def get_novaclient():
    #We need to set token, username, project_id, and catalog 
    #in user request object
    token = get_token()
    endpoint = _url_for("compute")
    if not endpoint:
        raise Exception("Endpoint is None")
    conn = nova_client.Client(username=username,
                            #we can set password and token inapi_key
                            api_key=token,
                            project_id=project_id,
                            auth_url=endpoint,
                            auth_token=token,
                            bypass_url=endpoint,
                            insecure=False,
                            cacert=None,
                            http_log_debug=False)
    return conn


novaconn = get_novaclient()


def create_vm(cnt, name_prefix=None):
    name = name_prefix + str(cnt)
    novaconn.servers.create(name, IMAGE_ID, FLAVOR_ID, nics=[{"net-id":NW_ID}])
    print name

def create_vms():
    try:
    	NUM_VMS = int(sys.argv[2])
    except IndexError as ex:
       NUM_VMS = 2
    try:
    	name_prefix = sys.argv[3]
        name_prefix = name_prefix + "-"
    except IndexError as ex:
       name_prefix = "vm-"
    for i in range(0, NUM_VMS):
        thread.start_new_thread(create_vm, (i, name_prefix))

def delete_vm(i):
    res = novaconn.servers.delete(i)
    print i

def clean_vms():
    vms = novaconn.servers.list()
    for vm in vms:
        thread.start_new_thread(delete_vm, (vm.id,))

def list_vms():
    vms = novaconn.servers.list()
    ip_list = []
    for vm in vms:
        for k, nw_info in vm.addresses.items():
	   ip_list.append(nw_info[0].get("addr"))
    print len(ip_list), len(set(ip_list)), set([x for x in ip_list if ip_list.count(x) > 1])

if sys.argv[1] in ['clean']:
    clean_vms()
if sys.argv[1] in ['create']:
    create_vms()
if sys.argv[1] in ['list']:
    list_vms()

while True:
    pass





