#python-keystoneclient>=1.0.0
#python-novaclient>=2.18.0
#neutronclient apis APIParamsCall, https://github.com/openstack/python-neutronclient/blob/master/neutronclient/v2_0/client.py

import os
import sys
import neutronclient
from neutronclient.neutron import client as neutron_client
import keystoneclient
from keystoneclient.v2_0 import client

import thread
#from six.moves import _thread as thread

auth_url= os.environ['OS_AUTH_URL']
username= os.environ['OS_USERNAME']
password= os.environ['OS_PASSWORD']
tenant_name= os.environ['OS_TENANT_NAME']
project_id = "60ee4aa06fc04b2c94c607d54252e5f6"
NETWORK_ID = "f4577238-f78f-4f3b-945e-b16bed35207d"

def get_keystoneclient():
    remote_addr = None
    conn = client.Client(username=username,
                        password=password,
                        tenant_id=project_id,
                        auth_url=auth_url,
                        original_ip=None,
                        insecure=False,
                        cacert=None,
                        debug=False)
    return conn

def get_token():
    conn = get_keystoneclient()
    try:
        return conn.auth_ref['token']['id']
    except Exception as ex:
        raise Exception('token not found')


def _url_for(service_type):
    catalog = get_keystoneclient().service_catalog.catalog
    service_catalog = catalog['serviceCatalog']
    for service in service_catalog:
        if service['type'] != service_type:
            continue
        for endpoint in service['endpoints']:
            if 'publicURL' in endpoint:
                return endpoint['publicURL']
        else:
            return None
    return None


def get_neutronclient():
    #We need to set token, username, project_id, and catalog 
    #in user request object
    token = get_token()
    endpoint = _url_for("network")
    if not endpoint:
        raise Exception("Endpoint is None")
    conn = neutron_client.Client('2.0',
                                endpoint_url=endpoint,
                                token=token)
    return conn


neutron = get_neutronclient()
#print dir(neutron)

def create_port(i):
	params = {"port": {"network_id": NETWORK_ID, "admin_state_up": True}}
	neutron.create_port(params)
	print i

def delete_port(i):
    network = neutron.delete_port(i)
    print i


def create_ports():
    NUM_PORTS = 50 
    if len(sys.argv) == 3:
	NUM_PORTS = int(sys.argv[2])
    for i in range(0, NUM_PORTS):
        thread.start_new_thread(create_port, (i,))


def clean_ports():
    ports = neutron.list_ports()
    for port in ports['ports']:
        if NETWORK_ID in [port['network_id']]:
            thread.start_new_thread(delete_port, (port['id'],))

def list_ips():
    ip_list = []
    ports = neutron.list_ports()
    for port in ports['ports']:
	for fip in port['fixed_ips']:
            ip_list.append(fip['ip_address'])
	    #print fip['ip_address']
    print len(ip_list), len(set(ip_list))

if sys.argv[1] in ['clean']:
    clean_ports()
if sys.argv[1] in ['create']:
    create_ports()
if sys.argv[1] in ['list']:
    list_ips()

while True:
    pass



