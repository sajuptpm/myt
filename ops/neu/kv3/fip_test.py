#python-keystoneclient>=1.0.0
#python-novaclient>=2.18.0
#neutronclient apis APIParamsCall, https://github.com/openstack/python-neutronclient/blob/master/neutronclient/v2_0/client.py

import os
import sys
import thread
from keystoneclient.auth.identity import v3
from keystoneclient import session
from keystoneclient.v3 import client
from neutronclient.neutron import client as neutron_client

auth_url= os.environ['OS_AUTH_URL']
username= os.environ['OS_USERNAME']
password= os.environ['OS_PASSWORD']
project_name= os.environ['OS_TENANT_NAME']
user_domain_name = 'Default'
project_domain_name = 'Default'
project_id = "d633b3ca12ac48bcb3383d0edd05109f"
domain_id = "default"
neutron_endpoint = "https://vpc.ind-west-1.staging.jiocloudservices.com:9696"
FLOATING_NETWORK_ID="0d172b88-ff3b-473e-9854-4a35b39a9b0d"
cert=None

auth = v3.Password(auth_url=auth_url,
                    username=username,
                    password=password,
                    project_id=project_id,
                    user_domain_id=domain_id)
sess = session.Session(auth=auth, verify=cert)
ks_conn = client.Client(session=sess)
token = ks_conn.session.get_token()

neutron = neutron_client.Client('2.0',
                            endpoint_url=neutron_endpoint,
                            token=token)

def create_fip(i):
	params = {"floatingip": {"floating_network_id": FLOATING_NETWORK_ID}}
	neutron.create_floatingip(params)
	print i

def delete_fip(i):
    network = neutron.delete_floatingip(i)
    print i


def create_fips():
    NUM_FIPS = 5 
    if len(sys.argv) == 3:
	NUM_FIPS = int(sys.argv[2])
    for i in range(0, NUM_FIPS):
        thread.start_new_thread(create_fip, (i,))


def clean_fips():
    fips = neutron.list_floatingips()
    for fip in fips['floatingips']:
        thread.start_new_thread(delete_fip, (fip['id'],))

def list_fips():
    ip_list = []
    fips = neutron.list_floatingips()
    for fip in fips['floatingips']:
    	ip_list.append(fip['floating_ip_address'])
    print len(ip_list), len(set(ip_list))

if sys.argv[1] in ['clean']:
    clean_fips()
if sys.argv[1] in ['create']:
    create_fips()
if sys.argv[1] in ['list']:
    list_fips()

while True:
    pass



