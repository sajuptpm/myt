#neutronclient apis APIParamsCall, https://github.com/openstack/python-neutronclient/blob/master/neutronclient/v2_0/client.py
#http://docs.openstack.org/developer/python-keystoneclient/using-sessions.html

import os
from keystoneclient.auth.identity import v3
from keystoneclient import session
from keystoneclient.v3 import client
from neutronclient.neutron import client as neutron_client

auth_url= os.environ['OS_AUTH_URL']
username= os.environ['OS_USERNAME']
password= os.environ['OS_PASSWORD']
project_name= os.environ['OS_TENANT_NAME']
user_domain_name = 'Default'
project_domain_name = 'Default'
project_id = "d633b3ca12ac48bcb3383d0edd05109f"
domain_id = "default"
neutron_endpoint = "https://vpc.ind-west-1.staging.jiocloudservices.com:9696"
cert = None

auth = v3.Password(auth_url=auth_url,
                    username=username,
                    password=password,
                    project_id=project_id,
                    user_domain_id=domain_id)
sess = session.Session(auth=auth, verify=cert)
ks_conn = client.Client(session=sess)
token = ks_conn.session.get_token()

nt_conn = neutron_client.Client('2.0',
                            endpoint_url=neutron_endpoint,
                            token=token)

print nt_conn.list_networks()

