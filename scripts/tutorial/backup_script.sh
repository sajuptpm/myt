#!/bin/bash

####Usage####
##chmod +x backup_script.sh
##./backup_script.sh myfile.txt

#### Validation ####
# "$#" gives number of arguments, try echo $#
if [ $# -eq 0 ]
then
echo "You must supply the name of the file or dir to be backup"
#exit/quit from script if no argument supplied
exit 1
fi

if [ -n $2 ]
then

    if [ ! -d $2 ]
    then
        echo "$2: is not a directory"
        exit 1
    fi

    BASE_PATH_OF_DIR_OR_FILE_TOBE_BACKUP=$2
fi

#### Backup Logic ####
DIR_OR_FILE_TOBE_BACKUP=$1          
OUT_PUT_FILE=my-backup-$(date +%Y%m%d%S).tar.gz
#Here "tar" is the backup command and option "-c" means create a new archive
#and -Z means gzip and "-f" means use archive file
echo $DIR_OR_FILE_TOBE_BACKUP
echo $BASE_PATH_OF_DIR_OR_FILE_TOBE_BACKUP
echo $OUT_PUT_FILE


if [ -z $BASE_PATH_OF_DIR_OR_FILE_TOBE_BACKUP ]
then
    COMMAND="tar -czf $OUT_PUT_FILE $DIR_OR_FILE_TOBE_BACKUP"
else
    COMMAND="tar -czf $OUT_PUT_FILE -C $BASE_PATH_OF_DIR_OR_FILE_TOBE_BACKUP $DIR_OR_FILE_TOBE_BACKUP"
fi

echo $COMMAND
$($COMMAND)


