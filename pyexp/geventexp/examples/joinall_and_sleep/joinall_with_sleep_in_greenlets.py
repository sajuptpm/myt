#http://zaiste.net/2013/01/async_python_gevent_introduction/
#http://blog.pythonisito.com/2012/07/gevent-and-greenlets.html

import gevent

def task(num): #<==3
    thread_id, parent = (id(gevent.getcurrent()), id(gevent.getcurrent().parent))
    #thread_id, parent = (gevent.getcurrent(), gevent.getcurrent().parent)
    print "Start: ", num, thread_id

    for i in range(100000001):
        if i == 100000000:
            print num + str(i)

    #Context switch with gevent.sleep:
    #Suspend/Pauses the current greenlet thread (yields to another greenlet)
    #and let other greenlet (In this case other spawned greenlet if any) to execute in that time.
    #Continue the execution (take control back) once the greenlet which took the control completed its execution.
    print "Greenlet Thread Suspend/Pauses for 0.1 minute", num
    gevent.sleep(0.1) #<==4

    for i in range(100000001):
        if i == 100000000:
            print num + str(i)
    
    print "End: ", num, thread_id


print "Main greenlet:%s and its parent:%s" %(gevent.getcurrent(), gevent.getcurrent().parent)
threads = [gevent.spawn(task, x) for x in ['A', 'B', 'C']] #<==1
gevent.joinall(threads) #<==2

#Parent greenlet
"""
Gevent has the hub/scheduler. The whole job is to schedule the tasks. AFAIK,
the hub uses queue to schedule them. If the task has a sleep [1], task is
enqueued for later execution [2].
By saying sleep, gevent puts task out of execution and doesn't guarantee
execution once time is elapsed.

Parent in gevent context is Hub(run inside an event loop). Like unix
process concept, gevent doesn't have parent process and child process.
Let's say if a task is spawn inside gevent task, they are still individual
tasks.

"""

"""
Only one greenlet can run at any given time. We talk about a context switch,
when execution flow changes from one greenlet to another. In other words, a
greenlet only yields to another greenlet only if a blocking function is
reached e.g. gevent.sleep() - this is known as cooperative multitasking.
Traditional threads may yield to other threads at any time, whenever OS decides
to switch the context - it is called preemptive multitasking.

Greenlets, sometimes referred to as "green threads," are a lightweight
structure that allows you to do some cooperative multithreading in Python
without the system overhead of real threads (like the thread or threading
module would use). The main thing to keep in mind when dealing with greenlets
is that a greenlet will never yield to another greenlet unless it calls some
function in gevent that yields. (Real threads can be interrupted at somewhat
arbitrary times by the operating system, causing a context switch.)

sleep() is one of the functions in gevent which will yield to other greenlets.
If you want to yield to other greenlets but don't care to wait a second if
there's no one ready to run, you can call gevent.sleep(0). 

If you're interested in waiting for the greenlets to complete, you can do so by
using the gevent.joinall command. joinall can also take a timeout param that
will stop waiting for the greenlets if they don't all complete after the given
timeout. In the basic case, you just pass a list of Greenlet objects to joinall

cooperative multitasking:
--------------------------
A type of multitasking in which the process currently controlling the CPU must
offer control to other processes. It is called cooperative because all programs
must cooperate for it to work. If one program does not cooperate, it can hog
the CPU. In contrast, preemptive multitasking forces applications to share the
CPU whether they want to or not.

The advantage of cooperative multitasking is that the programmer knows where
the program will be descheduled and can make sure that this will not cause
unwanted interaction with other processes.Thus cooperative multitasking can
have lower overheads than pre-emptive multitasking because of the greater
control it offers over when a task may be descheduled. 


greenlet and gevent:
---------------------
greenlet is a micro-thread because it doesn't have the overhead of a thread.
With thread, CPU will do a lot of context switching and many context switching
can be expensive. Greenlet helps solve that by doing context switching within
the Python interpreter but no scheduling.

gevent uses greenlets so it can do asynchronous network call. Also it
transparently spawn multiples greenlets  so you don't have to write code to
check for call back nightmare. Basically write your code in synchronous style
but gets asynchronous for free.

Coroutines:
-----------
Coroutines may switch their flow of control to another coroutine and resume
execution at the same point later.

Greenlet provides a method of switching context between cooperating functions.
This is very useful in an I/O bound context. This brings us to gevent, a
coroutine-based networking library built on greenlet.

https://words.volant.is/articles/understanding-gunicorns-async-worker-concurrency-model/

So in essence, when something is waiting for I/O, gevent makes it easy to be
doing other work. This obviously has big advantages for concurrent networking
code, as well as anything else that is I/O bound where the caller can be safely
suspended and resumed later.

"""




