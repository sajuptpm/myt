#http://sdiehl.github.io/gevent-tutorial/
#http://blog.pythonisito.com/2012/07/gevent-and-greenlets.html

import gevent
import random

def task(lst): #<==4
    thread_id, parent = (id(gevent.getcurrent()), id(gevent.getcurrent().parent))
    #thread_id, parent = (gevent.getcurrent(), gevent.getcurrent().parent)
    
    print "Start, Thread Id: ", thread_id, lst
    for x in lst:
        t = random.randint(0,2)

        #Context switch with gevent.sleep:
        #Suspend/Pauses the current greenlet thread (yields to another greenlet)
        #and let other greenlet (In this case other spawned greenlet if any) to execute in that time.
        #Continue the execution (take control back) once the greenlet which took the control completed its execution.
        print "Greenlet Thread Suspend/Pauses for %s minute" %(t), x
        gevent.sleep(t) #<==5
        print x
    print "End, Thread Id: ", thread_id

###Asynchronous###
print "Main greenlet:%s and its parent:%s" %(gevent.getcurrent(), gevent.getcurrent().parent)
s1=gevent.spawn(task, [1,2,3,4]) #<==1
s2=gevent.spawn(task, ['A','B','C','D']) #<==2
gevent.joinall([s1, s2]) #<==3

#Parent greenlet
"""
Gevent has the hub/scheduler. The whole job is to schedule the tasks. AFAIK,
the hub uses queue to schedule them. If the task has a sleep [1], task is
enqueued for later execution [2].
By saying sleep, gevent puts task out of execution and doesn't guarantee
execution once time is elapsed.

Parent in gevent context is Hub(run inside an event loop). Like unix
process concept, gevent doesn't have parent process and child process.
Let's say if a task is spawn inside gevent task, they are still individual
tasks.

"""






