#https://github.com/dcramer/taskmaster/blob/79a312c5cb3c34d00829fe9cf4204aeb478a0166/src/taskmaster/client.py#L26

"""

Quest:
This code has two pause (gevent.sleep(0)), in Consumer.start and Worker.run.
Here control goes to Worker.run when Consumer.start pauses and
Consumer.start gets control back when Worker.run pauses. There may be
benefit from this switching, but I am still not understanding it.

Ans:
The idea is that any non blocking call is an opportunity to switch
between greenlets. The monkey patching system will enable this.

If you don't have any place in your greenlets that are monkey patched
(and therefore allow switching), you can be a good citizen and drop in a
patched sleep call so that other greenlets will get time to
run. Otherwise, you'll simply hog the interpreter and no one else will
be able to run

Non-Blocking Call:
------------------
http://amix.dk/blog/post/19581 <==IMP

http://www.scottklement.com/rpg/socktut/nonblocking.html
urllib2 and requests are blocking calls. We can monkey patch it to make it
un-blocking.

In non-blocking mode, submits the command to be executed and then returns a
Result object immediately. The Result object gives you a way of getting a
result at a later time through its get method.


Greenlets never run in parallel/concurrently:
---------------------------------------------
Greenlets never run in parallel, they all share the same process and the same
thread, so, there is at most one of them running at a time.

Greenlets are green because they are co-routines ("co" from cooperation), thus,
it can not even be said that they run concurrently, because you need to
coordinate their running. Gevent does most of this work for you behind the
scenes, and knows from libevent (or libev) what greenlets are ready to run


"""

import gevent

class Worker(object):

    def __init__(self, consumer, target):
        self.consumer = consumer
        self.target = target

    def run(self):
        print "Worker.run, START"
        self.started = True

        while self.started:
            print "Greenlet thread (from Worker.run) Pauses, START"
            gevent.sleep(0)
            print "Greenlet thread (from Worker.run) Pauses, END"
            job = self.consumer.get_job()
            self.target(job)

        print "Worker.run, END"

class Consumer(object):

    def __init__(self, target):
        self.target = target

    def start(self):
        print "Consumer.start, START"
        self.started = True

        worker = Worker(self, self.target)
        gevent.spawn(worker.run)

        while self.started:
            print "Main Interpreter greenlet (from Consumer.start) Pauses, START"
            gevent.sleep(0)
            print "Main Interpreter greenlet (from Consumer.start) Pauses, END \n"
        
            for x in range(100):
                ##Do something
                pass

        print "Consumer.start, END"

    def get_job(self):
        return 10000

#############
def my_target_1(n):
    print "my_target_1, START"
    nums = []    
    while n > 0:
        nums.append(n)
        n -= 1
    print "my_target_1, END"

c = Consumer(my_target_1)
c.start()










