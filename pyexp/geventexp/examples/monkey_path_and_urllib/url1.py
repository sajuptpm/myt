"""
1) Run it and check the output.
2) Copy this code to python console and run it and check the output.Then run
"gevent.sleep(0.001)" and "get_status(g)" multiple times and check the ouput.
3)Comment "monkey.patch_all()",Then Copy this code to python console and 
run it and check the output.Then run "gevent.sleep(0.001)" and "get_status(g)" 
multiple times and check the ouput.
4)Comment "monkey.patch_all()", then add "geven.sleep(0.001)" before/after 
"res = urllib2.urlopen(url).read()", Then Copy this code to python console and 
run it and check the output.Then run "gevent.sleep(0.001)" and "get_status(g)" 
multiple times and check the ouput.

Notes:
a) We can use geven.sleep(0) to Pause and Resume when there is no monkey 
patched non-blocking call like 'urllib' in our greenlet thread)

"""


from gevent import monkey; 
monkey.patch_all()

import gevent, urllib2

def download(url): #<==2
    print "---Pauses greenlet---", url 
    #urlopen is a blocking call, make it non-blocking with monkey patch
    res = urllib2.urlopen(url).read() #<==3
    print "---Resumes greenlet---", url

    for x in range(100):
        ##Do something
        pass

    print "---Dead/End greenlet---", url
    return res
    
g = gevent.spawn(download,"http://gevent.org")

def get_status(g):
    print "g.started : ", g.started
    print "g.successful() : ", g.successful()
    print "g.dead : ", g.dead
    print "g.error() : ", g.error()
    print "g.exception : ", g.exception
    print "g.ready() : ", g.ready()
    print "g.value : ", type(g.value)


get_status(g)

gevent.sleep(0.001) #<==1

print "-------------" #<==4
get_status(g)


