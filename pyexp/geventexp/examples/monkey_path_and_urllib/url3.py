"""
1) Copy this code to python console.Then run "gevent.sleep(0.001)" and 
"get_status(g1), get_status(g2)" multiple times and check the ouput.

2) Comment "monkey.patch_all()" and Copy this code to python console.
Then run "gevent.sleep(0.001)" and "get_status(g1), get_status(g2)" 
and check the ouput.

3)Comment "monkey.patch_all()", then add "geven.sleep(0)" in download 
method before the line "res = urllib2.urlopen(url).read()".Then run
"gevent.sleep(0.001)" and "get_status(g1), get_status(g2)" multiple 
times and check the ouput.

"""

from gevent import monkey
monkey.patch_all()

import gevent, urllib2

def get_status(g):
    print "g.started : ", g.started
    print "g.successful() : ", g.successful()
    print "g.dead : ", g.dead
    print "g.error() : ", g.error()
    print "g.exception : ", g.exception
    print "g.ready() : ", g.ready()
    print "g.value : ", type(g.value)

def download(url):
    print "---In greenlet---", url
    get_status(gevent.getcurrent())
    print "---Pauses greenlet---", url
    res = urllib2.urlopen(url).read()
    print "---Resumes greenlet---", url
    for x in range(100):
        ##Do something
        pass
    print "---Dead/End greenlet---", url
    return res

g1 = gevent.spawn(download,"http://gevent.org")
g2 = gevent.spawn(download,"http://python.org")

#get_status(g1)
#print "--------"
#get_status(g2)
#gevent.sleep(0.001)





