"""
1) First run it and check the output.
2) Comment "monkey.patch_all()" and run it again and check the output.
3) Comment "monkey.patch_all()", then add "geven.sleep(0)" in download 
method before/after the line "res = urllib2.urlopen(url).read()" and run it 
again and check the output.

Notes:
a) We can use geven.sleep(0) to Pause and Resume when there is no monkey 
patched non-blocking call like 'urllib' in our greenlet thread)

"""

from gevent import monkey
monkey.patch_all()

import gevent, urllib2

def get_status(g):
    print "g.started : ", g.started
    print "g.successful() : ", g.successful()
    print "g.dead : ", g.dead
    print "g.error() : ", g.error()
    print "g.exception : ", g.exception
    print "g.ready() : ", g.ready()
    print "g.value : ", type(g.value)

def download(url):
    print "---In greenlet---", url
    get_status(gevent.getcurrent())
    print "---Pauses greenlet---", url
    res = urllib2.urlopen(url).read()
    print "---Resumes greenlet---", url
    for x in range(100):
        ##Do something
        pass
    print "---Dead/End greenlet---", url
    return res

g1 = gevent.spawn(download,"http://gevent.org")
g2 = gevent.spawn(download,"http://python.org")
threads = [g1, g2]
gevent.joinall(threads)

print "--Done---"

for x in threads:
    print "-----"
    get_status(x)



