
#https://github.com/Juniper/contrail-controller/blob/master/src/config/common/vnc_cpu_info.py#L113

import gevent
from gevent import monkey
monkey.patch_time() #monkey patch standard time library
import time


def task(num): #<==3
    thread_id, parent = (id(gevent.getcurrent()), id(gevent.getcurrent().parent))
    #thread_id, parent = (gevent.getcurrent(), gevent.getcurrent().parent)

    print "Start: ", num, thread_id

    while True:
        print num
        #http://www.gevent.org/gevent.html#gevent.sleep
        #Context switch with gevent.sleep:
        #Suspend/Pauses the current greenlet thread for 1 minute 
        #and let other greenlet (In this case there is no other greenlet) to execute in that time.
        #So continue in while loop after 1 minute wait. 
        print "Thread Suspend/Pauses for 1 minute"
        #gevent.sleep(1) #<==4
        time.sleep(1) #<==4
        num+=1

    #This will not print, since interpreter given only 2 minutes time to this greenlet.
    #In that time, while loop printed 2 numbers and interpreter taken the control back.
    print "End: ", num, thread_id

print "Main greenlet:%s and its parent:%s" %(gevent.getcurrent(), gevent.getcurrent().parent)
gevent.spawn(task, 1) #<==1

##Suspend/Pauses the current interpreter for 1 minute 
##and let other greenlet (In this case spawned greenlet) to execute in that time.
#gevent.sleep(1)

##Again Suspend/Pauses the current interpreter for 1 minute 
##and let other greenlet (In this case spawned greenlet) to resume execution in that time. <==Imp
#gevent.sleep(1)

### OR ###

##Suspend/Pauses the current interpreter (parent greenlet) for 2 minutes 
#3and let other greenlet (In this case spawned greenlet) to execute in that time.
print "Main Interpreter greenlet Suspend/Pauses for 2 minutes: START"
#gevent.sleep(2) #<==2
time.sleep(2) #<==2
print "Main Interpreter greenlet Suspend/Pauses for 2 minutes: END"

#Parent greenlet
"""
Gevent has the hub/scheduler. The whole job is to schedule the tasks. AFAIK,
the hub uses queue to schedule them. If the task has a sleep [1], task is
enqueued for later execution [2].
By saying sleep, gevent puts task out of execution and doesn't guarantee
execution once time is elapsed.

Parent in gevent context is Hub(run inside an event loop). Like unix
process concept, gevent doesn't have parent process and child process.
Let's say if a task is spawn inside gevent task, they are still individual
tasks.

"""






