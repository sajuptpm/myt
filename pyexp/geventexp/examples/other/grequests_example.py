#!/usr/bin/env python

import grequests as requests
import requests
import xml.etree.ElementTree as ET
from pprint import pprint
import gevent

HOST="192.168.56.102"
PROTOCOL="http"
PORT=8083

class Base(object):
    @classmethod
    def get_resource_list(cls, response, resource_name):
        result = []
        root = ET.fromstring(response.text)
        elem = root.find(resource_name)
        if elem is not None:
            elem = elem.find("list")
            if elem is not None:
                for child in elem:
                    data = {}
                    for c in child:
                        data[c.tag] = c.text
                    result.append(data)
        return result

    @classmethod
    def make_request(cls, action):
        r = None
        try:
            r = requests.get("{protocol}://{host}:{port}/{action}".format(protocol=PROTOCOL, host=HOST, port=PORT,action=action))
        except Exception as ex:
            return {"result":r, "success":False, "error":ex}
        return {"result":r, "success":True, "error":None}

class XmppServer(Base):
    @classmethod
    def get_xmpp_server_stat(cls):
        action = "Snh_ShowXmppServerReq"
        result = {}
        response = cls.make_request(action)
        if response['success']:
            res = response['result'] 
            if res.status_code in [200]:
                root = ET.fromstring(res.text)
                elem = root.find("current_connections")
                if elem is not None:
                    result[elem.tag] = elem.text
                elem = root.find("max_connections")
                if elem is not None:
                    result[elem.tag] = elem.text
        print result
        return result

    @classmethod
    def get_xmpp_server_connections(cls):
        action = "Snh_ShowXmppConnectionReq"
        result = {"connections":[]}
        response = cls.make_request(action)
        if response['success']:
            res = response['result']
            if res.status_code in [200]:
                rlist = cls.get_resource_list(res, "connections")
                result["connections"] = rlist
        print result
        return result

class BgpPeer(Base):
    @classmethod
    def get_bgp_neighbors(cls):
        #global HOST
        #HOST="191.168.56.102"
        action = "Snh_ShowBgpNeighborSummaryReq"
        result = {"neighbors":[]}
        response = cls.make_request(action)
        if response['success']:
            res = response['result']
            if res.status_code in [200]:
                rlist = cls.get_resource_list(res, "neighbors")
                result["neighbors"] = rlist
        print result
        return result



if __name__ == "__main__":
    result = {}
    g1 = gevent.spawn(XmppServer.get_xmpp_server_stat)
    g2 = gevent.spawn(XmppServer.get_xmpp_server_connections)
    g3 = gevent.spawn(BgpPeer.get_bgp_neighbors)
    threads = [g1, g3, g2]
    gevent.joinall(threads)
    #result["xmppserver"] = {"ShowXmppServerReq":XmppServer.get_xmpp_server_stat(),
    #z                        "ShowXmppConnectionReq":XmppServer.get_xmpp_server_connections()}
    #result["bgppeer"] = {"ShowBgpNeighborSummaryReq":BgpPeer.get_bgp_neighbors()}
    #pprint(result)




